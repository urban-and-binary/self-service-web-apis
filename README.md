## Development and setup

**Prerequisites**

-   Docker
-   Vscode
-   Visual Studio Code extension for Remote Containers - https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers
-   For windows - Visual Studio Code Remote - WSL extension - https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl

**Setup**

-   Open cloned project via Vscode
-   Click `Reopen in container` either via VSCode prompt or open vscode command pallete and find `Reopen in container`
-   Once everything has built you'll see `Dev Container: Civinity Self-Service Dev` at the bottom left
-   Run `npm run migrate-up` to run migrations
-   Run `npm run seed` to seed the database
-   Frontend is running on - http://localhost:8082/
-   Api-web is running on - http://localhost:8082/api/web

**Eslint**

-   To run eslint checks for whole project `npm run lint`

**Tests**

-   To run api tests `npm run test-api`

**Mongo db**

-   To revert migrations you can run `npm run migrate-down`
-   To create new migrations run `npm run create-migration -- your_migration_name`
-   After creating the migration file you can run `npm run migrate-up` and it will pick it up

**MongoDB Connection via extension**

-   Find the mongodb tab on the left bar of Vscode and open it up
-   Click on `Add Connection`
-   Select `Connect with connection string`
-   Copy the string from `config.js` `dbConnection` and paste in the prompt
-   Connection should be established
-   You can run queries on specific collections, by opening a collection and pressing on the search icon which shows up when hovering over `Documents` item

## Troubleshooting

### Building search tests fail

If DB records where manually altered/removed Mongoose will lose the indexes and error will be thrown:  
`StatusCodeError: 400 - {"message":"text index required for $text query"}`

Restart API services so Mongoose will rebuild the indexes on first connection.
Also this approach is for consideration if this becomes a problem: https://stackoverflow.com/a/59922531/1389334

## Documentation

Generate APIs documentation automatically, run: `npm run jsdoc`.
Folder `out` will be created. Open `out\index.html` to view full APIs decsriptions.
