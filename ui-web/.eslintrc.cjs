module.exports = {
	extends: ['../.eslintrc.cjs', 'plugin:react/recommended'],

	env: {
		browser: true,
	},
	parser: 'babel-eslint',
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		sourceType: 'module',
	},
	settings: {
		react: {
			version: '17.0.2',
		},
	},
	plugins: ['react-hooks', 'react'],
	rules: {
		'jsx-quotes': ['error', 'prefer-double'],
		'react/jsx-key': 0,
	},
};
