const { override, babelInclude, addWebpackAlias, removeModuleScopePlugin, overrideDevServer, watchAll } = require('customize-cra');
const path = require('path');

module.exports = {
	webpack: override(
		removeModuleScopePlugin(),

		babelInclude([path.resolve('src'), path.resolve(__dirname, '../common')]),

		addWebpackAlias({
			'@components': path.resolve(__dirname, 'src/components/'),
			'@utils': path.resolve(__dirname, 'src/utils/'),
			'@pages': path.resolve(__dirname, 'src/pages/'),
			'@assets': path.resolve(__dirname, 'src/assets/'),
			'@context': path.resolve(__dirname, 'src/context/'),
			'@constants': path.resolve(__dirname, 'src/constants/'),
			'@hooks': path.resolve(__dirname, 'src/hooks/'),
			'@api': path.resolve(__dirname, 'src/api/'),
			'@common': path.resolve(__dirname, '../common/'),
		}),
	),
	devServer: overrideDevServer(watchAll()),
};
