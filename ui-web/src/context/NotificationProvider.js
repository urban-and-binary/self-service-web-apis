import React from 'react';
import PropTypes from 'prop-types';
import { createContext, useState } from 'react';
import api from '@api/axios';

export const NotificationContext = createContext({});

export const NotificationProvider = ({ children }) => {
	const [notifs, setNotifs] = useState([]);

	const loadNotifs = async () => {
		try {
			const res = await api.get('announcements/unread');
			setNotifs(res.data);
		} catch (error) {
			console.error('Error getting notifications', error);
		}
	};

	const removeNotif = notifId => {
		setNotifs(notifs.filter(notif => notif._id !== notifId));
	};

	return <NotificationContext.Provider value={{ notifs, setNotifs, loadNotifs, removeNotif }}>{children}</NotificationContext.Provider>;
};

NotificationProvider.propTypes = {
	children: PropTypes.node,
};
