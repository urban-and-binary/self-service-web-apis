export const REGEX = {
	NUMBER: '[0-9]',
	LETTER: '[A-Z]',
};

export const ALERT = {
	SUCCESS: 'success',
	ERROR: 'error',
	INFO: 'info',
	WARNING: 'warning',
};
