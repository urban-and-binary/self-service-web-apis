import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import Button from '@components/Button/Button';

const Container = styled('div')(() => ({
	padding: '50px 60px 56px 60px',
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	textAlign: 'center',
}));
const Title = styled('p')(({ theme, size }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: size === 'large' ? theme.typography.fontSize.h1 : theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	marginTop: theme.spacing(3),
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const StyledButton = styled(Button)(({ theme }) => ({
	marginTop: theme.spacing(5),
	width: '90%',
}));

export const Notification = ({ message }) => {
	const { t } = useTranslation();

	const { title, text, action, to } = message;

	return (
		<Container>
			{title && <Title size={text?.length ? '' : 'large'}>{t(title)}</Title>}
			{text && <Text>{t(text)}</Text>}
			{action && <StyledButton to={to}>{t(action)}</StyledButton>}
		</Container>
	);
};

Notification.propTypes = {
	message: PropTypes.object.isRequired,
};
