import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import Button from '@components/Button/Button';
import { MoneyIcon, SalyIcon } from '@assets/icons';

const BlueBanner = styled('div')(({ theme }) => ({
	background: 'linear-gradient(283.08deg, #0B1B3C -1.69%, #496DB3 50.86%, #102D65 103.41%)',
	boxShadow: `${theme.shadow.s01}, inset -8px -8px 12px 7px rgba(57, 57, 57, 0.33), inset 8px 8px 18px 6px rgba(82, 106, 163, .9)`,
	borderRadius: 16,
	padding: '20px 40px',
}));
const StyledFrame = styled('div')(() => ({
	background: 'linear-gradient(282.47deg, rgba(254, 254, 254, 0.25) 46.71%, rgba(254, 254, 254, 0) 108.42%)',
	filter: 'drop-shadow(0px 2px 8px rgba(117, 131, 142, 0.04)) drop-shadow(0px 16px 24px rgba(52, 60, 68, 0.12))',
	backdropFilter: 'blur(4px)',
	borderRadius: 16,
	border: '2px solid rgba(254, 254, 254, 0.7)',
	padding: '24px 24px 30px 44px',
	display: 'flex',
	justifyContent: 'space-between',
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	lineHeight: '18px',
	color: theme.palette.base.white,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Amount = styled('p')(({ theme }) => ({
	margin: '10px 0',
	lineHeight: '40px',
	color: theme.palette.base.white,
	fontSize: 48,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Status = styled('span')(({ theme }) => ({
	color: theme.palette.base.white,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.m,
	background: theme.palette.default.error,
	borderRadius: 4,
	padding: '7px 16px',
}));
const StyledButton = styled(Button)(() => ({
	marginTop: '24px',
	padding: '14px 32px',
	background: 'white',
	border: 'none',
	boxShadow: 'none',
	'&:hover, &:focus': {
		border: 'none',
		padding: '14px 32px',
	},
}));
const StyledIcon = styled('div')(() => ({
	maxWidth: 280,
	marginRight: -60,
}));

const GreenBanner = styled('div')(() => ({
	marginTop: '18px',
	background: '#249A61',
	boxShadow:
		'0px 2px 8px rgba(117, 131, 142, 0.04), 0px 16px 24px rgba(52, 60, 68, 0.12), inset -8px -8px 12px 7px rgba(57, 57, 57, 0.33), inset 8px 8px 18px 6px rgba(95, 236, 155, 0.45)',
	borderRadius: 16,
	position: 'relative',
	textAlign: 'right',
}));
const TextPaid = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.white,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const StyledSaly = styled('div')(() => ({
	position: 'absolute',
	width: '40%',
	maxWidth: 200,
	marginTop: -24,
}));
const GreenInfo = styled('div')(() => ({
	padding: '30px',
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'flex-end',
}));
const GreenButton = styled(Button)(({ theme }) => ({
	marginTop: '12px',
	padding: '14px 32px',
	background: 'white',
	border: 'none',
	boxShadow: 'none',
	color: theme.palette.secondary.green002,
	'&:hover, &:focus': {
		border: 'none',
		padding: '14px 32px',
		color: theme.palette.secondary.green001,
	},
}));

export const Banner = ({ variant }) => {
	const { t } = useTranslation();

	const amount = '329.00';

	return (
		<>
			{variant === 'blue' && (
				<BlueBanner>
					<StyledFrame>
						<div>
							<Text>{t('AmountToPay')}</Text>
							<Amount>€ {amount}</Amount>
							<Status>{t('Unpaid')}</Status>
							<StyledButton variant="outlined">{t('OverviewAndPay')}</StyledButton>
						</div>
						<StyledIcon>
							<MoneyIcon width="100%" />
						</StyledIcon>
					</StyledFrame>
				</BlueBanner>
			)}
			{variant === 'green' && (
				<GreenBanner>
					<Grid container>
						<Grid xs={3}>
							<StyledSaly>
								<SalyIcon width="100%" />
							</StyledSaly>
						</Grid>
						<Grid xs={9}>
							<GreenInfo>
								<TextPaid>{t('PaidForServices')}!</TextPaid>
								<GreenButton variant="outlined">{t('PayInAdvance')}</GreenButton>
							</GreenInfo>
						</Grid>
					</Grid>
				</GreenBanner>
			)}
		</>
	);
};

Banner.propTypes = {
	variant: PropTypes.string.isRequired,
};
