import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { hextorgb } from '@utils/utils';
import { ChevronIcon, HomeIcon, TasksIcon, AnnouncementsIcon } from '@assets/icons';
import { Divider } from '@mui/material';

const Container = styled('div')(() => ({
	padding: '0 30px 0 16px',
}));
const Header = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
	padding: '33px 16px 30px 24px',
}));
const Title = styled('div')(({ theme }) => ({
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const TextLink = styled(Link)(({ theme }) => ({
	textDecoration: 'none',
	color: theme.palette.primary.blue003,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.m,
}));
const List = styled('div')(() => ({
	padding: '12px 16px 20px 16px',
}));
const Item = styled(Link)(({ theme }) => ({
	margin: '24px 0',
	display: 'flex',
	alignItems: 'center',
	textDecoration: 'none',
	color: theme.palette.base.dark,
}));
const TextWrapper = styled('div')(({ theme }) => ({
	flex: 1,
	marginLeft: theme.spacing(2),
}));
const ItemTitle = styled('p')(({ theme }) => ({
	margin: 0,
	textTransform: 'capitalize',
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const ItemText = styled('p')(({ theme }) => ({
	margin: '4px 0 0 0',
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const StyledArrow = styled(ChevronIcon)(({ theme }) => ({
	transform: 'rotate(-90deg)',
	color: theme.palette.base.lightGray,
}));
const ItemIcon = styled('div')(({ color }) => ({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	width: 44,
	height: 44,
	borderRadius: 8,
	backgroundColor: color,
	boxShadow: `1px 5px 0px -3px ${hextorgb(color, 0.4)}`,
}));

const data = [
	{
		type: 'Votings',
		text: '2012 - 12 - 15 dieną jūsų laukia balsavimas dėl namo renovacijos',
	},
	{
		type: 'Announcements',
		text: 'Šiandien nuo 12:00 iki 15:00 jūsų name nebus elektros',
	},
	{
		type: 'Tasks',
		text: 'Water pipes have been replaced at Antakalnio g. 112 - 12, Vilnius.',
	},
	{
		type: 'Announcements',
		text: 'New meeting is scheduled. ',
	},
	{
		type: 'Announcements',
		text: '2012 - 12 - 12 dieną jūsų laukia balsavimas dėl namo renovacijos',
	},
];

const Icon = ({ type }) => {
	const theme = useTheme();
	const ICON = {
		TASKS: 'Tasks',
		VOTINGS: 'Votings',
		ANNOUNCEMENTS: 'Announcements',
	};

	const renderType = value => {
		return {
			[ICON.TASKS]: { color: theme.palette.default.lightRed, icon: <TasksIcon /> },
			[ICON.VOTINGS]: { color: theme.palette.default.lightViolet, icon: <HomeIcon /> },
			[ICON.ANNOUNCEMENTS]: { color: theme.palette.primary.blue004, icon: <AnnouncementsIcon /> },
		}[value];
	};

	return <ItemIcon color={renderType(type).color}>{renderType(type).icon}</ItemIcon>;
};

export const Announcements = () => {
	const { t } = useTranslation();

	return (
		<Container>
			<Header>
				<Title>{t('Pending announcements')}</Title>
				<TextLink to="/announcements">{t('ViewAll')}</TextLink>
			</Header>
			<Divider />
			<List>
				{data.map(item => (
					<Item to="/announcements" key={item.text}>
						<Icon type={item.type} />
						<TextWrapper>
							<ItemTitle>{t(item.type)}</ItemTitle>
							<ItemText>{item.text}</ItemText>
						</TextWrapper>
						<StyledArrow />
					</Item>
				))}
			</List>
		</Container>
	);
};

Icon.propTypes = {
	type: PropTypes.string.isRequired,
};
