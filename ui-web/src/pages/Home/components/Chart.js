import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, ResponsiveContainer } from 'recharts';

const Container = styled('div')(() => ({
	padding: '40px 50px 50px 30px',
}));
const Wrapper = styled('div')(({ width }) => ({
	width: width - 80,
	height: 160,
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
	marginBottom: theme.spacing(4),
}));

const data = [
	{
		name: 'Sep',
		amount: 290,
	},
	{
		name: 'Oct',
		amount: 480,
	},
	{
		name: 'Nov',
		amount: 230,
	},
	{
		name: 'Dec',
		amount: 400,
	},
	{
		name: 'Jan',
		amount: 420,
	},
];

export const Chart = ({ title }) => {
	const [width, setWidth] = useState(0);
	const ref = useRef(null);

	useEffect(() => {
		setWidth(ref.current.clientWidth);
	}, []);

	return (
		<Container ref={ref}>
			<Title>{title}</Title>
			<Wrapper width={width}>
				<ResponsiveContainer width="100%" height="100%">
					<BarChart
						width={300}
						height={160}
						data={data}
						margin={{
							top: 5,
							right: 0,
							left: 0,
							bottom: 5,
						}}>
						<CartesianGrid />
						<XAxis dataKey="name" />
						<YAxis />
						<Bar dataKey="amount" fill="#499CFB" />
					</BarChart>
				</ResponsiveContainer>
			</Wrapper>
		</Container>
	);
};

Chart.propTypes = {
	title: PropTypes.string.isRequired,
};
