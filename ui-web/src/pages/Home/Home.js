import React, { useState } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Divider, Grid } from '@mui/material';
import { Dropdown, Paper } from '@components';
import { Chart } from './components/Chart';
import { Banner } from './components/Banner';
import { Notification } from './components/Notification';
import { Announcements } from './components/Announcements';

const Sidebar = styled('div')(({ theme }) => ({
	padding: '0 24px',
	'& > *': {
		marginBottom: theme.spacing(3),
	},
}));

const StyledDivider = styled(Divider)(({ theme }) => ({
	marginLeft: -1,
	borderColor: theme.palette.base.inactive,
}));

export const Home = () => {
	const { t } = useTranslation();

	// TODO: To be deleted
	const data = [
		{ label: t('AllUnits'), value: 1 },
		{ label: 'The Godfather', value: 2 },
		{ label: 'The Godfather: Part II', value: 3 },
		{ label: 'The Dark Knight', value: 4 },
		{ label: '12 Angry Men', value: 5 },
	];

	const paid = false;
	const [unit, setUnit] = useState(data[0]);
	const handleUnitSelect = (e, option) => {
		setUnit(option);
	};

	return (
		<Grid container>
			<Grid item xs={8} pr={3} pl={3}>
				<Grid container columnSpacing={3} rowSpacing={3}>
					<Grid item xs={5}>
						<Dropdown options={data} value={unit} onChange={handleUnitSelect} />
					</Grid>
					<Grid item xs={12}>
						<Banner variant={paid ? 'green' : 'blue'} />
					</Grid>
					<Grid item xs={12}>
						<Paper>
							<Announcements />
						</Paper>
					</Grid>
				</Grid>
			</Grid>
			<StyledDivider orientation="vertical" flexItem />
			<Grid item xs={4}>
				<Sidebar>
					<Paper>
						<Notification
							message={{
								to: '/',
								action: 'Donate',
								title: 'MakeDonation',
								text: 'DonationDescription',
							}}
						/>
					</Paper>
					<Paper>
						<Chart title={t('PaymentHIstory')} />
					</Paper>
				</Sidebar>
			</Grid>
		</Grid>
	);
};
