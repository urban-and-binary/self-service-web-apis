import React, { useState } from 'react';
import moment from 'moment';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';
import { Grid, Stack, TextareaAutosize, Box } from '@mui/material';
import { ChevronIcon, CheckedIcon } from '@assets/icons';
import { RadioSelectWithProgress } from './RadioSelectWithProgress';
import { Paper, Dialog } from '@components';
import Button from '@components/Button/Button';

const BackButton = styled(Link)(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	textDecoration: 'none',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Icon = styled(ChevronIcon)(() => ({
	transform: 'rotate(90deg)',
	marginRight: 20,
	width: '14px',
	height: '8px',
}));
const IconRight = styled(ChevronIcon)(() => ({
	transform: 'rotate(-90deg)',
	marginLeft: 10,
	fontSize: 8,
	fontWeight: 'bold',
}));
const Header = styled('div')(({ theme }) => ({
	background: theme.palette.primary.blue005,
	borderRadius: '16px 16px 0 0',
}));
const Content = styled('div')(() => ({}));
const PollTitle = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const DateTitle = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
}));
const Date = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const MoreInfo = styled(Link)(({ theme }) => ({
	textDecoration: 'none',
	color: theme.palette.primary.blue001,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));
const SuccessMessage = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.secondary.green002,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));
const InfoText = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const Textarea = styled(TextareaAutosize)(({ theme }) => ({
	border: 'none',
	background: theme.palette.base.background,
	width: '100% !important',
	padding: '16px 32px',
	borderRadius: 8,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
	fontFamily: theme.typography.fontFamily,
}));

export const SinglePoll = () => {
	const { t } = useTranslation();
	const { type } = useParams();
	const poll = type === 'poll';

	const [vote, setVote] = useState('');
	const [success, setSuccess] = useState(false);
	const [open, setOpen] = useState(false);

	const handleClose = () => {
		setOpen(false);
	};
	const handleConfirm = () => (poll ? setSuccess(true) : setOpen(true));

	return (
		<Grid container direction="column" pl={3} pr={3} pb={6}>
			<BackButton to="/polls-voting">
				<Icon /> {poll ? t('Polls') : t('Votings')}
			</BackButton>
			<Paper shadow="s02" sx={{ mt: 3 }}>
				<Header sx={{ pt: 5, pb: 4, pr: 5, pl: 3 }}>
					<PollTitle>{data.title}</PollTitle>
					<Subtitle sx={{ mt: 1 }}>{data.unit}</Subtitle>
					<Grid container item xs={12} mt={5} direction="row" justifyContent="space-between">
						<Stack direction="row" alignItems="center">
							<DateTitle sx={{ mr: '5px' }}>{t('PollDateRange')}</DateTitle>
							<Date>
								{data.startDate && moment(data.startDate).format('YYYY MM DD')} -{' '}
								{data.endDate && moment(data.endDate).format('YYYY MM DD')}
							</Date>
						</Stack>
						<MoreInfo to="/faq">
							{t('MoreAboutPollsAndVoting')} <IconRight />
						</MoreInfo>
					</Grid>
				</Header>
				<Content sx={{ pt: 4, pb: 4, pr: 5, pl: 3 }}>
					<Title>{data.question} *?</Title>
					<RadioSelectWithProgress selected={vote} onChange={setVote} success={success} />
				</Content>
			</Paper>
			<Paper shadow="s02" sx={{ mt: 2, p: 4, pl: 3 }}>
				<Title>{t('YourOffers')}</Title>
				<Textarea minRows={5} placeholder={t('YourMessage')} sx={{ mt: 2 }} />
			</Paper>
			{!poll && (
				<InfoText sx={{ mt: 4 }}>
					<CheckedIcon checked /> {t('VotePolicyAccept')}.
				</InfoText>
			)}
			{success && (
				<Grid item sx={{ mt: 4 }}>
					<SuccessMessage>{t('VoteAccepted')}!</SuccessMessage>
				</Grid>
			)}
			{!success && (
				<Grid item display="flex" justifyContent="flex-end" sx={{ mt: 4 }}>
					<Button onClick={handleConfirm} disabled={!vote}>
						{poll ? t('ConfirmAndVote') : t('ConfirmWithESign')}
					</Button>
				</Grid>
			)}
			<Dialog open={open} onClose={handleClose} showClose>
				<Box sx={{ width: 100 }} margin={{ xs: 8 }}>
					Document
				</Box>
			</Dialog>
		</Grid>
	);
};

const data = {
	title: 'Apklausa dėl laiptinės tvarkymo: valymo ir šlavimo',
	question: 'Ar pritariate, kad laiptinė būtų valoma tris kartus per savaitę',
	status: 'active',
	unit: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
	startDate: '2022-01-10T10:39:39.520Z',
	endDate: '2022-02-20T10:39:39.520Z',
	type: 'voting',
	read: false,
	createdAt: '2022-05-30T10:39:39.520Z',
	_id: '62949eeb70c40da830eeb4be',
};
