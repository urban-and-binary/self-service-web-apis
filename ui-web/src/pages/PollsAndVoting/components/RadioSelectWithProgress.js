import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { FormControlLabel, RadioGroup, Radio } from '@mui/material';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';

const Placeholder = styled('span')(() => ({
	padding: '16px 5px',
}));
const StyledFormControlLabel = styled(FormControlLabel)(() => ({
	'.MuiTypography-root': {
		width: '100%',
	},
}));
const StyledLabel = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
	width: '100%',
	maxWidth: '250px',
	height: 10,
	borderRadius: 5,
	[`&.${linearProgressClasses.colorPrimary}`]: {
		backgroundColor: theme.palette.base.white,
		border: `1px solid ${theme.palette.base.inactive}`,
	},
	[`& .${linearProgressClasses.bar}`]: {
		borderRadius: 5,
		backgroundColor: theme.palette.primary.blue001,
	},
}));

const Label = ({ label, value, count }) => (
	<StyledLabel>
		{label}
		<BorderLinearProgress variant="determinate" value={value} sx={{ ml: 2, mr: 2 }} />
		{count}({value}%)
	</StyledLabel>
);

export const RadioSelectWithProgress = ({ selected, onChange, success }) => {
	const { t } = useTranslation();

	return (
		<RadioGroup value={selected} defaultValue="1" name="poll" onChange={e => onChange(e.target.value)}>
			<StyledFormControlLabel
				value="1"
				control={success ? <Placeholder /> : <Radio />}
				label={<Label label={t('Agree')} value={4} count={1} />}
			/>
			<StyledFormControlLabel
				value="2"
				control={success ? <Placeholder /> : <Radio />}
				label={<Label label={t('Disagree')} value={4} count={1} />}
			/>
			<StyledFormControlLabel
				value="3"
				control={success ? <Placeholder /> : <Radio />}
				label={<Label label={t('HaveNoOpinion')} value={8} count={2} />}
			/>
		</RadioGroup>
	);
};

Label.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.number.isRequired,
	count: PropTypes.number.isRequired,
};

RadioSelectWithProgress.propTypes = {
	selected: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	success: PropTypes.bool.isRequired,
};
