import api from '@api/axios';
import React, { useState, useEffect } from 'react';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { showToast } from '@utils/utils';
import { Grid, Chip, useMediaQuery } from '@mui/material';
import { Filterbar, Table, CardList } from '@components';
import { OWNED_UNIT_STATUSES } from '@common/consts';
import { ALERT } from '@constants/general';

const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Status = styled(Chip)(({ theme }) => ({
	borderRadius: 4,
	color: theme.palette.base.white,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.m,
}));

export const PollsAndVoting = () => {
	const { t } = useTranslation();
	const navigate = useNavigate();
	const theme = useTheme();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	const defaultSelect = { title: t('AllUnits'), value: 'all' };
	const [filter, setFilter] = useState(defaultSelect);
	const [units, setUnits] = useState([defaultSelect]);

	const getOwnedUnits = async () => {
		try {
			const res = await api.get(`/owned-unit?status=${OWNED_UNIT_STATUSES.approved}`);
			const units = res.data.map(item => ({ title: item.label, value: item._id }));
			setUnits([defaultSelect, ...units]);
		} catch (error) {
			showToast({ title: t('OOPS!'), text: t('SomethingWrongTryAgain'), type: ALERT.ERROR });
		}
	};

	useEffect(() => {
		getOwnedUnits();
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const handleRowSelect = item => navigate(`/polls-voting/${item.type}?id=${item._id}`);

	return (
		<Grid container pl={3} pr={3}>
			<Title>{t('PollsAndVoting')}</Title>
			<Filterbar
				selectMenuItems={units}
				selectors={selectors}
				onItemSelect={(e, option) => setFilter(option)}
				selectedValue={filter}
			/>
			{isTablet ? (
				<CardList
					listCardMap={listCardMap}
					onCardPress={item => handleRowSelect(item)}
					tableHeadCells={tableHeadCells}
					dataList={data}
					markUnread
				/>
			) : (
				<Table tableHeadCells={tableHeadCells} dataList={data} onRowPress={item => handleRowSelect(item)} markUnread />
			)}
		</Grid>
	);
};

const listCardMap = {
	titleKey: 'title',
	subTitleKey: 'chip',
	content: [
		{
			dataKey: 'startDate',
			label: 'StartDate',
		},
		{
			dataKey: 'endDate',
			label: 'EndDate',
		},
		{
			dataKey: 'address',
		},
	],
};

const selectors = [
	{
		title: 'All',
		action: () => {
			return;
		},
	},
	{
		title: 'Polls',
		action: () => {
			return;
		},
	},
	{
		title: 'Votings',
		action: () => {
			return;
		},
	},
];

const tableHeadCells = [
	{
		name: 'ID',
		key: 'id',
		width: '45%',
	},
	{
		name: 'Status',
		key: 'status',
		width: '10%',
		sorting: 1,
	},
	{
		name: 'Unit',
		key: 'unitLabel',
		width: '35%',
	},
	{
		name: 'StartDay',
		key: 'startLabel',
		width: '25%',
		sorting: 1,
	},
	{
		name: 'EndDay',
		key: 'endLabel',
		width: '25%',
		sorting: 1,
	},
];

const data = [
	{
		title: 'Ar pritariate etapiniai namo renovacijai?',
		status: <Status label="Active" sx={{ backgroundColor: '#00BE73' }} />,
		unit: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
		startDate: '2022-01-10',
		endDate: '2022-02-30',
		type: 'voting',
		read: false,
		createdAt: '2022-05-30',
		_id: '62949eeb70c40da830eeb4be',
	},
	{
		title: 'Kaip vertinate pirmininko darbą?',
		status: <Status label="Ended" sx={{ backgroundColor: '#CCCCCC' }} />,
		unit: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
		startDate: '2022-01-10',
		endDate: '2022-02-30',
		type: 'poll',
		read: true,
		createdAt: '2022-05-30',
		_id: '62949eeb70c40da830eeb4be',
	},
	{
		title: 'Ar jaučiatės saugūs savo name ir laiptinėje?',
		status: <Status label="Ended" sx={{ backgroundColor: '#CCCCCC' }} />,
		unit: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
		startDate: '2022-01-10',
		endDate: '2022-02-30',
		type: 'poll',
		read: true,
		createdAt: '2022-05-30',
		_id: '62949eeb70c40da830eeb4be',
	},
];
