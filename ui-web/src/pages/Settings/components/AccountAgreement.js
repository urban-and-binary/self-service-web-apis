import React from 'react';
import { styled } from '@mui/system';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Divider, Box } from '@mui/material';
import { Switch } from '@components';

import PropTypes from 'prop-types';

const Title = styled('p')(({ theme }) => ({
	margin: 0,
	textTransform: 'capitalize',
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const StyledLabel = styled('p')(({ theme }) => ({
	a: {
		textDecoration: 'none',
		color: theme.palette.primary.blue002,
	},
}));

const Item = ({ title, name, text, value, onChange }) => (
	<Box sx={{ mt: 3 }}>
		<Title>{title}</Title>
		<Switch
			title={title}
			name={name}
			value={value}
			label={text}
			labelPlacement="start"
			sx={{ '.MuiFormControlLabel-label': { marginRight: 3 }, display: 'flex' }}
			onChange={onChange}
			disabled={name !== 'advertisment' && value ? true : false}
		/>
	</Box>
);

export const AccountAgreement = ({ data, onChange }) => {
	const { t } = useTranslation();

	return (
		<>
			<Item
				title={t(dataAgreement[0].type)}
				name={dataAgreement[0].name}
				text={
					<StyledLabel>
						{t('IveUnderstood')} <Link to="/">{t('PrivacyPolicy')}</Link> {t('And')} <Link to="/">{t('UsageTerms')}</Link>{' '}
						{t('AgreeToComply')}.
					</StyledLabel>
				}
				value={data?.privacyPolicy}
				onChange={onChange}
				disabled={true}
			/>
			<Divider />
			<Item
				title={t(dataAgreement[1].type)}
				name={dataAgreement[1].name}
				text={t('AgreeToDataManagement')}
				value={data?.personalData}
				onChange={onChange}
			/>
			<Divider />
			<Item
				title={t(dataAgreement[2].type)}
				name={dataAgreement[2].name}
				text={
					<StyledLabel>
						{t('IveUnderstood')} <Link to="/">{t('UserTerms')}</Link> {t('AgreeToComply')}.
					</StyledLabel>
				}
				value={data?.userRules}
				onChange={onChange}
			/>
			<Divider />
			<Item
				title={t(dataAgreement[3].type)}
				name={dataAgreement[3].name}
				text={t('AgreeToNewsletter')}
				value={data?.advertisment}
				onChange={onChange}
			/>
		</>
	);
};

const dataAgreement = [
	{ type: 'PrivacyPolicy', name: 'privacyPolicy' },
	{ type: 'PersonalData', name: 'personalData' },
	{ type: 'AccountAggreementsUserTerms', name: 'userRules' },
	{ type: 'Advertisement', name: 'advertisment' },
];

Item.propTypes = {
	title: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	text: PropTypes.object.isRequired,
	value: PropTypes.bool.isRequired,
	onChange: PropTypes.func.isRequired,
};

AccountAgreement.propTypes = {
	data: PropTypes.object,
	onChange: PropTypes.func.isRequired,
};
