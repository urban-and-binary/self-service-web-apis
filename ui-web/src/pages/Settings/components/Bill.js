import React from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { FormControlLabel, RadioGroup, Radio } from '@mui/material';
import PropTypes from 'prop-types';

const Title = styled('div')(({ theme }) => ({
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));

const StyledRadioGroup = styled(RadioGroup)(({ theme }) => ({
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));

export const Bill = ({ data, onChange }) => {
	const { t } = useTranslation();

	return (
		<>
			<Title sx={{ mt: 3 }}>{t('ChooseBestOption')}:</Title>
			<StyledRadioGroup value={String(data?.receiveFormat)} onChange={onChange} name={'receiveFormat'}>
				<FormControlLabel name="receiveFormat" value={dataBill[0].type} control={<Radio />} label={t(dataBill[0].text)} />
				<FormControlLabel name="receiveFormat" value={dataBill[1].type} control={<Radio />} label={t(dataBill[1].text)} />
				<FormControlLabel name="receiveFormat" value={dataBill[2].type} control={<Radio />} label={t(dataBill[2].text)} />
			</StyledRadioGroup>
		</>
	);
};

const dataBill = [
	{
		type: 'digital',
		text: 'ReceiveEBills',
	},
	{
		type: 'paper',
		text: 'ReceiveMailBills',
	},
	{
		type: 'both',
		text: 'ReceiveBillsMailEmail',
	},
];

Bill.propTypes = {
	data: PropTypes.object,
	onChange: PropTypes.func.isRequired,
};
