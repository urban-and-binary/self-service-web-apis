import api from '@api/axios';
import React, { useState, useEffect } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import { Accordion } from '@components';
import { AccountAgreement } from './components/AccountAgreement';
import { Bill } from './components/Bill';
import { useLocation } from 'react-router-dom';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';

const Title = styled('p')(({ theme }) => ({
	with: '100%',
	margin: 0,
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));

export const Settings = () => {
	const { t } = useTranslation();
	const location = useLocation();
	const [settings, setSettings] = useState({});

	const getSetting = async () => {
		try {
			const res = await api.get('/users/settings');
			setSettings(res.data);
		} catch (error) {
			showToast({ title: t('OOPS!'), text: t('SomethingWrongTryRefresh'), type: ALERT.ERROR });
		}
	};

	useEffect(() => {
		getSetting();
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const handleChangeAgreements = async (name, value) => {
		const agreement = settings.agreements;

		await api.put('/users/settings', { agreements: { ...agreement, [name]: value } });
		getSetting();
	};

	const handleChangeBills = async e => {
		const { name, value } = e.target;
		const bill = settings.bills;

		await api.put('/users/settings', { bills: { ...bill, [name]: value } });
		getSetting();
	};

	return (
		<Grid container pl={4} pr={4}>
			<Grid item xs={12}>
				<Title>{t('Settings')}</Title>
			</Grid>
			<Grid item xs={12} sm={8} sx={{ mt: 3 }}>
				<Accordion title={t('AccountAgreements')}>
					<AccountAgreement data={settings?.agreements} onChange={handleChangeAgreements} />
				</Accordion>
				<Accordion title={t('Bills')} sx={{ mt: 3 }} defaultExpanded={location.state?.open === 'billings'}>
					<Bill data={settings?.bills} onChange={handleChangeBills} />
				</Accordion>
			</Grid>
		</Grid>
	);
};
