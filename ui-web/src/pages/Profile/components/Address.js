import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Grid, Stack } from '@mui/material';
import { Formik, Form, Field } from 'formik';
import { Input, Spinner } from '@components';
import Button from '@components/Button/Button';
import { Link } from 'react-router-dom';

const HeadingTitle = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	lineHeight: theme.typography.lineHeight.small,
	color: theme.palette.base.gray,
}));

const Address = ({ editing, disableEditing, user, changeUserData }) => {
	const { t } = useTranslation();

	const [initialValues, setInitialValues] = useState({});

	const validationSchema = Yup.object({
		country: Yup.string().required(t('Required')),
		city: Yup.string().required(t('Required')),
		address: Yup.string().required(t('Required')),
	});

	const formikRef = useRef();

	useEffect(() => {
		if (!editing && formikRef.current) {
			formikRef.current.resetForm();
		}
	}, [editing]);

	useEffect(() => {
		setInitialValues({
			country: user?.country || '',
			city: user?.city || '',
			address: user?.address || '',
		});
	}, [user]);

	if (!user) {
		return <Spinner size={50} />;
	}

	return (
		<>
			<HeadingTitle sx={{ mb: 3 }}>{t('CorrespondenceAddress')}:</HeadingTitle>
			<Formik
				innerRef={ref => (formikRef.current = ref)}
				enableReinitialize
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={values => changeUserData(values, false, () => setInitialValues(values))}>
				{({ values, touched, errors }) => (
					<Form>
						<Grid container item xs={12} md={12} lg={6}>
							<Field
								id="country"
								name="country"
								label={t('Country')}
								type="text"
								component={Input}
								placeholder={t('Country')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.country}
								disabled={!editing}
							/>
							<Field
								id="city"
								name="city"
								label={t('City')}
								type="text"
								component={Input}
								placeholder={t('City')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.city}
								style={{ marginTop: 22 }}
								disabled={!editing}
							/>
							<Field
								id="address"
								name="address"
								label={t('Address')}
								type="text"
								component={Input}
								placeholder={t('Address')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.address}
								style={{ marginTop: 22 }}
								disabled={!editing}
							/>
						</Grid>
						<Text sx={{ mt: 4 }}>
							{t('ToCancelInfoMail')}{' '}
							<Link to="/settings" state={{ open: 'billings' }}>
								{t('ThisLink')}
							</Link>
						</Text>
						{editing && (
							<Stack spacing={2} direction={{ xs: 'column-reverse', sm: 'row' }} justifyContent="flex-end" sx={{ mt: 8 }}>
								<Button variant="outlined" onClick={e => disableEditing(e)}>
									{t('Cancel')}
								</Button>
								<Button type="submit">{t('SaveChanges')}</Button>
							</Stack>
						)}
					</Form>
				)}
			</Formik>
		</>
	);
};

export default Address;

Address.propTypes = {
	editing: PropTypes.bool,
	disableEditing: PropTypes.func,
	user: PropTypes.object,
	changeUserData: PropTypes.func,
};
