import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { styled } from '@mui/system';
import { Box } from '@mui/material';
import { Formik, Form, Field } from 'formik';
import { Button, Input, Modal, VerificationCode } from '@components';
import api from '@api/axios';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';

const Container = styled(Box)(({ theme }) => ({
	textAlign: 'center',
	color: theme.palette.base.dark,
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: '24px 0 32px 0',
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));

export const VerifyFieldModal = ({
	open,
	onClose,
	title,
	subTitle,
	fieldKey,
	fieldLabel,
	fieldPlaceholder,
	confText,
	validationSchema,
	submitEndpoint,
	sendEndpoint,
}) => {
	const { t } = useTranslation();

	const [field, setField] = useState(null);

	const onRequestUpdate = async (values, onSuccess) => {
		try {
			await api.post(sendEndpoint, values);
			onSuccess && onSuccess();
		} catch (error) {
			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryLoginAgain'),
				type: ALERT.ERROR,
			});
			handleClose();
		}
	};

	const onVerify = values => {
		onRequestUpdate(values, () => setField(values[fieldKey]));
	};

	const submitVerCode = async code => {
		try {
			await api.put(submitEndpoint, { code, [fieldKey]: field });
			handleClose(field);
		} catch (error) {
			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryLoginAgain'),
				type: ALERT.ERROR,
			});
			handleClose();
		}
	};

	const handleClose = changedField => {
		onClose(changedField);
		setField(null);
	};

	return (
		<Modal open={open} onClose={() => handleClose()}>
			{!field ? (
				<Container padding={{ xs: '40px 24px 60px 24px', sm: '40px 150px 60px 150px' }}>
					<Title>{title}</Title>
					<Subtitle>{subTitle}</Subtitle>
					<Formik initialValues={{ [fieldKey]: '' }} validationSchema={validationSchema} onSubmit={onVerify}>
						{({ values, touched, errors, setFieldValue, isValid, dirty }) => (
							<Form>
								<Field
									id={`verify-${fieldKey}`}
									name={fieldKey}
									label={fieldLabel}
									type="text"
									component={Input}
									placeholder={fieldPlaceholder}
									InputLabelProps={{
										shrink: true,
									}}
									fullWidth
									variant="standard"
									errors={errors}
									touched={touched}
									value={values[fieldKey]}
									onChange={e => setFieldValue(fieldKey, e.target.value)}
								/>
								<Button type="submit" fullwidth="true" sx={{ mt: 6 }} disabled={!dirty || !isValid}>
									{confText}
								</Button>
								<Button onClick={() => handleClose()} variant="outlined" fullwidth="true" sx={{ mt: 2 }}>
									{t('Cancel')}
								</Button>
							</Form>
						)}
					</Formik>
				</Container>
			) : (
				<VerificationCode
					onSubmit={submitVerCode}
					onResend={() => onRequestUpdate({ [fieldKey]: field })}
					onClose={() => handleClose()}
					title={title}
					text={t('EnterDigitCodeSent')}
					contact={field}
					length={6}
					sx={{ p: 6, pb: 8 }}
					paddingLeft={{ xs: 3, sm: 14 }}
					paddingRight={{ xs: 3, sm: 14 }}
				/>
			)}
		</Modal>
	);
};

VerifyFieldModal.propTypes = {
	open: PropTypes.bool,
	onClose: PropTypes.func.isRequired,
	title: PropTypes.string,
	subTitle: PropTypes.string,
	fieldKey: PropTypes.string.isRequired,
	validationSchema: PropTypes.object.isRequired,
	fieldLabel: PropTypes.string,
	fieldPlaceholder: PropTypes.string,
	confText: PropTypes.string.isRequired,
	submitEndpoint: PropTypes.string.isRequired,
	sendEndpoint: PropTypes.string.isRequired,
};
