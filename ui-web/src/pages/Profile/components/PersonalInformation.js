import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { ALERT } from '@constants/general';
import { showToast } from '@utils/utils';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { Grid, Stack } from '@mui/material';
import { EditButton, Input, Spinner } from '@components';
import Button from '@components/Button/Button';
import { VerifyFieldModal } from './VerifyFieldModal';

const ChangeField = styled('div')(() => ({
	display: 'flex',
	alignItems: 'flex-end',
	width: '100%',
}));

const PersonalInformation = ({ editing, disableEditing, user, changeUserData }) => {
	const { t } = useTranslation();

	const [initialValues, setInitialValues] = useState({});
	const [showChangeDialog, setShowChangeDialog] = useState(false);

	const validationSchema = Yup.object({
		name: Yup.string().required(t('Required')),
		surname: Yup.string().required(t('Required')),
	});

	const emailValidationSchema = Yup.object({
		email: Yup.string()
			.email(t('InvalidEmailFormat'))
			.required(t('Required')),
	});
	const phoneValidationSchema = Yup.object({
		phone: Yup.number()
			.typeError(t('ContactPhoneOnlyNumbers'))
			.required(t('Required')),
	});

	const formikRef = useRef();

	useEffect(() => {
		if (!editing && formikRef.current) {
			formikRef.current.resetForm();
		}
	}, [editing]);

	useEffect(() => {
		setInitialValues({
			name: user?.name || '',
			surname: user?.surname || '',
			email: user?.email || '',
			phone: user?.phone || '',
		});
	}, [user]);

	const handleChage = (key, value) => {
		if (value) {
			showToast({
				text: t('ProfileUpdated'),
				type: ALERT.SUCCESS,
			});
			setInitialValues({ ...initialValues, [key]: value });
		}

		setShowChangeDialog(false);
	};

	if (!user) {
		return <Spinner size={50} />;
	}

	return (
		<>
			<Formik
				enableReinitialize
				innerRef={ref => (formikRef.current = ref)}
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={values => changeUserData({ name: values.name, surname: values.surname }, false, () => setInitialValues(values))}>
				{({ values, touched, errors }) => (
					<Form>
						<Grid container item xs={12} md={12} lg={8}>
							<Field
								id="name"
								name="name"
								label={t('Name')}
								type="text"
								component={Input}
								placeholder={t('Name')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.name}
								disabled={!editing}
							/>
							<Field
								id="surname"
								name="surname"
								label={t('Surname')}
								type="text"
								component={Input}
								placeholder={t('Surname')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.surname}
								disabled={!editing}
								style={{ marginTop: 22 }}
							/>
							<ChangeField>
								<Field
									id="email"
									name="email"
									label={t('Email')}
									type="text"
									component={Input}
									placeholder={t('YourEmail')}
									InputLabelProps={{
										shrink: true,
										required: true,
									}}
									fullWidth
									variant="standard"
									errors={errors}
									touched={touched}
									value={values.email}
									style={{ marginTop: 22 }}
									disabled
								/>
								{editing && (
									<EditButton onClick={() => setShowChangeDialog('email')} sx={{ ml: 1 }}>
										{t('Change')}
									</EditButton>
								)}
							</ChangeField>
							<ChangeField>
								<Field
									id="phone"
									name="phone"
									label={t('Phone')}
									type="text"
									component={Input}
									placeholder={t('YourPhone')}
									InputLabelProps={{
										shrink: true,
									}}
									fullWidth
									variant="standard"
									errors={errors}
									touched={touched}
									value={values.phone}
									style={{ marginTop: 22 }}
									disabled
								/>
								{editing && (
									<EditButton onClick={() => setShowChangeDialog('phone')} sx={{ ml: 1 }}>
										{t('Change')}
									</EditButton>
								)}
							</ChangeField>
						</Grid>
						{editing && (
							<Stack spacing={2} direction={{ xs: 'column-reverse', sm: 'row' }} justifyContent="flex-end" sx={{ mt: 8 }}>
								<Button variant="outlined" onClick={e => disableEditing(e)}>
									{t('Cancel')}
								</Button>
								<Button type="submit">{t('SaveChanges')}</Button>
							</Stack>
						)}
					</Form>
				)}
			</Formik>
			<VerifyFieldModal
				open={showChangeDialog === 'email'}
				onClose={value => handleChage('email', value)}
				title={t('ChangeEmail')}
				subTitle={t('EmailChangeDesc')}
				fieldKey="email"
				fieldLabel={t('Email')}
				fieldPlaceholder={t('YourEmail')}
				confText={t('Update')}
				validationSchema={emailValidationSchema}
				submitEndpoint="users/profile/email"
				sendEndpoint="users/profile/email-code"
			/>
			<VerifyFieldModal
				open={showChangeDialog === 'phone'}
				onClose={value => handleChage('phone', value)}
				title={t('ChangePhone')}
				subTitle={t('PhoneChangeDesc')}
				fieldKey="phone"
				fieldLabel={t('Phone')}
				fieldPlaceholder={t('YourPhone')}
				confText={t('Update')}
				validationSchema={phoneValidationSchema}
				submitEndpoint="users/profile/phone"
				sendEndpoint="users/profile/phone-code"
			/>
		</>
	);
};

export default PersonalInformation;

PersonalInformation.propTypes = {
	editing: PropTypes.bool,
	user: PropTypes.object,
	changeUserData: PropTypes.func,
	disableEditing: PropTypes.func,
};
