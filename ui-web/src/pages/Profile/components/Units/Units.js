import React, { useState, useEffect } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Dialog, SuccessMessage } from '@components';
import Button from '@components/Button/Button';
import { Divider, CircularProgress } from '@mui/material';
import { Unit } from './Unit';
import { AddUnit } from './AddUnit';
import { ConfirmUnit } from './ConfirmUnit';
import api from '@api/axios';
import { OWNED_UNIT_STATUSES } from '@common/consts';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';

const StyledUnit = styled(Unit)(() => ({
	margin: '16px 0',
}));
const Footer = styled('div')(({ theme }) => ({
	display: 'flex',
	justifyContent: 'flex-end',
	marginTop: theme.spacing(5),
}));
const Address = styled('p')(() => ({
	margin: '24px 0 0 0',
}));

const LoadContainer = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'center',
}));

const Units = () => {
	const { t } = useTranslation();

	const [open, setOpen] = useState(false);
	const [unit, setUnit] = useState(null);
	const [step, setStep] = useState(0);

	const [units, setUnits] = useState({
		approved: [],
		pending: [],
	});
	const [loadingUnits, setLoadingUnits] = useState(false);

	useEffect(() => {
		loadOwnedUnits();
	}, []);

	const saveUnit = () =>
		api.post('/owned-unit', {
			building: unit.building.id,
			buildingExtId: unit.building.externalId,
			unitExtId: unit.unit.id,
		});

	const loadOwnedUnits = async () => {
		setLoadingUnits(true);

		const approved = [];
		const pending = [];

		try {
			const res = await api.get('/owned-unit');

			res.data.forEach(respUnit => {
				if (respUnit.status === OWNED_UNIT_STATUSES.approved) {
					approved.push(respUnit);
				} else {
					pending.push(respUnit);
				}
			});
		} finally {
			setLoadingUnits(false);
		}

		setUnits({
			approved,
			pending,
		});
	};

	const handleNext = values => {
		setUnit(values);
		setStep(2);
	};

	const handleConfirm = async () => {
		try {
			await saveUnit();

			setStep(3);

			loadOwnedUnits();
		} catch (error) {
			handleClose();

			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryLoginAgain'),
				type: ALERT.ERROR,
			});
		}
	};

	const handleClose = () => {
		setOpen(false);
		setStep(0);
		setUnit(null);
	};

	return (
		<>
			{loadingUnits ? (
				<LoadContainer>
					<CircularProgress color="inherit" size={50} />
				</LoadContainer>
			) : (
				<>
					{units.approved.map(item => (
						<StyledUnit key={item._id} status={item.status} to={`/unit/${item._id}`} title={item.title} text={item.label} />
					))}
					{!!units.pending.length && <Divider sx={{ margin: '24px 0' }} />}
					{units.pending.map(item => (
						<StyledUnit key={item._id} to="/" status={item.status} type={'disabled'} title={item.title} text={item.label} />
					))}
				</>
			)}
			<Footer>
				<Button
					onClick={() => {
						setOpen(true);
						setStep(1);
					}}>
					+ {t('AddNewUnit')}
				</Button>
			</Footer>
			<Dialog open={open} onClose={handleClose} showClose={step !== 2}>
				{step === 1 && <AddUnit onCancel={handleClose} onConfirm={values => handleNext(values)} />}
				{step === 2 && <ConfirmUnit unit={unit} onCancel={handleClose} onConfirm={handleConfirm} />}
				{step === 3 && (
					<SuccessMessage text={t('UnitAdded')} sx={{ mt: -10 }}>
						<Address>{unit.label}</Address>
					</SuccessMessage>
				)}
			</Dialog>
		</>
	);
};

export default Units;
