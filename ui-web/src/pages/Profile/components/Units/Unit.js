import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { ChevronIcon, HomeIcon } from '@assets/icons';
import { OWNED_UNIT_STATUSES } from '@common/consts';

const Main = styled(Link)(({ theme, status, disabled }) => ({
	background: theme.palette.base.background,
	borderRadius: 16,
	padding: '16px 30px 16px 16px',
	display: 'flex',
	alignItems: 'center',
	textDecoration: 'none',
	color: status === OWNED_UNIT_STATUSES.approved ? theme.palette.base.dark : theme.palette.base.gray,
	pointerEvents: disabled ? 'none' : '',
}));
const Icon = styled('div')(({ theme, status }) => ({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	width: 40,
	height: 40,
	borderRadius: 8,
	backgroundColor: status === OWNED_UNIT_STATUSES.approved ? theme.palette.primary.blue003 : theme.palette.base.inactive,
	boxShadow: `0px 6px 0px -4px ${status === OWNED_UNIT_STATUSES.approved ? theme.palette.primary.blue004 : 'transparent'}`,
}));
const TextWrapper = styled('div')(({ theme }) => ({
	flex: 1,
	marginLeft: theme.spacing(2),
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	textTransform: 'capitalize',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const StyledArrow = styled(ChevronIcon)(() => ({
	transform: 'rotate(-90deg)',
}));
const Badge = styled('span')(({ theme }) => ({
	color: 'white',
	padding: '7px 8px',
	borderRadius: 4,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.m,
	backgroundColor: theme.palette.default.error,
}));

export const Unit = ({ to, status = OWNED_UNIT_STATUSES.approved, title, text, ...props }) => {
	const { t } = useTranslation();

	const approved = status === OWNED_UNIT_STATUSES.approved;
	const disabled = [OWNED_UNIT_STATUSES.pending, OWNED_UNIT_STATUSES.failed].includes(status);

	return (
		<Main to={to} status={status} disabled={disabled} {...props}>
			<Icon status={status}>
				<HomeIcon />
			</Icon>
			<TextWrapper>
				{title && approved && <Title>{title}</Title>}
				<Text>{text}</Text>
				{disabled && <Badge>{t('PendingApproval')}</Badge>}
			</TextWrapper>
			{approved && <StyledArrow />}
		</Main>
	);
};

Unit.propTypes = {
	to: PropTypes.string.isRequired,
	status: PropTypes.oneOf(Object.values(OWNED_UNIT_STATUSES)),
	title: PropTypes.string,
	text: PropTypes.string.isRequired,
};
