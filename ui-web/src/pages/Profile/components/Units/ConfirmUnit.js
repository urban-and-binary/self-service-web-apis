import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { useMediaQuery } from '@mui/material';
import { Checkbox } from '@components';
import Button from '@components/Button/Button';

const Container = styled('div')(() => ({
	maxWidth: 500,
	marginTop: -80,
	padding: '40px 50px',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	textAlign: 'center',
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Buttons = styled('div')(() => ({
	display: 'flex',
	flexWrap: 'wrap',
	justifyContent: 'flex-end',
}));

export const ConfirmUnit = ({ unit, onCancel, onConfirm }) => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

	const [confirmed, setConfirmed] = useState(false);

	return (
		<Container>
			<Title>{t('AddUnitConfirmation')}</Title>
			<Checkbox
				name="confirmed"
				value={confirmed}
				onChange={(name, checked) => setConfirmed(checked)}
				label={unit?.label}
				labelPlacement="start"
				bg={confirmed ? 'true' : 'false'}
				sx={{ mt: 4, width: '100%' }}
			/>
			<Buttons sx={{ mt: 5 }}>
				<Button variant="outlined" onClick={onCancel} fullwidth={isMobile} sx={{ mr: isMobile ? 0 : 2 }}>
					{t('Cancel')}
				</Button>
				<Button disabled={!confirmed} onClick={onConfirm} fullwidth={isMobile} sx={{ mt: isMobile ? 1 : 0 }}>
					{t('Confirm')}
				</Button>
			</Buttons>
		</Container>
	);
};

ConfirmUnit.propTypes = {
	unit: PropTypes.object,
	onCancel: PropTypes.func,
	onConfirm: PropTypes.func,
};
