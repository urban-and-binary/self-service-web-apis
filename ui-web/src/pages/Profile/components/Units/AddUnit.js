import api from '@api/axios';
import React, { useEffect, useRef, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { Grid, useMediaQuery } from '@mui/material';
import { BigDropdown, Dropdown } from '@components';
import Button from '@components/Button/Button';
import { debounce, sortBy } from 'lodash';
import qs from 'query-string';
import { formUnitLabel } from '@common/utils';

const Container = styled(Grid)(() => ({
	marginTop: -80,
	padding: '24px 34px 43px 32px',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const Unit = styled('p')(({ theme }) => ({
	marginTop: theme.spacing(4),
	marginLeft: theme.spacing(4),
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const Buttons = styled('div')(() => ({
	display: 'flex',
	flexWrap: 'wrap',
	justifyContent: 'flex-end',
}));

const pageSize = 50;

export const AddUnit = ({ onCancel, onConfirm }) => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

	const buildingSearch = useRef('');

	const [building, setBuilding] = useState();
	const [unit, setUnit] = useState(null);

	const [hasNextPage, setHasNextPage] = useState(true);

	const [units, setUnits] = useState(null);
	const [buildings, setBuildings] = useState([]);

	const [loadingBuildings, setLoadingBuildings] = useState(false);
	const [loadingUnits, setLoadingUnits] = useState(false);

	useEffect(() => {
		loadBuildings({});
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const loadBuildings = async ({ search, lastPageItemId, lastPageItemTitle, loadMore = false }) => {
		setLoadingBuildings(true);

		try {
			const query = qs.stringify({ pageSize, search, lastPageItemId, lastPageItemTitle });

			const res = await api.get(`/buildings?${query}`);

			let newBuildings = res.data.map(respBuilding => ({
				id: respBuilding._id,
				externalId: respBuilding.externalId,
				label: respBuilding.title,
				// this is mainly the full building address without the city or municipality
				name: respBuilding.name,
				street: respBuilding.street,
				houseNumber: respBuilding.houseNumber,
				houseLetter: respBuilding.houseLetter,
				municipality: respBuilding.municipality,
				city: respBuilding.city,
			}));

			const nextPage = newBuildings.length === pageSize;

			if (hasNextPage !== nextPage) {
				setHasNextPage(nextPage);
			}

			if (loadMore) {
				newBuildings = [...buildings, ...newBuildings];
			}

			setBuildings(newBuildings);
		} finally {
			setLoadingBuildings(false);
		}
	};

	const loadUnits = async buildingId => {
		setLoadingUnits(true);

		try {
			const res = await api.get(`/buildings/${buildingId}/units`);

			setUnits(
				sortBy(
					res.data.map(respUnit => ({
						id: respUnit.id,
						label: `${respUnit.unitNumber || ''}${respUnit.unitLetter || ''}`,
						buildingId: respUnit.buildingId,
						unitNumber: respUnit.unitNumber,
						unitLetter: respUnit.unitLetter,
						name: respUnit.name,
					})),
					['unitNumber', 'unitLetter', 'name'],
				),
			);
		} finally {
			setLoadingUnits(false);
		}
	};

	const onLoadMore = () => {
		if (buildings.length && buildings.length >= pageSize) {
			const lastBuilding = buildings[buildings.length - 1];
			loadBuildings({
				search: buildingSearch.current,
				lastPageItemId: lastBuilding.id,
				lastPageItemTitle: lastBuilding.label,
				loadMore: true,
			});
		}
	};

	const handleBuilding = (e, option) => {
		setBuilding(option);
		setUnit(null);
		loadUnits(option.externalId);
	};

	const handleUnit = (e, option) => {
		setUnit(option);
	};

	const onBuildingSearch = (e, value, type) => {
		if (type === 'reset') {
			buildingSearch.current = '';
			loadBuildings({});
		} else {
			buildingSearch.current = value;
		}

		// reset values before search
		setHasNextPage(true);
		setBuildings([]);

		loadBuildings({ search: buildingSearch.current });
	};

	const debouncedSearch = useMemo(() => debounce(onBuildingSearch, 500), []);

	const label = unit && building && formUnitLabel(building, unit);
	return (
		<Container width={{ sm: 500 }}>
			<Title>{t('AddNewUnit')}</Title>
			<Subtitle>{t('HereYouCanAdd')}</Subtitle>
			<BigDropdown
				disablePortal={false}
				loading={loadingBuildings}
				hasNextPage={hasNextPage}
				loadingText={t('Loading')}
				options={buildings}
				onChange={handleBuilding}
				onLoadMore={onLoadMore}
				label={t('PickBuilding')}
				placeholder={t('TypeWordToSearch')}
				// overriding filtering options for backend search to work
				filterOptions={x => x}
				onInputChange={debouncedSearch}
				sx={{ marginTop: 9 }}
			/>
			<Dropdown
				disablePortal={false}
				loading={loadingUnits}
				ListboxProps={{ style: { maxHeight: '200px' } }}
				options={units || []}
				onChange={handleUnit}
				value={unit}
				label={t('PickUnit')}
				placeholder={`+ ${t('PickUnit')}`}
				disabled={!building}
				sx={{ marginTop: 4 }}
			/>
			{unit && <Unit>{label}</Unit>}
			<Buttons sx={{ mt: 5 }}>
				<Button variant="outlined" onClick={onCancel} fullwidth={isMobile} sx={{ mr: isMobile ? 0 : 2 }}>
					{t('Cancel')}
				</Button>
				<Button
					disabled={!unit}
					onClick={() =>
						onConfirm({
							unit,
							building,
							label,
						})
					}
					fullwidth={isMobile}
					sx={{ mt: isMobile ? 1 : 0 }}>
					{t('Confirm')}
				</Button>
			</Buttons>
		</Container>
	);
};

AddUnit.propTypes = {
	data: PropTypes.object,
	onCancel: PropTypes.func,
	onConfirm: PropTypes.func,
};
