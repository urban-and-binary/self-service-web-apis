import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { Grid, Stack } from '@mui/material';
import { Input, PasswordValidator } from '@components';
import Button from '@components/Button/Button';

const PasswordLabel = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));

const initialValues = {
	currentPassword: '',
	newPassword: '',
	repeatPassword: '',
};

const Passwords = ({ editing, changeUserData, disableEditing }) => {
	const { t } = useTranslation();
	const schema = Yup.object({
		currentPassword: Yup.string().required(t('Required')),
		newPassword: Yup.string().required('PasswordRequired'),
		repeatPassword: Yup.string()
			.oneOf([Yup.ref('newPassword'), null], 'Passwords must match')
			.required('Repeat password is required'),
	});

	const formikRef = useRef();

	useEffect(() => {
		if (!editing && formikRef.current) {
			formikRef.current.resetForm();
		}
	}, [editing]);

	return (
		<>
			<PasswordLabel sx={{ mb: 3 }}>{t('ChangePassword')}</PasswordLabel>
			<Formik
				innerRef={ref => (formikRef.current = ref)}
				initialValues={initialValues}
				validationSchema={schema}
				onSubmit={values => changeUserData(values, true)}>
				{({ values, touched, errors }) => (
					<Form id="password-form">
						<Grid container item xs={12} md={12} lg={6}>
							<Field
								id="currentPassword"
								name="currentPassword"
								label={t('CurrentPassword')}
								type="password"
								component={Input}
								placeholder={t('CurrentPassword')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.currentPassword}
								disabled={!editing}
							/>
							<Field
								id="newPassword"
								name="newPassword"
								label={t('NewPassword')}
								type="password"
								component={Input}
								placeholder={t('NewPassword')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.newPassword}
								disabled={!editing}
								sx={{ mt: 3 }}
							/>
							<Field
								id="repeatPassword"
								name="repeatPassword"
								label={t('RepeatNewPassword')}
								type="password"
								component={Input}
								placeholder={t('RepeatNewPassword')}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.repeatPassword}
								sx={{ mt: 3 }}
								disabled={!editing}
							/>
						</Grid>
						<PasswordValidator password={values.newPassword} repeatPassword={values.repeatPassword} sx={{ mt: 2 }} />
						{editing && (
							<Stack spacing={2} direction={{ xs: 'column-reverse', sm: 'row' }} justifyContent="flex-end" sx={{ mt: 8 }}>
								<Button variant="outlined" onClick={e => disableEditing(e)}>
									{t('Cancel')}
								</Button>
								<Button form="password-form" type="submit">
									{t('SaveChanges')}
								</Button>
							</Stack>
						)}
					</Form>
				)}
			</Formik>
		</>
	);
};

Passwords.propTypes = {
	editing: PropTypes.bool,
	disableEditing: PropTypes.func,
	changeUserData: PropTypes.func,
};

export default Passwords;
