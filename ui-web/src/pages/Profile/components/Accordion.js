import api from '@api/axios';
import useUser from '@hooks/useUser';
import React, { useState, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import { ChevronIcon } from '@assets/icons';
import { useTranslation } from 'react-i18next';
import { ALERT } from '@constants/general';
import { EditButton } from '@components';
import { showToast } from '@utils/utils';

const StyledAccordion = styled(Accordion)(({ theme }) => ({
	borderRadius: theme.shape.br2,
	boxShadow: theme.shadow.s01,
	'&:before, &:after': {
		display: 'none',
	},
}));
const StyledSummary = styled(AccordionSummary)(() => ({
	'& .MuiAccordionSummary-content': {
		margin: 0,
	},
	padding: '30px 30px 30px 37px',
	minHeight: 30,
}));
const StyledDetails = styled(AccordionDetails)(() => ({
	padding: '0 30px 40px 37px',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
	paddingRight: 20,
}));

const AccordionComp = ({ title, children, editable = false }) => {
	const [opened, setOpened] = useState(false);
	const [editing, setEditing] = useState(false);
	const { setUser } = useUser();
	const { t } = useTranslation();

	const handleEditing = e => {
		e.stopPropagation();
		setEditing(lastState => !lastState);
	};

	const disableEditing = e => {
		e.preventDefault();
		setEditing(false);
	};

	const changeUserData = async (values, password = false, reinitialize = false) => {
		try {
			const url = password ? '/users/profile/password' : '/users/profile/info';
			const res = await api.put(url, values);
			setUser({ user: res.data });
			setEditing(false);
			reinitialize && reinitialize();
			showToast({
				text: t('ProfileUpdated'),
				type: ALERT.SUCCESS,
			});
		} catch (error) {
			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryLoginAgain'),
				type: ALERT.ERROR,
			});
		}
	};

	return (
		<StyledAccordion disableGutters onChange={() => setOpened(lastState => !lastState)} square sx={{ mb: 3 }}>
			<StyledSummary
				expandIcon={<ChevronIcon sx={{ fontSize: 16, color: 'black' }} />}
				aria-controls="panel1a-content"
				id="panel1a-header">
				<Title>{title}</Title>
				{opened && editable && <EditButton onClick={handleEditing}>{t('Edit')}</EditButton>}
			</StyledSummary>
			<StyledDetails>{cloneElement(children, { editing, disableEditing, changeUserData })}</StyledDetails>
		</StyledAccordion>
	);
};

export default AccordionComp;

AccordionComp.propTypes = {
	title: PropTypes.string,
	editable: PropTypes.bool,
	children: PropTypes.node,
};
