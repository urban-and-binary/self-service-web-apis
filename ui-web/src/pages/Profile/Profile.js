import useUser from '@hooks/useUser';
import React from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import Address from './components/Address';
import Units from './components/Units/Units';
import Passwords from './components/Passwords';
import Accordion from './components/Accordion';
import PersonalInformation from './components/PersonalInformation';

const Title = styled('p')(({ theme }) => ({
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));

export const Profile = () => {
	const { t } = useTranslation();
	const { data } = useUser();
	const user = data.user || null;

	return (
		<Grid container item direction="column" pr={3} pl={3} xs={12} md={8}>
			<Title sx={{ mb: 3 }}>{t('Profile')}</Title>
			<Accordion title={t('PersonalInformation')} editable>
				<PersonalInformation user={user} />
			</Accordion>
			<Accordion title={t('Address')} editable>
				<Address user={user} />
			</Accordion>
			<Accordion title={t('Passwords')} editable>
				<Passwords user={user} />
			</Accordion>
			<Accordion title={t('Units')}>
				<Units />
			</Accordion>
		</Grid>
	);
};
