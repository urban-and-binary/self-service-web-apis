import React from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { GhostIcon } from '@assets/icons';
import { useLocation } from 'react-router-dom';
import { Card } from '@components';
import Button from '@components/Button/Button';
import { Box } from '@mui/material';

const Container = styled(Box)(() => ({
	maxWidth: 700,
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
}));
const Text = styled('p')(({ theme }) => ({
	textAlign: 'center',
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
	flex: 1,
}));

export const LoginError = () => {
	const { t } = useTranslation();
	const location = useLocation();
	const state = location?.state;

	const text = state?.text || 'TryAgain';
	const to = state?.to || state?.from?.pathname || '/login';
	const cancelTo = state?.cancelTo || state?.from?.pathname || '/login';

	return (
		<Card>
			<Container
				sx={{
					minWidth: { xs: 300, sm: 500, md: 700 },
					padding: { xs: '110px 24px 60px 24px', sm: '110px 180px 60px 180px' },
				}}>
				<GhostIcon width="115" />
				<Text sx={{ mt: 3, mb: 12 }}>{t(state?.message || 'SomethingWrongTryAgain')}</Text>

				<Button to={to} fullwidth="true" sx={{ mt: 2 }}>
					{t(text)}
				</Button>
				<Button to={cancelTo} variant="outlined" fullwidth="true" sx={{ mt: 2 }}>
					{t('Cancel')}
				</Button>
			</Container>
		</Card>
	);
};
