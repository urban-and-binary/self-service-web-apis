import api from '@api/axios';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { passwordStrength, removeSpaces, showToast, removeJWT } from '@utils/utils';
import { Input, Dialog, VerificationCode, Spinner } from '@components';
import { LoginCard } from './components/LoginCard';
import { Switch } from './components/Switch';
import { Divider, Grid, Box } from '@mui/material';
import { GoogleIcon, FacebookIcon, AppleIcon, CheckedIcon } from '@assets/icons';
import Button from '@components/Button/Button';
import { ALERT } from '@constants/general';
import { LOGIN_TYPE } from '@common/consts';

const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	marginTop: theme.spacing(1),
	marginBottom: theme.spacing(3),
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const FormContainer = styled('div')(({ theme }) => ({
	marginTop: theme.spacing(4),
}));
const BottomText = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	p: {
		margin: 0,
	},
}));
const ButtonSignUp = styled(Link)(({ theme }) => ({
	textDecoration: 'none',
	textTransform: 'capitalize',
	marginLeft: theme.spacing(1),
	color: theme.palette.primary.blue003,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const ButtonAltLogin = styled('button')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 14,
	border: `1px solid ${theme.palette.base.inactive}`,
	backgroundColor: 'white',
	width: '100%',
	height: '48px',
	color: theme.palette.base.lightGray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
	svg: {
		width: 27,
		height: 27,
	},
}));
const DividerText = styled('p')(({ theme }) => ({
	margin: '32px 0',
	color: theme.palette.base.lightGray,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
}));
const LoginText = styled('div')(({ theme }) => ({
	width: '100%',
	marginTop: 10,
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'space-between',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
	a: {
		textDecoration: 'none',
		color: theme.palette.primary.blue002,
	},
}));
const RememberButton = styled('button')(({ theme }) => ({
	padding: '3px 3px 3px 0',
	margin: 0,
	display: 'flex',
	alignItems: 'center',
	border: 'none',
	background: 'none',
	cursor: 'pointer',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
	svg: {
		marginRight: 10,
	},
}));
const SignupText = styled('div')(({ theme }) => ({
	marginTop: 10,
	display: 'flex',
	flexWrap: 'wrap',
	alignItems: 'center',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
	cursor: 'pointer',
	p: {
		margin: 0,
		padding: '0 4px 0 10px',
	},
	a: {
		textDecoration: 'none',
		color: theme.palette.primary.blue002,
	},
}));

const initialValues = {
	email: '',
	password: '',
	phone: '',
	repeatPassword: '',
};

export const Login = ({ url }) => {
	const { t } = useTranslation();

	const navigate = useNavigate();
	const location = useLocation();
	const from = location.state?.from?.pathname || '/';

	const [loginType, setLoginType] = useState(LOGIN_TYPE.email);
	const [rememberMe, setRememberMe] = useState(false);
	const [acceptTerms, setAcceptTerms] = useState(false);
	const [open, setOpen] = useState(false);
	const [loading, setLoading] = useState(false);

	// validation schema object
	let loginSchema = {
		email: Yup.string()
			.email(t('InvalidEmailFormat'))
			.required(t('Required')),
		password: Yup.string()
			.required(t('PasswordRequired'))
			.test('weak-password', t('WeakPassword'), function(password) {
				return passwordStrength(password);
			}),
	};
	if (loginType === LOGIN_TYPE.phone) {
		loginSchema = {
			phone: Yup.string().required(t('Required')),
		};
	}
	const signupSchema = {
		email: Yup.string()
			.email(t('InvalidEmailFormat'))
			.required(t('Required')),
		password: Yup.string()
			.required(t('PasswordRequired'))
			.test('weak-password', t('WeakPassword'), function(password) {
				return passwordStrength(password);
			}),
		repeatPassword: Yup.string()
			.oneOf([Yup.ref('password'), null], t('PasswordsMustMatch'))
			.required(t('RepeatPasswordRequired')),
	};

	const schema = url === 'login' ? loginSchema : signupSchema;
	const validationSchema = Yup.object(schema);

	const sendVerificationCode = async values => {
		try {
			setLoading(true);
			await api.post('/auth/send-code', {
				loginType,
				phone: values.phone,
			});
			setLoading(false);
		} catch (error) {
			navigate('/error', { state: { from: location, message: error.response.data.message } });
		}
	};

	const login = async values => {
		try {
			// before the login, clear local or session storage
			removeJWT();

			const res = await api.post('/auth/login', {
				loginType,
				email: values.email,
				password: values.password,
				phone: values.phone,
				verificationCode: values.verificationCode,
			});

			if (rememberMe === true) {
				localStorage.setItem('jwt', res.data.token);
			} else {
				sessionStorage.setItem('jwt', res.data.token);
			}

			const to = values.verificationCode ? '/onboarding' : from;
			navigate(to, { replace: true, state: { email: values.email } });
		} catch (error) {
			const message = error.response?.data?.message;
			if (values.verificationCode) {
				showToast({
					title: t('WrongCode'),
					text: t('You entered the wrong code, try again'),
					type: ALERT.ERROR,
				});
			} else {
				if (message === 'UserNotFound') {
					navigate('/error', { state: { to: '/signup', cancelTo: '/login', text: 'CreateAccount', message } });
				} else {
					navigate('/error', { state: { to: '/login', cancelTo: '/login', message } });
				}
			}
		}
	};

	const signup = async values => {
		try {
			// before the signup, clear local or session storage
			removeJWT();

			setLoading(true);

			await api.post('/auth/signup', {
				email: values.email,
				password: values.password,
			});
			setLoading(false);
		} catch (error) {
			const message = error.response?.data?.message;
			if (message === 'UserAlreadyExists') {
				navigate('/error', { state: { to: '/login', cancelTo: '/signup', text: 'PleaseLogIn', message } });
			} else {
				navigate('/error', { state: { to: '/signup', cancelTo: '/signup', message } });
			}
		}
	};

	const handleSubmit = async values => {
		if (loginType === LOGIN_TYPE.phone) {
			sendVerificationCode(values);
			setOpen(true);
		} else {
			login(values);
		}
	};

	const handleCodeSubmit = (code, values) => {
		login({
			loginType,
			email: values.email,
			password: values.password,
			phone: values.phone,
			verificationCode: code,
		});
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleFormChange = ({ setErrors }) => {
		setLoginType(LOGIN_TYPE.email);
		setErrors({});
	};

	const onKeyDown = (e, values, errors) => {
		if (e.key === 'Enter' && url === 'login' && values.email && values.password && !errors.email && !errors.password) {
			login(values);
		}
	};

	const title = url === 'login' ? 'LogIn' : 'SignUp';

	return (
		<LoginCard>
			<Grid item xs={12} md={7} sx={{ pt: 4, pb: 4 }}>
				<Title>{t(title)}</Title>
				<Text>
					{t('PleaseChooseHow')} {t(title)}:
				</Text>
				{url === 'login' && <Switch onChange={setLoginType} />}
				<Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
					{({ values, setFieldValue, touched, errors, setErrors }) => (
						<Form>
							<FormContainer>
								{loginType === LOGIN_TYPE.email ? (
									<>
										<Field
											id="email"
											name="email"
											label={t('Email')}
											type="text"
											component={Input}
											placeholder={t('YourEmail')}
											InputLabelProps={{
												shrink: true,
												required: true,
											}}
											fullWidth
											variant="standard"
											errors={errors}
											touched={touched}
											value={values.email}
											onChange={e => setFieldValue('email', removeSpaces(e.target.value))}
											onKeyDown={e => onKeyDown(e, values, errors)}
										/>
										<Field
											id="password"
											name="password"
											label={t('Password')}
											type="password"
											component={Input}
											placeholder={t('Password')}
											InputLabelProps={{
												shrink: true,
												required: true,
											}}
											fullWidth
											variant="standard"
											sx={{ mt: 3 }}
											errors={errors}
											touched={touched}
											value={values.password}
											onKeyDown={e => onKeyDown(e, values, errors)}
										/>
										{url === 'signup' && (
											<Field
												id="repeatPassword"
												name="repeatPassword"
												label={t('RepeatPassword')}
												type="password"
												component={Input}
												placeholder={t('RepeatPassword')}
												InputLabelProps={{
													shrink: true,
													required: true,
												}}
												fullWidth
												variant="standard"
												errors={errors}
												touched={touched}
												value={values.repeatPassword}
												sx={{ mt: 3 }}
											/>
										)}
									</>
								) : (
									<>
										<Text>{t('PhoneToSendVerificationCode')}</Text>
										<Text />
										<Field
											id="phone"
											name="phone"
											label={t('Phone')}
											type="text"
											component={Input}
											placeholder="+370"
											InputLabelProps={{
												shrink: true,
											}}
											fullWidth
											variant="standard"
											errors={errors}
											touched={touched}
											value={values.phone}
										/>
									</>
								)}
							</FormContainer>
							{url === 'login' && (
								<LoginText>
									<RememberButton type="button" onClick={() => setRememberMe(!rememberMe)}>
										<CheckedIcon checked={rememberMe} />
										{t('RememberMe')}
									</RememberButton>
									{loginType === LOGIN_TYPE.email ? <Link to="/password-reset">{t('ForgotPassword')}?</Link> : ''}
								</LoginText>
							)}
							{url === 'signup' && (
								<SignupText onClick={() => setAcceptTerms(!acceptTerms)}>
									<CheckedIcon checked={acceptTerms} />
									<p>{t('CreatingAccountIAccept')}</p>
									<Link to="/terms-and-conditions">{t('CreatingAccountTermsConditions')}</Link>
								</SignupText>
							)}
							<Divider>
								<DividerText>{t('OrLogin')}</DividerText>
							</Divider>
							<Grid container spacing={2}>
								<Grid item xs={4}>
									<ButtonAltLogin disabled>
										<GoogleIcon disabled />
									</ButtonAltLogin>
								</Grid>
								<Grid item xs={4}>
									<ButtonAltLogin disabled>
										<FacebookIcon disabled />
									</ButtonAltLogin>
								</Grid>
								<Grid item xs={4}>
									<ButtonAltLogin disabled>
										<AppleIcon disabled />
									</ButtonAltLogin>
								</Grid>
								<Grid item xs={6}>
									<ButtonAltLogin disabled>{t('SmartID')}</ButtonAltLogin>
								</Grid>
								<Grid item xs={6}>
									<ButtonAltLogin disabled>{t('MobileSignature')}</ButtonAltLogin>
								</Grid>
							</Grid>

							{url === 'login' && (
								<Button
									type="submit"
									fullwidth="true"
									sx={{ mt: 4, mb: 3 }}
									disabled={
										loginType === LOGIN_TYPE.email
											? !values.email || !values.password || errors.email || errors.password
											: !values.phone
									}>
									{t(title)}
								</Button>
							)}
							{url === 'signup' && (
								<Button
									type="button"
									fullwidth="true"
									onClick={() => {
										signup(values);
										setOpen(true);
									}}
									sx={{ mt: 4, mb: 3 }}
									disabled={
										!acceptTerms ||
										!values.email ||
										!values.password ||
										errors.email ||
										errors.password ||
										errors.repeatPassword
									}>
									{t(title)}
								</Button>
							)}
							<Dialog open={open} onClose={handleClose} container>
								{loading ? (
									<Box sx={{ width: 100 }} margin={{ xs: 14, sm: 20, m: 25 }}>
										<Spinner size={100} />
									</Box>
								) : (
									<>
										<VerificationCode
											onSubmit={code => handleCodeSubmit(code, values)}
											onResend={() => {
												loginType === LOGIN_TYPE.email ? signup(values) : sendVerificationCode(values);
											}}
											onClose={handleClose}
											title={t('UserVerification')}
											text={t('EnterDigitCodeSent')}
											contact={loginType === LOGIN_TYPE.email ? values.email : values.phone}
											clearAfter
											length={6}
											sx={{ p: 6, pb: 8 }}
											paddingLeft={{ xs: 3, sm: 14 }}
											paddingRight={{ xs: 3, sm: 14 }}
										/>
									</>
								)}
							</Dialog>
							{url === 'login' ? (
								<BottomText>
									<Text>{t('DontHaveAccount')}</Text>
									<ButtonSignUp to="/signup" onClick={() => handleFormChange({ setErrors })}>
										{t('SignUp')}
									</ButtonSignUp>
								</BottomText>
							) : (
								<BottomText>
									<Text>{t('AlreadyHaveAccount')}</Text>
									<ButtonSignUp to="/login" onClick={() => handleFormChange({ setErrors })}>
										{t('LogIn')}
									</ButtonSignUp>
								</BottomText>
							)}
						</Form>
					)}
				</Formik>
			</Grid>
		</LoginCard>
	);
};

Login.propTypes = {
	url: PropTypes.string.isRequired,
};
