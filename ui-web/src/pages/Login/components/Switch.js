import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';

const Container = styled('div')(({ theme }) => ({
	display: 'flex',
	position: 'relative',
	transition: '2s',
	width: '100%',
	height: '40px',
	borderRadius: 100,
	backgroundColor: theme.palette.base.background,
}));
const ActiveButton = styled('button')(({ theme, position }) => ({
	position: 'absolute',
	left: position === 'start' ? 0 : '',
	right: position === 'end' ? 0 : '',
	// transition: '2s',
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	width: '50%',
	height: '40px',
	borderRadius: 100,
	color: theme.palette.base.white,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
	backgroundColor: theme.palette.primary.blue002,
	boxShadow: theme.shadow.s03,
	zIndex: 1,
	border: 'none',
}));
const PassiveButton = styled('div')(() => ({
	width: '100%',
	height: '40px',
	button: {
		width: '50%',
		height: '40px',
		border: 'none',
		background: 'transparent',
		cursor: 'pointer',
		textAlign: 'center',
	},
}));

export const Switch = ({ onChange }) => {
	const { t } = useTranslation();

	const [position, setPosition] = useState('start');
	const [active, setActive] = useState('email');

	const handleClick = value => {
		if (value === 'email') {
			setPosition('start');
			setActive(value);
			onChange(value);
		} else {
			setPosition('end');
			setActive(value);
			onChange(value);
		}
	};

	return (
		<>
			<Container position={position}>
				<ActiveButton position={position}>{t(active === 'email' ? 'Email' : 'Phone')}</ActiveButton>
				<PassiveButton>
					<button onClick={() => handleClick('email')}>{t('Email')}</button>
					<button onClick={() => handleClick('phone')}>{t('Phone')}</button>
				</PassiveButton>
			</Container>
		</>
	);
};

Switch.propTypes = {
	onChange: PropTypes.func,
};
