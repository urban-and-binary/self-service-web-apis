import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { List, Grid, Box, useMediaQuery } from '@mui/material';
import { LogoWhiteIcon, PhoneIcon, AppStoreLogo, GooglePlayLogo } from '@assets/icons';
import { LanguageSelect } from '@components';

const Container = styled(Box)(({ theme }) => ({
	backgroundColor: theme.palette.base.white,
	boxShadow: theme.shadow.s01,
	borderRadius: '30px',
	padding: '30px 30px 30px 38px',
}));
const ImageBox = styled(Grid)(({ theme }) => ({
	padding: '40px',
	borderRadius: '30px',
	boxShadow: theme.shadow.s01,
	background: 'linear-gradient(171.61deg, #0B1B3C -12.32%, #496DB3 63.34%, #889EC9 95.65%)',
}));
const Info = styled(Grid)(({ theme }) => ({
	color: theme.palette.base.white,
	display: 'flex',
	flexDirection: 'column',
	justifyContent: 'space-between',
	padding: '25px 0 25px 0',
	paddingTop: '17%',
}));
const ListItemWrapper = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
}));
const ListItem = styled('li')(({ theme }) => ({
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
	listStyleType: 'none',
}));
const BulletBox = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
}));
const Bullet = styled('span')(() => ({
	position: 'absolute',
	width: '10px',
	height: '10px',
	borderRadius: '50%',
	backgroundColor: 'white',
}));
const StyledText = styled('p')(({ theme }) => ({
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const StoreLogo = styled('img')(() => ({
	width: '108px',
	height: '32px',
}));

export const LoginCard = ({ children }) => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

	const list = ['PayBillsOnePlace', 'BuildingNewsNotifications', 'VoteAnywhere', 'SendRequest'];

	return (
		<Container margin={{ xs: '50px 16px 50px 16px', sm: '50px 50px 50px 60px' }}>
			<Grid display="flex" justifyContent="flex-end" mb={1}>
				<LanguageSelect />
			</Grid>
			<Grid container>
				<ImageBox item xs={12} md={6}>
					<LogoWhiteIcon width="143" />
					<Grid container>
						{!isMobile && (
							<Grid item xs={12} sm={6}>
								<PhoneIcon width="100%" />
							</Grid>
						)}
						<Info item xs={12} sm={6}>
							{!isMobile && (
								<List>
									{list.map(item => (
										<ListItemWrapper key={item} sx={{ mb: 2 }}>
											<BulletBox>
												<Bullet />
											</BulletBox>
											<ListItem sx={{ pl: 3 }}>{t(item)}</ListItem>
										</ListItemWrapper>
									))}
								</List>
							)}
							<div>
								<StyledText sx={{ mb: 2 }}>{t('DowanloadApp')}</StyledText>
								<Link to="/">
									<StoreLogo src={AppStoreLogo} alt="App store" sx={{ mr: 2 }} />
								</Link>
								<Link to="/">
									<StoreLogo src={GooglePlayLogo} alt="Google play" />
								</Link>
							</div>
						</Info>
					</Grid>
				</ImageBox>
				<Grid container item justifyContent="center" xs={12} md={6}>
					{children}
				</Grid>
			</Grid>
		</Container>
	);
};

LoginCard.propTypes = {
	children: PropTypes.node,
};
