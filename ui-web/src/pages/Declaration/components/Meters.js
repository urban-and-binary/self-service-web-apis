import api from '@api/axios';
import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { ALERT, REGEX } from '@constants/general';
import { Grid, useMediaQuery, TableCell, TableRow } from '@mui/material';
import { Paper, MeterInput } from '@components';
import { BasicTable } from './Table';
import { MobileTable } from './MobileTable';
import { meterType } from './utils';
import Button from '@components/Button/Button';
import { showToast } from '@utils/utils';

const Header = styled('div')(() => ({
	display: 'flex',
	flexWrap: 'wrap',
	alignItems: 'center',
	justifyContent: 'space-between',
}));
const Center = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const MeterId = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
// const Input = styled('input')(({ theme }) => ({
// 	height: 56,
// 	maxWidth: 270,
// 	borderRadius: theme.spacing(2),
// 	border: `2px solid ${theme.palette.primary.blue004}`,
// 	color: theme.palette.base.dark,
// 	fontSize: theme.typography.fontSize.base,
// 	fontWeight: theme.typography.fontWeight.r,
// 	'&:focus': {
// 		outline: 'none !important',
// 	},
// 	caretColor: theme.palette.primary.blue003,
// 	padding: '0 16px 0 16px',
// }));

const Type = styled('p')(({ theme }) => ({
	margin: 0,
	fontWeight: theme.typography.fontWeight.bold,
}));
const HeadCell = styled(TableCell)(({ theme }) => ({
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));

const AttentionLabel = styled('span')(({ theme }) => ({
	color: theme.palette.default.lightRed,
}));

export const Meters = ({ data, onDeclare }) => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	const [declaration, setDeclaration] = useState({});
	const [loading, setLoading] = useState(false);

	const saveMeters = async () => {
		const meters = [];

		Object.entries(declaration).forEach(([key, value]) => {
			if (value) {
				meters.push({ id: key, valueTo: value });
			}
		});

		setLoading(true);
		try {
			await api.post('/meters/reading', { meters });
			setLoading(false);
			onDeclare();
		} catch (error) {
			setLoading(false);
			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryLoginAgain'),
				type: ALERT.ERROR,
			});
		}
	};

	const handleDeclaration = e => {
		const { name, value } = e.target;
		if (value !== '' && !RegExp(REGEX.NUMBER).test(value)) {
			e.preventDefault();
		} else {
			setDeclaration({ ...declaration, [name]: value });
		}
	};

	//  TODO: uncomment when meter description ready on api
	// const [edit, setEdit] = useState(false);
	// const [description, setDescription] = useState('');

	// const handleDescription = e => {
	// 	setDescription(e.target.value);
	// };
	// const handleEdit = item => {
	// 	item.description && setDescription(item.description);
	// 	setEdit(item.id);
	// };

	// const handleSubmit = () => {
	// 	setEdit(false);
	// 	setDescription('');
	// };

	const thead = [t('Type'), t('QuantityFrom'), t('QuantityTo'), t('Difference'), t('Tariff')];

	const row = (id, type, from, to, difference, tariff, customerDeclared) => (
		<>
			<TableCell sx={{ width: '20%' }}>
				<Type>{t(meterType(type).title)}</Type>
			</TableCell>
			<TableCell sx={{ width: '25%' }}>
				<Center>
					{customerDeclared ? to : from} {meterType(type).symbol}
				</Center>
			</TableCell>
			<TableCell sx={{ width: '25%' }}>
				<Center>
					{customerDeclared ? (
						<MeterInput
							id={id}
							value={declaration[id] || ''}
							onChange={handleDeclaration}
							placeholder={t('To')}
							symbol={meterType(type).symbol}
							sx={{ width: 160 }}
						/>
					) : (
						<>
							{to} {meterType(type).symbol}
						</>
					)}
				</Center>
			</TableCell>
			<TableCell sx={{ width: '15%' }}>
				{difference} {meterType(type).symbol}
			</TableCell>
			<TableCell sx={{ width: '15%' }}>
				{tariff} €/{meterType(type).symbol},
			</TableCell>
		</>
	);

	const rowsMobile = (id, type, from, to, difference, customerDeclared) => (
		<>
			<TableRow>
				<HeadCell>{t('Type')}</HeadCell>
				<TableCell align="right">
					<Type>{t(meterType(type).title)}</Type>
				</TableCell>
			</TableRow>
			<TableRow>
				<HeadCell>{t('QuantityFrom')}</HeadCell>
				<TableCell align="right">
					{customerDeclared ? to : from} {meterType(type).symbol}
				</TableCell>
			</TableRow>
			<TableRow>
				<HeadCell>{t('QuantityTo')}</HeadCell>
				<TableCell align="right" sx={{ display: 'flex', mr: '2px' }}>
					{customerDeclared ? (
						<MeterInput
							id={id}
							value={declaration[id] || ''}
							onChange={handleDeclaration}
							placeholder={t('To')}
							symbol={meterType(type).symbol}
							sx={{ width: 160 }}
						/>
					) : (
						<>
							{to} {meterType(type).symbol}
						</>
					)}
				</TableCell>
			</TableRow>
			<TableRow>
				<HeadCell>{t('Difference')}</HeadCell>
				<TableCell align="right">
					{difference} {meterType(type).symbol}
				</TableCell>
			</TableRow>
		</>
	);

	const generateRows = (item, mobile) => {
		const rowType = (id, type, from, to, difference, tariff, customerDeclared) => {
			let diff;

			if (customerDeclared) {
				diff = Number(declaration[id]) > to ? declaration[id] - to : '-';
			} else {
				diff = difference;
			}

			return mobile
				? rowsMobile(id, type, from, to, diff, customerDeclared)
				: row(id, type, from, to, diff, tariff, customerDeclared);
		};
		return [rowType(item.id, item.localType, item.valueFrom, item.valueTo, item.difference, 0.168, item.customerDeclared)];
	};

	const isMetersToDeclare = useMemo(() => data?.meters?.some(met => met.customerDeclared), [data?.meters]);

	return (
		<>
			{data?.meters.map((item, idx) => (
				<Paper key={idx} shadow="s02" sx={{ mb: 3, p: 3 }}>
					<Header>
						<Center sx={{ flexWrap: 'wrap' }}>
							{meterType(item.localType).icon}
							<Title>{t(meterType(item.localType).title)}</Title>
							{/* {edit === item.id ? (
								<Input id="id" name="name" value={description} onChange={handleDescription} type="text" sx={{ ml: 2 }} />
							) : (
								item?.description && <Title>: {item?.description}</Title>
							)} */}
							<MeterId sx={{ ml: 2 }}>
								( {item.name} ) {!item.customerDeclared && <AttentionLabel>( {t('DeclaredByAdmin')} )</AttentionLabel>}
							</MeterId>
						</Center>
						{/* {edit === item.id ? (
							<Button variant="outlined" onClick={handleSubmit} sx={{ width: 226, height: 48 }}>
								{t('SaveChanges')}
							</Button>
						) : (
							<EditButton onClick={() => handleEdit(item)}>{t('Edit')}</EditButton>
						)} */}
					</Header>
					{isTablet ? (
						<MobileTable trows={generateRows(item, 'mobile')} sx={{ mt: 4 }} />
					) : (
						<BasicTable
							thead={thead}
							trows={generateRows(item)}
							sx={{ mt: 4, 'td, th': { border: 'none', padding: '8px 5px 8px 5px' } }}
						/>
					)}
				</Paper>
			))}
			<Grid item xs={12} display="flex" justifyContent="flex-end">
				<Button sx={{ mt: 1, minWidth: '220px' }} onClick={saveMeters} disabled={loading || !isMetersToDeclare}>
					{t('Declare')}
				</Button>
			</Grid>
		</>
	);
};

Meters.propTypes = {
	data: PropTypes.object,
	onDeclare: PropTypes.func,
};
