import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { useMediaQuery, Stack, TableCell } from '@mui/material';
import { Paper } from '@components';
import { meterType } from './utils';
import { BasicTable } from './Table';

const Center = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
}));
const Bold = styled('p')(({ theme }) => ({
	margin: 0,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Gray = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const UnitTitle = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const UnitId = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));

export const Units = ({ data }) => {
	const theme = useTheme();
	const { t } = useTranslation();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	const row = ({ date, localType, valueFrom, valueTo, measurementUnits, difference }) => {
		return (
			<>
				{!isTablet && (
					<TableCell sx={{ width: '20%', minWidth: '113px' }}>
						<Bold>{date && moment(date).format('YYYY - MM - DD')}</Bold>
					</TableCell>
				)}
				<TableCell sx={{ width: '20%' }}>
					<Center>
						{meterType(localType).icon}
						{t(meterType(localType).title)}
					</Center>
				</TableCell>
				<TableCell sx={{ width: '20%' }}>
					{valueFrom} {measurementUnits}
				</TableCell>
				<TableCell sx={{ width: '20%' }}>
					{valueTo} {measurementUnits}
				</TableCell>
				{!isTablet && (
					<TableCell sx={{ width: '20%' }}>
						{difference} {measurementUnits}
					</TableCell>
				)}
			</>
		);
	};

	const thead = isTablet
		? [t('Type'), t('QuantityFrom'), t('QuantityTo')]
		: [t('Date'), t('Type'), t('QuantityFrom'), t('QuantityTo'), t('Difference')];
	const generateRows = unit => unit?.meters.map((item, idx) => row({ date: idx === 0 && unit.lastReading, ...item }));

	return (
		<>
			{data.map(unit => (
				<Paper key={unit._id} shadow="s02" sx={{ mb: 3, p: 3 }}>
					<Center sx={{ flexWrap: 'wrap' }}>
						<UnitTitle sx={{ mr: 2 }}>{unit.label}</UnitTitle>
						<UnitId>( {unit.customerCode} )</UnitId>
					</Center>
					{isTablet && unit?.meters && (
						<Stack spacing={1} direction="row" alignItems="center" sx={{ mt: 2 }}>
							<Gray>{t('Date')}:</Gray>
							<Bold>{unit?.meters[0]?.lastReading && moment(unit?.meters[0]?.lastReading).format('YYYY - MM - DD')}</Bold>
						</Stack>
					)}
					{unit?.meters ? <BasicTable sx={{ mt: 4 }} theadWidth="20%" thead={thead} trows={generateRows(unit)} /> : t('NoData')}
				</Paper>
			))}
		</>
	);
};

Units.propTypes = {
	data: PropTypes.array,
};
