import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Table, TableBody, TableContainer, TableRow } from '@mui/material';

const Body = styled(TableBody)(() => ({
	tr: {
		td: {
			padding: '16px 0 16px 0',
			border: 'none',
		},
	},
}));
const Divider = styled(TableRow)(({ theme }) => ({
	borderBottom: `1px solid ${theme.palette.base.inactive}`,
}));

export const MobileTable = ({ trows, ...props }) => (
	<TableContainer {...props}>
		<Table>
			{trows.map((row, idx) => (
				<Body key={idx}>
					{row}
					{trows?.length > idx + 1 && <Divider />}
				</Body>
			))}
		</Table>
	</TableContainer>
);

MobileTable.propTypes = {
	trows: PropTypes.array,
};
