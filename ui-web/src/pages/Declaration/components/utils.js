import React from 'react';
import { UNIT_METER_TYPES } from '@common/consts';
import { LightningIcon, ThermoColdIcon, ThermoHotIcon, GasIcon } from '@assets/icons';

export const meterType = value => {
	return {
		[UNIT_METER_TYPES.electricity]: { icon: <LightningIcon sx={{ fontSize: 24, mr: 1 }} />, title: 'Electricity', symbol: 'kWh' },
		[UNIT_METER_TYPES.heating]: { icon: <GasIcon sx={{ fontSize: 24, mr: 1 }} />, title: 'Gas', symbol: 'm3' },
		[UNIT_METER_TYPES.coldWater]: { icon: <ThermoColdIcon sx={{ fontSize: 24, mr: 1 }} />, title: 'ColdWater', symbol: 'm3' },
		[UNIT_METER_TYPES.hotWater]: { icon: <ThermoHotIcon sx={{ fontSize: 24, mr: 1 }} />, title: 'HotWater', symbol: 'm3' },
	}[value];
};
