import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';

const HeadCell = styled(TableCell)(({ theme }) => ({
	padding: '0 0 16px 0',
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Row = styled(TableRow)(({ theme }) => ({
	td: {
		padding: '24px 0 24px 0',
		color: theme.palette.base.dark,
		fontSize: theme.typography.fontSize.base,
		fontWeight: theme.typography.fontWeight.r,
	},
	'&:last-child td, &:last-child th': { border: 0 },
}));

export const BasicTable = ({ thead, trows, theadWidth, ...props }) => (
	<TableContainer {...props}>
		<Table>
			<TableHead>
				<TableRow sx={{ th: { border: 0 } }}>
					{thead.map((item, idx) => (
						<HeadCell sx={{ width: theadWidth }} key={idx}>
							{item}
						</HeadCell>
					))}
				</TableRow>
			</TableHead>
			<TableBody>
				{trows.map((row, idx) => (
					<Row key={idx}>{row}</Row>
				))}
			</TableBody>
		</Table>
	</TableContainer>
);

BasicTable.propTypes = {
	thead: PropTypes.array,
	trows: PropTypes.array,
	theadWidth: PropTypes.string,
};
