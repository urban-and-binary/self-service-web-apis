import api from '@api/axios';
import React, { useState, useEffect } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid, useMediaQuery } from '@mui/material';
import { Dropdown, Spinner, Dialog, SuccessMessage } from '@components';
import { ChevronIcon } from '@assets/icons';
import { Meters } from './components/Meters';
import { Units } from './components/Units';
import Button from '@components/Button/Button';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';
import { useTheme } from '@mui/styles';
import { sortBy } from 'lodash';

const BackButton = styled('button')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	border: 'none',
	background: 'none',
	cursor: 'pointer',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Icon = styled(ChevronIcon)(() => ({
	transform: 'rotate(90deg)',
	marginRight: 20,
	width: '14px',
	height: '8px',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));

export const Declaration = () => {
	const { t } = useTranslation();
	const defaultSelect = { label: t('AllUnits'), value: 0 };
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

	const [success, setSuccess] = useState(false);
	const [loading, setLoading] = useState(true);
	const [declare, setDeclare] = useState(false);
	const [units, setUnits] = useState([]);
	const [unit, setUnit] = useState(defaultSelect);

	const getUnits = async () => {
		try {
			const res = await api.get('/owned-unit/meters');

			setUnits(
				res.data.map(resUnit => ({
					...resUnit,
					meters: sortBy(resUnit.meters, ['type', 'name']),
				})),
			);
			setLoading(false);
		} catch (error) {
			showToast({
				title: t('OOPS!'),
				text: t('SomethingWrongTryReloadLogin'),
				type: ALERT.ERROR,
			});
		}
	};

	useEffect(() => {
		getUnits();
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const handleUnitSelect = (e, option) => {
		setUnit(option);
	};

	const handleBack = () => {
		setDeclare(false);
	};

	const handleDeclare = () => {
		setDeclare(false);
		setSuccess(true);
	};

	const selectedUnit = unit?.value === 0 ? units : units.filter(item => item._id === unit._id);
	const page = declare ? 2 : 1;

	return (
		<Grid container pl={3} pr={3}>
			{page === 1 && (
				<>
					<Grid container item xs={12}>
						<Grid item xs={12}>
							<Title>{t('MetersDeclaration')}</Title>
						</Grid>
						<Grid container item xs={12} sx={{ mt: 1 }} spacing={2}>
							<Grid item xs={12} sm={6}>
								<Dropdown options={units} value={unit} onChange={handleUnitSelect} />
							</Grid>
							<Grid item xs={12} sm={6} display="flex" justifyContent={isMobile ? 'center' : 'flex-end'} alignItems="center">
								<Button
									sx={{ mt: 'auto', mb: 'auto', minWidth: '220px' }}
									onClick={() => setDeclare(true)}
									disabled={unit?.value === 0 || !unit.meters?.length}>
									{t('Declare')}
								</Button>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} mt={3}>
						{loading ? (
							<Grid container justifyContent="center" mt={10}>
								<Spinner />
							</Grid>
						) : (
							<Units data={selectedUnit} />
						)}
					</Grid>
				</>
			)}
			{page === 2 && (
				<>
					<Grid item xs={12} sm={5}>
						<Grid item xs={12}>
							<BackButton onClick={handleBack}>
								<Icon /> {t('MetersDeclaration')}
							</BackButton>
						</Grid>

						<Dropdown sx={{ mt: 3 }} options={units} value={unit} onChange={handleUnitSelect} />
					</Grid>
					<Grid item xs={12} mt={3}>
						<Meters data={selectedUnit[0]} onDeclare={handleDeclare} />
					</Grid>
				</>
			)}
			<Dialog open={success} onClose={() => setSuccess(false)} showClose>
				<SuccessMessage text={`${t('ThankYou')}!`} sx={{ mt: -10 }}>
					{t('MetersDeclarationSuccessful')}
				</SuccessMessage>
			</Dialog>
		</Grid>
	);
};
