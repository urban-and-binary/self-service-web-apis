import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import qs from 'query-string';
import { ListDetailPage } from '@components';
import api from '@api/axios';
import { ANNOUNCEMENT_QUERY_TYPE_VALUES, OWNED_UNIT_STATUSES } from '@common/consts';
import { useNotification } from '@hooks/useNotification';
import { useLocation } from 'react-router-dom';

const PAGE_SIZE = 5;

export const Announcements = () => {
	const { loadNotifs, notifs } = useNotification();
	const { t } = useTranslation();

	const annQuery = useRef({
		type: ANNOUNCEMENT_QUERY_TYPE_VALUES.all,
		ownedUnit: 'all',
	});
	const mounted = useRef(false);

	const [stOrder, setStOrder] = useState(-1);
	const [listLoading, setListLoading] = useState(false);

	const [ownedUnits, setOwnedUnits] = useState([
		{
			title: t('AllUnits'),
			value: 'all',
		},
	]);

	const [announcements, setAnnouncements] = useState([]);
	const [annTotal, setAnnTotal] = useState(0);
	const [annPage, setAnnPage] = useState(1);
	const location = useLocation();

	const onAnnouncementSelect = async ann => {
		if (ann && !ann.read) {
			const newAnns = [...announcements];
			const annInd = newAnns.findIndex(newAnn => newAnn._id === ann._id);

			if (annInd !== -1) {
				newAnns[annInd].read = true;
				setAnnouncements(newAnns);
			}

			api.post(`/announcements/${ann._id}/read`)
				.then(() => {
					loadNotifs();
				})
				.catch(err => console.error('Announcement read error', err));
		}
	};

	const onPageChange = (e, page) => {
		loadAnnouncements({ page });
		setAnnPage(page);
	};

	const filterSelect = query => {
		annQuery.current = {
			...annQuery.current,
			...query,
		};

		// so if some filter values change we want to reset pagination
		// do know this should not apply for sorting
		setAnnPage(1);

		loadAnnouncements({ query: annQuery.current, page: 1 });
	};

	const onSort = field => {
		if (field === 'createdAt') {
			const newSort = stOrder === 1 ? -1 : 1;
			setStOrder(newSort);
			loadAnnouncements({ sortOrder: newSort });
		}
	};

	const loadOwnedUnits = async () => {
		try {
			const res = await api.get(`/owned-unit?status=${OWNED_UNIT_STATUSES.approved}`);

			setOwnedUnits([
				{
					title: t('AllUnits'),
					value: 'all',
				},
				...res.data.map(ownedUnit => ({
					title: ownedUnit.label,
					name: ownedUnit.label,
					value: ownedUnit._id,
				})),
			]);
		} catch (error) {
			console.error(error);
		}
	};

	const loadAnnouncements = async ({
		query = annQuery.current,
		page = annPage,
		pageSize = PAGE_SIZE,
		sortField = 'createdAt',
		sortOrder = stOrder,
	}) => {
		try {
			mounted.current && setListLoading(true);
			const queryString = qs.stringify({ ...query, page, pageSize, sortField, sortOrder });

			const res = await api.get(`/announcements?${queryString}`);

			if (mounted.current) {
				setAnnouncements(
					res.data.data.map(ann => ({
						title: ann.title,
						createdAt: moment(ann.createdAt).format('YYYY - MM - DD'),
						unitLabel: ann.unitLabel,
						description: ann.description,
						_id: ann._id,
						read: ann.read,
					})),
				);
				setAnnTotal(res.data.total);
			}
		} catch (error) {
			console.error(error);
		} finally {
			mounted.current && setListLoading(false);
		}
	};

	useEffect(() => {
		// so since notifications can be "read"
		// while announcements page is opened
		// we'll want to refresh the data when this happen
		// to show updated announcements
		if (mounted.current) {
			loadAnnouncements({});
		}
	}, [notifs]); // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		mounted.current = true;

		loadOwnedUnits();
		loadAnnouncements({});

		return () => {
			mounted.current = false;
		};
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const selectors = useMemo(
		() => [
			{
				title: 'All',
				action: () => filterSelect({ type: ANNOUNCEMENT_QUERY_TYPE_VALUES.all }),
			},
			{
				title: 'Private',
				action: () => filterSelect({ type: ANNOUNCEMENT_QUERY_TYPE_VALUES.private }),
			},
			{
				title: 'Common',
				action: () => filterSelect({ type: ANNOUNCEMENT_QUERY_TYPE_VALUES.common }),
			},
		],
		[], // eslint-disable-line react-hooks/exhaustive-deps
	);

	const tableHeadCells = useMemo(
		() => [
			{
				name: 'AnnouncementTitle',
				key: 'title',
				width: '35%',
			},
			{
				name: 'AnnouncementDate',
				key: 'createdAt',
				width: '30%',
				sorting: stOrder,
			},
			{
				name: 'AnnouncementUnit',
				key: 'unitLabel',
				width: '35%',
			},
		],
		[stOrder],
	);

	return (
		<ListDetailPage
			title="Announcements"
			selectMenuItems={ownedUnits}
			onItemSelect={e => filterSelect({ ownedUnit: e.target.value })}
			selectors={selectors}
			tableHeadCells={tableHeadCells}
			data={announcements}
			listCardMap={listCardMap}
			detailCardMap={detailCardMap}
			onSort={onSort}
			onPageChange={onPageChange}
			pageCount={Math.ceil(annTotal / PAGE_SIZE)}
			listLoading={listLoading}
			page={annPage}
			onDataItemSelect={onAnnouncementSelect}
			defSelItem={location?.state?.selItem}
			markUnread
		/>
	);
};

const listCardMap = {
	titleKey: 'title',
	content: [
		{
			dataKey: 'createdAt',
		},
		{
			dataKey: 'unitLabel',
		},
	],
};

const detailCardMap = {
	titleKey: 'title',
	contentHeaderKey: 'unitLabel',
	content: [
		{
			dataKey: 'createdAt',
			label: 'Date',
		},
		{
			dataKey: 'description',
			label: 'Announcement',
			innerHtml: true,
		},
	],
};
