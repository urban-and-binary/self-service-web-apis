import api from '@api/axios';
import React from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { useNavigate, useLocation } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { passwordStrength } from '@utils/utils';
import { InputAdornment, IconButton, Box } from '@mui/material';
import { Input, Card, PasswordValidator } from '@components';
import Button from '@components/Button/Button';
import { EyeIcon, EyeCloseIcon } from '@assets/icons';

const Container = styled(Box)(({ theme }) => ({
	maxWidth: 700,
	color: theme.palette.base.dark,
	textAlign: 'center',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: '24px 0 32px 0',
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const StyledIconButton = styled(IconButton)(({ theme }) => ({
	color: theme.palette.base.gray,
}));

const initialValues = {
	password: '',
	repeatPassword: '',
	showPassword: false,
	showRepeatPassword: false,
};

export const PasswordChange = () => {
	const { t } = useTranslation();
	const navigate = useNavigate();
	const location = useLocation();

	const schema = Yup.object({
		password: Yup.string()
			.required(t('PasswordRequired'))
			.test('weak-password', t('WeakPassword'), function(password) {
				return passwordStrength(password);
			}),
		repeatPassword: Yup.string()
			.required(t('ConfirmPasswordRequired'))
			.oneOf([Yup.ref('password'), null], ''),
	});

	const handleMouseDownPassword = event => {
		event.preventDefault();
	};

	const onSubmit = async values => {
		const { email, phone, verificationCode } = location?.state;
		try {
			await api.post('auth/password-change', {
				email,
				phone,
				verificationCode,
				password: values.password,
				password1: values.repeatPassword,
			});
			navigate('/login');
		} catch (error) {
			navigate('/error', { state: { to: '/password-reset', cancelTo: '/login' } });
		}
	};

	const disabled = ({ values, errors }) =>
		!values.password || !values.repeatPassword || errors.password || errors.repeatPassword || values.password !== values.repeatPassword;

	return (
		<Card>
			<Container
				sx={{
					padding: { xs: '40px 24px 60px 24px', sm: '40px 150px 60px 150px' },
				}}>
				<Title>{t('ChangePassword')}</Title>
				<Subtitle>{t('ChangePasswordInfo')}</Subtitle>
				<Formik initialValues={initialValues} validationSchema={schema} onSubmit={onSubmit}>
					{({ values, touched, errors, setFieldValue }) => (
						<Form>
							<Field
								id="password"
								name="password"
								label={t('NewPassword')}
								type={values.showPassword ? 'text' : 'password'}
								component={Input}
								placeholder={t('NewPassword')}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.password}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								InputProps={{
									endAdornment: (
										<InputAdornment position="start">
											<StyledIconButton
												aria-label="toggle password visibility"
												onClick={() => setFieldValue('showPassword', !values.showPassword)}
												onMouseDown={handleMouseDownPassword}
												edge="start">
												{values.showPassword ? (
													<EyeCloseIcon sx={{ fontSize: 16 }} />
												) : (
													<EyeIcon sx={{ fontSize: 16 }} />
												)}
											</StyledIconButton>
										</InputAdornment>
									),
								}}
							/>

							<Field
								id="repeatPassword"
								name="repeatPassword"
								label={t('ConfirmPassword')}
								type={values.showRepeatPassword ? 'text' : 'password'}
								component={Input}
								placeholder={t('ConfirmPassword')}
								fullWidth
								variant="standard"
								errors={errors}
								touched={touched}
								value={values.repeatPassword}
								sx={{ mt: 3 }}
								InputLabelProps={{
									shrink: true,
									required: true,
								}}
								InputProps={{
									endAdornment: (
										<InputAdornment position="start">
											<StyledIconButton
												aria-label="toggle password visibility"
												onClick={() => setFieldValue('showRepeatPassword', !values.showRepeatPassword)}
												onMouseDown={handleMouseDownPassword}
												edge="start">
												{values.showRepeatPassword ? (
													<EyeCloseIcon sx={{ fontSize: 16 }} />
												) : (
													<EyeIcon sx={{ fontSize: 16 }} />
												)}
											</StyledIconButton>
										</InputAdornment>
									),
								}}
							/>
							<PasswordValidator password={values.password} repeatPassword={values.repeatPassword} sx={{ mt: 2 }} />
							<Button type="submit" disabled={disabled({ values, errors })} fullwidth="true" sx={{ mt: 7 }}>
								{t('ChangePassword')}
							</Button>
						</Form>
					)}
				</Formik>
			</Container>
		</Card>
	);
};
