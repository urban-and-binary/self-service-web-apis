import api from '@api/axios';
import React, { useState } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { removeJWT } from '@utils/utils';
import { Card, Input, VerificationCode } from '@components';
import Button from '@components/Button/Button';
import { LanguageSelect } from '@components';
import { Box, Grid } from '@mui/material';

const Container = styled(Box)(({ theme }) => ({
	textAlign: 'center',
	color: theme.palette.base.dark,
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: '24px 0 32px 0',
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const Text = styled('p')(({ theme }) => ({
	margin: '16px 0',
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.m,
}));

const initialValues = {
	email: '',
	phone: '',
};

export const PasswordReset = () => {
	const { t } = useTranslation();
	const navigate = useNavigate();
	const validationSchema = Yup.object({
		email: Yup.string()
			.email(t('InvalidEmailFormat'))
			.nullable(),
		phone: Yup.number()
			.nullable()
			.typeError(t('ContactPhoneInvalid')),
	});

	const [verifyCode, setVerifyCode] = useState(false);
	const [contact, setContact] = useState({});

	const onSubmit = async values => {
		const { email, phone } = values;

		try {
			// before the reset, clear local or session storage
			removeJWT();

			const body = email ? { email } : { phone };
			await api.post('/auth/password-forgot', body);
			setContact(body);
			setVerifyCode(true);
		} catch (error) {
			const message = error.response?.data?.message;
			navigate('/error', { state: { to: '/password-reset', cancelTo: '/login', message } });
		}
	};

	const handleCodeSubmit = async code => {
		const body = { ...contact, verificationCode: code };

		try {
			await api.post('/auth/verify-code', body);
			navigate('/password-change', { state: body });
		} catch (error) {
			navigate('/error', { state: { to: '/password-reset', cancelTo: '/login' } });
		}
	};
	const handleResend = async () => {
		onSubmit(contact);
	};
	const handleClose = () => {
		navigate('/login');
	};

	const PasswordReset = () => (
		<Container padding={{ xs: '40px 24px 60px 24px', sm: '40px 150px 60px 150px' }}>
			<Grid display="flex" justifyContent="flex-end" mb={1}>
				<LanguageSelect />
			</Grid>
			<Title>{t('ForgotPassword')}</Title>
			<Subtitle>{t('EmailToSendVerificationCode')}</Subtitle>
			<Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
				{({ values, touched, errors, setFieldValue }) => (
					<Form>
						<Field
							id="email"
							name="email"
							label={t('Email')}
							type="text"
							component={Input}
							placeholder={t('YourEmail')}
							InputLabelProps={{
								shrink: true,
							}}
							fullWidth
							variant="standard"
							errors={errors}
							touched={touched}
							value={values.email}
							onFocus={() => setFieldValue('phone', '')}
						/>
						<Text>{t('Or')}</Text>
						<Field
							id="phone"
							name="phone"
							label={t('Phone')}
							type="text"
							component={Input}
							placeholder="+370"
							InputLabelProps={{
								shrink: true,
							}}
							fullWidth
							variant="standard"
							errors={errors}
							touched={touched}
							value={values.phone}
							onFocus={() => setFieldValue('email', '')}
						/>
						<Button
							type="submit"
							fullwidth="true"
							disabled={!(values.email || values.phone) || errors.email || errors.phone}
							sx={{ mt: 6 }}>
							{t('SendVerificationCode')}
						</Button>
						<Button to={'/login'} variant="outlined" fullwidth="true" sx={{ mt: 2 }}>
							{t('Cancel')}
						</Button>
					</Form>
				)}
			</Formik>
		</Container>
	);

	return (
		<Card sx={{ maxWidth: 700 }}>
			{verifyCode ? (
				<VerificationCode
					onSubmit={code => handleCodeSubmit(code)}
					onResend={handleResend}
					onClose={handleClose}
					title={t('ForgotPassword')}
					text={t('EnterDigitCodeSent')}
					contact={contact.email || contact.phone}
					length={6}
					sx={{ p: 6, pb: 8 }}
					paddingLeft={{ xs: 3, sm: 14 }}
					paddingRight={{ xs: 3, sm: 14 }}
				/>
			) : (
				<PasswordReset />
			)}
		</Card>
	);
};
