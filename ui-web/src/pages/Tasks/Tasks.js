import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Chip } from '@mui/material';
import { styled } from '@mui/system';
import { ListDetailPage } from '@components';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import qs from 'query-string';
import api from '@api/axios';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';
import { OWNED_UNIT_STATUSES, TASK_STATUS } from '@common/consts';

const Status = styled(Chip)(({ theme }) => ({
	borderRadius: 4,
	color: theme.palette.base.white,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.m,
}));

const statusChip = {
	[TASK_STATUS.done]: {
		label: 'Done',
		color: '#499CFB',
	},
	[TASK_STATUS.inProgress]: {
		label: 'InProgress',
		color: '#FFC400',
	},
	[TASK_STATUS.planned]: {
		label: 'TaskPlanned',
		color: '#00BE73',
	},
	[TASK_STATUS.onHold]: {
		label: 'OnHold',
		color: '#989898',
	},
	[TASK_STATUS.closed]: {
		label: 'Closed',
		color: '#EB5757',
	},
	[TASK_STATUS.cannotComplete]: {
		label: 'CannotComplete',
		color: '#EB5757',
	},
	[TASK_STATUS.canceled]: {
		label: 'Canceled',
		color: '#EB5757',
	},
};

const PAGE_SIZE = 5;

export const Tasks = () => {
	const { t } = useTranslation();

	const [listLoading, setListLoading] = useState(false);
	const [tasks, setTasks] = useState([]);
	const [taskTotal, setTaskTotal] = useState(0);
	const [sort, setSort] = useState({
		field: 'startDate',
		order: 'desc',
	});
	const [page, setPage] = useState(1);
	const taskQuery = useRef({
		ownedUnit: 'all',
	});

	const [ownedUnits, setOwnedUnits] = useState([
		{
			title: t('AllUnits'),
			value: 'all',
		},
	]);

	const loadOwnedUnits = async () => {
		try {
			const res = await api.get(`/owned-unit?status=${OWNED_UNIT_STATUSES.approved}`);

			setOwnedUnits([
				{
					title: t('AllUnits'),
					value: 'all',
				},
				...res.data.map(ownedUnit => ({
					title: ownedUnit.label,
					name: ownedUnit.label,
					value: ownedUnit._id,
				})),
			]);
		} catch (error) {
			console.error(error);
		}
	};

	const filterSelect = query => {
		taskQuery.current = {
			...taskQuery.current,
			...query,
		};

		// so if some filter values change we want to reset pagination
		// do know this should not apply for sorting
		setPage(1);

		loadTasks({ query: taskQuery.current, pageReq: 1 });
	};

	const onSort = field => {
		let newSort = {
			...sort,
		};

		if (newSort.field === field) {
			newSort.order = newSort.order === 'asc' ? 'desc' : 'asc';
		} else {
			newSort = {
				field,
				order: 'asc',
			};
		}

		setSort(newSort);

		loadTasks({ sortReq: newSort });
	};

	const onPageChange = (e, pageValue) => {
		loadTasks({ pageReq: pageValue });
		setPage(pageValue);
	};

	const loadTasks = async ({ query = taskQuery.current, pageReq = page, pageSize = PAGE_SIZE, sortReq = sort }) => {
		try {
			setListLoading(true);

			const queryString = qs.stringify({ ...query, page: pageReq, pageSize, sortField: sortReq.field, sortOrder: sortReq.order });

			const resp = await api.get(`/tasks?${queryString}`);

			setTasks(
				resp.data.tasks.map(task => ({
					title: task.title,
					address: task.unitLabel,
					startDate: task.startDate && moment(task.startDate).format('YYYY - MM - DD'),
					endDate: task.endDate && moment(task.endDate).format('YYYY - MM - DD'),
					chip: <Status label={statusChip[task.status]?.label} sx={{ backgroundColor: statusChip[task.status]?.color }} />,
					reason: task.reason,
					description: task.description,
					sumDescription:
						task.grandTotalLabel || task.grandTotalSum ? (
							<div>
								{task.grandTotalLabel || ''}{' '}
								{task.grandTotalSum && (
									<b>
										{task.grandTotalSum.toFixed(2)} {task.currencyCode}
									</b>
								)}
							</div>
						) : null,
					documents: task.workOrderDocuments.map(document => ({
						...document,
						documentApiUrl: `/tasks/${task.id}/file/${document.Id}`,
					})),
					_id: task.id,
				})),
			);
			setTaskTotal(resp.data.total);
		} catch (error) {
			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryAgain'),
				type: ALERT.ERROR,
			});
		}

		setListLoading(false);
	};

	useEffect(() => {
		loadOwnedUnits();
		loadTasks({});
	}, []);

	const selectors = useMemo(
		() => [
			{
				title: 'All',
				action: () => filterSelect({ status: 'all' }),
			},
			{
				title: 'Planned',
				action: () => filterSelect({ status: TASK_STATUS.planned }),
			},
			{
				title: 'SelectorsInProgress',
				action: () => filterSelect({ status: TASK_STATUS.inProgress }),
			},
			{
				title: 'SelectorsDone',
				action: () => filterSelect({ status: TASK_STATUS.done }),
			},
		],
		[], // eslint-disable-line react-hooks/exhaustive-deps
	);

	const tableHeadCells = [
		{
			name: 'TasksTitle',
			key: 'title',
			width: '26%',
		},
		{
			name: 'TasksUnit',
			key: 'address',
			width: '26%',
		},
		{
			name: 'TasksStartDate',
			key: 'startDate',
			width: '17%',
			sorting: sort.field === 'startDate' ? (sort.order === 'asc' ? 1 : -1) : 2,
		},
		{
			name: 'TasksEndDate',
			key: 'endDate',
			width: '17%',
			sorting: sort.field === 'endDate' ? (sort.order === 'asc' ? 1 : -1) : 2,
		},
		{
			name: 'TasksStatus',
			key: 'status',
			width: '10%',
		},
	];

	return (
		<ListDetailPage
			title="Tasks"
			selectMenuItems={ownedUnits}
			onItemSelect={e => filterSelect({ ownedUnit: e.target.value })}
			selectors={selectors}
			tableHeadCells={tableHeadCells}
			data={tasks}
			listCardMap={listCardMap}
			detailCardMap={detailCardMap}
			onSort={onSort}
			onPageChange={onPageChange}
			pageCount={Math.ceil(taskTotal / PAGE_SIZE)}
			listLoading={listLoading}
			page={page}
		/>
	);
};

const listCardMap = {
	titleKey: 'title',
	subTitleKey: 'chip',
	content: [
		{
			dataKey: 'startDate',
			label: 'StartDate',
		},
		{
			dataKey: 'endDate',
			label: 'EndDate',
		},
		{
			dataKey: 'address',
		},
	],
};

const detailCardMap = {
	titleKey: 'title',
	subTitleKey: 'chip',
	contentHeaderKey: 'address',
	content: [
		{
			dataKey: 'startDate',
			label: 'StartDate',
		},
		{
			dataKey: 'endDate',
			label: 'EndDate',
		},
		{
			dataKey: 'reason',
			label: 'Reason',
		},
		{
			dataKey: 'sumDescription',
			label: 'SumWithVat',
		},
		{
			dataKey: 'description',
			label: 'Description',
		},
	],
};
