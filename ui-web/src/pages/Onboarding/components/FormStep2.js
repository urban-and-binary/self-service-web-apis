import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { FormGroup } from '@mui/material';
import { Input, Switch } from '@components';

const Text = styled('p')(({ theme }) => ({
	margin: '24px 0 16px 0',
	textAlign: 'left',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const StyledLabel = styled('p')(({ theme }) => ({
	a: {
		textDecoration: 'none',
		color: theme.palette.primary.blue002,
	},
}));

export const FormStep2 = ({ errors, touched, values, setFieldValue }) => {
	const { t } = useTranslation();

	return (
		<>
			<Field
				id="address"
				name="address"
				label={t('AddressCorrespondence') + ` (${t('UnnecessaryDigital')}):`}
				type="text"
				component={Input}
				placeholder={t('YourAddress')}
				InputLabelProps={{
					shrink: true,
				}}
				fullWidth
				variant="standard"
				errors={errors}
				touched={touched}
				value={values.address}
				required={!values.invoiceAgree ? true : false}
			/>
			<Text>{t('Agreements')}:</Text>
			<FormGroup>
				<Switch
					name="rulesAgree"
					value={values.rulesAgree}
					onChange={setFieldValue}
					label={
						<StyledLabel>
							{t('IveUnderstood')} <Link to="/">{t('PrivacyPolicy')}</Link> {t('And')} <Link to="/">{t('UsageTerms')}</Link>{' '}
							{t('AgreeToComply')}.
						</StyledLabel>
					}
				/>
				<Switch name="personalData" value={values.personalData} onChange={setFieldValue} label={t('AgreeToDataManagement')} />
				<Switch
					name="userRules"
					value={values.userRules}
					onChange={setFieldValue}
					label={
						<StyledLabel>
							{t('IveUnderstood')} <Link to="/">{t('UserTerms')}</Link> {t('AgreeToComply')}
						</StyledLabel>
					}
				/>
				<Switch name="invoiceAgree" value={values.invoiceAgree} onChange={setFieldValue} label={t('GetEBills')} />
				<Switch name="newsAgree" value={values.newsAgree} onChange={setFieldValue} label={t('AgreeToNewsletter')} />
			</FormGroup>
		</>
	);
};

FormStep2.propTypes = {
	errors: PropTypes.object.isRequired,
	touched: PropTypes.object.isRequired,
	values: PropTypes.object.isRequired,
	setFieldValue: PropTypes.func.isRequired,
};
