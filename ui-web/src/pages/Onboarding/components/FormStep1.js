import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { Input } from '@components';

const StyledField = styled(Field)(({ theme }) => ({
	marginBottom: theme.spacing(3),
}));

export const FormStep1 = ({ errors, touched, values }) => {
	const { t } = useTranslation();

	return (
		<>
			<StyledField
				id="name"
				name="name"
				label={t('Name')}
				type="text"
				component={Input}
				placeholder={t('Name')}
				InputLabelProps={{
					shrink: true,
					required: true,
				}}
				fullWidth
				variant="standard"
				errors={errors}
				touched={touched}
				value={values.name}
			/>
			<StyledField
				id="surname"
				name="surname"
				label={t('Surname')}
				type="text"
				component={Input}
				placeholder={t('Surname')}
				InputLabelProps={{
					shrink: true,
					required: true,
				}}
				fullWidth
				variant="standard"
				errors={errors}
				touched={touched}
				value={values.surname}
			/>
			{/* <StyledField
				id="email"
				name="email"
				label={t('ContactEmail')}
				type="text"
				component={Input}
				placeholder={t('YourEmail')}
				InputLabelProps={{
					shrink: true,
					required: true,
				}}
				fullWidth
				variant="standard"
				errors={errors}
				touched={touched}
				value={values.email}
			/> */}
			<Field
				id="phone"
				name="phone"
				label={t('ContactPhone')}
				type="text"
				component={Input}
				placeholder="+370"
				InputLabelProps={{
					shrink: true,
				}}
				fullWidth
				variant="standard"
				errors={errors}
				touched={touched}
				value={values.phone}
			/>
		</>
	);
};

FormStep1.propTypes = {
	errors: PropTypes.object.isRequired,
	touched: PropTypes.object.isRequired,
	values: PropTypes.object.isRequired,
};
