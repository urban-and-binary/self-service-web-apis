import api from '@api/axios';
import useAuth from '@hooks/useAuth';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { Box, Grid, useMediaQuery } from '@mui/material';
import { Card } from '@components';
import { FormStep1 } from './components/FormStep1';
import { FormStep2 } from './components/FormStep2';
import Button from '@components/Button/Button';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';
import { LanguageSelect } from '@components';

const Container = styled(Box)(() => ({
	textAlign: 'center',
	margin: 'auto',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: '24px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const StepWrapper = styled('div')(({ theme }) => ({
	display: 'flex',
	justifyContent: 'center',
	marginBottom: theme.spacing(7),
}));
const Step = styled('p')(({ theme, active }) => ({
	margin: '0 8px',
	width: 32,
	height: 32,
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
	borderRadius: '8px',
	boxShadow: theme.shadow.s03,
	color: active === 'true' ? 'white' : theme.palette.primary.blue002,
	backgroundColor: active === 'true' ? theme.palette.primary.blue002 : theme.palette.base.white,
}));
const ButtonWrapper = styled('div')(({ theme }) => ({
	display: 'flex',
	justifyContent: 'flex-end',
	marginTop: theme.spacing(5),
}));
const StyledButton = styled(Button)(({ isTablet }) => ({
	width: isTablet ? '100%' : 160,
}));

export const Onboarding = () => {
	const { t } = useTranslation();
	const theme = useTheme();
	const { setAuth } = useAuth();
	const navigate = useNavigate();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	const [step, setStep] = useState(1);
	const max = 2;

	const initialValues = {
		name: '',
		surname: '',
		// email: location.state.email || '',
		phone: '',
		address: '',
		rulesAgree: true,
		personalData: true,
		userRules: true,
		invoiceAgree: false,
		newsAgree: false,
	};

	const validationSchema = Yup.object({
		name: Yup.string().required(t('NameRequired')),
		surname: Yup.string().required(t('SurnameRequired')),
		// email: Yup.string()
		// 	.email(t('MustBeEmail'))
		// 	.required(t('EmailRequired')),
		// TODO: proper phone validation
		phone: Yup.string()
			.nullable()
			.matches(/^\+?[\d\s]*$/, t('ContactPhoneOnlyNumbers')),
		address: Yup.string().when('invoiceAgree', {
			is: false,
			then: Yup.string().required(t('FillAddressOrSelectDigital')),
		}),
	});

	const onSubmit = async values => {
		try {
			const res = await api.put('/users/profile/info', {
				name: values.name,
				surname: values.surname,
				// cause we dont want to pass in the phone number to the update
				// if it has not been added
				phone: values.phone || undefined,
				address: values.address,
				isOnboarded: true,
			});
			await api.put('/users/settings', {
				agreements: {
					privacyPolicy: values.rulesAgree,
					personalData: values.personalData,
					userRules: values.userRules,
					advertisment: values.newsAgree,
				},
				bills: { receiveFormat: values.invoiceAgree ? 'digital' : 'paper' },
			});
			setAuth({ user: res.data });
			navigate('/', { replace: true });
		} catch (error) {
			navigate('/error', { state: { to: '/onboarding', cancelTo: '/login' } });
		}
	};

	const onNext = async phone => {
		try {
			if (phone) {
				await api.get(`/users/validate-phone?phone=${phone}`);
			}

			setStep(step + 1);
		} catch (error) {
			showToast({
				title: t('OOPS!'),
				text: t(error.response?.data?.message || 'SomethingWrongTryAgain'),
				type: ALERT.ERROR,
			});
		}
	};

	return (
		<Card sx={{ borderRadius: '30px', mb: 7 }} width={{ xs: '100%', sm: '80%' }} maxWidth={1100} headerLeft={!isTablet}>
			<Container width={{ xs: '100%', sm: '70%', md: '50%' }} sx={{ pt: 8, pb: 10, pl: 3, pr: 3 }}>
				<Grid display="flex" justifyContent="flex-end" mb={1}>
					<LanguageSelect />
				</Grid>
				<Title>{t('WelcomeFirst')}...</Title>
				<Subtitle>{t('CheckAccountInformation')}</Subtitle>
				<StepWrapper>
					<Step active={step === 1 ? 'true' : 'false'}>1</Step>
					<Step active={step === 2 ? 'true' : 'false'}>2</Step>
				</StepWrapper>

				<Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
					{({ values, touched, errors, setFieldValue }) => (
						<Form>
							{step === 1 && <FormStep1 errors={errors} touched={touched} values={values} />}
							{step === 2 && <FormStep2 errors={errors} touched={touched} values={values} setFieldValue={setFieldValue} />}
							<ButtonWrapper sx={{ mt: 5 }}>
								{step > 1 && (
									<StyledButton onClick={() => setStep(step - 1)} variant="outlined" sx={{ mr: 2 }} isTablet={isTablet}>
										{t('Back')}
									</StyledButton>
								)}
								{step !== max && (
									<StyledButton
										type="button"
										onClick={() => onNext(values.phone)}
										disabled={!values.name || !values.surname || errors.email || errors.phone}
										isTablet={isTablet}>
										{t('Next')}
									</StyledButton>
								)}
								{step === max && (
									<StyledButton
										type="submit"
										disabled={!(values.rulesAgree && values.personalData && values.userRules) || errors.address}
										isTablet={isTablet}>
										{t('Save')}
									</StyledButton>
								)}
							</ButtonWrapper>
						</Form>
					)}
				</Formik>
			</Container>
		</Card>
	);
};
