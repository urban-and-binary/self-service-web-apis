import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { Grid } from '@mui/material';
import { Input, Switch, Checkbox, SuccessMessage } from '@components';
import Button from '@components/Button/Button';

const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const Label = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Buttons = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'flex-end',
}));
const Permissions = styled('div')(() => ({
	display: 'flex',
	flexDirection: 'column',
}));
const Message = styled('p')(() => ({
	textAlign: 'center',
}));

const initialValues = {
	email: '',
	bills: false,
	claims: false,
	declarations: false,
	role: null,
};

export const AddContact = ({ onCancel }) => {
	const { t } = useTranslation();
	const [success, setSuccess] = useState(false);
	const email = 'testukas@gmail.com';

	const validationSchema = Yup.object({
		email: Yup.string()
			.email(t('InvalidEmailFormat'))
			.required(t('Required')),
	});

	const onSubmit = () => {
		setSuccess(true);
	};

	if (success) {
		return (
			<SuccessMessage sx={{ mt: -10 }}>
				<Message>
					Jūsų kvietimas pridėti nuomininką adresu Antakalnio g. 112 - 12, Vilniaus m., Vilniaus m. sav. sėkmingai išsiųstas
					elektroniniu paštu <span>{email}</span>.
				</Message>
			</SuccessMessage>
		);
	}

	return (
		<Grid width={{ sm: 500 }} sx={{ pt: 3, pb: 5, pl: 4, pr: 4, mt: -10 }}>
			<Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
				{({ values, touched, errors, setFieldValue }) => (
					<Form>
						<Title>{t('AddContact')}</Title>
						<Subtitle>{t('HereYouCanAdd')}</Subtitle>
						<Field
							id="email"
							name="email"
							label="Email"
							type="text"
							component={Input}
							placeholder={t('YourEmail')}
							InputLabelProps={{
								shrink: true,
								required: true,
							}}
							fullWidth
							variant="standard"
							errors={errors}
							touched={touched}
							value={values.email}
							sx={{ mt: 5 }}
						/>
						<Label sx={{ mt: 4 }}>{t('ChooseRole')}:</Label>
						<Checkbox
							name="role"
							radio
							value={values.role === 'tenant'}
							onChange={name => setFieldValue(name, 'tenant')}
							label={t('Tenant')}
							labelPlacement="start"
							bg={values.role === 'tenant' ? 'true' : 'false'}
							sx={{ mt: 1, mb: 1, width: '100%' }}
						/>
						<Checkbox
							name="role"
							radio
							value={values.role === 'coOwner'}
							onChange={name => setFieldValue(name, 'coOwner')}
							label={t('CoOwner')}
							labelPlacement="start"
							bg={values.role === 'coOwner' ? 'true' : 'false'}
							sx={{ mt: 1, mb: 1, width: '100%' }}
						/>
						<Label sx={{ mt: 1 }}>{t('Permissions')}:</Label>
						<Permissions>
							<Switch
								name="bills"
								value={values.bills}
								onChange={setFieldValue}
								label={t('PayBills')}
								labelPlacement="start"
							/>
							<Switch
								name="claims"
								value={values.claims}
								onChange={setFieldValue}
								label={t('AllowClaims')}
								labelPlacement="start"
							/>
							<Switch
								name="declarations"
								value={values.declarations}
								onChange={setFieldValue}
								label={t('AllowMeterDeclaration')}
								labelPlacement="start"
							/>
						</Permissions>
						<Buttons sx={{ mt: 5 }}>
							<Button type="button" variant="outlined" onClick={onCancel} sx={{ mr: 2 }}>
								{t('Cancel')}
							</Button>
							<Button type="submit">{t('Confirm')}</Button>
						</Buttons>
					</Form>
				)}
			</Formik>
		</Grid>
	);
};

AddContact.propTypes = {
	data: PropTypes.object,
	onCancel: PropTypes.func,
	onConfirm: PropTypes.func,
};
