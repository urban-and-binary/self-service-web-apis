import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Formik, Form, Field } from 'formik';
import { Dialog, Input } from '@components';
import Button from '@components/Button/Button';

const Container = styled('div')(() => ({
	minWidth: 400,
	padding: '0 32px 40px 32px',
}));
const Buttons = styled('div')(({ theme }) => ({
	display: 'flex',
	justifyContent: 'flex-end',
	marginTop: theme.spacing(5),
}));

const initialValues = {
	description: 'Nuomojamas Jurgitai',
};

export const EditName = ({ open, onClose, onConfirm }) => {
	const { t } = useTranslation();

	const onSubmit = () => {};

	return (
		<Dialog open={open} onClose={onClose} showClose title={t('EditDescription')}>
			<Formik initialValues={initialValues} onSubmit={onSubmit}>
				{({ values }) => (
					<Form>
						<Container>
							<Field
								id="description"
								name="description"
								label={t('Description')}
								type="text"
								component={Input}
								placeholder={t('TypeHere')}
								InputLabelProps={{
									shrink: true,
								}}
								fullWidth
								variant="standard"
								value={values.description}
							/>
							<Buttons>
								<Button type="button" variant="outlined" onClick={onClose} sx={{ mr: 2 }}>
									{t('Cancel')}
								</Button>
								<Button onClick={onConfirm} type="submit">
									{t('Confirm')}
								</Button>
							</Buttons>
						</Container>
					</Form>
				)}
			</Formik>
		</Dialog>
	);
};

EditName.propTypes = {
	open: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired,
	onConfirm: PropTypes.func.isRequired,
};
