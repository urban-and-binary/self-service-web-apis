import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Divider } from '@mui/material';
import { Paper } from '@components';

const H3 = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));

export const Card = ({ children, title }) => (
	<Paper sx={{ padding: '32px 28px 24px 24px', mb: 3 }}>
		<H3 sx={{ mb: 3 }}>{title}</H3>
		<Divider />
		{children}
	</Paper>
);

Card.propTypes = {
	title: PropTypes.string.isRequired,
	children: PropTypes.node,
};
