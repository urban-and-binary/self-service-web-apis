import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Switch } from '@components';
import { Accordion, AccordionSummary, AccordionDetails, Divider, Grid } from '@mui/material';
import { ChevronIcon, TrashIcon } from '@assets/icons';

const StyledAccordion = styled(Accordion)(() => ({
	boxShadow: 'none',
	'.Mui-expanded': {
		minHeight: '0px !important',
	},
}));
const Summary = styled(AccordionSummary)(() => ({
	minHeight: 0,
	padding: 0,
	'& .MuiAccordionSummary-content, & .MuiAccordionSummary-content.Mui-expanded': {
		margin: 0,
	},
}));
const Details = styled(AccordionDetails)(() => ({
	padding: 0,
}));
const Permissions = styled('div')(({ theme }) => ({
	display: 'flex',
	flexDirection: 'column',
	'label .MuiFormControlLabel-label': {
		width: '100%',
		fontSize: theme.typography.fontSize.base,
		fontWeight: theme.typography.fontWeight.r,
	},
}));
const Title = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const Section = styled('div')(() => ({
	padding: '24px 0',
	display: 'flex',
}));
const DeleteButton = styled('button')(({ theme }) => ({
	padding: 0,
	display: 'flex',
	alignItems: 'center',
	cursor: 'pointer',
	background: 'none',
	border: 'none',
	height: 40,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
	'&:hover': {
		color: theme.palette.base.main,
	},
}));
const DeleteWrapper = styled('div')(() => ({
	display: 'flex',
	flexWrap: 'wrap',
	justifyContent: 'space-between',
}));

export const Contact = ({ contact }) => {
	const { t } = useTranslation();
	const [permissions, setPermissions] = useState(contact.permissions);

	const handlePermissions = (name, value) => {
		setPermissions({ ...permissions, [name]: value });
	};

	return (
		<Grid sx={{ mt: 3 }}>
			<StyledAccordion>
				<Summary expandIcon={<ChevronIcon />}>
					<Title sx={{ mr: 2 }}>{t('Tenant')}:</Title>
					<Text>{contact.name}</Text>
				</Summary>
				<Details>
					<Section sx={{ pt: 0 }}>
						<Title sx={{ mr: 2, color: 'transparent' }}>{t('Tenant')}:</Title>
						<Grid xs={12}>
							<Text>{contact.phone}</Text>
							<DeleteWrapper>
								<Text>{contact.email}</Text>
								<DeleteButton>
									<TrashIcon sx={{ mr: 1 }} />
									{t('DeleteContact')}
								</DeleteButton>
							</DeleteWrapper>
						</Grid>
					</Section>
					<Divider sx={{ borderStyle: 'dashed' }} />
					<Section>
						<Title sx={{ mr: 2, pt: '4px' }}>{t('Permissions')}:</Title>
						<Permissions>
							<Switch
								name="bills"
								value={permissions.bills}
								onChange={handlePermissions}
								label={t('PayBills')}
								labelPlacement="start"
							/>
							<Switch
								name="claims"
								value={permissions.claims}
								onChange={handlePermissions}
								label={t('AllowClaims')}
								labelPlacement="start"
							/>
							<Switch
								name="declarations"
								value={permissions.declarations}
								onChange={handlePermissions}
								label={t('AllowMeterDeclaration')}
								labelPlacement="start"
							/>
						</Permissions>
					</Section>
				</Details>
			</StyledAccordion>
		</Grid>
	);
};

Contact.propTypes = {
	contact: PropTypes.object.isRequired,
};
