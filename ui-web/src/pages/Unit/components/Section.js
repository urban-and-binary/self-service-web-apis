import React from 'react';
import { styled } from '@mui/system';
import PropTypes from 'prop-types';
import { Divider } from '@mui/material';

const Spacing = styled('div')(() => ({
	padding: '24px 0',
}));

export const Section = ({ children, last }) => (
	<>
		<Spacing>{children}</Spacing>
		{!last && <Divider sx={{ borderStyle: 'dashed' }} />}
	</>
);

Section.propTypes = {
	children: PropTypes.node,
	last: PropTypes.bool,
};
