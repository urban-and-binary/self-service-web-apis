import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import { Paper, EditButton } from '@components';
import { EditName } from './EditName';
import building from '@assets/images/building.png';

const Header = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'flex-end',
}));
const Title = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.bold,
}));
const AddressTitle = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.bold,
}));
const BuildingImage = styled('img')(() => ({
	width: '100%',
	borderRadius: 16,
	margin: '24px 0',
}));
const InfoTitle = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
	textAlign: 'right',
}));
const InfoText = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));

const InfoRow = ({ title, text }) => (
	<>
		<Grid item xs={6}>
			<InfoTitle>{title}:</InfoTitle>
		</Grid>
		<Grid item xs={6} pl={2}>
			<InfoText>{text}</InfoText>
		</Grid>
	</>
);

export const Main = ({ unit }) => {
	const { t } = useTranslation();

	const [open, setOpen] = useState(false);
	const handleClick = () => {
		setOpen(false);
	};

	return (
		<Paper sx={{ p: 4, mb: 3 }}>
			<Header>
				<EditButton onClick={() => setOpen(true)}>{t('Edit')}</EditButton>
				<EditName open={open} onClose={handleClick} onConfirm={handleClick} />
			</Header>
			<Title>{unit.title}</Title>
			<AddressTitle>{unit.label}</AddressTitle>
			<BuildingImage src={building} />
			<Grid container>
				<InfoRow title={t('RegisterCode')} text={unit.RegisterCode} />
				<InfoRow title={t('BuildingYear')} text={unit.building.buildYear} />
				<InfoRow
					title={t('BuildingFloor')}
					text={!unit.floorNumber && !unit.building.floors ? '' : `${unit.floorNumber || ''}/${unit.building.floors || ''}`}
				/>
				<InfoRow title={t('SquareMeters')} text={unit.areaIndoor} />
				<InfoRow title={t('EnergyClass')} text={unit.energyClass} />
			</Grid>
		</Paper>
	);
};

Main.propTypes = {
	unit: PropTypes.object.isRequired,
};

InfoRow.propTypes = {
	title: PropTypes.string.isRequired,
	text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
