import React from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Stack } from '@mui/material';
import Button from '@components/Button/Button';

const Container = styled('div')(({ theme }) => ({
	textAlign: 'center',
	padding: theme.spacing(3),
	borderRadius: 16,
	backgroundColor: theme.palette.primary.blue004,
}));
const Title = styled('p')(({ theme }) => ({
	margin: 10,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.r,
}));
const StyledButton = styled(Button)(() => ({
	backgroundColor: 'white',
}));

export const Notification = ({ ...props }) => {
	const { t } = useTranslation();

	return (
		<Container {...props}>
			<Title>{t('InvitationTenantWaitingApproval')}</Title>
			<Stack spacing={2} direction={{ xs: 'column', sm: 'row' }} justifyContent="center" sx={{ pt: 1 }}>
				<StyledButton variant="outlined">{t('CancelInvitation')}</StyledButton>
				<Button>{t('ResendInvitation')}</Button>
			</Stack>
		</Container>
	);
};
