import React, { useEffect, useState } from 'react';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';
import { Grid, Stack, useMediaQuery, Table, TableBody, TableCell, TableRow } from '@mui/material';
import { AppSpinner, Dialog, ViewDownloadApiFile } from '@components';
import Button from '@components/Button/Button';
import { Section } from './components/Section';
import { Card } from './components/Card';
import { Main } from './components/Main';
import { Contact } from './components/Contact';
import { AddContact } from './components/AddContact';
import { Notification } from './components/Notification';
import { ChevronIcon } from '@assets/icons';
import moment from 'moment';
import api from '@api/axios';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';
import { FUND_TYPES } from '@common/consts';

const FUND_TITLES = {
	[FUND_TYPES.accumulated]: 'GeneralUseFund',
	[FUND_TYPES.elevator]: 'ElevatorFund',
	[FUND_TYPES.renovation]: 'RenovationFund',
};

const BackButton = styled(Link)(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	textDecoration: 'none',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Icon = styled(ChevronIcon)(() => ({
	transform: 'rotate(90deg)',
	marginRight: 20,
	width: '14px',
	height: '8px',
}));
const ButtonWrapper = styled('div')(({ theme }) => ({
	display: 'flex',
	justifyContent: 'flex-end',
	marginBottom: theme.spacing(3),
}));
const Title = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));
const CategoryTitle = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: '8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const Email = styled('a')(({ theme }) => ({
	margin: '8px 0',
	textDecoration: 'none',
	color: theme.palette.primary.blue002,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const Spacing = styled('div')(() => ({
	padding: '24px 0',
}));

const CustomCell = styled(TableCell)(() => ({
	border: 0,
	paddingBottom: 0,
	paddingLeft: 0,
}));

export const Unit = () => {
	const { t } = useTranslation();
	const theme = useTheme();
	const [open, setOpen] = useState(false);
	const [unit, setUnit] = useState();

	const params = useParams();

	const invitation = !!params.id;

	const handleOpen = () => {
		setOpen(true);
	};
	const handleClose = () => {
		setOpen(false);
	};

	const AddContactButton = () => (
		<Button variant="outlined" onClick={handleOpen}>
			+ {t('AddContact')}
		</Button>
	);

	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	useEffect(() => {
		const loadUnit = async () => {
			try {
				const unitResp = await api.get(`/owned-unit/${params.id}`);

				setUnit(unitResp.data);
			} catch (error) {
				showToast({
					title: t('OOPS!'),
					text: t(error.response?.data?.message || 'SomethingWrongTryLoginAgain'),
					type: ALERT.ERROR,
				});
			}
		};

		loadUnit();
	}, [params.id]);

	return (
		<>
			{unit ? (
				<Grid container pr={3} pl={3} columnSpacing={3}>
					<Grid container item xs={12} mb={3} direction="row" justifyContent="space-between">
						<BackButton to="/profile">
							<Icon /> {t('BackToProfile')}
						</BackButton>
						{isTablet && <AddContactButton />}
					</Grid>
					{isTablet && invitation && (
						<Grid item xs={12}>
							<Notification sx={{ mb: 3 }} />
						</Grid>
					)}
					<Grid item xs={12} md={5}>
						<Main unit={unit} />
						<Card title={t('Administration')}>
							{administration.map((item, idx) => (
								<Section key={idx} last={administration.length === idx + 1}>
									<Title>{item.title}</Title>
									<Text>{item.name}</Text>
									<Text>{item.phone}</Text>
									<Email href={`mailto:${item.email}`}>{item.email}</Email>
								</Section>
							))}
						</Card>
					</Grid>
					<Grid item xs={12} md={7}>
						<ButtonWrapper>
							{!isTablet && <AddContactButton />}
							<Dialog open={open} onClose={handleClose} showClose>
								<AddContact onCancel={handleClose} />
							</Dialog>
						</ButtonWrapper>
						{!isTablet && invitation && <Notification sx={{ mb: 3 }} />}
						{contact && (
							<Card title={t('RelatedContact')}>
								<Contact contact={contact} />
							</Card>
						)}
						{!!unit.funds.length && (
							<Card title={t('AccumulativeFunds')}>
								{unit.funds.map((fund, idx) => (
									<Section key={idx} last={unit.funds.length === idx + 1}>
										<Grid container>
											<Grid item xs={12}>
												<Title>{t(FUND_TITLES[fund.type])}:</Title>
											</Grid>
											<Grid item xs={6}>
												<CategoryTitle>{t('TotalAmount')}:</CategoryTitle>
												<Text>
													{fund.totalAmount} {fund.currency}
												</Text>
											</Grid>
											<Grid item xs={6}>
												<CategoryTitle>{t('YourAmount')}:</CategoryTitle>
												<Text>
													{fund.paid || 0} {fund.currency}
												</Text>
											</Grid>
										</Grid>
									</Section>
								))}
							</Card>
						)}

						<Card title={t('AdditionalInformation')}>
							<Spacing>
								<Table>
									<TableBody>
										{additional.map((item, idx) => (
											<TableRow key={`info-item-${idx}`}>
												<CustomCell sx={{ fontWeight: 700 }}>{item.title}:</CustomCell>
												<CustomCell>{item.value}</CustomCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</Spacing>
						</Card>
						{!!unit.documents.length && (
							<Card title={t('Documents')}>
								{unit.documents.map((document, idx) => (
									<Section key={idx} last={unit.documents.length === idx + 1}>
										<Stack direction="row" padding={{ md: '0 40px' }}>
											<Text sx={{ mr: 3 }}>{moment(document.LastModifiedDate).format('YYYY - MM - DD')}</Text>
											<Text sx={{ flex: 1, wordBreak: 'break-word' }}>
												{document.Title}.{document.FileExtension}
											</Text>
											<ViewDownloadApiFile
												defaultDocument={{
													...document,
													documentApiUrl: `/owned-unit/${params.id}/file/${document.Id}`,
												}}
											/>
										</Stack>
									</Section>
								))}
							</Card>
						)}
					</Grid>
				</Grid>
			) : (
				<AppSpinner />
			)}
		</>
	);
};

const contact = {
	name: 'Jurgita Kupstyte',
	phone: '+370 68 444 222',
	email: 'jurgitakupstyte@gmail.com',
	permissions: { bills: false, claims: false, declarations: false },
};
const administration = [
	{
		title: 'Manager',
		name: 'Sydney Newton',
		phone: '+370 68 444 444',
		email: 'sydneynewton@civinity.com',
	},
	{
		title: 'Electrician',
		name: 'Sydney Newton',
		phone: '+370 68 444 444',
		email: 'ewton@civinity.com',
	},
	{
		title: 'Cleaning department',
		name: 'Sydney Newton',
		phone: '+370 68 444 444',
		email: 'sydnewton@civinity.com',
	},
];
const additional = [
	{ title: 'Pirmininkas', value: 'Aadam Fischer' },
	{ title: 'Elevators', value: 'UAB Nano liftai; +370 68 232 232' },
	{ title: 'Stairwell cleaning schedule', value: 'Every monday and wednesday' },
	{ title: 'Accountant', value: 'Michelle Lutz' },
	{ title: 'Site supervision', value: 'UAB Adminta' },
];
