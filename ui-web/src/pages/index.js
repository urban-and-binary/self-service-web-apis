import { Home } from '@pages/Home/Home';
import { Login } from '@pages/Login/Login';
import { Onboarding } from '@pages/Onboarding/Onboarding';
import { LoginError } from '@pages/LoginError/LoginError';
import { PasswordReset } from '@pages/Password/PasswordReset';
import { PasswordChange } from '@pages/Password/PasswordChange';
import { Requests } from '@pages/Requests/Requests';
import { Settings } from '@pages/Settings/Settings';
import { Profile } from '@pages/Profile/Profile';
import { Tasks } from '@pages/Tasks/Tasks';
import { Unit } from '@pages/Unit/Unit';
import { Faq } from '@pages/FAQ/FAQ';
import { Announcements } from '@pages/Announcements/Announcements';
import { Declaration } from '@pages/Declaration/Declaration';
import { PollsAndVoting } from '@pages/PollsAndVoting/PollsAndVoting';
import { SinglePoll } from '@pages/PollsAndVoting/components/SinglePoll';

export {
	Home,
	Login,
	Onboarding,
	LoginError,
	PasswordReset,
	PasswordChange,
	Requests,
	Settings,
	Profile,
	Tasks,
	Unit,
	Faq,
	Announcements,
	Declaration,
	PollsAndVoting,
	SinglePoll,
};
