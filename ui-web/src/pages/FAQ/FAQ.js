import React, { useState } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid, InputBase } from '@mui/material';
import { Accordion } from '@components';

const GridTitle = styled(Grid)(() => ({
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'start',
	justifyContent: 'end',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const Search = styled(InputBase)(({ theme }) => ({
	height: 56,
	padding: '0 14px',
	color: theme.palette.base.dark,
	borderRadius: theme.spacing(2),
	border: `2px solid ${theme.palette.base.lightGray}`,
	input: {
		padding: '5px 0 5px',
		'::placeholder': {
			opacity: 1,
			color: theme.palette.base.gray,
		},
	},
}));

export const Faq = () => {
	const { t } = useTranslation();
	const [search, setSearch] = useState('');

	return (
		<Grid container pl={2} pr={3}>
			<GridTitle item xs={12} sm={6} sx={{ pl: 1 }}>
				<Title>{t('Faq')}</Title>
			</GridTitle>
			<Grid item xs={12} sm={6}>
				<Search
					id="search"
					name="search"
					type="text"
					placeholder={t('Search')}
					fullWidth
					value={search}
					onChange={e => setSearch(e.target.value)}
				/>
			</Grid>

			<Grid item xs={12} sx={{ mt: 3 }}>
				<Accordion title={t('ManagementServices')}>
					<Text sx={{ mt: 3 }}>
						Mūsų tikslas - greitai, kokybiškai ir laiku suteikti geriausiai Jūsų poreikius atitinkančias daugiabučio ir jo
						teritorijos priežiūros paslaugas. Už suteiktas paslaugas kiekvieną mėnesį yra pateikiamas mokėjimo pranešimas
						(sąskaita), kuriame detalizuojamas namui teikiamų paslaugų kiekis ir įkainiai.
					</Text>
				</Accordion>
				<Accordion title={t('DocumentsCertificates')}>
					<Text sx={{ mt: 3 }}>
						Mūsų tikslas - greitai, kokybiškai ir laiku suteikti geriausiai Jūsų poreikius atitinkančias daugiabučio ir jo
						teritorijos priežiūros paslaugas. Už suteiktas paslaugas kiekvieną mėnesį yra pateikiamas mokėjimo pranešimas
						(sąskaita), kuriame detalizuojamas namui teikiamų paslaugų kiekis ir įkainiai.
					</Text>
				</Accordion>
			</Grid>
		</Grid>
	);
};
