import React from 'react';
import { Toast } from '@components';
import { toast } from 'react-toastify';
import { ALERT } from '@constants/general';
import { uniqueId } from 'lodash';

export const hextorgb = (hex = '#000000', opacity = 1) => {
	const r = parseInt(hex.slice(1, 3), 16);
	const g = parseInt(hex.slice(3, 5), 16);
	const b = parseInt(hex.slice(5, 7), 16);
	if (opacity) {
		return `rgba(${r}, ${g}, ${b}, ${opacity})`;
	}
	return `rgb(${r}, ${g}, ${b})`;
};

export const passwordStrength = password => /^(?=.*[A-Z])(?=.*\d)[a-zA-Z\d@$!%*#?&]{8,}$/.test(password);

export const removeSpaces = value => value.replace(/ /g, '');

export const showToast = ({ title = '', text = '', type = ALERT.INFO } = {}) => {
	const toastId = uniqueId();
	toast(<Toast sx={{ mt: 2 }} timeout={5000} toastId={toastId} message={{ title, text, type }} />, {
		toastId,
	});
};

export const removeJWT = () => {
	localStorage.removeItem('jwt');
	sessionStorage.removeItem('jwt');
};
