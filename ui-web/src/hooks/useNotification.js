import { NotificationContext } from '@context/NotificationProvider';
import { useContext } from 'react';

export const useNotification = () => {
	return useContext(NotificationContext);
};
