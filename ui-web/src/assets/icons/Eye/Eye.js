import React from 'react';
import { SvgIcon } from '@mui/material';

function EyeIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 14 12">
			<path
				fill="none"
				d="M0.585938 6.00001C0.585938 6.00001 2.91927 1.33334 7.0026 1.33334C11.0859 1.33334 13.4193 6.00001 13.4193 6.00001C13.4193 6.00001 11.0859 10.6667 7.0026 10.6667C2.91927 10.6667 0.585938 6.00001 0.585938 6.00001Z"
				stroke="currentColor"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				fill="none"
				d="M7 7.75C7.9665 7.75 8.75 6.9665 8.75 6C8.75 5.0335 7.9665 4.25 7 4.25C6.0335 4.25 5.25 5.0335 5.25 6C5.25 6.9665 6.0335 7.75 7 7.75Z"
				stroke="currentColor"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</SvgIcon>
	);
}

export default EyeIcon;
