import React from 'react';
import { SvgIcon } from '@mui/material';

function PolygonIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 6 5">
			<path
				d="M3.3962 4.48532C3.19605 4.74532 2.80395 4.74532 2.6038 4.48532L0.155585 1.305C-0.0975138 0.976212 0.136868 0.5 0.551788 0.5L5.44821 0.5C5.86313 0.5 6.09751 0.976213 5.84441 1.305L3.3962 4.48532Z"
				fill="#FEFEFE"
			/>
		</SvgIcon>
	);
}

export default PolygonIcon;
