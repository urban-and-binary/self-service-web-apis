// Logos
import LogoIcon from './Logo/Logo';
import LogoWhiteIcon from './Logo/LogoWhite';
import PhoneIcon from './Phone/Phone';
import AppStoreLogo from './app-store-badge.png';
import GooglePlayLogo from './google-play-badge.png';
import GoogleIcon from './Google/Google';
import FacebookIcon from './Facebook/Facebook';
import AppleIcon from './Apple/Apple';
import MoneyIcon from './Money/Money';
import SalyIcon from './Saly/Saly';

// Icons
import HomeIcon from './Home/Home';
import BillsIcon from './Bills/Bills';
import RequestsIcon from './Requests/Requests';
import TasksIcon from './Tasks/Tasks';
import DeclarationsIcon from './Declarations/Declarations';
import PoolsIcon from './Pools/Pools';
import DocumentsIcon from './Documents/Documents';
import EasyPandaIcon from './EasyPanda/EasyPanda';
import AnnouncementsIcon from './Announcements/Announcements';
import SettingsIcon from './Settings/Settings';
import BellIcon from './Bell/Bell';
import PolygonIcon from './Polygon/Polygon';
import ChevronIcon from './Chevron/Chevron';
import UserIcon from './User/User';
import LogoutIcon from './Logout/Logout';
import CheckedIcon from './Checked/Checked';
import GhostIcon from './Ghost/Ghost';
import EditIcon from './Edit/Edit';
import CircleIcon from './Circle/Circle';
import CloseIcon from './Close/Close';
import GreenCharacterIcon from './GreenCharacter/GreenCharacter';
import ClipIcon from './Clip/Clip';
import EyeIcon from './Eye/Eye';
import EyeCloseIcon from './EyeClose/EyeClose';
import DownloadIcon from './Download/Download';
import TrashIcon from './Trash/Trash';
import AlertIcon from './Alert/Alert';
import BurgerIcon from './Burger/Burger';
import ThermoColdIcon from './Energy/ThermoCold';
import ThermoHotIcon from './Energy/ThermoHot';
import LightningIcon from './Energy/Lightning';
import SunIcon from './Energy/Sun';
import MoonIcon from './Energy/Moon';
import GasIcon from './Energy/Gas';
import { ImageIcon } from './ImageIcon/ImageIcon';

// Country Flags
import EnglandIcon from './en.png';
import LithuaniaIcon from './lt.png';
import EstoniaIcon from './est.png';

export {
	LogoIcon,
	LogoWhiteIcon,
	PhoneIcon,
	HomeIcon,
	BillsIcon,
	RequestsIcon,
	TasksIcon,
	DeclarationsIcon,
	PoolsIcon,
	DocumentsIcon,
	EasyPandaIcon,
	AnnouncementsIcon,
	SettingsIcon,
	BellIcon,
	PolygonIcon,
	EnglandIcon,
	LithuaniaIcon,
	EstoniaIcon,
	AppStoreLogo,
	GooglePlayLogo,
	ChevronIcon,
	UserIcon,
	LogoutIcon,
	GoogleIcon,
	FacebookIcon,
	AppleIcon,
	CheckedIcon,
	GhostIcon,
	EditIcon,
	CircleIcon,
	ClipIcon,
	MoneyIcon,
	SalyIcon,
	CloseIcon,
	GreenCharacterIcon,
	DownloadIcon,
	EyeIcon,
	EyeCloseIcon,
	TrashIcon,
	AlertIcon,
	BurgerIcon,
	ThermoColdIcon,
	ThermoHotIcon,
	LightningIcon,
	SunIcon,
	MoonIcon,
	GasIcon,
	ImageIcon,
};
