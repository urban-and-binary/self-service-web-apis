import React from 'react';
import PropTypes from 'prop-types';
import logo from './logo-white.png';

function LogoWhiteIcon(props) {
	return <img src={logo} width={props.width} height={props.height} alt="Logo" />;
}

LogoWhiteIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
};

export default LogoWhiteIcon;
