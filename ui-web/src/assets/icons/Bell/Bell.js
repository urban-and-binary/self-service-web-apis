import React from 'react';
import { SvgIcon } from '@mui/material';

function BellIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 16 16" fill="none">
			<path
				d="M12 5.33301C12 4.27214 11.5786 3.25473 10.8284 2.50458C10.0783 1.75444 9.06087 1.33301 8 1.33301C6.93913 1.33301 5.92172 1.75444 5.17157 2.50458C4.42143 3.25473 4 4.27214 4 5.33301C4 9.99967 2 11.333 2 11.333H14C14 11.333 12 9.99967 12 5.33301Z"
				stroke="currentColor"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
				fill="none"
				fillOpacity="0"
			/>
			<path
				d="M9.15042 14C9.03321 14.2021 8.86498 14.3698 8.66257 14.4864C8.46016 14.6029 8.23067 14.6643 7.99708 14.6643C7.7635 14.6643 7.53401 14.6029 7.3316 14.4864C7.12919 14.3698 6.96096 14.2021 6.84375 14"
				stroke="currentColor"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</SvgIcon>
	);
}

export default BellIcon;
