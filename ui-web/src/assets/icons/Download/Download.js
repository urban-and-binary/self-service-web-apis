import React from 'react';
import { SvgIcon } from '@mui/material';

function DownloadIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 14 12">
			<path
				fill="none"
				d="M4.66406 8.91666L6.9974 11.25L9.33073 8.91666"
				stroke="currentColor"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path fill="none" d="M7 6V11.25" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
			<path
				fill="none"
				d="M12.18 9.55249C12.6872 9.19587 13.0675 8.68691 13.2658 8.09949C13.4641 7.51208 13.47 6.87675 13.2828 6.28572C13.0955 5.69469 12.7248 5.17871 12.2244 4.81265C11.724 4.44659 11.12 4.24951 10.5 4.24999H9.76502C9.58958 3.56625 9.26131 2.93122 8.80493 2.39271C8.34854 1.85419 7.77594 1.42623 7.13023 1.14103C6.48451 0.855836 5.78251 0.720841 5.07708 0.74621C4.37164 0.771578 3.68115 0.956649 3.05759 1.28749C2.43403 1.61833 1.89365 2.08632 1.47713 2.65623C1.06061 3.22613 0.778802 3.8831 0.652924 4.57768C0.527046 5.27226 0.560378 5.98634 0.75041 6.66617C0.940442 7.346 1.28222 7.97387 1.75002 8.50249"
				stroke="currentColor"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</SvgIcon>
	);
}

export default DownloadIcon;
