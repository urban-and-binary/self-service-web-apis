import React from 'react';
import PropTypes from 'prop-types';
import image from './ghost.png';

function GhostIcon(props) {
	return <img src={image} width={props.width} height={props.height} alt="Logo" />;
}

GhostIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
};

export default GhostIcon;
