import React from 'react';
import { SvgIcon } from '@mui/material';

function Edit(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 17 17">
			<path d="M7.5 13.666H13.875" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
			<path
				fill="none"
				d="M10.6875 1.97916C10.9693 1.69737 11.3515 1.53906 11.75 1.53906C11.9473 1.53906 12.1427 1.57793 12.325 1.65344C12.5073 1.72895 12.673 1.83963 12.8125 1.97916C12.952 2.11869 13.0627 2.28434 13.1382 2.46664C13.2137 2.64895 13.2526 2.84434 13.2526 3.04166C13.2526 3.23899 13.2137 3.43438 13.1382 3.61669C13.0627 3.79899 12.952 3.96464 12.8125 4.10416L3.95833 12.9583L1.125 13.6667L1.83333 10.8333L10.6875 1.97916Z"
				stroke="currentColor"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</SvgIcon>
	);
}

export default Edit;
