import React from 'react';
import { SvgIcon } from '@mui/material';

function TrashIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 14 14" fill="none">
			<path d="M1.75 3.5H2.91667H12.25" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" fill="none" />
			<path
				d="M4.66406 3.49935V2.33268C4.66406 2.02326 4.78698 1.72652 5.00577 1.50772C5.22456 1.28893 5.52131 1.16602 5.83073 1.16602H8.16406C8.47348 1.16602 8.77023 1.28893 8.98902 1.50772C9.20781 1.72652 9.33073 2.02326 9.33073 2.33268V3.49935M11.0807 3.49935V11.666C11.0807 11.9754 10.9578 12.2722 10.739 12.491C10.5202 12.7098 10.2235 12.8327 9.91406 12.8327H4.08073C3.77131 12.8327 3.47456 12.7098 3.25577 12.491C3.03698 12.2722 2.91406 11.9754 2.91406 11.666V3.49935H11.0807Z"
				stroke="currentColor"
				strokeLinecap="round"
				strokeLinejoin="round"
				fill="none"
			/>
			<path d="M5.83594 6.41602V9.91601" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" fill="none" />
			<path d="M8.16406 6.41602V9.91601" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" fill="none" />
		</SvgIcon>
	);
}

export default TrashIcon;
