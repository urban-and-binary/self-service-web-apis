import React from 'react';
import PropTypes from 'prop-types';
import { SvgIcon } from '@mui/material';

function FacebookIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 31 31" fill="none">
			<circle cx="15.5" cy="15.5" r="13.5625" fill="url(#paint0_linear_46_31171)" />
			<path
				d="M20.5508 19.6478L21.1532 15.8198H17.3844V13.3368C17.3844 12.2893 17.9099 11.2676 19.598 11.2676H21.3125V8.00865C21.3125 8.00865 19.7572 7.75 18.2709 7.75C15.1656 7.75 13.1379 9.58379 13.1379 12.9022V15.8198H9.6875V19.6478H13.1379V28.9022C13.8306 29.0082 14.5392 29.0625 15.2611 29.0625C15.9831 29.0625 16.6917 29.0082 17.3844 28.9022V19.6478H20.5508Z"
				fill="white"
			/>
			<defs>
				<linearGradient id="paint0_linear_46_31171" x1="15.5" y1="1.9375" x2="15.5" y2="28.9821" gradientUnits="userSpaceOnUse">
					<stop stopColor={props.disabled ? '#EAEAEA' : '#18ACFE'} />
					<stop offset="1" stopColor={props.disabled ? '#EAEAEA' : '#0163E0'} />
				</linearGradient>
			</defs>
		</SvgIcon>
	);
}

FacebookIcon.propTypes = {
	disabled: PropTypes.bool,
};

export default FacebookIcon;
