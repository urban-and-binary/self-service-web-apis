import React from 'react';
import PropTypes from 'prop-types';
import image from './Money.png';

function MoneyIcon(props) {
	return <img src={image} width={props.width} height={props.height} alt="" />;
}

MoneyIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
};

export default MoneyIcon;
