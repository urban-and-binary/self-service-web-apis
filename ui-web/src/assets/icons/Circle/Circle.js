import React from 'react';
import { SvgIcon } from '@mui/material';

function Circle(props) {
	return (
		<SvgIcon {...props} width="14" height="14" viewBox="0 0 22 22">
			<path
				d="M11 21C16.5228 21 21 16.5228 21 11C21 5.47715 16.5228 1 11 1C5.47715 1 1 5.47715 1 11C1 16.5228 5.47715 21 11 21Z"
				stroke="currentColor"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
				fill="none"
			/>
		</SvgIcon>
	);
}

export default Circle;
