import React from 'react';
import PropTypes from 'prop-types';
import image from './GreenCharacter.png';

function GreenCharacterIcon(props) {
	return <img src={image} width={props.width} height={props.height} alt="Success" />;
}

GreenCharacterIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
};

export default GreenCharacterIcon;
