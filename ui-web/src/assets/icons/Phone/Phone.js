import React from 'react';
import PropTypes from 'prop-types';
import phone from './phone.png';

function PhoneIcon(props) {
	return <img src={phone} width={props.width} height={props.height} alt="" />;
}

PhoneIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
};

export default PhoneIcon;
