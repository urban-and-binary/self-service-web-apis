import React from 'react';
import { SvgIcon } from '@mui/material';

function LightningIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 24 24" fill="none">
			<path
				fill="none"
				d="M9 22.4993L10.5 14.9993L4.5 12.7493L15 1.49927L13.5 8.99927L19.5 11.2493L9 22.4993Z"
				stroke="#00BE73"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</SvgIcon>
	);
}

export default LightningIcon;
