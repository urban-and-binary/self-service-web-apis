import React from 'react';
import { SvgIcon } from '@mui/material';

function GasIcon(props) {
	return (
		<SvgIcon {...props} viewBox="0 0 24 24" fill="none">
			<path
				fill="none"
				d="M11.5 22C7.35833 22 4 19.418 4 15V14.912C4 12.794 5.115 11.1 6.8125 10C8.43667 8.948 9.39667 7.01 9.15667 5L8.6875 2L10.4258 2.795C13.5567 4.225 16.1642 6.707 17.8542 9.861C18.5972 11.2311 18.9951 12.8293 19 14.464V15C19 16.562 18.58 17.895 17.8542 18.965"
				stroke="#00BE73"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				fill="none"
				d="M11.5 22C10.1192 22 9 20.567 9 18.8C9 17.4 9.84667 16.279 10.5917 15.252L11.5 14L12.4083 15.252C13.1533 16.28 14 17.4 14 18.8C14 20.567 12.8808 22 11.5 22Z"
				stroke="#00BE73"
				strokeWidth="1.5"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</SvgIcon>
	);
}

export default GasIcon;
