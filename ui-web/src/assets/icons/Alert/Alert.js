import React from 'react';
import PropTypes from 'prop-types';
import { ALERT } from '@constants/general';

import success from './success.png';
import warning from './warning.png';
import error from './error.png';
import info from './info.png';

function AlertIcon(props) {
	const renderIcon = value => {
		return {
			[ALERT.SUCCESS]: success,
			[ALERT.INFO]: info,
			[ALERT.ERROR]: error,
			[ALERT.WARNING]: warning,
		}[value];
	};

	return <img src={renderIcon(props.type)} width={props.width} height={props.height} alt={props.type} />;
}

AlertIcon.propTypes = {
	width: PropTypes.number,
	height: PropTypes.number,
	type: PropTypes.string.isRequired,
};

export default AlertIcon;
