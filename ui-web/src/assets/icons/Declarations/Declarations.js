import React from 'react';
import { SvgIcon } from '@mui/material';

function DeclarationsIcon(props) {
	return (
		<SvgIcon {...props}>
			<path
				d="M20 2H4C2.89543 2 2 2.89543 2 4V8C2 9.10457 2.89543 10 4 10H20C21.1046 10 22 9.10457 22 8V4C22 2.89543 21.1046 2 20 2Z"
				stroke="currentColor"
				fill="none"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M20 14H4C2.89543 14 2 14.8954 2 16V20C2 21.1046 2.89543 22 4 22H20C21.1046 22 22 21.1046 22 20V16C22 14.8954 21.1046 14 20 14Z"
				stroke="currentColor"
				fill="none"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path d="M6 6H6.01" stroke="currentColor" fill="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
			<path d="M6 18H6.01" stroke="currentColor" fill="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
		</SvgIcon>
	);
}

export default DeclarationsIcon;
