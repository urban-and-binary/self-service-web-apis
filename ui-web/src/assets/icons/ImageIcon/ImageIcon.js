import React from 'react';

export const ImageIcon = props => (
	<svg width={14} height={14} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
		<path
			d="M11.083 1.75H2.917c-.645 0-1.167.522-1.167 1.167v8.166c0 .645.522 1.167 1.167 1.167h8.166c.645 0 1.167-.522 1.167-1.167V2.917c0-.645-.522-1.167-1.167-1.167Z"
			stroke="#393939"
			strokeLinecap="round"
			strokeLinejoin="round"
		/>
		<path
			d="M4.96 5.833a.875.875 0 1 0 0-1.75.875.875 0 0 0 0 1.75ZM12.247 8.75 9.331 5.833 2.914 12.25"
			stroke="#393939"
			strokeLinecap="round"
			strokeLinejoin="round"
		/>
	</svg>
);
