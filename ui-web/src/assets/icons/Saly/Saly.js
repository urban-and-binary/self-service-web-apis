import React from 'react';
import PropTypes from 'prop-types';
import image from './Saly.png';

function SalyIcon(props) {
	return <img src={image} width={props.width} height={props.height} alt="" />;
}

SalyIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
};

export default SalyIcon;
