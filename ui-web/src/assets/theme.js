import { createTheme } from '@mui/material';
import '@fontsource/metropolis/400.css';
import '@fontsource/metropolis/500.css';
import '@fontsource/metropolis/700.css';

export const palette = {
	primary: {
		blue001: '#005AC8',
		blue002: '#499CFB',
		blue003: '#94C6FF',
		blue004: '#C7E1FF',
		blue005: '#E8F3FF',
	},
	secondary: {
		green001: '#048A55',
		green002: '#00BE73',
		green003: '#07D785',
		green004: '#C5FBE1',
		green005: '#E8FCF2',
	},
	base: {
		main: '#151515',
		dark: '#393939',
		gray: '#989898',
		lightGray: '#CCCCCC',
		inactive: '#EAEAEA',
		background: '#F5F7F9',
		white: '#FEFEFE',
		black: '#000000',
	},
	default: {
		error: '#EB5757',
		warning: '#FFC400',
		lightViolet: '#D9CFFF',
		lightRed: '#FFA9A9',
		lightYellow: '#FFEDB0',
	},
};

export const typography = {
	fontSize: {
		xs: 12,
		small: 14,
		base: 16,
		h1: 32,
		h2: 25,
		h3: 20,
	},
	fontWeight: {
		r: '400',
		m: '500',
		bold: '700',
	},
	lineHeight: {
		xs: '18px',
		small: '20px',
		base: '24px',
		h1: '40px',
	},
};

const shadow = {
	s01: '0px 2px 8px rgba(117, 131, 142, 0.04), 0px 16px 24px rgba(52, 60, 68, 0.12)',
	s02: '0px 2px 8px rgba(117, 131, 142, 0.08), 0px 20px 32px rgba(52, 60, 68, 0.16)',
	s03: '0px 0px 2px rgba(117, 131, 142, 0.04), 0px 4px 8px rgba(52, 60, 68, 0.16)',
};

const theme = createTheme({
	shape: {
		br1: '8px',
		br2: '16px',
	},
	palette: {
		primary: {
			main: palette.primary.blue001,
			blue001: palette.primary.blue001,
			blue002: palette.primary.blue002,
			blue003: palette.primary.blue003,
			blue004: palette.primary.blue004,
			blue005: palette.primary.blue005,
		},
		secondary: {
			main: palette.secondary.green001,
			green001: palette.secondary.green001,
			green002: palette.secondary.green002,
			green003: palette.secondary.green003,
			green004: palette.secondary.green004,
			green005: palette.secondary.green005,
		},
		base: {
			white: palette.base.white,
			gray: palette.base.gray,
			dark: palette.base.dark,
			inactive: palette.base.inactive,
			lightGray: palette.base.lightGray,
			black: palette.base.black,
			background: palette.base.background,
		},
		default: {
			error: palette.default.error,
			warning: palette.default.warning,
			lightViolet: palette.default.lightViolet,
			lightRed: palette.default.lightRed,
			lightYellow: palette.default.lightYellow,
		},
	},
	shadow: {
		s01: shadow.s01,
		s02: shadow.s02,
		s03: shadow.s03,
	},
	typography: {
		fontFamily: 'Metropolis, sans-serif',
		fontSize: {
			xs: typography.fontSize.xs,
			small: typography.fontSize.small,
			base: typography.fontSize.base,
			h1: typography.fontSize.h1,
			h2: typography.fontSize.h2,
			h3: typography.fontSize.h3,
		},
		fontWeight: {
			r: '400',
			m: '500',
			bold: '700',
		},
		lineHeight: {
			xs: '18px',
			small: '20px',
			base: '24px',
			h1: '40px',
		},
	},
	components: {
		MuiCssBaseline: {
			styleOverrides: {},
		},
		MuiFormControlLabel: {
			styleOverrides: {
				label: {
					color: palette.base.dark,
					fontWeight: '400',
					fontSize: typography.fontSize.small,
					lineHeight: '20px',
					textAlign: 'left',
				},
			},
		},
		MuiSwitch: {
			styleOverrides: {
				root: {
					width: '70px',
					height: '50px',
				},
				switchBase: {
					top: '4px',
					left: '4px',
				},
				thumb: {
					boxShadow: 'none',
					top: '5px',
					left: '5px',
					width: '24px',
					height: '24px',
				},
				track: {
					borderRadius: 100,
				},
			},
		},
		MuiInputLabel: {
			styleOverrides: {
				root: {
					color: palette.base.dark,
					fontSize: typography.fontSize.small,
					fontWeight: '700',
					'&.Mui-focused, &.Mui-error': {
						color: palette.base.dark,
					},
				},
				asterisk: {
					color: palette.default.error,
				},
			},
		},
		MuiFormHelperText: {
			styleOverrides: {
				root: {
					fontSize: typography.fontSize.xs,
					fontWeight: '400',
					color: palette.default.error,
				},
			},
		},
		MuiInput: {
			styleOverrides: {
				root: {
					marginTop: '20px !important',
					borderRadius: 16,
					fontSize: typography.fontSize.base,
					fontWeight: typography.fontWeight.r,
					backgroundColor: palette.base.background,
					'&.Mui-focused': {
						width: 'calc(100% + 2px)',
						border: `2px solid ${palette.primary.blue003}`,
						input: {
							padding: '10px 10px 10px 14px',
						},
					},
					'&:hover': {
						width: 'calc(100% + 2px)',
						border: `2px solid ${palette.primary.blue003}`,
						input: {
							padding: '10px 10px 10px 14px',
						},
					},
					'&.Mui-disabled': {
						border: 'none',
						input: {
							padding: '12px 12px 12px 16px',
						},
					},
					'&:before': {
						display: 'none',
					},
					'&:after': {
						display: 'none',
					},
				},
				input: {
					borderRadius: 16,
					padding: '12px 12px 12px 16px',
					color: palette.base.main,
					lineHeight: typography.lineHeight.base,
					caretColor: palette.primary.blue003,
					'::placeholder': {
						color: palette.base.lightGray,
					},
				},
			},
		},
		MuiAutocomplete: {
			styleOverrides: {
				endAdornment: {
					right: '28px !important',
					top: 16,
				},
				popupIndicator: {
					transition: '.2s',
					'&:hover': {
						backgroundColor: 'unset',
					},
				},
				popper: {
					'.MuiPaper-root': {
						background: palette.base.background,
						marginTop: 8,
						borderRadius: 16,
						boxShadow: shadow.s03,
						'.MuiAutocomplete-listbox': {
							padding: '16px 16px 23px 16px',
						},
						'.MuiAutocomplete-option': {
							paddingTop: 16,
							paddingBottom: 16,
							fontSize: typography.fontSize.small,
							fontWeight: typography.fontWeight.r,
							backgroundColor: 'unset !important',
						},
						'.Mui-focused': {
							backgroundColor: `${palette.primary.blue005} !important`,
							borderRadius: 16,
						},
					},
				},
				input: {
					'::placeholder': {
						fontSize: typography.fontSize.small,
						color: palette.base.lightGray,
					},
				},
				root: {
					'.MuiOutlinedInput-root': {
						color: palette.base.dark,
						fontSize: typography.fontSize.small,
						fontWeight: typography.fontWeight.bold,
						background: palette.base.background,
						borderRadius: 16,
						'&:hover': {
							boxShadow: shadow.s03,
						},
						'.MuiOutlinedInput-notchedOutline': {
							border: 'none',
						},
					},
					'&.Mui-focused': {
						'.MuiOutlinedInput-root': {
							boxShadow: shadow.s03,
						},
						input: {
							caretColor: palette.primary.blue003,
						},
					},
					'.MuiInputLabel-root': {
						marginLeft: -14,
						marginTop: -9,
					},
					'.MuiButtonBase-root': {
						color: palette.base.dark,
					},
					'.Mui-disabled': {
						'.MuiButtonBase-root': {
							color: palette.base.lightGray,
						},
						'&:hover': {
							boxShadow: 'none',
						},
					},
				},
			},
		},
	},
});

export default theme;
