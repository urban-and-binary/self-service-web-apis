import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@mui/material';

export const Input = ({ componentType, field, form: { touched, errors }, ...props }) => {
	const error = touched[field.name] && errors[field.name] ? true : false;
	const helperText = error ? errors[field.name] : null;
	const Component = componentType || TextField;
	field.value = field.value || '';

	return (
		<Component error={error} helperText={helperText} {...field} {...props}>
			{props.children}
		</Component>
	);
};

Input.propTypes = {
	componentType: PropTypes.node,
	field: PropTypes.object.isRequired,
	form: PropTypes.object.isRequired,
	children: PropTypes.node,
	style: PropTypes.object,
};
