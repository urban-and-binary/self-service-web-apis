import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Divider } from '@mui/material';
import { REGEX } from '@constants/general';

const Container = styled('div')(() => ({
	display: 'flex',
}));
const Input = styled('input')(({ theme }) => ({
	border: 'none',
	height: 48,
	width: '100%',
	background: theme.palette.base.background,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
	borderRadius: theme.spacing(2),
	padding: '5px 16px',
	paddingRight: theme.spacing(8),
	caretColor: theme.palette.primary.blue003,
	'::placeholder': {
		color: theme.palette.base.lightGray,
	},
	'&:focus': {
		outline: `2px solid ${theme.palette.primary.blue003} !important`,
	},
}));
const Symbol = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	padding: '5px 0',
	marginLeft: theme.spacing(-8),
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const StyledDivider = styled(Divider)(({ theme }) => ({
	borderColor: theme.palette.base.inactive,
}));

export const MeterInput = ({ id, value, onChange, placeholder, symbol, ...props }) => (
	<Container {...props}>
		<Input
			id={id}
			name={id}
			value={value}
			onChange={onChange}
			type="text"
			placeholder={placeholder}
			onKeyPress={event => {
				if (!RegExp(REGEX.NUMBER).test(event.key)) {
					event.preventDefault();
				}
			}}
		/>
		<Symbol>
			<StyledDivider orientation="vertical" sx={{ mr: 1, ml: 1 }} />
			{symbol}
		</Symbol>
	</Container>
);

MeterInput.propTypes = {
	id: PropTypes.string,
	value: PropTypes.string,
	symbol: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
};
