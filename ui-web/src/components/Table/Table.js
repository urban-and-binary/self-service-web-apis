import React, { Fragment } from 'react';
import { styled } from '@mui/system';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Divider, Paper } from '@mui/material';
import { ChevronIcon } from '@assets/icons';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Spinner, StyledPagination } from '@components';

const StyledContainer = styled('div')(() => ({
	marginBottom: 120,
}));

const StyledTableContainer = styled(TableContainer)(({ theme }) => ({
	borderRadius: theme.shape.br2,
	padding: '38px 22px 8px 22px',
	boxShadow: theme.shadow.s02,
	'&:before, &:after': {
		display: 'none',
	},
	margin: '34px 0',
}));

const StyledTable = styled(Table)(({ theme }) => ({
	lineHeight: theme.typography.lineHeight.small,
}));

const StyledTableHead = styled(TableHead)(() => ({
	'& th': {
		paddingLeft: 15,
	},
	'& th:first-of-type': {
		paddingLeft: 33,
	},
}));

const StyledTHCell = styled(TableCell)(({ theme }) => ({
	color: theme.palette.base.gray,
	fontWeight: theme.typography.fontWeight.bold,
	fontSize: theme.typography.fontSize.small,
	borderBottom: 'none',
	padding: 0,
	paddingBottom: 13,
}));

const StyledTHRow = styled(TableRow)(() => ({
	height: 0,
}));

const StyledTableRow = styled(TableRow)(({ theme, unread }) => ({
	'&:hover': {
		backgroundColor: theme.palette.primary.blue005,
		borderRadius: theme.shape.br2,
		boxShadow: theme.shadow.s01,
		fontWeight: theme.typography.fontWeight.bold,
	},
	'&:hover > td': {
		fontWeight: theme.typography.fontWeight.m,
	},
	cursor: 'pointer',
	'& td': {
		border: 'none',
		fontWeight: unread === 'true' ? theme.typography.fontWeight.bold : theme.typography.fontWeight.r,
	},
	'& td:first-of-type': {
		borderRadius: `${theme.shape.br2} 0 0 ${theme.shape.br2}`,
		paddingLeft: 33,
	},
	'& td:last-of-type': {
		borderRadius: `0 ${theme.shape.br2} ${theme.shape.br2} 0`,
	},
}));

const StyledBodyCell = styled(TableCell)(() => ({
	padding: '28px 15px 28px 15px',
}));

const StyledChevronIcon = styled(ChevronIcon)(({ theme }) => ({
	width: 8,
	'& path': {
		stroke: theme.palette.base.gray,
		strokeWidth: 3,
	},
}));

const StyledChevronIcon2 = styled(ChevronIcon)(({ theme }) => ({
	color: theme.palette.base.lightGray,
	transform: 'rotate(-90deg)',
}));

const Sort = styled('div')(() => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	cursor: 'pointer',
}));

const SortIcon = styled('div')(() => ({
	display: 'flex',
	flexDirection: 'column',
	marginLeft: 7.5,
}));

const StyledDivider = styled(Divider)(() => ({
	marginTop: 8,
	marginBottom: 8,
}));

const SpinnerContainer = styled('div')(() => ({
	height: '150px',
	display: 'flex',
	justifyContent: 'center',
}));

const BasicTable = ({ tableHeadCells, dataList, onRowPress, onSort, page, onPageChange, pageCount, loading, markUnread }) => {
	const tableBodyRows = dataList.map(item => Object.values(item).slice(0, tableHeadCells.length));

	const { t } = useTranslation();

	return (
		<StyledContainer>
			<StyledTableContainer component={Paper}>
				<StyledTable>
					<StyledTableHead>
						<StyledTHRow>
							{tableHeadCells.map(cell => (
								<StyledTHCell key={cell.key} width={cell.width}>
									{cell.sorting ? (
										<Sort onClick={() => onSort(cell.key)}>
											{t(cell.name)}
											<SortIcon>
												{cell.sorting === 1 ? (
													<StyledChevronIcon />
												) : (
													<>
														{cell.sorting === -1 && (
															<StyledChevronIcon style={{ transform: 'rotate(180deg)' }} />
														)}
													</>
												)}
											</SortIcon>
										</Sort>
									) : (
										t(cell.name)
									)}
								</StyledTHCell>
							))}
							<StyledTHCell width={'4%'}></StyledTHCell>
						</StyledTHRow>
					</StyledTableHead>
					<TableBody>
						{loading ? (
							<tr>
								<td colSpan="100%">
									<SpinnerContainer>
										<Spinner />
									</SpinnerContainer>
								</td>
							</tr>
						) : (
							<>
								{tableBodyRows.map((rows, idx) => (
									<Fragment key={`${idx}a`}>
										<StyledTableRow
											onClick={() => onRowPress(dataList[idx])}
											unread={markUnread && !dataList[idx].read ? 'true' : 'false'}>
											{rows.map((row, idx) => (
												<StyledBodyCell key={`${idx}b`}>{row}</StyledBodyCell>
											))}
											<StyledBodyCell align="left">
												<StyledChevronIcon2 sx={{ fontSize: 14, mt: 1 }} />
											</StyledBodyCell>
										</StyledTableRow>
										{idx + 1 !== tableBodyRows.length && (
											<TableRow sx={{ '& td': { border: 0 } }}>
												<TableCell sx={{ padding: 0 }} colSpan={rows.length + 1}>
													<StyledDivider />
												</TableCell>
											</TableRow>
										)}
									</Fragment>
								))}
							</>
						)}
					</TableBody>
				</StyledTable>
			</StyledTableContainer>
			<StyledPagination count={pageCount} page={page} onChange={onPageChange} />
		</StyledContainer>
	);
};

export default BasicTable;

BasicTable.propTypes = {
	tableHeadCells: PropTypes.array,
	dataList: PropTypes.array,
	onRowPress: PropTypes.func,
	onSort: PropTypes.func,
	page: PropTypes.number,
	onPageChange: PropTypes.func,
	pageCount: PropTypes.number,
	loading: PropTypes.bool,
	markUnread: PropTypes.bool,
};
