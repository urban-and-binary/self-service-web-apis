import React, { useState } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Grid, MenuItem, Button, Select } from '@mui/material';
import { ChevronIcon } from '@assets/icons';
import PropTypes from 'prop-types';

const StyledChevronIcon = styled(ChevronIcon)(() => ({
	width: 8,
	marginRight: 24,
}));
const StyledSelect = styled(Select)(({ theme }) => ({
	width: 250,
	height: 52,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
	borderRadius: theme.shape.br2,
	backgroundColor: theme.palette.base.background,
	'& .MuiOutlinedInput-notchedOutline': {
		border: 'none',
	},
	'&:hover': {
		backgroundColor: theme.palette.primary.blue003,
		boxShadow: theme.shadow.s02,
	},
}));
const StyledMenuItem = styled(MenuItem)(({ theme }) => ({
	fontSize: theme.typography.fontSize.small,
	'&:selected': {
		backgroundColor: theme.palette.primary.blue005,
	},
	'&:hover': {
		backgroundColor: theme.palette.primary.blue003,
	},
}));
const StyledButton = styled(Button)(({ theme, active }) => ({
	borderRadius: theme.shape.br2,
	height: 36,
	whiteSpace: 'nowrap',
	fontSize: theme.typography.fontSize.small,
	backgroundColor: active ? theme.palette.primary.blue003 : theme.palette.base.background,
	textTransform: 'none',
	boxShadow: active ? theme.shadow.s02 : 'none',
	color: active ? theme.palette.base.white : theme.palette.base.black,
	'&:hover': {
		backgroundColor: theme.palette.primary.blue003,
		boxShadow: theme.shadow.s02,
		color: theme.palette.base.white,
	},
	padding: '8px 40px 8px 40px',
}));

export const Filterbar = ({ selectMenuItems, onItemSelect, selectors }) => {
	const { t } = useTranslation();

	const [selectorInd, setSelectorInd] = useState(0);

	const onClickSelector = (action, idx) => {
		setSelectorInd(idx);
		action();
	};

	return (
		<Grid container alignItems="center" rowSpacing={2} columnSpacing={5}>
			<Grid item>
				<StyledSelect
					IconComponent={StyledChevronIcon}
					sx={{ mt: 1 }}
					defaultValue={selectMenuItems[0]?.value}
					onChange={onItemSelect}>
					{selectMenuItems.map((item, idx) => (
						<StyledMenuItem key={idx} value={item.value}>
							{item.title}
						</StyledMenuItem>
					))}
				</StyledSelect>
			</Grid>
			<Grid item>
				{selectors.map((selector, idx) => (
					<StyledButton
						key={idx}
						active={idx === selectorInd ? 'true' : null}
						onClick={() => onClickSelector(selector.action, idx)}
						variant="contained"
						sx={{ mr: 2, mt: 1 }}>
						{t(selector.title)}
					</StyledButton>
				))}
			</Grid>
		</Grid>
	);
};

Filterbar.propTypes = {
	selectMenuItems: PropTypes.array.isRequired,
	onItemSelect: PropTypes.func,
	selectors: PropTypes.array.isRequired,
	inTwoLines: PropTypes.bool,
};
