import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '@mui/material/Pagination';
import { styled } from '@mui/system';
import Stack from '@mui/material/Stack';

const StyledPag = styled(Pagination)(({ theme }) => ({
	alignItem: 'flex-end',
	'& > *': {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	'& ul li:first-of-type, ul li:last-of-type': {
		display: 'none',
	},
	'& .MuiButtonBase-root': {
		color: theme.palette.base.lightGray,
	},
	'& .Mui-selected': {
		color: theme.palette.primary.blue003,
		borderRadius: theme.spacing(1),
		boxShadow: theme.shadow.s03,
	},
	'& .MuiPaginationItem-root.Mui-selected': {
		backgroundColor: theme.palette.base.white,
	},
}));

export const StyledPagination = props => <Stack spacing={2}>{!!props.count && <StyledPag {...props} shape="rounded" />}</Stack>;

StyledPagination.propTypes = {
	count: PropTypes.number,
};
