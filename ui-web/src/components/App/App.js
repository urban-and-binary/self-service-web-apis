import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import i18next from 'i18next';
import {
	Login,
	LoginError,
	Onboarding,
	Settings,
	Profile,
	Home,
	Requests,
	Tasks,
	PasswordReset,
	PasswordChange,
	Unit,
	Faq,
	Announcements,
	Declaration,
	PollsAndVoting,
	SinglePoll,
} from '@pages';
import { Layout, RequireLogin, RequirePermission, RequireOnboarding, RequireNotOnboarded } from '@components';
import { permissions } from '@common/permissions';

export const App = () => {
	useEffect(() => {
		if (localStorage.getItem('i18nextLng')?.length > 2) {
			i18next.changeLanguage('en');
		}
	}, []);

	return (
		<Routes>
			{/* Public routes */}
			<Route path="/signup" element={<Login url="signup" />} />
			<Route path="/login" element={<Login url="login" />} />
			<Route path="/error" element={<LoginError />} />
			<Route path="/password-reset" element={<PasswordReset />} />
			<Route path="/password-change" element={<PasswordChange />} />

			{/* Logged in routes */}
			<Route element={<RequireLogin />}>
				<Route element={<RequireOnboarding />}>
					<Route path="/" element={<Layout />}>
						<Route path="/" element={<Home />} />
						<Route path="/faq" element={<Faq />} />
						<Route path="/settings" element={<Settings />} />
						<Route path="/profile" element={<Profile />} />
						<Route path="/announcements" element={<Announcements />} />
						<Route path="/declarations" element={<Declaration />} />
						<Route path="/polls-voting">
							<Route path=":type" element={<SinglePoll />} />
							<Route path="" element={<PollsAndVoting />} />
						</Route>

						{/* Protected routes */}
						<Route element={<RequirePermission accessPerms={[permissions.requests.read]} />}>
							<Route path="/requests" element={<Requests />} />
						</Route>
						<Route element={<RequirePermission accessPerms={[permissions.tasks.read]} />}>
							<Route path="/tasks" element={<Tasks />} />
						</Route>
						<Route element={<RequirePermission accessPerms={[permissions.units.read]} />}>
							<Route path="/unit/:id" element={<Unit />} />
						</Route>
					</Route>
				</Route>
				<Route element={<RequireNotOnboarded />}>
					<Route path="/onboarding" element={<Onboarding />} />
				</Route>
			</Route>

			<Route path="*" element={'Page not found'} />
		</Routes>
	);
};
