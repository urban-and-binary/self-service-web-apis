import React, { useMemo } from 'react';
import { Grid, Divider } from '@mui/material';
import { styled } from '@mui/system';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import { ChevronIcon } from '@assets/icons';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const Content = styled('div')(({ theme }) => ({
	padding: '0 32px 0 32px',
	cursor: 'pointer',
	'&.selected': {
		padding: 24,
		borderRadius: 16,
		backgroundColor: theme.palette.primary.blue004,
		boxShadow: `0px 17px 0px -7px ${theme.palette.primary.blue005}`,
	},
}));
const StyledTableRow = styled(TableRow)(({ theme, unread }) => ({
	'& td': {
		border: 'none',
		fontWeight: unread === 'true' ? theme.typography.fontWeight.bold : theme.typography.fontWeight.r,
	},
}));

const StyledTableCell = styled(TableCell)(() => ({
	padding: 0,
	verticalAlign: 'top',
	paddingBottom: 24,
}));
const TableText = styled('p')(({ theme }) => ({
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
	color: theme.palette.base.gray,
	margin: 0,
}));

const StyledChevronIcon = styled(ChevronIcon)(({ theme }) => ({
	transform: 'rotate(-90deg)',
	color: theme.palette.base.lightGray,
	'&.rotate': {
		transform: 'rotate(90deg)',
		color: theme.palette.base.dark,
	},
}));
const ChipChevronGrid = styled(Grid)(() => ({
	display: 'flex',
	alignItems: 'center',
}));

const ChevronGrid = styled(Grid)(() => ({
	display: 'flex',
	justifyContent: 'flex-end',
}));

const TitleGrid = styled(Grid)(({ theme, unread }) => ({
	fontWeight: unread === 'true' ? theme.typography.fontWeight.bold : theme.typography.fontWeight.r,
}));

const SimpleRow = styled('div')(({ theme, unread }) => ({
	fontWeight: unread === 'true' ? theme.typography.fontWeight.bold : theme.typography.fontWeight.r,
}));

const CardListItem = ({ index, selectedId, listCardMap, data, onClick, markUnread }) => {
	const { t } = useTranslation();

	const cardMapRows = useMemo(() => {
		// filtering data with existing values mentioned in listCardMap
		const avCardRows = listCardMap.content.filter(dataMap => data[dataMap.dataKey]);

		const labeledCardRows = [];
		const cardRows = [];

		avCardRows.forEach(dataMap => {
			if (dataMap.label) {
				labeledCardRows.push(dataMap);
			} else {
				cardRows.push(dataMap);
			}
		});

		return {
			labeledCardRows,
			cardRows,
		};
	}, [listCardMap]); // eslint-disable-line react-hooks/exhaustive-deps

	const unread = markUnread && !data.read ? 'true' : 'false';

	return (
		<>
			<Content onClick={() => onClick(index)} className={selectedId === data._id && 'selected'}>
				<TitleGrid container sx={{ mb: 3 }} unread={unread}>
					<Grid item xs={8}>
						{data[listCardMap.titleKey]}
					</Grid>
					<Grid item xs={4}>
						<ChipChevronGrid container>
							<Grid item xs={8}>
								{data[listCardMap.subTitleKey]}
							</Grid>
							<ChevronGrid item xs={4}>
								<StyledChevronIcon sx={{ fontSize: 14 }} className={selectedId === data._id && 'rotate'} />
							</ChevronGrid>
						</ChipChevronGrid>
					</Grid>
				</TitleGrid>

				{!!cardMapRows.labeledCardRows?.length && (
					<Table>
						<TableBody>
							{cardMapRows.labeledCardRows.map((rowMap, index) => (
								<StyledTableRow key={`card-list-item-labeled-${index}`} unread={unread}>
									<StyledTableCell>
										<TableText>{t(rowMap.label)}:</TableText>
									</StyledTableCell>
									<StyledTableCell>{data[rowMap.dataKey]}</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				)}
				{cardMapRows.cardRows?.map((rowMap, index) => (
					<SimpleRow key={`card-list-item-${index}`} unread={unread}>
						{data[rowMap.dataKey]}
					</SimpleRow>
				))}
			</Content>
			<Divider style={{ margin: '24px 0 24px 0' }} />
		</>
	);
};

CardListItem.propTypes = {
	data: PropTypes.object,
	index: PropTypes.number,
	selectedId: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
	onClick: PropTypes.func,
	listCardMap: PropTypes.shape({
		titleKey: PropTypes.string.isRequired,
		subTitleKey: PropTypes.string,
		content: PropTypes.arrayOf(
			PropTypes.shape({
				dataKey: PropTypes.string.isRequired,
				label: PropTypes.string,
			}),
		),
	}).isRequired,
	markUnread: PropTypes.bool,
};

export default CardListItem;
