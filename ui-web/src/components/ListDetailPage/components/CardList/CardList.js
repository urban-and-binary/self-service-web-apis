import React from 'react';
import PropTypes from 'prop-types';
import CardListItem from './components/CardListItem';
import { styled } from '@mui/system';
import { Spinner, StyledPagination } from '@components';

const Container = styled('div')(() => ({
	margin: '40px 24px 0 0',
}));

const CardListContainer = styled('div')(() => ({
	marginBottom: 98,
}));

const SpinnerContainer = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'center',
}));

const CardList = ({ selectedId, dataList, listCardMap, onCardPress, page, onPageChange, pageCount, loading, markUnread }) => {
	return (
		<Container>
			<CardListContainer>
				{loading ? (
					<SpinnerContainer>
						<Spinner />
					</SpinnerContainer>
				) : (
					<>
						{dataList.map((item, idx) => (
							<CardListItem
								key={idx}
								index={idx}
								markUnread={markUnread}
								onClick={() => onCardPress(item)}
								selectedId={selectedId}
								listCardMap={listCardMap}
								data={item}
							/>
						))}
					</>
				)}
			</CardListContainer>
			<StyledPagination count={pageCount} page={page} onChange={onPageChange} />
		</Container>
	);
};

CardList.propTypes = {
	selectedId: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
	dataList: PropTypes.array,
	onCardPress: PropTypes.func,
	listCardMap: PropTypes.shape({
		titleKey: PropTypes.string.isRequired,
		subTitleKey: PropTypes.string,
		content: PropTypes.arrayOf(
			PropTypes.shape({
				dataKey: PropTypes.string.isRequired,
				label: PropTypes.string,
			}),
		),
	}).isRequired,
	page: PropTypes.number,
	onPageChange: PropTypes.func,
	pageCount: PropTypes.number,
	loading: PropTypes.bool,
	markUnread: PropTypes.bool,
};

export default CardList;
