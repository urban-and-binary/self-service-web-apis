import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Divider, IconButton, Table, TableBody, TableCell, TableRow } from '@mui/material';
import File from './components/File';
import { CloseIcon } from '@assets/icons';

const Container = styled('div')(({ theme }) => ({
	borderRadius: theme.shape.br2,
	boxShadow: theme.shadow.s02,
	color: theme.palette.base.dark,
	maxHeight: 'calc(100vh - 255px)',
	overflow: 'auto',
}));
const Content = styled('div')(() => ({
	padding: '0 32px 32px 24px',
}));
const Header = styled('div')(() => ({
	display: 'flex',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	width: '100%',
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const CloseButton = styled(IconButton)(({ theme }) => ({
	display: 'flex',
	alignItems: 'start',
	color: theme.palette.base.gray,
	'&:hover': {
		backgroundColor: 'unset',
		color: theme.palette.base.dark,
	},
}));
const SubTitle = styled('div')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
}));
const Files = styled('div')(() => ({
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'flex-start',
}));

const StyledTableCell = styled(TableCell)(() => ({
	padding: 0,
	border: 'none',
	verticalAlign: 'top',
	paddingBottom: 32,
}));
const TableText = styled('p')(({ theme }) => ({
	marginTop: 2,
	marginRight: 24,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));

const TableTextValue = styled('div')(() => ({
	lineHeight: 'unset',
}));

const TitleContainer = styled('div')(() => ({
	padding: '24px 0 0 24px',
}));

const Row = ({ title, text, innerHtml }) => (
	<TableRow>
		<StyledTableCell>
			<TableText>{title}:</TableText>
		</StyledTableCell>
		<StyledTableCell>
			<TableTextValue>
				{innerHtml ? <div style={{ lineHeight: 'unset' }} dangerouslySetInnerHTML={{ __html: text }} /> : text}
			</TableTextValue>
		</StyledTableCell>
	</TableRow>
);

const CardDetail = ({ selectedItem, detailCardMap, onClose, ...props }) => {
	const { t } = useTranslation();
	const ref = useRef(null);

	const scrollTop = () => {
		ref.current.scrollTo({
			top: 0,
			behavior: 'smooth',
		});
	};

	useEffect(() => {
		scrollTop();
	});

	return (
		<Container ref={ref} {...props}>
			{selectedItem && (
				<>
					<Header>
						<TitleContainer>
							<Title>{selectedItem[detailCardMap.titleKey]}</Title>
							{detailCardMap.subTitleKey && selectedItem[detailCardMap.subTitleKey] && (
								<SubTitle>{selectedItem[detailCardMap.subTitleKey]}</SubTitle>
							)}
						</TitleContainer>
						<CloseButton aria-label="close" onClick={onClose} sx={{ padding: 4 }}>
							<CloseIcon />
						</CloseButton>
					</Header>

					<Content sx={{ mt: 1 }}>
						{detailCardMap.contentHeaderKey && selectedItem[detailCardMap.contentHeaderKey] && (
							<SubTitle sx={{ mt: 2 }}>{selectedItem[detailCardMap.contentHeaderKey]}</SubTitle>
						)}

						<Table sx={{ mt: 5 }}>
							<TableBody>
								{detailCardMap.content
									.filter(detMap => selectedItem[detMap.dataKey])
									.map((detMap, index) => (
										<Row
											key={`card-detail-row-${index}`}
											title={t(detMap.label)}
											text={selectedItem[detMap.dataKey]}
											innerHtml={detMap.innerHtml}
										/>
									))}
							</TableBody>
						</Table>
						<Divider sx={{ mt: 4, mb: 3 }} />
						<Files>
							{selectedItem.documents && selectedItem.documents.map(document => <File key={document.Id} file={document} />)}
						</Files>
					</Content>
				</>
			)}
		</Container>
	);
};

Row.propTypes = {
	title: PropTypes.string,
	text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
	innerHtml: PropTypes.bool,
};

CardDetail.propTypes = {
	selectedItem: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
	onClose: PropTypes.func,
	detailCardMap: PropTypes.shape({
		titleKey: PropTypes.string.isRequired,
		subTitleKey: PropTypes.string,
		contentHeaderKey: PropTypes.string,
		content: PropTypes.arrayOf(
			PropTypes.shape({
				dataKey: PropTypes.string.isRequired,
				label: PropTypes.string.isRequired,
				innerHtml: PropTypes.bool,
			}),
		),
	}).isRequired,
};

export default CardDetail;
