import React from 'react';
import { ClipIcon, ImageIcon } from '@assets/icons';
import { styled } from '@mui/system';
import PropTypes from 'prop-types';
import mime from 'mime-types';
import { ViewDownloadApiFile } from '@components';

const Container = styled('div')(({ theme }) => ({
	display: 'inline-flex',
	alignItems: 'center',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.m,
	backgroundColor: theme.palette.base.background,
	borderRadius: 4,
	marginBottom: 16,
	padding: '8px 18px 8px 18px',
}));

const FileTitle = styled('div')(() => ({
	marginRight: '15px',
}));

const File = ({ file }) => {
	const mimeType = mime.lookup(file.FileExtension);

	return (
		<Container>
			{mimeType.includes('image') ? <ImageIcon style={{ marginRight: 10 }} /> : <ClipIcon style={{ marginRight: 10 }} />}
			<FileTitle>{file.Title}</FileTitle>
			<ViewDownloadApiFile defaultDocument={file} />
		</Container>
	);
};

export default File;

File.propTypes = {
	file: PropTypes.shape({
		Id: PropTypes.string.isRequired,
		Title: PropTypes.string,
		FileExtension: PropTypes.string.isRequired,
		ContentSize: PropTypes.number.isRequired,
		documentApiUrl: PropTypes.string.isRequired,
	}).isRequired,
};
