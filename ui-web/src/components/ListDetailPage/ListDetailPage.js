import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { Grid, Divider, useMediaQuery } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { Filterbar, Table, Dialog } from '@components';
import CardDetail from './components/CardDetail/CardDetail';
import CardList from './components/CardList/CardList';

const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h1,
	fontWeight: theme.typography.fontWeight.bold,
}));

export const ListDetailPage = ({
	title,
	selectMenuItems,
	onItemSelect,
	selectors,
	tableHeadCells,
	data,
	listCardMap,
	detailCardMap,
	onSort,
	onPageChange,
	pageCount,
	page,
	listLoading,
	onDataItemSelect,
	defSelItem,
	markUnread,
}) => {
	const theme = useTheme();
	const { t } = useTranslation();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	const [selRow, setSelRow] = useState(false);

	const handleRowSelect = item => {
		onDataItemSelect && onDataItemSelect(item);
		setSelRow(item);
	};

	const handleClose = () => {
		handleRowSelect(false);
	};

	useEffect(() => {
		if (defSelItem) {
			setSelRow(defSelItem);
		}
	}, [defSelItem]);

	const tableView = selRow === false && !isTablet;

	return (
		<Grid container pl={3} pr={3}>
			<Grid item xs={tableView || isTablet ? 12 : 6}>
				<Title sx={{ mb: 3 }}>{t(title)}</Title>
				<Filterbar selectMenuItems={selectMenuItems} selectors={selectors} onItemSelect={onItemSelect} />
				{tableView ? (
					<Table
						tableHeadCells={tableHeadCells}
						dataList={data}
						onRowPress={handleRowSelect}
						onSort={onSort}
						page={page}
						onPageChange={onPageChange}
						pageCount={pageCount}
						loading={listLoading}
						markUnread={markUnread}
					/>
				) : (
					<CardList
						selectedId={selRow._id}
						dataList={data}
						listCardMap={listCardMap}
						onCardPress={handleRowSelect}
						page={page}
						onPageChange={onPageChange}
						pageCount={pageCount}
						loading={listLoading}
						markUnread={markUnread}
					/>
				)}
			</Grid>
			{selRow && (
				<>
					{!isTablet && <Divider orientation="vertical" flexItem style={{ marginRight: '-1px' }} />}
					{!isTablet ? (
						<Grid item xs={6}>
							<CardDetail
								selectedItem={selRow}
								detailCardMap={detailCardMap}
								onClose={handleClose}
								sx={{ position: 'sticky', ml: 2, mr: 2, top: '20px' }}
							/>
						</Grid>
					) : (
						<Dialog open={selRow !== false} onClose={handleClose} container>
							<CardDetail selectedItem={selRow} detailCardMap={detailCardMap} onClose={handleClose} />
						</Dialog>
					)}
				</>
			)}
		</Grid>
	);
};

ListDetailPage.propTypes = {
	title: PropTypes.string.isRequired,
	selectMenuItems: PropTypes.arrayOf(
		PropTypes.shape({
			title: PropTypes.string.isRequired,
			value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
		}),
	),
	onItemSelect: PropTypes.func,
	selectors: PropTypes.arrayOf(
		PropTypes.shape({
			title: PropTypes.string.isRequired,
			action: PropTypes.func.isRequired,
		}),
	),
	tableHeadCells: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string.isRequired,
			width: PropTypes.string,
			sorting: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
		}),
	).isRequired,
	data: PropTypes.array.isRequired,
	listCardMap: PropTypes.shape({
		titleKey: PropTypes.string.isRequired,
		subTitleKey: PropTypes.string,
		content: PropTypes.arrayOf(
			PropTypes.shape({
				dataKey: PropTypes.string.isRequired,
				label: PropTypes.string,
			}),
		),
	}).isRequired,
	detailCardMap: PropTypes.shape({
		titleKey: PropTypes.string.isRequired,
		subTitleKey: PropTypes.string,
		contentHeaderKey: PropTypes.string,
		content: PropTypes.arrayOf(
			PropTypes.shape({
				dataKey: PropTypes.string.isRequired,
				label: PropTypes.string.isRequired,
				innerHtml: PropTypes.bool,
			}),
		),
	}).isRequired,
	onSort: PropTypes.func,
	onPageChange: PropTypes.func,
	pageCount: PropTypes.number,
	page: PropTypes.number,
	listLoading: PropTypes.bool,
	onDataItemSelect: PropTypes.func,
	defSelItem: PropTypes.object,
	markUnread: PropTypes.bool,
};
