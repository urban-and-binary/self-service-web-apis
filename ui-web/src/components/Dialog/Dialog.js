import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { Dialog as Component, IconButton, Fade, Backdrop, Box } from '@mui/material';
import { CloseIcon } from '@assets/icons';
import { hextorgb } from '@utils/utils';

const Header = styled('div')(() => ({
	display: 'flex',
	height: 80,
}));
const TitleWrapper = styled('div')(() => ({
	padding: '24px 32px 0 32px',
	width: '100%',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Subtitle = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const CloseButton = styled(IconButton)(({ theme }) => ({
	color: theme.palette.base.gray,
	'&:hover': {
		backgroundColor: 'unset',
		color: theme.palette.base.dark,
	},
}));
const StyledBox = styled(Box)(({ theme, fullScreen }) => ({
	maxWidth: fullScreen ? 'unset' : 800,
	overflow: 'auto',
	backgroundColor: theme.palette.base.white,
}));

export const Dialog = ({ children, title, subtitle, container, open, onClose, showClose, fullScreen, ...props }) => {
	const theme = useTheme();

	return (
		<Component
			fullScreen={fullScreen}
			aria-labelledby="transition-modal-title"
			aria-describedby="transition-modal-description"
			open={open}
			onClose={onClose}
			closeAfterTransition
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 200,
			}}
			sx={{
				'.MuiDialog-paper': {
					borderRadius: '16px',
				},
				'.MuiBackdrop-root': {
					backgroundColor: `${hextorgb(theme.palette.base.lightGray, 0.65)}`,
				},
				backdropFilter: 'blur(5px)',
			}}
			{...props}>
			<Fade in={open}>
				<StyledBox fullScreen={fullScreen}>
					{!container && (
						<Header>
							<TitleWrapper>
								{title && <Title>{title}</Title>}
								{subtitle && <Subtitle>{subtitle}</Subtitle>}
							</TitleWrapper>

							{showClose && (
								<CloseButton aria-label="close" onClick={onClose} sx={{ padding: 4 }}>
									<CloseIcon />
								</CloseButton>
							)}
						</Header>
					)}
					{children}
				</StyledBox>
			</Fade>
		</Component>
	);
};

Dialog.propTypes = {
	title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
	subtitle: PropTypes.string,
	open: PropTypes.bool.isRequired,
	showClose: PropTypes.bool,
	container: PropTypes.bool,
	onClose: PropTypes.func.isRequired,
	children: PropTypes.node.isRequired,
	fullScreen: PropTypes.bool,
};
