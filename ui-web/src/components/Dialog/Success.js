import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { GreenCharacterIcon } from '@assets/icons';

const Container = styled('div')(() => ({
	maxWidth: 500,
	display: 'flex',
	flexDirection: 'column',
	padding: '52px 32px',
	alignItems: 'center',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	textAlign: 'center',
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	textAlign: 'center',
	color: theme.palette.base.main,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.r,
}));
const Content = styled('div')(({ theme }) => ({
	p: {
		color: theme.palette.base.main,
		fontSize: theme.typography.fontSize.base,
		fontWeight: theme.typography.fontWeight.r,
	},
	span: {
		color: theme.palette.primary.blue002,
	},
}));

export const SuccessMessage = ({ children, title, text, ...props }) => {
	return (
		<Container {...props}>
			<GreenCharacterIcon width="200" />
			<Title>{title}</Title>
			<Text>{text}</Text>
			<Content>{children}</Content>
		</Container>
	);
};

SuccessMessage.propTypes = {
	title: PropTypes.string,
	text: PropTypes.string,
	children: PropTypes.node,
};
