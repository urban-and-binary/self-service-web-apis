import React, { useEffect } from 'react';
import { styled } from '@mui/system';
import { Outlet } from 'react-router-dom';
import Navbar from './Navbar/Navbar';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import { useNotification } from '@hooks/useNotification';

const Container = styled('div')(() => ({
	display: 'flex',
}));

const Content = styled('div')(() => ({
	width: '100%',
	display: 'flex',
	flexDirection: 'column',
}));
const Page = styled('div')(() => ({
	minHeight: 'calc(100vh - 255px)',
}));

export const Layout = () => {
	const { loadNotifs } = useNotification();

	useEffect(() => {
		// loading notification data here
		// because this is called for all routes for a
		// fully logged in user
		loadNotifs();
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<Container>
			<Navbar />
			<Content>
				<Header />
				<Page>
					<Outlet />
				</Page>
				<Footer />
			</Content>
		</Container>
	);
};
