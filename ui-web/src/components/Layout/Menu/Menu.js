import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import { Divider, Box } from '@mui/material';
import { LogoIcon, BurgerIcon } from '@assets/icons';
import { primary, secondary } from './data';
import { useNotification } from '@hooks/useNotification';

const Container = styled('div')(({ theme }) => ({
	backgroundColor: theme.palette.base.white,
	borderRight: `1px solid ${theme.palette.base.inactive}`,
	'& img': {
		display: 'flex',
		margin: 'auto',
	},
}));
const MenuButton = styled('button')(({ theme }) => ({
	border: 0,
	background: 'unset',
	cursor: 'pointer',
	color: theme.palette.base.gray,
}));
const Button = styled(Link)(({ theme, type }) => ({
	fontSize: theme.typography.fontSize.small,
	fontWeight: type === 'active' ? theme.typography.fontWeight.bold : theme.typography.fontWeight.m,
	color: type === 'active' ? theme.palette.base.white : theme.palette.base.gray,
	backgroundColor: type === 'active' ? theme.palette.primary.blue002 : '',
	display: 'flex',
	justifyContent: 'left',
	alignItems: 'center',
	padding: '16px 20px',
	margin: '16px 0',
	borderRadius: theme.shape.br1,
	textDecoration: 'none',
	'& svg': {
		fontSize: 20,
	},
}));
const Label = styled('p')(() => ({
	margin: 0,
	minWidth: '130px',
}));
const Badge = styled('span')(({ theme }) => ({
	borderRadius: '50%',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
	backgroundColor: theme.palette.default.error,
	color: 'white',
	minWidth: 18,
	minHeight: 18,
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	'&.topLeft': {
		position: 'absolute',
		marginLeft: '-10px',
		marginTop: '-20px',
	},
}));
const StyledDivider = styled(Divider)(() => ({
	margin: '40px 20px 34px 20px',
}));
const Count = styled('div')(() => ({
	marginTop: '1px',
}));

const NavButton = ({ button, showLabel = true, count }) => {
	const location = useLocation();
	const { t } = useTranslation();

	return (
		<Button to={button.url} type={button.url === location.pathname ? 'active' : ''}>
			{button.icon}
			{showLabel && <Label sx={{ ml: 2, mr: 1 }}>{t(button.label)}</Label>}
			{!!count && (
				<Badge className={!showLabel && 'topLeft'}>
					<Count>{count > 9 ? '9+' : count}</Count>
				</Badge>
			)}
		</Button>
	);
};

export const Menu = ({ onClick, small, showLabel }) => {
	const { notifs } = useNotification();

	return (
		<Container sx={{ mt: 4, mb: 4, pr: 2, pl: small ? 2 : 4 }}>
			{small ? (
				<MenuButton onClick={onClick} sx={{ p: 4 }}>
					<BurgerIcon sx={{ fontSize: 20 }} />
				</MenuButton>
			) : (
				<Box sx={{ p: 2, mb: 4 }}>
					<Link to="/">
						<LogoIcon width="143" />
					</Link>
				</Box>
			)}
			{primary.map((item, idx) => (
				<NavButton button={item} showLabel={showLabel} key={idx} />
			))}
			<StyledDivider />
			{secondary.map((item, idx) => (
				<NavButton button={item} showLabel={showLabel} count={item.key === 'announcements' ? notifs?.length : 0} key={idx} />
			))}
		</Container>
	);
};

Menu.propTypes = {
	onClick: PropTypes.func,
	showLabel: PropTypes.bool,
	small: PropTypes.bool.isRequired,
};

NavButton.propTypes = {
	button: PropTypes.object,
	showLabel: PropTypes.bool,
	count: PropTypes.number,
};
