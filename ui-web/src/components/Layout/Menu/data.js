import React from 'react';
import {
	HomeIcon,
	BillsIcon,
	RequestsIcon,
	TasksIcon,
	DeclarationsIcon,
	PoolsIcon,
	DocumentsIcon,
	EasyPandaIcon,
	AnnouncementsIcon,
	SettingsIcon,
} from '@assets/icons';

export const primary = [
	{ url: '/', label: 'HomePage', icon: <HomeIcon />, permissions: [] },
	{ url: '/bills', label: 'BillsAndPayments', icon: <BillsIcon />, permissions: [] },
	{ url: '/requests', label: 'Requests', icon: <RequestsIcon />, permissions: [] },
	{ url: '/tasks', label: 'Tasks', icon: <TasksIcon />, permissions: [] },
	{ url: '/declarations', label: 'MetersDeclaration', icon: <DeclarationsIcon />, permissions: [] },
	{ url: '/polls-voting', label: 'PollsAndVoting', icon: <PoolsIcon />, permissions: [] },
	{ url: '/documents', label: 'Documents', icon: <DocumentsIcon />, permissions: [] },
	{ url: '/easypanda', label: 'Easy panda', icon: <EasyPandaIcon />, permissions: [] },
];
export const secondary = [
	{ url: '/announcements', label: 'Announcements', key: 'announcements', icon: <AnnouncementsIcon />, permissions: [] },
	{ url: '/settings', label: 'Settings', icon: <SettingsIcon />, permissions: [] },
];
