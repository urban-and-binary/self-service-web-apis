import React from 'react';
import { Link } from 'react-router-dom';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { useMediaQuery } from '@mui/material';
import { AppStoreLogo, GooglePlayLogo } from '@assets/icons';

const Container = styled('div')(({ theme }) => ({
	width: '100%',
	minHeight: '133px',
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'space-between',
	backgroundColor: theme.palette.base.white,
	padding: '40px 24px 60px 24px',
	marginTop: 'auto',
}));
const NavItem = styled(Link)(({ theme }) => ({
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
	textDecoration: 'none',
	marginRight: theme.spacing(4),
	'&:hover': {
		color: theme.palette.base.main,
	},
}));
const Contact = styled('p')(({ theme }) => ({
	color: theme.palette.primary.blue002,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const StoreLogo = styled('img')(() => ({
	marginRight: '16px',
	width: '108px',
	height: '32px',
}));

const LogoContainer = styled('div')(() => ({
	display: 'flex',
}));

const Footer = () => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	return (
		<Container sx={{ alignItems: 'center', flexDirection: isTablet ? 'column' : 'row' }}>
			<div>
				<NavItem to="/" sx={{ mr: isTablet ? 2 : 4 }}>
					Civinity - civinity.lt
				</NavItem>
				<NavItem to="/" sx={{ mr: isTablet ? 2 : 4 }}>
					{t('PrivacyPolicy')}
				</NavItem>
				<NavItem to="/faq">{t('Faq')}</NavItem>
			</div>
			<Contact>{t('CallCenter')}: 8 700 55 188</Contact>
			<LogoContainer>
				<StoreLogo src={AppStoreLogo} alt="App store" />
				<StoreLogo src={GooglePlayLogo} alt="Google play" />
			</LogoContainer>
		</Container>
	);
};

export default Footer;
