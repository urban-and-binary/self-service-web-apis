import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { Drawer, Box, useMediaQuery } from '@mui/material';
import { LanguageSelect } from '@components';
import { BurgerIcon } from '@assets/icons';
import Notifications from './components/Notifications';
import UserMenu from './components/UserMenu';
import { Menu } from '../Menu/Menu';

const Container = styled('div')(({ theme }) => ({
	width: '100%',
	display: 'flex',
	justifyContent: 'space-between',
	backgroundColor: theme.palette.base.white,
}));
const Center = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
}));
const NavItem = styled(Link)(({ theme }) => ({
	textDecoration: 'none',
	marginRight: theme.spacing(4),
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
	'&:hover': {
		color: theme.palette.base.dark,
	},
}));
const BurgerButton = styled('button')(({ theme }) => ({
	border: 0,
	background: 'unset',
	cursor: 'pointer',
	width: '100%',
	color: theme.palette.base.gray,
}));

const Header = () => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	const [open, setOpen] = useState(false);

	const toggleDrawer = open => event => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}

		setOpen(open);
	};

	return (
		<Container sx={{ pt: 4, pb: 4, pl: 4, pr: isTablet ? 1 : 4 }}>
			<Drawer anchor={'left'} open={open} onClose={toggleDrawer(false)}>
				<Box sx={{ width: 350 }} role="presentation" onClick={toggleDrawer(false)} onKeyDown={toggleDrawer(false)}>
					<Menu small={false} />
				</Box>
			</Drawer>
			<Center>
				{isTablet ? (
					<BurgerButton onClick={toggleDrawer(true)} sx={{ p: 2 }}>
						<BurgerIcon sx={{ fontSize: 20 }} />
					</BurgerButton>
				) : (
					<>
						<NavItem to="/contacts">{t('ContactUs')}</NavItem>
						<NavItem to="/faq">{t('Faq')}</NavItem>
					</>
				)}
			</Center>
			<Center>
				<LanguageSelect />
				<Notifications />
				<UserMenu />
			</Center>
		</Container>
	);
};

export default Header;
