import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { IconButton, Badge } from '@mui/material';
import ReactTimeAgo from 'react-time-ago';
import { Dropdown } from './Dropdown';
import { BellIcon } from '@assets/icons';
import { useNotification } from '@hooks/useNotification';
import { useNavigate } from 'react-router-dom';
import api from '@api/axios';

const StyledBellIcon = styled(BellIcon)(() => ({
	width: '14px',
	height: '14px',
}));
const Container = styled('div')(() => ({
	width: '400px',
	padding: '24px 40px 32px 24px',
}));
const Section = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'space-between',
	width: '100%',
}));
const PrimaryText = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
}));
const SecondaryText = styled('div')(({ theme }) => ({
	margin: 0,
	a: {
		textDecoration: 'none',
		color: 'unset',
	},
	color: theme.palette.primary.blue003,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.bold,
	'&:hover': {
		cursor: 'pointer',
	},
}));
const ItemContainer = styled('div')(({ theme }) => ({
	margin: '24px 0',
	color: theme.palette.base.black,
	'&:hover': {
		cursor: 'pointer',
	},
}));
const TitleContainer = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'space-between',
}));
const Title = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	p: {
		margin: 0,
		fontSize: theme.typography.fontSize.small,
		fontWeight: theme.typography.fontWeight.bold,
		lineHeight: theme.typography.lineHeight.small,
	},
}));
const Time = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const StatusBadge = styled('div')(({ theme }) => ({
	width: '12px',
	height: '12px',
	borderRadius: '50%',
	marginRight: theme.spacing(2),
	backgroundColor: theme.palette.primary.blue003,
}));
const StyledBadge = styled(Badge)(({ theme }) => ({
	'& .MuiBadge-badge': {
		fontSize: theme.typography.fontSize.xs,
		fontWeight: theme.typography.fontWeight.r,
		minWidth: 18,
		height: 18,
		right: 1,
		bottom: 1,
	},
}));
const Button = styled(IconButton)(({ theme }) => ({
	boxShadow: theme.shadow.s03,
	width: 32,
	height: 32,
}));

const Notifications = () => {
	const { t, i18n } = useTranslation();
	const { notifs, setNotifs, removeNotif } = useNotification();
	const [onClose, setOnClose] = useState(false);

	const navigate = useNavigate();

	const allRead = async () => {
		try {
			await api.post('/announcements/all-read');

			// so when all unread announcements are read
			// notifs dissapear as they are the unread announcemnets
			setNotifs([]);
			setOnClose(!onClose);
		} catch (error) {
			console.error('Mark all as read error', error);
		}
	};

	const onNotifClick = async notif => {
		try {
			await api.post(`/announcements/${notif._id}/read`);
			removeNotif(notif._id);
			setOnClose(!onClose);
			navigate('/announcements', { state: { selItem: notif } });
		} catch (error) {
			console.error('Announcement read error', error);
		}
	};

	const NotificationsList = () => (
		<Container>
			<Section>
				<PrimaryText>
					{t('Notifications')} ({notifs?.length})
				</PrimaryText>
				<SecondaryText onClick={allRead}>{t('AllAsRead')}</SecondaryText>
			</Section>
			{notifs.map((item, idx) => {
				return (
					<ItemContainer key={idx} onClick={() => onNotifClick(item)}>
						<TitleContainer>
							<Title>
								<StatusBadge />
								<p>{item.title}</p>
							</Title>
							<Time>
								<ReactTimeAgo date={new Date(item.createdAt)} locale={i18n.language} />
							</Time>
						</TitleContainer>
						<Text>{item.description}</Text>
					</ItemContainer>
				);
			})}
			<Section>
				<SecondaryText>
					<Link onClick={() => setOnClose(!onClose)} to="/announcements">
						{t('ViewAllNotifications')}
					</Link>
				</SecondaryText>
			</Section>
		</Container>
	);

	return (
		<Dropdown
			onClose={onClose}
			button={
				<StyledBadge badgeContent={notifs?.length} color="error" overlap="circular">
					<Button>
						<StyledBellIcon />
					</Button>
				</StyledBadge>
			}
			menuContent={<NotificationsList />}
			sx={{ mr: 4 }}
		/>
	);
};

export default Notifications;
