import React, { useState } from 'react';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { IconButton, MenuItem, Badge } from '@mui/material';
import { Dropdown } from './Dropdown';
import { PolygonIcon, EnglandIcon, LithuaniaIcon } from '@assets/icons';

const StyledBadge = styled(Badge)(({ theme }) => ({
	'& .MuiBadge-badge': {
		backgroundColor: theme.palette.base.inactive,
		minWidth: 16,
		height: 16,
		padding: 0,
		right: 4,
		bottom: 2,
	},
}));
const Button = styled(IconButton)(({ theme }) => ({
	boxShadow: theme.shadow.s03,
	width: 32,
	height: 32,
}));
const Label = styled('p')(() => ({
	margin: 0,
	textTransform: 'uppercase',
}));

const LanguageSelect = () => {
	const { i18n } = useTranslation();
	const language = localStorage.getItem('i18nextLng');

	const languages = [
		{ key: 'en', label: 'English', icon: EnglandIcon },
		{ key: 'lt', label: 'Lithuanian', icon: LithuaniaIcon },
	];

	const [close, setClose] = useState(false);

	const handleSelect = event => {
		const { myValue } = event.currentTarget.dataset;
		setClose(!close);
		i18n.changeLanguage(myValue);
	};

	return (
		<Dropdown
			button={
				<StyledBadge
					badgeContent={<PolygonIcon sx={{ fontSize: 6 }} />}
					anchorOrigin={{
						vertical: 'bottom',
						horizontal: 'right',
					}}>
					<Button>
						<Label>{language}</Label>
					</Button>
				</StyledBadge>
			}
			menuContent={languages.map(lang => (
				<MenuItem key={lang.key} onClick={handleSelect} data-my-value={lang.key}>
					<img src={lang.icon} alt="" width="24" height="24" />
				</MenuItem>
			))}
			onClose={close}
			sx={{ mr: 3 }}
		/>
	);
};

export default LanguageSelect;
