import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Menu } from '@mui/material';

const StyledMenu = styled(Menu)(({ theme }) => ({
	'& .MuiPaper-root': {
		// marginLeft: '-16px',
		marginTop: theme.spacing(2),
		boxShadow: theme.shadow.s02,
		color: theme.palette.base.dark,
		borderRadius: theme.shape.br1,
	},
}));
const ButtonWrapper = styled('div')(({ theme, open }) => ({
	button: {
		color: open ? theme.palette.base.white : theme.palette.base.dark,
		backgroundColor: open ? theme.palette.primary.blue003 : '',
		fontSize: theme.typography.fontSize.xs,
		fontWeight: theme.typography.fontWeight.m,
		'&:hover': {
			color: theme.palette.base.white,
			backgroundColor: theme.palette.primary.blue003,
			'.dynamic-text': {
				color: theme.palette.base.white,
			},
		},
		'.dynamic-text': {
			color: open ? theme.palette.base.white : '',
		},
		'.arrow': {
			color: theme.palette.base.dark,
			transition: '.2s',
			transform: open ? 'rotate(180deg)' : '',
		},
	},
}));

export const Dropdown = ({ button, menuContent, onClose, ...props }) => {
	const [anchorEl, setAnchorEl] = useState(null);
	const open = Boolean(anchorEl);

	const handleClick = event => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	useEffect(() => {
		setAnchorEl(null);
	}, [onClose]);

	return (
		<>
			<ButtonWrapper
				id="basic-button"
				aria-controls={open ? 'basic-menu' : undefined}
				aria-haspopup="true"
				aria-expanded={open ? 'true' : undefined}
				onClick={handleClick}
				open={open}
				{...props}>
				{button}
			</ButtonWrapper>

			<StyledMenu
				id="basic-menu"
				anchorEl={anchorEl}
				open={open}
				onClose={handleClose}
				MenuListProps={{
					'aria-labelledby': 'basic-button',
				}}>
				{menuContent}
			</StyledMenu>
		</>
	);
};

Dropdown.propTypes = {
	button: PropTypes.node,
	menuContent: PropTypes.node,
	onClose: PropTypes.bool,
};
