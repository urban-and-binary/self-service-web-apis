import api from '@api/axios';
import useUser from '@hooks/useUser';
import React, { useState, useEffect } from 'react';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Dropdown } from './Dropdown';
import { Avatar, Divider, useMediaQuery } from '@mui/material';
import { UserIcon, LogoutIcon, HomeIcon, ChevronIcon } from '@assets/icons';
import useAuth from '@hooks/useAuth';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';

const Button = styled('button')(() => ({
	border: 'none',
	backgroundColor: 'transparent',
	padding: '8px 24px 8px 16px',
	borderRadius: 16,
	display: 'flex',
	alignItems: 'center',
	height: 58,
	cursor: 'pointer',
}));
const User = styled('div')(() => ({
	display: 'flex',
	flexDirection: 'column',
	justifyContent: 'center',
	alignItems: 'flex-start',
}));
const Name = styled('p')(({ theme }) => ({
	margin: 0,
	textAlign: 'left',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.m,
}));
const JobTitle = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.bold,
}));

const MenuContainer = styled('div')(() => ({
	padding: '10px 24px 18px 24px',
	minWidth: '256px',
}));
const PrimaryText = styled('p')(({ theme }) => ({
	margin: '0 0 8px 0',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const SecondaryText = styled(Link)(({ theme }) => ({
	textDecoration: 'none',
	margin: '16px 0',
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
	color: theme.palette.primary.blue003,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Menu = styled('div')(() => ({
	margin: '0 0 0 20px',
	display: 'flex',
	flexDirection: 'column',
}));
const MenuItem = styled(Link)(({ theme }) => ({
	textDecoration: 'none',
	margin: '16px 0',
	display: 'flex',
	alignItems: 'center',
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.m,
	'& svg': {
		width: '20px',
		height: '20px',
		marginRight: '20px',
	},
}));
const StyledDivider = styled(Divider)(() => ({
	margin: '16px 0',
}));

const UserMenu = () => {
	const { t } = useTranslation();
	const theme = useTheme();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));
	const { data, setUser } = useUser();
	const { user } = data;
	const { setAuth } = useAuth();

	const [close, setClose] = useState(false);

	const handleSelect = () => {
		setClose(!close);
	};

	const getUser = async () => {
		try {
			const res = await api.get('/users');
			setUser({ user: res.data });
		} catch (error) {
			showToast({ title: t('OOPS!'), text: t('SomethingWrongTryLoginAgain'), type: ALERT.ERROR });
		}
	};

	const logout = async () => {
		setClose(!close);
		setAuth({});
		await api.post('/auth/logout');
	};

	useEffect(() => {
		getUser();
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const UserButton = () => (
		<Button>
			<Avatar alt="" src="" sx={{ mr: 1 }} />
			{!isTablet && (
				<User sx={{ mr: 2 }}>
					<Name>{user?.name && user?.surname ? `${user.name} ${user.surname}` : user?.email}</Name>
					<JobTitle className="dynamic-text">{t('Owner')}</JobTitle>
				</User>
			)}
			<ChevronIcon className="arrow" sx={{ fontSize: 10 }} />
		</Button>
	);

	const UserMenu = () => (
		<MenuContainer>
			<Menu>
				<MenuItem to="/profile" onClick={handleSelect}>
					<UserIcon />
					{t('Profile')}
				</MenuItem>
				<MenuItem to="/login" onClick={logout}>
					<LogoutIcon />
					{t('LogOut')}
				</MenuItem>
			</Menu>
			<StyledDivider />
			<Menu>
				<PrimaryText>{t('SwitchAccount')}</PrimaryText>
				{accounts.map(item => (
					<MenuItem to="/" key={item.title}>
						<HomeIcon />
						{item.title}
					</MenuItem>
				))}
			</Menu>
			<SecondaryText to="/">+ {t('AddAccount')}</SecondaryText>
		</MenuContainer>
	);

	return <Dropdown button={<UserButton />} menuContent={<UserMenu />} onClose={close} />;
};

export default UserMenu;

const accounts = [{ title: 'Admi' }, { title: 'Civity' }];
