import React from 'react';
import { useTheme } from '@mui/styles';
import { useMediaQuery } from '@mui/material';
import { Menu } from '../Menu/Menu';

const Navbar = () => {
	const theme = useTheme();
	const isTablet = useMediaQuery(theme.breakpoints.down('md'));

	return <>{!isTablet && <Menu small={false} />}</>;
};

export default Navbar;
