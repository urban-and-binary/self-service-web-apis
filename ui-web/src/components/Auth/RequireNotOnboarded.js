import useAuth from '@hooks/useAuth';
import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';

export const RequireNotOnboarded = () => {
	const { auth } = useAuth();

	return auth.user?.isOnboarded === false ? <Outlet /> : <Navigate to="/" replace />;
};
