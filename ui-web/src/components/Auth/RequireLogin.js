import api from '@api/axios';
import useAuth from '@hooks/useAuth';
import React, { useEffect } from 'react';
import { AppSpinner } from '@components';
import { useLocation, useNavigate, Navigate, Outlet } from 'react-router-dom';

export const RequireLogin = () => {
	const { auth, setAuth } = useAuth();
	const location = useLocation();
	const navigate = useNavigate();

	const verifySession = async () => {
		try {
			const res = await api.get('/session');
			setAuth({ user: res.data });
		} catch (error) {
			navigate('/login', { state: { from: location } });
		}
	};

	useEffect(() => {
		if (!auth?.user) {
			verifySession();
		}
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const token = sessionStorage.getItem('jwt') || localStorage.getItem('jwt');

	if (!auth.user) {
		return <AppSpinner />;
	}

	return token ? <Outlet /> : <Navigate to="/login" state={{ from: location }} replace />;
};
