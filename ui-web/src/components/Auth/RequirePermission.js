import React from 'react';
import PropTypes from 'prop-types';
import { useLocation, Navigate, Outlet } from 'react-router-dom';
import useAuth from '@hooks/useAuth';
import { checkPermissions } from '@common/permissions';

export const RequirePermission = ({ accessPerms }) => {
	const { auth } = useAuth();
	const location = useLocation();

	const userPermissions = auth?.user?.permissions;

	return userPermissions && checkPermissions({ userPermissions, accessPerms, doThrow: false }) ? (
		<Outlet />
	) : (
		<Navigate to="/" state={{ from: location }} replace />
	);
};

RequirePermission.propTypes = {
	accessPerms: PropTypes.arrayOf(PropTypes.string).isRequired,
};
