import useAuth from '@hooks/useAuth';
import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';

export const RequireOnboarding = () => {
	const { auth } = useAuth();

	return auth.user?.isOnboarded ? <Outlet /> : <Navigate to="/onboarding" replace />;
};
