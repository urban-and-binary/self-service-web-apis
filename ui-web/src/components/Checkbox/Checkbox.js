import React from 'react';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import PropTypes from 'prop-types';
import { Checkbox as CheckboxComponent, Radio, FormControlLabel } from '@mui/material';
import { CheckedIcon } from '@assets/icons';

const Container = styled(FormControlLabel)(({ theme, bg }) => ({
	margin: 0,
	borderRadius: 16,
	padding: '17px 16px',
	backgroundColor: bg === 'true' ? theme.palette.primary.blue005 : theme.palette.base.background,
	'.MuiFormControlLabel-label': {
		width: '100%',
		color: theme.palette.base.dark,
		fontSize: theme.typography.fontSize.base,
		fontWeight: theme.typography.fontWeight.r,
	},
}));
const StyledCheckbox = styled(CheckboxComponent)(({ theme }) => ({
	color: theme.palette.base.inactive,
	padding: 0,
	'& .MuiSvgIcon-root': {
		width: 22,
		height: 22,
	},
	'&:hover': {
		backgroundColor: 'unset',
	},
	'&.Mui-checked': {
		color: theme.palette.primary.blue002,
	},
}));

const StyledRadio = styled(Radio)(({ theme }) => ({
	color: theme.palette.base.inactive,
	padding: 0,
	'& .MuiSvgIcon-root': {
		width: 16,
		height: 16,
	},
	'&:hover': {
		backgroundColor: 'unset',
	},
	'&.Mui-checked': {
		color: theme.palette.primary.blue002,
	},
}));

export const Checkbox = ({ label, name, value, onChange, disabled, radio, ...props }) => {
	const theme = useTheme();

	const handleToggle = e => {
		onChange(name, e.target.checked);
	};

	const InputComponent = radio ? StyledRadio : StyledCheckbox;

	return (
		<Container
			control={
				<InputComponent
					checkedIcon={radio ? <CheckedIcon color={theme.palette.primary.blue002} /> : undefined}
					name={name}
					onChange={e => handleToggle(e)}
					value={value}
					checked={value}
					disabled={disabled}
				/>
			}
			label={label}
			{...props}
		/>
	);
};

Checkbox.propTypes = {
	label: PropTypes.node,
	name: PropTypes.string.isRequired,
	value: PropTypes.bool.isRequired,
	onChange: PropTypes.func.isRequired,
	disabled: PropTypes.bool,
	radio: PropTypes.bool,
};
