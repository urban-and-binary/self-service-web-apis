import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { Modal as Component, Fade, Backdrop, Box } from '@mui/material';
import { CloseIcon } from '@assets/icons';
import { hextorgb } from '@utils/utils';

const StyledBox = styled(Box)(({ theme }) => ({
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	maxWidth: 800,
	maxHeight: 600,
	overflow: 'auto',
	borderRadius: 16,
	backgroundColor: theme.palette.base.white,
	boxShadow: theme.shadow.s01,
}));
const StyledCloseIcon = styled(CloseIcon)(({ theme }) => ({
	position: 'absolute',
	top: 30,
	right: 40,
	cursor: 'pointer',
	color: theme.palette.base.gray,
	'&:hover': {
		color: theme.palette.base.dark,
	},
}));

export const Modal = ({ children, open, onClose, showClose, ...props }) => {
	const theme = useTheme();

	return (
		<Component
			aria-labelledby="transition-modal-title"
			aria-describedby="transition-modal-description"
			open={!!open}
			onClose={onClose}
			closeAfterTransition
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 200,
			}}
			sx={{
				backdropFilter: 'blur(5px)',
				'.MuiBackdrop-root': {
					backgroundColor: `${hextorgb(theme.palette.base.lightGray, 0.65)}`,
				},
			}}
			{...props}>
			<Fade in={!!open}>
				<StyledBox>
					{showClose && <StyledCloseIcon onClick={onClose} />}
					{children}
				</StyledBox>
			</Fade>
		</Component>
	);
};

Modal.propTypes = {
	open: PropTypes.bool,
	showClose: PropTypes.bool,
	onClose: PropTypes.func.isRequired,
	children: PropTypes.node.isRequired,
};
