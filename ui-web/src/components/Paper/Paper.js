import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';

const Container = styled('div')(({ theme, shadow }) => ({
	boxShadow: theme.shadow[shadow || 's01'],
	borderRadius: 16,
}));

export const Paper = ({ children, ...props }) => <Container {...props}>{children}</Container>;

Paper.propTypes = {
	children: PropTypes.node,
};
