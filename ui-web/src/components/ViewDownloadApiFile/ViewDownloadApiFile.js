import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import streamsaver from 'streamsaver';
import { useTranslation } from 'react-i18next';
import mime from 'mime-types';
import FilePreview from 'react-file-preview-latest';
import { showToast } from '@utils/utils';
import { ALERT } from '@constants/general';
import api from '@api/axios';
import { Dialog, Spinner } from '@components';
import { EyeIcon, DownloadIcon } from '@assets/icons';

const SpinnerContainer = styled('div')(() => ({
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
}));
const DocumentButton = styled('button')(({ theme }) => ({
	color: theme.palette.base.lightGray,
	cursor: 'pointer',
	background: 'none',
	border: 'none',
	'&:hover': {
		color: theme.palette.base.dark,
	},
}));
const StyledDialogTitle = styled('p')(() => ({
	textOverflow: 'ellipsis',
	overflow: 'hidden',
}));
const FileContainer = styled('div')(() => ({
	// 80px - dialog header height
	height: 'calc(100vh - 80px)',
	width: '100%',
	' > div': {
		width: '100% !important',
		height: '100%',
		' > object': {
			width: '100% !important',
			height: '100% !important',
		},
		' > img': {
			width: '100% !important',
			height: 'unset !important',
		},
	},
}));

export const ViewDownloadApiFile = ({ defaultDocument }) => {
	const { t } = useTranslation();

	const [document, setDocument] = useState(defaultDocument);
	const [previewFile, setPreviewFile] = useState(null);

	const setDocumentLoading = value => {
		setDocument({ ...document, loading: value });
	};

	const downloadFile = (fileName, fileSize) => {
		setDocumentLoading(true);

		const fileStream = streamsaver.createWriteStream(fileName, { size: fileSize });

		const token = sessionStorage.getItem('jwt') || localStorage.getItem('jwt');

		fetch(`/api/web${document.documentApiUrl}`, {
			headers: { Authorization: `Bearer ${token}` },
		})
			.then(response => {
				if (response.status === 200) {
					setDocumentLoading(false);
					response.body.pipeTo(fileStream).then((success, error) => {
						if (error) {
							showToast({ title: t('OOPS!'), text: t('SomethingWrongTryAgain'), type: ALERT.ERROR });
						}
					});
				} else if (response.status === 404) {
					showToast({ title: t('OOPS!'), text: t('NotFound'), type: ALERT.ERROR });
				} else {
					showToast({ title: t('OOPS!'), text: t('SomethingWrongTryAgain'), type: ALERT.ERROR });
				}
			})
			.catch(error =>
				showToast({ title: t('OOPS!'), text: t(error.response?.data?.message || 'SomethingWrongTryAgain'), type: ALERT.ERROR }),
			)
			.finally(() => {
				setDocumentLoading(false);
			});
	};

	const loadFilePreview = async (title, extension) => {
		setDocumentLoading(true);

		try {
			const response = await api.get(document.documentApiUrl, {
				responseType: 'blob',
			});

			const mimeType = mime.lookup(extension);

			const url = URL.createObjectURL(new Blob([response.data], { type: mimeType }));

			setPreviewFile({
				url,
				title,
				mimeType,
			});
		} catch (error) {
			let errorText = 'SomethingWrongTryAgain';

			if (error.response.status === 404) {
				errorText = 'NotFound';
			}

			showToast({ title: t('OOPS!'), text: t(errorText), type: ALERT.ERROR });
		}

		setDocumentLoading(false);
	};

	return (
		<>
			{document.loading ? (
				<SpinnerContainer>
					<Spinner size={20} />
				</SpinnerContainer>
			) : (
				<>
					<Dialog
						fullScreen
						open={!!previewFile}
						onClose={() => setPreviewFile(null)}
						title={<StyledDialogTitle>{previewFile?.title}</StyledDialogTitle>}
						showClose>
						<FileContainer>
							<FilePreview
								type={'url'}
								url={previewFile?.url}
								onError={previewError => console.error('previewError', previewError)}
							/>
						</FileContainer>
					</Dialog>
					<DocumentButton
						sx={{ mr: '5px' }}
						onClick={() => loadFilePreview(`${document.Title}.${document.FileExtension}`, document.FileExtension)}>
						<EyeIcon />
					</DocumentButton>
					<DocumentButton onClick={() => downloadFile(`${document.Title}.${document.FileExtension}`, document.ContentSize)}>
						<DownloadIcon />
					</DocumentButton>
				</>
			)}
		</>
	);
};

ViewDownloadApiFile.propTypes = {
	defaultDocument: PropTypes.shape({
		Id: PropTypes.string.isRequired,
		Title: PropTypes.string,
		FileExtension: PropTypes.string.isRequired,
		ContentSize: PropTypes.number.isRequired,
		documentApiUrl: PropTypes.string.isRequired,
	}).isRequired,
};
