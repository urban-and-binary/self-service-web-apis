import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { LogoIcon } from '@assets/icons';
import { Link } from 'react-router-dom';
import { Box } from '@mui/material';

const Container = styled('div')(({ theme }) => ({
	width: '100%',
	minHeight: '100vh',
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	backgroundColor: theme.palette.base.background,
}));
const Header = styled('div')(({ theme }) => ({
	padding: theme.spacing(4),
	width: '100%',
}));
const StyledCard = styled(Box)(({ theme }) => ({
	borderRadius: '16px',
	boxShadow: theme.shadow.s02,
	backgroundColor: theme.palette.base.white,
}));

export const Card = ({ children, headerLeft, ...props }) => {
	return (
		<Container sx={{ pt: 0, pl: 2, pr: 2, pb: 2 }}>
			<Header sx={{ pl: 7, pr: 7, textAlign: headerLeft ? 'left' : 'center' }}>
				<Link to="/">
					<LogoIcon width="143" />
				</Link>
			</Header>
			<StyledCard {...props}>{children}</StyledCard>
		</Container>
	);
};

Card.propTypes = {
	children: PropTypes.node,
	headerLeft: PropTypes.bool,
};
