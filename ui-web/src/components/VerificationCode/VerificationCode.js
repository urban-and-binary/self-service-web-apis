import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { values } from 'lodash';
import { REGEX } from '@constants/general';
import { Grid } from '@mui/material';
import Button from '@components/Button/Button';

const Container = styled(Grid)(() => ({
	textAlign: 'center',
	justifyContent: 'center',
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.gray,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));
const Contact = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.primary.blue002,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const ButtonWrapper = styled('div')(() => ({
	width: '100%',
	maxWidth: 400,
	justifyContent: 'center',
}));
const InputWrapper = styled('div')(() => ({
	width: '100%',
	display: 'flex',
	justifyContent: 'center',
}));
const Input = styled('input')(({ theme }) => ({
	width: '100%',
	height: 50,
	maxWidth: 50,
	borderRadius: theme.spacing(1),
	border: `1px solid ${theme.palette.base.lightGray}`,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h2,
	fontWeight: theme.typography.fontWeight.m,
	textAlign: 'center',
	'&:focus': {
		outline: 'none !important',
		borderColor: theme.palette.primary.blue003,
	},
	caretColor: theme.palette.primary.blue003,
}));

export const VerificationCode = ({ title, text, contact, length, onSubmit, onClose, onResend, clearAfter, ...props }) => {
	const { t } = useTranslation();
	const [code, setCode] = useState({});

	const handleInput = e => {
		const { name, value, id } = e.target;
		setCode({ ...code, [name]: value });

		const isLast = Number(id) === length;
		if (value !== '' && !isLast) {
			document.getElementById(Number(id) + 1).focus();
		}
	};

	const onKeyDown = e => {
		const { value, id } = e.target;
		const isFirst = Number(id) === 1;

		if (e.keyCode === 8 && value === '' && !isFirst) {
			document.getElementById(Number(id) - 1).focus();
		}
	};

	useEffect(() => {
		const value = values(code).join('');
		if (value.length === length) {
			onSubmit(value);
			if (clearAfter) {
				setCode({});
				document.getElementById(1).focus();
			}
		}
	}, [code, length]); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<Container container {...props}>
			<div>
				{title && <Title>{title}</Title>}
				{text && <Text sx={{ mt: 3 }}>{text}</Text>}
				{contact && <Contact>{contact}</Contact>}
			</div>

			<InputWrapper sx={{ mt: 5 }}>
				{[...Array(length).keys()].map((item, idx) => (
					<Input
						key={idx}
						id={idx + 1}
						name={`digit-${idx}`}
						value={code[`digit-${idx}`] || ''}
						onKeyPress={event => {
							if (!RegExp(REGEX.NUMBER).test(event.key)) {
								event.preventDefault();
							}
						}}
						onChange={handleInput}
						onKeyDown={onKeyDown}
						maxLength={1}
						autoFocus={idx === 0}
						type="text"
						sx={{ ml: 1, mr: 1 }}
					/>
				))}
			</InputWrapper>

			<ButtonWrapper sx={{ pl: 4, pr: 4, mt: 20 }}>
				<Button fullwidth="true" onClick={onResend}>
					{t('ResendCode')}
				</Button>
				<Button fullwidth="true" onClick={onClose} variant="inset" sx={{ mt: 2 }}>
					{t('Cancel')}
				</Button>
			</ButtonWrapper>
		</Container>
	);
};

VerificationCode.propTypes = {
	title: PropTypes.string,
	text: PropTypes.string,
	contact: PropTypes.string,
	clearAfter: PropTypes.bool,
	length: PropTypes.number.isRequired,
	onSubmit: PropTypes.func,
	onResend: PropTypes.func,
	onClose: PropTypes.func,
};
