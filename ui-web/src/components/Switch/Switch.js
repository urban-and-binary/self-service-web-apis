import React from 'react';
import { styled } from '@mui/system';
import PropTypes from 'prop-types';
import { FormControlLabel, Switch as SwitchComponent } from '@mui/material';

const Container = styled(FormControlLabel)(({ theme }) => ({
	margin: 0,
	'.MuiFormControlLabel-label': {
		width: '100%',
		color: theme.palette.base.dark,
		fontSize: theme.typography.fontSize.base,
		fontWeight: theme.typography.fontWeight.r,
	},
}));
const StyledSwitch = styled(SwitchComponent)(({ theme }) => ({
	'& .MuiSwitch-switchBase.Mui-checked': {
		color: 'white',
	},
	'& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
		backgroundColor: theme.palette.primary.blue002,
		opacity: 1,
	},
	'& .MuiSwitch-switchBase + .MuiSwitch-track': {
		backgroundColor: theme.palette.base.inactive,
		opacity: 1,
	},
}));

export const Switch = ({ label, name, value, onChange, disabled, ...props }) => {
	const handleToggle = e => {
		onChange(name, e.target.checked);
	};

	return (
		<Container
			control={<StyledSwitch name={name} onChange={e => handleToggle(e)} value={value} checked={value} disabled={disabled} />}
			label={label}
			{...props}
		/>
	);
};

Switch.propTypes = {
	label: PropTypes.node,
	name: PropTypes.string.isRequired,
	value: PropTypes.bool.isRequired,
	onChange: PropTypes.func.isRequired,
	disabled: PropTypes.bool,
};
