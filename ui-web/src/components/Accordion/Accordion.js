import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Accordion as Component, AccordionSummary, AccordionDetails, Divider } from '@mui/material';
import { ChevronIcon } from '@assets/icons';

const StyledAccordion = styled(Component)(({ theme }) => ({
	borderRadius: theme.spacing(2),
	boxShadow: theme.shadow.s02,
	'&:before, &:after': {
		display: 'none',
	},
}));
const StyledDivider = styled(Divider)(({ theme }) => ({
	borderColor: theme.palette.base.inactive,
}));
const Title = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.h3,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Summary = styled(AccordionSummary)(() => ({
	minHeight: 30,
	'& .MuiAccordionSummary-content': {
		margin: 0,
	},
}));

export const Accordion = ({ title, defaultExpanded, children }) => {
	return (
		<StyledAccordion defaultExpanded={defaultExpanded} disableGutters square sx={{ mb: 2 }}>
			<Summary sx={{ pl: 3, pr: 4, pt: 4, pb: 4 }} expandIcon={<ChevronIcon sx={{ fontSize: 14, color: 'black' }} />}>
				<Title>{title}</Title>
			</Summary>
			<AccordionDetails sx={{ pl: 3, pr: 4, pt: 0, pb: 6 }}>
				<StyledDivider />
				{children}
			</AccordionDetails>
		</StyledAccordion>
	);
};

Accordion.propTypes = {
	title: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
	defaultExpanded: PropTypes.bool,
};
