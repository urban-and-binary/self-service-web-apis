import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTranslation } from 'react-i18next';
import { CheckedIcon, CircleIcon } from '@assets/icons';
import { REGEX } from '@constants/general';

const Container = styled('div')(({ theme }) => ({
	textAlign: 'left',
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
}));
const Text = styled('p')(({ theme, color }) => ({
	margin: 0,
	color: color === 'green' ? theme.palette.secondary.green002 : theme.palette.base.dark,
	fontSize: theme.typography.fontSize.xs,
	fontWeight: theme.typography.fontWeight.r,
}));
const List = styled('ul')(() => ({
	margin: 0,
	paddingInlineStart: 0,
	listStyleType: 'none',
}));
const ListItem = styled('li')(() => ({
	display: 'flex',
	alignItems: 'center',
}));
const Row = styled('div')(() => ({
	display: 'flex',
	alignItems: 'center',
}));

const Requirement = ({ validator, children }) => (
	<ListItem sx={{ mt: 1 }}>
		{validator ? <CheckedIcon checked /> : <CircleIcon />}
		<Text sx={{ ml: 1 }}>{children}</Text>
	</ListItem>
);

export const PasswordValidator = ({ password, repeatPassword, ...props }) => {
	const { t } = useTranslation();

	const validLetter = RegExp(REGEX.LETTER).test(password) && RegExp(REGEX.LETTER).test(repeatPassword);
	const validNumber = RegExp(REGEX.NUMBER).test(password) && RegExp(REGEX.NUMBER).test(repeatPassword);
	const validMatch = password !== '' && password === repeatPassword;
	const passed = validLetter && validNumber && validMatch;

	return (
		<Container {...props}>
			{passed ? (
				<Row>
					<CheckedIcon checked />
					<Text sx={{ ml: 1 }} color="green">
						{t('AllGood')}
					</Text>
				</Row>
			) : (
				<>
					<Text>{t('PasswordMustContain')}:</Text>
					<List>
						<Requirement validator={validLetter}>{t('OneCapitalLetter')}</Requirement>
						<Requirement validator={validNumber}>{t('OneNumber')}</Requirement>
						<Requirement validator={validMatch}>{t('PasswordsMustMatch')}</Requirement>
					</List>
				</>
			)}
		</Container>
	);
};

Requirement.propTypes = {
	children: PropTypes.node,
	validator: PropTypes.bool,
};

PasswordValidator.propTypes = {
	password: PropTypes.string,
	repeatPassword: PropTypes.string,
};
