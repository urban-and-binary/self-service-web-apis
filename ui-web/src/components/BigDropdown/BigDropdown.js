import React from 'react';
import PropTypes from 'prop-types';
import { Autocomplete, TextField, CircularProgress, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { FixedSizeList } from 'react-window';
import { ChevronIcon } from '@assets/icons';
import InfiniteLoader from 'react-window-infinite-loader';

const StyledIcon = styled(ChevronIcon)(() => ({
	width: '10px',
	height: '6px',
}));

const Row = styled(Typography)(({ theme }) => ({
	margin: '0 16px',
	padding: 16,
	borderRadius: 16,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
	'&:hover': {
		cursor: 'pointer',
		backgroundColor: theme.palette.primary.blue005,
	},
}));

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef(function OuterElementType(props, ref) {
	const outerProps = React.useContext(OuterElementContext);
	// we need to pass in styles here to override any styles coming in from props
	// to override material ui styles applied and to align the list style to be more like
	// of our simple dropdown styles

	return (
		<div
			ref={ref}
			{...props}
			{...outerProps}
			// we want to override role here to be undefined
			// because for some reason when role is passed in as "listbox"
			// when we load more data, the list scrolls back to top
			// must be some internal material-ui things.
			// Also because of undefining this prop we need to style the row items ourselves
			// here, see the Row styled component above
			role={undefined}
			style={{ ...props.style, ...outerProps.style, padding: 0, marginTop: 16 }}
		/>
	);
});

OuterElementType.propTypes = {
	style: PropTypes.object,
};

// Adapter for react-window
const InfiniteScrollDropDown = React.forwardRef(function ListboxComponent(props, ref) {
	const { children: itemData, onLoadMore, loading, hasNextPage, ...other } = props;

	const itemCount = hasNextPage ? itemData.length + 1 : itemData.length;

	const loadMoreItems = () => {
		if (!loading && hasNextPage) {
			onLoadMore();
		}
	};

	const isItemLoaded = index => !hasNextPage || index < itemData.length;

	const ListRow = props => {
		const { data, index, style } = props;
		const dataSet = data[index] || [];

		let content;

		if (!isItemLoaded(index)) {
			content = 'Loading';
		} else {
			content = dataSet[1].label;
		}

		return (
			<li style={style}>
				{/* Row styled component styles seem to not be passed in when the
					material ui elements className is passed in, so instead we pass in "will-be-custom"
					to override the material ui className and then our Row styled components styles
					get passed in correctly. Main thing why we want to use the Row styled
					is because there's backgroundColor !important set on the className and there's no background
					color change when hovering the row element because of this */}
				<Row {...dataSet[0]} className="will-be-custom" noWrap>
					{content}
				</Row>
			</li>
		);
	};

	ListRow.propTypes = {
		data: PropTypes.array,
		index: PropTypes.number,
		style: PropTypes.object,
		scrollTo: PropTypes.func,
	};

	return (
		<div ref={ref}>
			<OuterElementContext.Provider value={other}>
				<InfiniteLoader isItemLoaded={isItemLoaded} itemCount={itemCount} loadMoreItems={loadMoreItems}>
					{({ onItemsRendered, ref }) => (
						<FixedSizeList
							onItemsRendered={onItemsRendered}
							ref={ref}
							itemData={itemData}
							height={200}
							width="100%"
							outerElementType={OuterElementType}
							innerElementType="ul"
							itemSize={53}
							itemCount={itemCount}>
							{ListRow}
						</FixedSizeList>
					)}
				</InfiniteLoader>
			</OuterElementContext.Provider>
		</div>
	);
});

InfiniteScrollDropDown.propTypes = {
	children: PropTypes.array,
	onLoadMore: PropTypes.func,
	loading: PropTypes.bool,
	hasNextPage: PropTypes.bool,
};

export const BigDropdown = ({ label, placeholder, onLoadMore, hasNextPage, ...otherProps }) => (
	<Autocomplete
		disablePortal
		disableClearable
		popupIcon={otherProps.loading ? <CircularProgress color="inherit" size={20} /> : <StyledIcon />}
		ListboxComponent={InfiniteScrollDropDown}
		ListboxProps={{
			onLoadMore,
			loading: otherProps.loading,
			hasNextPage,
		}}
		renderOption={(props, option) => [props, option]}
		renderInput={params => (
			<TextField
				{...params}
				label={label}
				placeholder={placeholder}
				InputLabelProps={{
					shrink: true,
				}}
			/>
		)}
		{...otherProps}
	/>
);

BigDropdown.propTypes = {
	label: PropTypes.string,
	placeholder: PropTypes.string,
	loading: PropTypes.bool,
	onLoadMore: PropTypes.func,
	hasNextPage: PropTypes.bool,
};
