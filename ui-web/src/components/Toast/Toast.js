import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { useTheme } from '@mui/styles';
import { ALERT } from '@constants/general';
import { AlertIcon, CloseIcon } from '@assets/icons';
import { Alert, AlertTitle, IconButton, Grow } from '@mui/material';
import { toast } from 'react-toastify';

const Container = styled(Alert)(({ theme }) => ({
	// backgroundColor: theme.palette.base.white,
	borderRadius: theme.shape.br1,
	boxShadow: theme.shadow.s03,
	maxWidth: 550,
	padding: '10px 30px 10px 18px',
	'.MuiAlert-icon': {
		padding: '0',
	},
	'.MuiAlert-action': {
		padding: '0 0 0 16px',
	},
}));
const Icon = styled(CloseIcon)(({ theme }) => ({
	color: theme.palette.base.gray,
}));
const Title = styled(AlertTitle)(({ theme, color }) => ({
	color: color,
	marginBottom: 0,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.bold,
}));
const Text = styled('p')(({ theme }) => ({
	margin: 0,
	color: theme.palette.base.dark,
	fontSize: theme.typography.fontSize.small,
	fontWeight: theme.typography.fontWeight.r,
}));

export const Toast = ({ message, toastId, timeout, ...props }) => {
	const theme = useTheme();

	useEffect(() => {
		const timer =
			timeout &&
			setTimeout(() => {
				toast.dismiss(toastId);
			}, timeout);

		return () => clearTimeout(timer);
	}, [message]); // eslint-disable-line react-hooks/exhaustive-deps

	const renderType = value => {
		return {
			[ALERT.INFO]: { color: theme.palette.primary.blue002, icon: <AlertIcon type={ALERT.INFO} width={40} height={40} /> },
			[ALERT.ERROR]: { color: theme.palette.default.error, icon: <AlertIcon type={ALERT.ERROR} width={40} height={40} /> },
			[ALERT.WARNING]: { color: theme.palette.default.warning, icon: <AlertIcon type={ALERT.WARNING} width={40} height={40} /> },
			[ALERT.SUCCESS]: { color: theme.palette.secondary.green002, icon: <AlertIcon type={ALERT.SUCCESS} width={40} height={40} /> },
		}[value];
	};

	const type = message?.type || props.type || 'error';
	const title = message?.title || props.title || '';
	const text = message?.text || props.text || '';

	return (
		<Grow in={!!message} timeout={200}>
			<Container
				severity={type}
				icon={renderType(type).icon}
				action={
					<IconButton aria-label="close" color="inherit" size="medium" onClick={() => toast.dismiss(toastId)}>
						<Icon sx={{ fontSize: 14 }} />
					</IconButton>
				}>
				<Title color={renderType(type).color}>{title}</Title>
				<Text>{text}</Text>
			</Container>
		</Grow>
	);
};

Toast.propTypes = {
	toastId: PropTypes.string.isRequired,
	type: PropTypes.string,
	title: PropTypes.string,
	text: PropTypes.string,
	timeout: PropTypes.number,
	message: PropTypes.object,
};

// export default Toast;
