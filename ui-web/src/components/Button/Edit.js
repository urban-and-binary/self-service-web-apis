import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { EditIcon } from '@assets/icons';

const StyledEdit = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	border: 'none',
	background: 'none',
	cursor: 'pointer',
	color: theme.palette.primary.blue003,
	'&:hover': {
		color: theme.palette.primary.blue001,
	},
}));
const EditText = styled('p')(({ theme }) => ({
	margin: 0,
	fontSize: theme.typography.fontSize.base,
	fontWeight: theme.typography.fontWeight.m,
}));

export const EditButton = ({ children, onClick, ...props }) => (
	<StyledEdit onClick={onClick} {...props}>
		<EditIcon sx={{ fontSize: 17, mr: 1 }} />
		<EditText>{children}</EditText>
	</StyledEdit>
);

EditButton.propTypes = {
	onClick: PropTypes.func,
	children: PropTypes.node,
};
