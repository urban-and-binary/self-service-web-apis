import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { Link } from 'react-router-dom';

const getStyle = ({ theme, variant, disabled }) => {
	if (variant === 'default') {
		return {
			fontSize: theme.typography.fontSize.base,
			fontWeight: theme.typography.fontWeight.bold,
			transition: '.2s',
			color: theme.palette.base.white,
			backgroundColor: disabled ? theme.palette.base.inactive : theme.palette.primary.blue002,
			'&:hover': {
				boxShadow: disabled ? '' : theme.shadow.s01,
				backgroundColor: disabled ? '' : theme.palette.primary.blue001,
			},
			'&:active': {
				boxShadow: 'none',
				backgroundColor: theme.palette.primary.blue003,
			},
		};
	} else {
		return {
			fontSize: theme.typography.fontSize.base,
			fontWeight: theme.typography.fontWeight.bold,
			color: disabled ? theme.palette.base.inactive : theme.palette.primary.blue002,
			backgroundColor: 'transparent',
			border: `1px solid ${disabled ? theme.palette.base.inactive : theme.palette.primary.blue002}`,
			'&:hover': {
				boxShadow: theme.shadow.s01,
				padding: '6px 39px',
				color: theme.palette.primary.blue001,
				border: `2px solid ${theme.palette.primary.blue001}`,
				backgroundColor: theme.palette.base.white,
			},
			'&:active': {
				boxShadow: 'none',
				padding: '6px 40px',
				color: theme.palette.primary.blue002,
				border: `1px solid ${theme.palette.primary.blue002}`,
				backgroundColor: theme.palette.primary.blue005,
			},
		};
	}
};

const defaultStyle = {
	border: 'none',
	textDecoration: 'none',
	minHeight: 43,
	padding: '6px 40px',
	borderRadius: 100,
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
};

const StyledButton = styled('button')(({ theme, variant = 'default', disabled, fullwidth }) => {
	const style = getStyle({ theme, variant, disabled });
	return {
		cursor: disabled ? '' : 'pointer',
		width: fullwidth ? '100%' : '',
		...defaultStyle,
		...style,
	};
});

const StyledLink = styled(Link)(({ theme, variant = 'default', disabled, fullwidth }) => {
	const style = getStyle({ theme, variant, disabled });
	return {
		pointerEvents: disabled ? 'none' : '',
		width: fullwidth ? '100%' : '',
		...defaultStyle,
		...style,
	};
});

const Button = ({ to, variant, onClick, children, ...props }) =>
	!to ? (
		<StyledButton onClick={onClick} variant={variant} {...props}>
			{children}
		</StyledButton>
	) : (
		<StyledLink to={to} onClick={onClick} variant={variant} {...props}>
			{children}
		</StyledLink>
	);

Button.propTypes = {
	to: PropTypes.string,
	variant: PropTypes.string,
	onClick: PropTypes.func,
	children: PropTypes.node,
};

export default Button;
