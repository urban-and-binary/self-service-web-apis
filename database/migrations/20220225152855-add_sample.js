// NOTE: This IS a sample migration it can be removed
module.exports = {
	async up(db) {
		if (!await db.collection('samples')) await db.createCollection('samples');
	},

	async down(db) {
		await db.collection('samples').drop();
	},
};
