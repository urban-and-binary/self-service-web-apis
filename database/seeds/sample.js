const { dbConnection } = require('../../config');
const MongoClient = require('mongodb').MongoClient;

async function sampleSeed() {
	const client = new MongoClient(dbConnection, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

	try {
		await client.connect();

		const samples = client.db('selfservice').collection('samples');

		await samples.insertOne({ name: 'Sample from seed' });
	} catch (err) {
		console.error(err.stack);
	} finally {
		await client.close();
	}
}

sampleSeed();
