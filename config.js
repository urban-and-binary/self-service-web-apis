module.exports = {
	httpPort: 3000,
	dbConnection: process.env.MONGO_CONNECTION_STRING || 'mongodb://db:27017/selfservice',
	mailersendApiToken: process.env.MAILERSEND_API_TOKEN,
	emailFrom: process.env.EMAIL_FROM || 'no-reply@civinity.com',
	emailFromName: process.env.EMAIL_FROM_NAME || 'Civinity',
	debugEmailResponse: process.env.DEBUG_EMAIL_RESPONSE || false,
	salesforce: {
		clientId: process.env.SALESFORCE_CLIENT_ID,
		clientSecret: process.env.SALESFORCE_CLIENT_SECRET,
		username: process.env.SALESFORCE_USERNAME,
		password: process.env.SALESFORCE_PASSWORD,
		baseUrl: process.env.SALESFORCE_BASE_URL,
		loginUrl: process.env.SALESFORCE_LOGIN_URL,
	},
	tokenForSalesforce: process.env.TOKEN_FOR_SALESFORCE,
	selfServiceSupportEmail: process.env.SELFSERVICE_SUPPORT_EMAIL,
};
