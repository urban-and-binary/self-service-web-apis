login-to-aws-docker-registry:
	aws ecr get-login-password --region eu-north-1 | docker login --username AWS --password-stdin 135937204349.dkr.ecr.eu-north-1.amazonaws.com

ci-runner-build-push:
	(cd docker/ci-runner \
		&& docker build -t 135937204349.dkr.ecr.eu-north-1.amazonaws.com/ci-runner:latest . \
		&& docker push 135937204349.dkr.ecr.eu-north-1.amazonaws.com/ci-runner:latest \
	)
