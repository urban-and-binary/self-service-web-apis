module.exports = {
	semi: true,
	singleQuote: true,
	useTabs: true,
	tabWidth: 4,
	trailingComma: 'all',
	jsxBracketSameLine: true,
	printWidth: 140,
	arrowParens: 'avoid',
};
