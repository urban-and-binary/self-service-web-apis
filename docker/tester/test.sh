#/bin/sh

./wait-for db:27017 -t 40 -- echo db is up || exit 1
./wait-for api-web:3000 -- echo admin is up || exit 1
./wait-for nginx:80 -- echo nginx is up || exit 1

cd /tests

npm run seed
npm run test-ci
