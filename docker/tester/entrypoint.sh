#!/bin/bash

function cleanup()
{
	unlink /tests/node_modules
}

trap cleanup EXIT

rm -rf /tests/node_modules
ln -sf /srv/node_modules /tests/

bash -c "$@"
