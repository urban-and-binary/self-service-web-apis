**Structure**

We'll have the `common` folder for things shared between api and frontend

**Permissions**

So currently we have this route/page based permissions with the options of read and write

```javascript
const permissions = {
	sample: {
		read: 'sample.read',
		write: 'sample.write',
	},
};
```

So each role will have permissions applied to it, and using the deserializer the permissions will be applied to the user based on their role

```javascript
const user = [(firstName: 'Sample'), (lastName: 'Sample'), (role: 'owner'), (permissions: ['sample.read', 'sample.write'])];
```

We'll also have a `checkPermissions` util function, which will simply check if any of the passed in accessPermissions exist within the user permissions. This should be used for frontend & api checking.

**Handling permissions on api routes**

Typically you should just add a middleware to the top of a specific route to easily handle permissions for all future routes being added to the main route file, using api request methods

```javascript
// sample.js file - main route file for all "/sample" endpoints

// middleware to check all endpoints of this route
router.use((req, res, next) => {
	checkPermissions({
		userPermissions: [req.user.permissions],
		accessPerms: [permissions.sample[req.method === 'GET' ? 'read' : 'write']],
	});

	next();
});

router.get(
	'/',
	aw(async (req, res) => {
		res.json({
			message: 'Sample route accessed',
		});
	}),
);
```

In some cases. for example we'd want to use the POST method for reading some certain data, so we can handle it by moving it above our general request based api, and applying a specific read permission check

```javascript
// sample.js file - main route file for all "/sample" endpoints

// Specific middleware for routes not abiding by api request rules
router.use((req, res, next) => {
	checkPermissions({
		userPermissions: [req.user.permissions],
		accessPerms: [permissions.sample.read],
	});

	next();
});

router.post(
	'/post-read',
	aw(async (req, res) => {
		res.json({
			message: 'Sample POST route, for reading accessed',
		});
	}),
);

// middleware to check all endpoints of this route
router.use((req, res, next) => {
	checkPermissions({
		userPermissions: [req.user.permissions],
		accessPerms: [permissions.sample[req.method === 'GET' ? 'read' : 'write']],
	});

	next();
});

router.get(
	'/',
	aw(async (req, res) => {
		res.json({
			message: 'Sample route accessed',
		});
	}),
);
```

If we just have one endpoint with this kind of an exception we can simply add the `checkPermissions` to the top of the endpoint function, and move the endpoint to the top of the route file. The above example is more for if endpoints routes might need an exception.

**Auth/Permission handling on UI**

So we'll have a group of routes protected by a route check which will check the `user` object in the `authContext` . So put all of the routes which need a login under here.

```javascript
<Route element={<RequireLogin />}></Route>
```

For specific routes we'll use a similar approach, just with the `RequirePerm` component, where you'll also have to pass in what kind of permissions are needed for the user to access this route. Do NOTE: it takes in an array of permissions, so its assumed that the user needs to have at least one of these permissions.

```javascript
<Route element={<RequirePerm accessPerms={[permissions.samples.read]} />}>
	<Route path="/sample" element={<Sample />} />
</Route>
```

**Sending emails via MailerSend**

MailerSend (by MailerLite) transactional emails API is integrated for sending emails by providing MailerSend template IDs and custom variables.

MailerSend API token key must be in .env (or container environment):

```
MAILERSEND_API_TOKEN="abcdefghijklmnoprst1234"
```

Import `sendMail` method and required template types from `services/mailService.js`. Example usage of `sendMail` method:

```javascript
const { sendMail, TEMPLATES } = require('services/mailService.js');

await sendMail({
	to: 'john@example.com',
	template: TEMPLATES.EMAIL_CONFIRM,
	subject: 'Email confirmation',
	data: {
		name: 'John',
		confirmEmailUrl: 'https://some-url-address/to-confirm-email',
	},
});
```

**Authentification**

WEB routes:

-   `api/web/auth/signup`
    Request:

```
{
    "email": "test@test.com",
    "password": "xyz"
}
```

    Response:

```
{
	"user": <{user object}>,
	"token": <token>
}
```

    Errors:

```
{
    "message": "EmailAndPasswordRequired"
}
```

```
{
    "message": "DuplicateKey"
}
```

-   `api/web/auth/login`
    Request:

```
{
    "email": "test@test.com",
    "password": "xyz"
}
```

    Response:

```
{
	"user": <{user object}>,
	"token": <token>
}
```

    Errors:

```
{
    "message": "EmailAndPasswordRequired"
}
```

```
{
"message": "InvalidCredentials"
}
```

JWT token is stored in client side, with every request all APIs will go through `passport.authenticate('jwt')` automatically.

**Constants**
All error/http code constants can be seen in `utils/constants.js`
