module.exports = {
	root: true,
	extends: ['eslint:recommended'],
	env: {
		node: true,
		es6: true,
		jest: true,
	},
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
	},
	plugins: ['no-only-tests'],
	rules: {
		quotes: ['error', 'single', { avoidEscape: true }],
		semi: ['error', 'always'],
		indent: [
			'error',
			'tab',
			{
				SwitchCase: 1,
				ignoredNodes: [
					'TemplateLiteral',
					'CallExpression > MemberExpression.callee',
					'ConditionalExpression > ObjectExpression',
					'ConditionalExpression > ArrayExpression',
					'ConditionalExpression > ConditionalExpression',
					'ConditionalExpression > CallExpression > ArrowFunctionExpression > BlockStatement > ReturnStatement',
					'ConditionalExpression > ArrowFunctionExpression > BlockStatement ',
				],
			},
		],
		'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
		'keyword-spacing': ['warn', { before: true, after: true }],
		'space-before-blocks': 'warn',
		'space-before-function-paren': ['error', { anonymous: 'ignore', named: 'never' }],
		'space-in-parens': 'error',
		'comma-dangle': ['error', 'always-multiline'],
		'key-spacing': ['warn', { beforeColon: false, afterColon: true }],
		'no-trailing-spaces': 'warn',

		'no-console': ['error', { allow: ['warn', 'error', 'info', 'debug'] }],
		eqeqeq: ['error', 'smart'],

		'react/jsx-one-expression-per-line': 'off',
		'no-tabs': 'off',
		'linebreak-style': ['error', 'unix'],
		'arrow-parens': 'off',
		'anchor-is-valid': 'off',
		'object-curly-newline': 'off',
		'object-curly-spacing': ['error', 'always'],
		'no-only-tests/no-only-tests': 'error',
	},
};
