export const OWNED_UNIT_STATUSES = {
	approved: 'approved',
	failed: 'failed',
	pending: 'pending',
};

export const ANNOUNCEMENT_QUERY_TYPE_VALUES = {
	all: 'all',
	private: 'private',
	common: 'common',
};

export const errors = {
	InvalidPassword: 'InvalidPassword',
	InvalidField: 'InvalidField',
	PasswordRequired: 'PasswordRequired',
	EmailAndPasswordRequired: 'EmailAndPasswordRequired',
	EmailAndVerificationCodeRequired: 'EmailAndVerificationCodeRequired',
	PhoneAndVerificationCodeRequired: 'PhoneAndVerificationCodeRequired',
	VerificationCodeRequired: 'VerificationCodeRequired',
	EmailOrPhoneRequired: 'EmailOrPhoneRequired',
	PhoneRequired: 'PhoneRequired',
	InvalidEmail: 'InvalidEmail',
	EmailNotSent: 'EmailNotSent',
	InvalidCredentials: 'InvalidCredentials',
	InvalidVerification: 'InvalidVerification',
	InvalidUpdateVerification: 'InvalidUpdateVerification',
	UserAlreadyExists: 'UserAlreadyExists',
	UserNotFound: 'UserNotFound',
	NotFound: 'NotFound',
	DuplicateKey: 'DuplicateKey',
	WeakPassword: 'WeakPassword',
	NotEqual: 'NotEqual',
	UnitAlreadyAssigned: 'UnitAlreadyAssigned',
	RequestParameterMissing: 'RequestParameterMissing',
	NotAuthorized: 'NotAuthorized',
	FieldMissing: 'FieldMissing',
	PhoneExists: 'PhoneExists',
};

export const UNIT_METER_TYPES = {
	coldWater: 'coldWater',
	hotWater: 'hotWater',
	electricity: 'electricity',
	heating: 'heating',
};

export const LOGIN_TYPE = {
	email: 'email',
	phone: 'phone',
};

export const FUND_TYPES = {
	accumulated: 'Accumulated',
	elevator: 'Elevator',
	renovation: 'Renovation',
};

export const TASK_STATUS = {
	planned: 'planned',
	inProgress: 'inProgress',
	done: 'done',
	onHold: 'onHold',
	closed: 'closed',
	cannotComplete: 'cannotComplete',
	canceled: 'canceled',
};
