/**
 * @namespace auth
 */
/**
 * @namespace users
 */
/**
 * @namespace session
 */
/**
 * @namespace buildings
 */
/**
 * @namespace announcements
 */
/**
 * @namespace meters
 */
/**
 * @namespace ownedUnit
 */
/**
 * @namespace tasks
 */

/**
 * @typedef {Object} User
 * @property {string} name
 * @property {string} email
 * @property {string} verifiedAt
 * @property {boolean} isOnboarded
 * @property {string} role
 * @property {Array} permissions
 */

/**
 * @typedef {Object} UserExpanded
 * @property {string} _id
 * @property {string} email
 * @property {string} name
 * @property {string} phone
 * @property {string} address
 * @property {string} country
 * @property {string} countryCode
 * @property {boolean} isOnboarded
 * @property {string} role
 * @property {date} verifiedAt
 * @property {date} verificationExpiry
 * @property {string} verificationType
 * @property {date} createdAt
 * @property {date} updatedAt
 */

/**
 * @typedef {Object} Settings
 * @property {Object} agreements
 * @property {boolean} agreements.privacyPolicy
 * @property {boolean} agreements.personalData
 * @property {boolean} agreements.userRules
 * @property {boolean} agreements.advertisment
 * @property {Object} bills
 * @property {string} bills.receiveFormat
 */

/**
 * @typedef {Object} Buildings
 * @property {string} _id
 * @property {string} title
 * @property {string} houseNumber
 * @property {string} houseLetter
 * @property {string} postcode
 * @property {string} city
 * @property {string} countryCode
 * @property {date} createdAt
 * @property {date} updatedAt
 */

/**
 * @typedef {Object} SalesforceDoc
 * @property {string} Id
 * @property {string} Title
 * @property {date} LastModifiedDate
 * @property {string} FileExtension
 * @property {number} ContentSize
 */

/**
 * @typedef {Object} Task
 * @property {string} id
 * @property {string} title
 * @property {string} unitLabel
 * @property {date} startDate
 * @property {date} endDate
 * @property {string} status
 * @property {string} description
 * @property {string} reason
 * @property {string} grandTotalLabel
 * @property {number} grandTotalSum
 * @property {string} currencyCode
 * @property {Array.<SalesforceDoc>} workOrderDocuments
 */
