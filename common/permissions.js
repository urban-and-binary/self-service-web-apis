export const permissions = {
	units: {
		read: 'units.read',
		write: 'units.write',
	},
	billing: {
		read: 'billing.read',
		write: 'billing.write',
	},
	requests: {
		read: 'requests.read',
		write: 'requests.write',
	},
	tasks: {
		read: 'tasks.read',
		write: 'tasks.write',
	},
	meters: {
		read: 'meters.read',
		write: 'meters.write',
	},
};

export const OWNER_ROLE = 'owner';

export const roles = {
	[OWNER_ROLE]: [
		permissions.units.read,
		permissions.units.write,
		permissions.billing.read,
		permissions.billing.write,
		permissions.requests.read,
		permissions.requests.write,
		permissions.tasks.read,
		permissions.tasks.write,
		permissions.meters.read,
		permissions.meters.write,
	],
};

/**
 * Throws unauthorized error if non of the passed in permissions
 * exist within userPermissions, so if at least one of the passed in permissions
 * exists, all is good, no errors thrown
 *
 * @param {array} userPermissions array of strings, containing user permissions
 * @param {array} accessPerm array of permission strings to userPermissions by
 * @param {boolean} doThrow instead of returning false, throws error when true. Defaults to true
 *
 * @returns boolean value if permission exists or doesnt.
 */

export function checkPermissions({ userPermissions, accessPerms, doThrow = true }) {
	if (!accessPerms.some(accessPermission => userPermissions.includes(accessPermission))) {
		if (doThrow) {
			throw Error('Not Authorized');
		}

		return false;
	}

	return true;
}
