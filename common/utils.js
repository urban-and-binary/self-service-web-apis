export const formUnitLabel = (building, unit) =>
	`${building.street} ${building.houseNumber}${building.houseLetter || ''} - ${unit.unitNumber}${unit.unitLetter || ''}, ${
		building.municipality ? building.municipality + ', ' : ''
	}${building.city}`;
