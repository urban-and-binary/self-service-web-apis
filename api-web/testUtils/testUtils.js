const { faker } = require('@faker-js/faker');
//models
const User = require('../models/User');
// constants
const { LOGIN_TYPE } = require('../../common/consts');

const utils = {};

utils.signupAndLogin = async source => {
	const loginType = LOGIN_TYPE.email;
	const { email, password } = utils.randomEmailPassword();

	await source.auth.signup({ email, password });

	const user = await User.findOne({ email }).select('+verificationCode');

	const resLogin = await source.auth.login({ loginType, email, password, verificationCode: user.verificationCode });

	return { user, password, resLogin };
};

utils.logInEmail = async (source, { email, password }) => {
	const loginType = LOGIN_TYPE.email;
	return source.auth.login({ loginType, email, password });
};

utils.logInPhone = async (source, { phone }) => {
	const loginType = LOGIN_TYPE.phone;

	// signup with email
	await utils.signupAndLogin(source);
	await source.user.update({ phone });

	// send verification code on login
	await source.auth.sendCode({ loginType, phone });

	const { verificationCode } = await User.findOne({ phone }).select('+verificationCode');
	return await source.auth.login({ loginType, phone, verificationCode });
};

utils.randomEmailPassword = () => {
	const email = faker.internet.email(null, null, faker.internet.domainWord() + '.test');
	// edge case with pattern [A-Z\d]: does not always ensure that digit is present, added number as prefix to be certain
	const password = faker.internet.password(8, false, /[A-Z\d@$!%*#?&]/, faker.datatype.number(1));

	return { email, password };
};

utils.randomPhone = () => {
	return faker.phone.phoneNumber();
};

/* reject throws an object with text */
utils.throwMessage = (code, message) => {
	return code + ' - {"message":"' + message + '"}';
};

module.exports = { utils };
