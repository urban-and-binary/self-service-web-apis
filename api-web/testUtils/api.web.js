const clientNativeRequest = require('request-promise-native');
const { faker } = require('@faker-js/faker');
// constants
const { LOGIN_TYPE } = require('../../common/consts');

class ApiWeb {
	constructor({ resolveWithFullResponse = false } = {}) {
		const defaults = {
			resolveWithFullResponse,
			jar: clientNativeRequest.jar(),
			baseUrl: 'http://nginx/api/web',
			json: true,
		};

		this.clientRequest = clientNativeRequest.defaults(defaults);
		const unhashedPassword = 'A1234567';

		this.user = {
			get: async () =>
				await this.clientRequest.get({
					url: '/users',
				}),
			create: async ({
				email = faker.internet.email(null, null, faker.internet.domainWord() + '.test'),
				password = unhashedPassword,
				name = faker.name.findName(),
			} = {}) => {
				const user = this.clientRequest.post({
					url: '/auth/signup',
					json: { email, password, name },
				});
				return user;
			},
			//didn't manage to take req.body as a whole parameter here, so divided to specific properties
			update: async ({ name, phone, address, postcode, city, countryCode } = {}) => {
				const user = this.clientRequest.put({
					url: '/users/profile/info',
					json: { name, phone, address, postcode, city, countryCode },
				});
				return user;
			},
			profilePasswordUpdate: async ({ currentPassword, newPassword, repeatPassword } = {}) => {
				const user = this.clientRequest.put({
					url: '/users/profile/password',
					json: { currentPassword, newPassword, repeatPassword },
				});
				return user;
			},
			settings: async () => {
				const settings = this.clientRequest.get({
					url: '/users/settings',
				});
				return settings;
			},
			settingsUpdate: async ({ agreements, bills }) => {
				return this.clientRequest.put({
					url: '/users/settings',
					json: { agreements, bills },
				});
			},
			updateEmail: async ({ email, code } = {}) =>
				this.clientRequest.put({
					url: '/users/profile/email',
					json: { email, code },
				}),
			updateEmailCode: async ({ email } = {}) =>
				this.clientRequest.post({
					url: '/users/profile/email-code',
					json: { email },
				}),
			updatePhone: async ({ phone, code } = {}) =>
				this.clientRequest.put({
					url: '/users/profile/phone',
					json: { phone, code },
				}),
			updatePhoneCode: async ({ phone } = {}) =>
				this.clientRequest.post({
					url: '/users/profile/phone-code',
					json: { phone },
				}),
			validatePhone: async phone => {
				return this.clientRequest.get({
					url: `/users//validate-phone?phone=${phone}`,
				});
			},
		};

		this.auth = {
			signup: async ({
				email = faker.internet.email(null, null, faker.internet.domainWord() + '.test'),
				password = unhashedPassword,
			} = {}) => {
				const user = this.clientRequest.post({
					url: '/auth/signup',
					json: { email, password },
				});
				return user;
			},
			login: async ({ loginType, email, password, phone, verificationCode } = {}) => {
				if (loginType === LOGIN_TYPE.email && email === undefined) {
					const { user } = await this.user.create();
					email = user.email;
					password = unhashedPassword;
				}
				const login = await this.clientRequest.post({
					url: '/auth/login',
					json: { loginType, email, password, phone, verificationCode },
				});

				const body = resolveWithFullResponse ? login.body : login;

				if (!body || !body.token) {
					return body || {};
				}

				this.clientRequest = clientNativeRequest.defaults({
					...defaults,
					headers: {
						Authorization: `Bearer ${body.token}`,
					},
				});
				return login;
			},
			passwordForgot: async ({ email, phone }) => {
				await this.clientRequest.post({
					url: '/auth/password-forgot',
					json: { email, phone },
				});
			},
			sendCode: async ({ loginType, phone }) => {
				await this.clientRequest.post({
					url: '/auth/send-code',
					json: { loginType, phone },
				});
			},
			verifyCode: async ({ email, phone, verificationCode }) => {
				await this.clientRequest.post({
					url: '/auth/verify-code',
					json: { email, phone, verificationCode },
				});
			},
			passwordChange: async ({ email, phone, verificationCode, password, password1 }) => {
				await this.clientRequest.post({
					url: '/auth/password-change',
					json: { email, phone, verificationCode, password, password1 },
				});
			},
			get: async () =>
				await this.clientRequest.get({
					url: '/auth',
				}),

			logout: async () =>
				await this.clientRequest.post({
					url: '/auth/logout',
				}),
		};

		this.buildings = {
			create: async ({
				street = faker.address.streetAddress() + 'ĖĮŠŲŪ',
				houseNumber = faker.datatype.number(),
				postcode = faker.address.zipCode(),
				city = faker.address.city(),
			} = {}) => {
				const building = this.clientRequest.post({
					url: '/buildings',
					json: { street, houseNumber, postcode, city },
				});
				return building;
			},
			list: async ({ pageSize = 50, search, lastPageItemId, lastPageItemTitle }) =>
				await this.clientRequest.get({ url: '/buildings', qs: { pageSize, search, lastPageItemId, lastPageItemTitle } }),
		};

		this.ownedUnits = {
			list: ({ status }) => this.clientRequest.get({ url: '/owned-unit', qs: { status } }),
		};

		this.session = {
			get: async () => {
				return this.clientRequest.get({
					url: '/session',
				});
			},
		};

		this.announcements = {
			list: async ({ type, ownedUnit, userId, sortField = 'createdAt', sortOrder = 1, pageSize = 5, page = 1 }) =>
				await this.clientRequest.get({
					url: '/announcements',
					qs: { type, ownedUnit, userId, sortField, sortOrder, pageSize, page },
				}),

			annRead: async ({ annId }) => await this.clientRequest.post(`/announcements/${annId}/read`),
			getUnread: async () => await this.clientRequest.get('/announcements/unread'),
			markAllRead: async () => await this.clientRequest.post('/announcements/all-read'),
		};
	}
}

module.exports = ApiWeb;
