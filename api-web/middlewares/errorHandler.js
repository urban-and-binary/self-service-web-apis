const { errors } = require('../../common/consts');

// eslint-disable-next-line no-unused-vars
function errorHandler(error, req, res, next) {
	let status = 500;
	let message = error.message;

	if (
		// prettier-ignore
		[
			errors.InvalidPassword,
			errors.PasswordRequired,
			errors.EmailAndPasswordRequired,
			errors.UserAlreadyExists,
			errors.EmailAndVerificationCodeRequired,
			errors.PhoneAndVerificationCodeRequired,
			errors.VerificationCodeRequired,
			errors.WeakPassword,
			errors.EmailAndVerificationCodeRequired,
			errors.EmailOrPhoneRequired,
			errors.NotEqual,
			errors.UnitAlreadyAssigned,
			errors.RequestParameterMissing,
			errors.InvalidUpdateVerification,
			errors.InvalidField,
			errors.FieldMissing,
		].includes(error.message)
	) {
		status = 400;
	}

	if ([errors.InvalidVerification, errors.InvalidCredentials].includes(error.message)) {
		status = 401;
	}

	if (error.message === errors.NotAuthorized) {
		status = 403;
	}

	if ([errors.UserNotFound, errors.NotFound].includes(error.message)) {
		status = 404;
	}

	if (error.message.includes('duplicate key') && error.keyPattern.email) {
		status = 400;
		message = errors.UserAlreadyExists;
	}

	console.error(error);
	res.status(status).json({ message });
}

module.exports = errorHandler;
