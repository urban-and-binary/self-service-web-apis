//test services
const ApiWeb = require('../testUtils/api.web');
const apiWeb = new ApiWeb();
//common services
const { utils } = require('../testUtils/testUtils');
//db
const mongoose = require('mongoose');
require('../utils/dbConnect');
const { httpStatus } = require('../utils/constants');
const { roles } = require('../../common/permissions');

// session
test('should not get session info when unauthorized', async () => {
	await apiWeb.session.get().catch(res => {
		expect(res.statusCode).toBe(httpStatus.Unauthorized);
	});
});

test('should get session info when authorized & should also return permissions & role with session', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	const user = await apiWeb.session.get();
	expect(user).toHaveProperty('email');
	expect(user).toHaveProperty('role');
	expect(user.email).toBe(email);
	expect(user.permissions).toEqual(roles[user.role]);
});

afterAll(async () => {
	await mongoose.disconnect();
	await mongoose.connection.close();
});
