const { faker } = require('@faker-js/faker');
//common services
const { createAnnouncement } = require('../services/announcementService');
const { createOwnedUnit } = require('../services/ownedUnitService');
const { utils } = require('../testUtils/testUtils');
const ApiWeb = require('../testUtils/api.web');
const apiWeb = new ApiWeb();
const mongoose = require('mongoose');
const OwnedUnit = require('../models/OwnedUnit');
const { OWNED_UNIT_STATUSES, ANNOUNCEMENT_QUERY_TYPE_VALUES } = require('../../common/consts');
//db
require('../utils/dbConnect');

test('should create & correctly filter & list various types of announcements', async () => {
	const { user: user1, password: password1 } = await utils.signupAndLogin(apiWeb);

	const globAnnTitle = 'Global announcement';
	await createAnnouncement({ title: globAnnTitle, description: globAnnTitle });

	const sharedBuilding = await apiWeb.buildings.create({ postcode: '00-ABC-123' });

	const buildingExtId = `${faker.random.word()} - ${new Date()}`;
	const user1UnitExtId1 = `${faker.random.word()} - ${new Date()}`;

	const user1OwnedUnit = await createOwnedUnit({
		building: sharedBuilding._id,
		buildingExtId,
		unitExtId: user1UnitExtId1,
		testLabel: `${faker.random.word()} - ${new Date()}`,
		owner: user1._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(user1OwnedUnit._id, { status: OWNED_UNIT_STATUSES.approved });

	// announcement for sharedBuilding - where user1 & user2 each will have a unit
	const sharedBuildingAnnouncement = 'Shared building announcement';
	await createAnnouncement({
		title: sharedBuildingAnnouncement,
		description: sharedBuildingAnnouncement,
		buildingExtId,
		unitLabel: sharedBuilding.title,
	});

	// announcement specificly for user1 owned unit
	const user1OwnedUnitAnn = 'Owned unit announcement for user1';
	await createAnnouncement({
		title: user1OwnedUnitAnn,
		description: user1OwnedUnitAnn,
		buildingExtId,
		unitExtId: user1UnitExtId1,
		unitLabel: user1OwnedUnit.label,
	});

	// announcement specificly for user1
	const user1Announcement = 'User 1 announcement';
	await createAnnouncement({
		title: user1Announcement,
		description: user1Announcement,
		forSpecUser: user1._id,
	});

	const user1Building = await apiWeb.buildings.create({ postcode: '00-ABC-122' });

	const user1BuildingExtId = `${faker.random.word()} - ${new Date()}`;
	const user1UnitExtId2 = `${faker.random.word()} - ${new Date()}`;
	const user1BuildingLabel = `${faker.random.word()} - ${new Date()}`;

	const user1OwnedUnit2 = await createOwnedUnit({
		building: sharedBuilding._id,
		buildingExtId: user1BuildingExtId,
		unitExtId: user1UnitExtId2,
		testLabel: user1BuildingLabel,
		owner: user1._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(user1OwnedUnit2._id, { status: OWNED_UNIT_STATUSES.approved });

	// announcement for user1Building - where only user1 has a unit
	const user1BuildingAnn = 'User1 building announcement';
	await createAnnouncement({
		title: user1BuildingAnn,
		description: user1BuildingAnn,
		buildingExtId: user1BuildingExtId,
		unitLabel: user1Building.title,
	});

	// announcement & owned unit creation for user2
	const { user: user2, password: password2 } = await utils.signupAndLogin(apiWeb);

	const user2UnitExtId1 = `${faker.random.word()} - ${new Date()}`;

	const user2OwnedUnit = await createOwnedUnit({
		building: sharedBuilding._id,
		buildingExtId,
		unitExtId: user2UnitExtId1,
		testLabel: `${faker.random.word()} - ${new Date()}`,
		owner: user2._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(user2OwnedUnit._id, { status: OWNED_UNIT_STATUSES.approved });

	// announcement specificly for user2 owned unit
	const user2OwnedUnitAnn = 'Owned unit announcement for user2';
	await createAnnouncement({
		title: user2OwnedUnitAnn,
		description: user2OwnedUnitAnn,
		buildingExtId,
		unitExtId: user2UnitExtId1,
		unitLabel: user2OwnedUnit.label,
	});

	// announcement specificly for user2
	const user2Announcement = 'User 2 announcement';
	await createAnnouncement({
		title: user2Announcement,
		description: user2Announcement,
		forSpecUser: user2._id,
	});

	const user2Building = await apiWeb.buildings.create({ postcode: '00-ABC-142' });

	const user2BuildingExtId = `${faker.random.word()} - ${new Date()}`;
	const user2UnitExtId2 = `${faker.random.word()} - ${new Date()}`;
	const user2BuildingLabel = `${faker.random.word()} - ${new Date()}`;

	const user2OwnedUnit2 = await createOwnedUnit({
		building: user2Building._id,
		buildingExtId: user2BuildingExtId,
		unitExtId: user2UnitExtId2,
		testLabel: user2BuildingLabel,
		owner: user2._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(user2OwnedUnit2._id, { status: OWNED_UNIT_STATUSES.approved });

	// announcement for user2Building - where only user2 has a unit
	const user2BuildingAnn = 'User2 building announcement';
	await createAnnouncement({
		title: user2BuildingAnn,
		description: user2BuildingAnn,
		buildingExtId: user2BuildingExtId,
		unitLabel: user2Building.title,
	});

	const sharedBuildingExtId = `${faker.random.word()} - ${new Date()}`;
	const sharedUnitExtId = `${faker.random.word()} - ${new Date()}`;
	const sharedUnitLabel = `${faker.random.word()} - ${new Date()}`;

	// Creating two owned units for the same unit according to external ids
	// so this is the first owned unit
	const user1user2OwnedUnit1 = await createOwnedUnit({
		building: sharedBuilding._id,
		buildingExtId: sharedBuildingExtId,
		unitExtId: sharedUnitExtId,
		testLabel: sharedUnitLabel,
		owner: user1._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(user1user2OwnedUnit1._id, { status: OWNED_UNIT_STATUSES.approved });

	// this is the second owned unit
	const user1user2OwnedUnit2 = await createOwnedUnit({
		building: sharedBuilding._id,
		buildingExtId: sharedBuildingExtId,
		unitExtId: sharedUnitExtId,
		testLabel: sharedUnitLabel,
		owner: user2._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(user1user2OwnedUnit2._id, { status: OWNED_UNIT_STATUSES.approved });

	const sharedUnitOnlyUser1Ann = `${faker.random.word()} - ${new Date()}`;
	// creating Announcement only for the second user and for the shared unit between the two
	// users
	await createAnnouncement({
		title: sharedUnitOnlyUser1Ann,
		description: sharedUnitOnlyUser1Ann,
		buildingExtId: sharedBuildingExtId,
		unitExtId: sharedUnitExtId,
		unitLabel: sharedUnitLabel,
		forSpecUser: user1._id,
	});

	// checking the filtering with user1
	await utils.logInEmail(apiWeb, { email: user1.email, password: password1 });

	// we will be sorting by latest created at for tests to work both in development and on pipeline
	let anns = await apiWeb.announcements.list({ sortField: 'createdAt', sortOrder: -1, pageSize: 10 });
	let annsData = anns.data;
	let annsTotal = anns.total;

	expect(annsTotal > 5).toBe(true);

	// and since we're sorting by createdAt date, we can check
	// returned items in the appropriate order we've created them

	// user1 specific announcement for associated shared unit between both users
	expect(annsData[0].description).toBe(sharedUnitOnlyUser1Ann);
	expect(annsData[0].title).toBe(sharedUnitOnlyUser1Ann);
	expect(annsData[0].buildingExtId).toBe(sharedBuildingExtId);
	expect(annsData[0].unitExtId).toBe(sharedUnitExtId);
	expect(annsData[0].unitLabel).toBe(sharedUnitLabel);
	expect(user1._id.equals(annsData[0].forSpecUser)).toBe(true);

	// checking announcement for building where only user1 is the owner
	expect(annsData[1].description).toBe(user1BuildingAnn);
	expect(annsData[1].title).toBe(user1BuildingAnn);
	expect(annsData[1].buildingExtId).toBe(user1BuildingExtId);
	expect(annsData[1].unitLabel).toBe(user1Building.title);
	expect(annsData[1]).not.toHaveProperty('unitExtId');
	expect(annsData[1]).not.toHaveProperty('forSpecUser');

	// checking user1 specific announcement
	expect(annsData[2].description).toBe(user1Announcement);
	expect(annsData[2].title).toBe(user1Announcement);
	expect(annsData[2]).not.toHaveProperty('buildingExtId');
	expect(annsData[2]).not.toHaveProperty('unitExtId');
	expect(user1._id.equals(annsData[2].forSpecUser)).toBe(true);

	// checking user1 owned unit
	expect(annsData[3].description).toBe(user1OwnedUnitAnn);
	expect(annsData[3].title).toBe(user1OwnedUnitAnn);
	expect(annsData[3].buildingExtId).toBe(buildingExtId);
	expect(annsData[3].unitLabel).toBe(user1OwnedUnit.label);
	expect(annsData[3].unitExtId).toBe(user1UnitExtId1);
	expect(annsData[3]).not.toHaveProperty('forSpecUser');

	// checking shared building
	expect(annsData[4].description).toBe(sharedBuildingAnnouncement);
	expect(annsData[4].title).toBe(sharedBuildingAnnouncement);
	expect(annsData[4].buildingExtId).toBe(buildingExtId);
	expect(annsData[4].unitLabel).toBe(sharedBuilding.title);
	expect(annsData[4]).not.toHaveProperty('unitExtId');
	expect(annsData[4]).not.toHaveProperty('forSpecUser');

	// so checking global announcement here
	expect(annsData[5].description).toBe(globAnnTitle);
	expect(annsData[5].title).toBe(globAnnTitle);
	expect(annsData[5]).not.toHaveProperty('buildingExtId');
	expect(annsData[5]).not.toHaveProperty('unitExtId');
	expect(annsData[5]).not.toHaveProperty('forSpecUser');

	if (annsData[6]) {
		// and if there might be a 6th or more announcements we expect it
		// to be some global announcement created in the development
		// environment via tests or other means
		expect(annsData[6]).not.toHaveProperty('buildingExtId');
		expect(annsData[6]).not.toHaveProperty('unitExtId');
		expect(annsData[6]).not.toHaveProperty('forSpecUser');
	}

	// querying private announcements
	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
		pageSize: 10,
		type: ANNOUNCEMENT_QUERY_TYPE_VALUES.private,
	});
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal).toBe(2);

	// user1 specific announcement for associated shared unit between both users
	expect(annsData[0].description).toBe(sharedUnitOnlyUser1Ann);
	expect(annsData[0].title).toBe(sharedUnitOnlyUser1Ann);
	expect(annsData[0].buildingExtId).toBe(sharedBuildingExtId);
	expect(annsData[0].unitExtId).toBe(sharedUnitExtId);
	expect(annsData[0].unitLabel).toBe(sharedUnitLabel);
	expect(user1._id.equals(annsData[0].forSpecUser)).toBe(true);

	// checking user1 specific announcement
	expect(annsData[1].description).toBe(user1Announcement);
	expect(annsData[1].title).toBe(user1Announcement);
	expect(annsData[1]).not.toHaveProperty('buildingExtId');
	expect(annsData[1]).not.toHaveProperty('unitExtId');
	expect(user1._id.equals(annsData[1].forSpecUser)).toBe(true);

	// querying common announcements
	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
		pageSize: 10,
		type: ANNOUNCEMENT_QUERY_TYPE_VALUES.common,
	});
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal > 3).toBe(true);
	expect(annsData[0].title).toBe(user1BuildingAnn);
	expect(annsData[1].title).toBe(user1OwnedUnitAnn);
	expect(annsData[2].title).toBe(sharedBuildingAnnouncement);
	expect(annsData[3].title).toBe(globAnnTitle);

	if (annsData[4]) {
		// and if there might be a 6th or more announcements we expect it
		// to be some global announcement created in the development
		// environment via tests or other means
		expect(annsData[4]).not.toHaveProperty('buildingExtId');
		expect(annsData[4]).not.toHaveProperty('unitExtId');
		expect(annsData[4]).not.toHaveProperty('forSpecUser');
	}

	// querying by owned unit
	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
		pageSize: 10,
		ownedUnit: user1OwnedUnit._id.toString(),
	});
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal).toBe(2);
	expect(annsData[0].title).toBe(user1OwnedUnitAnn);
	expect(annsData[1].title).toBe(sharedBuildingAnnouncement);

	// checking the filtering with user2
	await utils.logInEmail(apiWeb, { email: user2.email, password: password2 });

	// we will be sorting by latest created at for tests to work both in development and on pipeline
	anns = await apiWeb.announcements.list({ sortField: 'createdAt', sortOrder: -1, pageSize: 10 });
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal > 4).toBe(true);

	// and since we're sorting by createdAt date, we can check
	// returned items in the appropriate order we've created them

	// checking announcement for building where only user2 is the owner
	expect(annsData[0].description).toBe(user2BuildingAnn);
	expect(annsData[0].title).toBe(user2BuildingAnn);
	expect(annsData[0].buildingExtId).toBe(user2BuildingExtId);
	expect(annsData[0].unitLabel).toBe(user2Building.title);
	expect(annsData[0]).not.toHaveProperty('unitExtId');
	expect(annsData[0]).not.toHaveProperty('forSpecUser');

	// checking user2 specific announcement
	expect(annsData[1].description).toBe(user2Announcement);
	expect(annsData[1].title).toBe(user2Announcement);
	expect(annsData[1]).not.toHaveProperty('buildingExtId');
	expect(annsData[1]).not.toHaveProperty('unitExtId');
	expect(user2._id.equals(annsData[1].forSpecUser)).toBe(true);

	// checking user2 owned unit
	expect(annsData[2].description).toBe(user2OwnedUnitAnn);
	expect(annsData[2].title).toBe(user2OwnedUnitAnn);
	expect(annsData[2].buildingExtId).toBe(buildingExtId);
	expect(annsData[2].unitLabel).toBe(user2OwnedUnit.label);
	expect(annsData[2].unitExtId).toBe(user2UnitExtId1);
	expect(annsData[2]).not.toHaveProperty('forSpecUser');

	// checking shared building
	expect(annsData[3].description).toBe(sharedBuildingAnnouncement);
	expect(annsData[3].title).toBe(sharedBuildingAnnouncement);
	expect(annsData[3].buildingExtId).toBe(buildingExtId);
	expect(annsData[3].unitLabel).toBe(sharedBuilding.title);
	expect(annsData[3]).not.toHaveProperty('unitExtId');
	expect(annsData[3]).not.toHaveProperty('forSpecUser');

	// so checking global announcement here
	expect(annsData[4].description).toBe(globAnnTitle);
	expect(annsData[4].title).toBe(globAnnTitle);
	expect(annsData[4]).not.toHaveProperty('buildingExtId');
	expect(annsData[4]).not.toHaveProperty('unitExtId');
	expect(annsData[4]).not.toHaveProperty('forSpecUser');

	if (annsData[5]) {
		// and if there might be a 6th or more announcements we expect it
		// to be some global announcement created in the development
		// environment via tests or other means
		expect(annsData[5]).not.toHaveProperty('buildingExtId');
		expect(annsData[5]).not.toHaveProperty('unitExtId');
		expect(annsData[5]).not.toHaveProperty('forSpecUser');
	}

	// querying private announcements
	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
		pageSize: 10,
		type: ANNOUNCEMENT_QUERY_TYPE_VALUES.private,
	});
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal).toBe(1);

	// querying common announcements
	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
		pageSize: 10,
		type: ANNOUNCEMENT_QUERY_TYPE_VALUES.common,
	});
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal > 3).toBe(true);
	expect(annsData[0].title).toBe(user2BuildingAnn);
	expect(annsData[1].title).toBe(user2OwnedUnitAnn);
	expect(annsData[2].title).toBe(sharedBuildingAnnouncement);
	expect(annsData[3].title).toBe(globAnnTitle);

	if (annsData[4]) {
		// and if there might be a 6th or more announcements we expect it
		// to be some global announcement created in the development
		// environment via tests or other means
		expect(annsData[4]).not.toHaveProperty('buildingExtId');
		expect(annsData[4]).not.toHaveProperty('unitExtId');
		expect(annsData[4]).not.toHaveProperty('forSpecUser');
	}

	// querying by owned unit
	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
		pageSize: 10,
		ownedUnit: user2OwnedUnit._id.toString(),
	});
	annsData = anns.data;
	annsTotal = anns.total;

	expect(annsTotal).toBe(2);

	expect(annsData[0].title).toBe(user2OwnedUnitAnn);
	expect(annsData[1].title).toBe(sharedBuildingAnnouncement);
});

test('should read announcement for specific user', async () => {
	const { user: user2, password: password2 } = await utils.signupAndLogin(apiWeb);

	// signing up with another user
	await utils.signupAndLogin(apiWeb);

	const title = `${faker.random.word()} - ${new Date()}`;
	await createAnnouncement({ title, description: title });

	let anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
	});

	let annsData = anns.data;

	expect(annsData[0].title).toBe(title);
	expect(annsData[0].read).toBe(false);

	await apiWeb.announcements.annRead({ annId: annsData[0]._id });

	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
	});

	annsData = anns.data;

	expect(annsData[0].title).toBe(title);
	expect(annsData[0].read).toBe(true);

	// so for user2 the announcement should still be considered unread
	await utils.logInEmail(apiWeb, { email: user2.email, password: password2 });

	anns = await apiWeb.announcements.list({
		sortField: 'createdAt',
		sortOrder: -1,
	});

	annsData = anns.data;

	expect(annsData[0].title).toBe(title);
	expect(annsData[0].read).toBe(false);
});

test('should get unread & mark all unread announcements as read', async () => {
	const { user } = await utils.signupAndLogin(apiWeb);

	const title1 = `${faker.random.word()} - ${new Date()} - 1`;
	await createAnnouncement({ title: title1, description: title1, forSpecUser: user._id });

	const title2 = `${faker.random.word()} - ${new Date()} - 2`;
	const anns2 = await createAnnouncement({ title: title2, description: title2, forSpecUser: user._id });

	const title3 = `${faker.random.word()} - ${new Date()} - 3`;
	await createAnnouncement({ title: title3, description: title3, forSpecUser: user._id });

	// note sorted by createdAt -1
	let unread = await apiWeb.announcements.getUnread();

	// there might be some global unread announcements created
	// in development environment
	expect(unread.length > 2).toBe(true);

	// this check should work as unread announcements are sorted by createdAt: -1
	// so latest on top
	expect(unread[0].title).toBe(title3);
	expect(unread[1].title).toBe(title2);
	expect(unread[2].title).toBe(title1);

	// lets read the middle one and then we should only receive the 3 & 1
	await apiWeb.announcements.annRead({ annId: anns2._id.toString() });

	unread = await apiWeb.announcements.getUnread();

	// there might be some global unread announcements created
	// in development environment
	expect(unread.length > 1).toBe(true);

	// this check should work as unread announcements are sorted by createdAt: -1
	// so latest on top
	expect(unread[0].title).toBe(title3);
	expect(unread[1].title).toBe(title1);

	await apiWeb.announcements.markAllRead();

	unread = await apiWeb.announcements.getUnread();

	// and here even the globally unread announcements should be read
	expect(unread).toHaveLength(0);
});

afterAll(async () => {
	await mongoose.disconnect();
	await mongoose.connection.close();
});
