const { faker } = require('@faker-js/faker');
//common services
const { utils } = require('../testUtils/testUtils');
const ApiWeb = require('../testUtils/api.web');
const apiWeb = new ApiWeb();
//db
const mongoose = require('mongoose');
require('../utils/dbConnect');

test('should search for buildings with case insensitive, diacritic characters', async () => {
	await utils.signupAndLogin(apiWeb);

	await apiWeb.buildings.create({ postcode: '00-ABC-123' });
	await apiWeb.buildings.create({ postcode: '00-ABC-456' });

	const street = faker.random.word() + 'ŽĖŠŪ';
	const postcode = faker.address.zipCode();

	const building = await apiWeb.buildings.create({ street, postcode });

	const combinedSearch = `${street.toLowerCase()} ${postcode}`;
	const resultsWithMatch = await apiWeb.buildings.list({ search: combinedSearch });
	expect(resultsWithMatch.length).toBeGreaterThan(0);
	expect(resultsWithMatch[0]._id).toBe(building._id);

	const resultsWithoutMatch = await apiWeb.buildings.list({ search: 'DOES-NOT-EXIST' });
	expect(resultsWithoutMatch.length).toBe(0);
});

test('should load buildings with pageSize and lastItemData', async () => {
	await utils.signupAndLogin(apiWeb);

	const nowTimeStamp = new Date().getTime();

	const street = `${faker.random.word()} ${nowTimeStamp}`;
	// adding letters to front so it would be returned when listing as it is created
	// because buildings are sorted in alphabetical order
	const build1 = await apiWeb.buildings.create({ street: `a${street}` });
	const build2 = await apiWeb.buildings.create({ street: `b${street}` });

	const build3 = await apiWeb.buildings.create({ street: `c${street}` });
	const build4 = await apiWeb.buildings.create({ street: `d${street}` });

	let buildings = await apiWeb.buildings.list({ search: `${nowTimeStamp}` });

	expect(buildings.length).toBe(4);

	buildings.forEach(building => {
		expect([build1._id, build2._id, build3._id, build4._id]).toContain(building._id);
	});

	buildings = await apiWeb.buildings.list({ pageSize: 2, search: `${nowTimeStamp}` });
	expect(buildings.length).toBe(2);

	buildings.forEach(building => {
		expect([build1._id, build2._id]).toContain(building._id);
	});

	buildings = await apiWeb.buildings.list({
		pageSize: 2,
		search: `${nowTimeStamp}`,
		lastPageItemId: build2._id,
		lastPageItemTitle: build2.title,
	});
	expect(buildings.length).toBe(2);

	buildings.forEach(building => {
		expect([build3._id, build4._id]).toContain(building._id);
	});
});

afterAll(async () => {
	await mongoose.disconnect();
	await mongoose.connection.close();
});
