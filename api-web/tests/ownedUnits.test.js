const { faker } = require('@faker-js/faker');
//common services
const { createOwnedUnit } = require('../services/ownedUnitService');
const { errors, OWNED_UNIT_STATUSES } = require('../../common/consts');
const { utils } = require('../testUtils/testUtils');
const ApiWeb = require('../testUtils/api.web');
const apiWeb = new ApiWeb();
//db
const mongoose = require('mongoose');
const OwnedUnit = require('../models/OwnedUnit');
require('../utils/dbConnect');

test('should create and list owned units', async () => {
	const { user } = await utils.signupAndLogin(apiWeb);

	const building = await apiWeb.buildings.create({ postcode: '00-ABC-123' });

	let buildingExtId = faker.random.word();
	let unitExtId = faker.random.word();
	let label = faker.random.word();

	const ownedUnit1 = await createOwnedUnit({
		building: building._id,
		buildingExtId,
		unitExtId,
		testLabel: label,
		owner: user._id,
		skipSF: true,
		testUnitNumber: 1,
	});

	buildingExtId = faker.random.word();
	unitExtId = faker.random.word();
	label = faker.random.word();

	const ownedUnit2 = await createOwnedUnit({
		building: building._id,
		buildingExtId,
		unitExtId,
		testLabel: label,
		owner: user._id,
		skipSF: true,
		testUnitNumber: 1,
	});

	const ownedUnits = await apiWeb.ownedUnits.list({});

	expect(ownedUnits).toHaveLength(2);

	ownedUnits.forEach(ownedUnit => {
		expect([ownedUnit1._id.toString(), ownedUnit2._id.toString()]).toContain(ownedUnit._id);
	});
});

test('should not create the same owned unit for the same user', async () => {
	const { user } = await utils.signupAndLogin(apiWeb);

	const building = await apiWeb.buildings.create({ postcode: '00-ABC-123' });

	const buildingExtId = faker.random.word();
	const unitExtId = faker.random.word();
	const label = faker.random.word();
	await createOwnedUnit({
		building: building._id,
		buildingExtId,
		unitExtId,
		testLabel: label,
		owner: user._id,
		skipSF: true,
		testUnitNumber: 1,
	});

	await expect(
		createOwnedUnit({
			building: building._id,
			buildingExtId,
			unitExtId,
			testLabel: label,
			owner: user._id,
			skipSF: true,
			testUnitNumber: 1,
		}),
	).rejects.toThrow(errors.UnitAlreadyAssigned);
});

test('should list owned units by status', async () => {
	const { user } = await utils.signupAndLogin(apiWeb);

	const building = await apiWeb.buildings.create({ postcode: '00-ABC-123' });

	let buildingExtId = faker.random.word();
	let unitExtId = faker.random.word();
	let label = faker.random.word();

	const ownedUnit1 = await createOwnedUnit({
		building: building._id,
		buildingExtId,
		unitExtId,
		testLabel: label,
		owner: user._id,
		skipSF: true,
		testUnitNumber: 1,
	});
	await OwnedUnit.findByIdAndUpdate(ownedUnit1._id, { status: OWNED_UNIT_STATUSES.approved });

	buildingExtId = faker.random.word();
	unitExtId = faker.random.word();
	label = faker.random.word();

	await createOwnedUnit({
		building: building._id,
		buildingExtId,
		unitExtId,
		testLabel: label,
		owner: user._id,
		skipSF: true,
		testUnitNumber: 1,
	});

	const ownedUnits = await apiWeb.ownedUnits.list({ status: OWNED_UNIT_STATUSES.approved });

	expect(ownedUnits).toHaveLength(1);

	expect(ownedUnits[0]._id).toBe(ownedUnit1._id.toString());
});

afterAll(async () => {
	await mongoose.disconnect();
	await mongoose.connection.close();
});
