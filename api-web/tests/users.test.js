const { faker } = require('@faker-js/faker');
//common services
const { utils } = require('../testUtils/testUtils');
const { httpStatus } = require('../utils/constants');
const { errors, LOGIN_TYPE } = require('../../common/consts');
const ApiWeb = require('../testUtils/api.web');
const apiWeb = new ApiWeb();
//db
const mongoose = require('mongoose');
require('../utils/dbConnect');
//models
const User = require('../models/User');

// /profile/info
test('should update user', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	const name = faker.name.findName();
	await apiWeb.user.update({ name });
	const user = await User.findOne({ email });
	expect(user.name).toBe(name);
});

// /settings
test('should get settings info', async () => {
	await utils.signupAndLogin(apiWeb);

	await apiWeb.user.settings().catch(res => {
		expect(res).toHaveProperty('agreements');
		expect(res).toHaveProperty('bills');
	});
});

// /settings
test('should not update settings when request is empty', async () => {
	await utils.signupAndLogin(apiWeb);
	//await apiWeb.user.settingsUpdate({});

	await expect(apiWeb.user.settingsUpdate({})).rejects.toThrow({
		message: utils.throwMessage(httpStatus.BadRequest, errors.RequestParameterMissing),
	});

	// const settings = await apiWeb.user.settings();
	// expect(settings.agreements.privacyPolicy).toBeFalsy();
	// expect(settings.bills.receiveFormat).toBe('digital');
});

// /settings
test('should not update agreements settings', async () => {
	await utils.signupAndLogin(apiWeb);

	await apiWeb.user.settingsUpdate({ agreements: { privacyPolicy: true, personalData: true, userRules: true } });

	const settingsAgree = await apiWeb.user.settings();
	expect(settingsAgree.agreements.privacyPolicy).toBeTruthy();
	expect(settingsAgree.agreements.personalData).toBeTruthy();
	expect(settingsAgree.agreements.userRules).toBeTruthy();

	await apiWeb.user.settingsUpdate({ agreements: { privacyPolicy: false, personalData: false, userRules: false } });

	const settingsNotAgree0 = await apiWeb.user.settings();
	expect(settingsNotAgree0.agreements.privacyPolicy).toBeTruthy();
	expect(settingsNotAgree0.agreements.personalData).toBeTruthy();
	expect(settingsNotAgree0.agreements.userRules).toBeTruthy();

	await apiWeb.user.settingsUpdate({ agreements: { privacyPolicy: false, personalData: true, userRules: false } });

	const settingsNotAgree1 = await apiWeb.user.settings();
	expect(settingsNotAgree1.agreements.privacyPolicy).toBeTruthy();
	expect(settingsNotAgree1.agreements.personalData).toBeTruthy();
	expect(settingsNotAgree1.agreements.userRules).toBeTruthy();
});

// /settings
test('should update settings', async () => {
	await utils.signupAndLogin(apiWeb);
	await apiWeb.user.settingsUpdate({
		agreements: { privacyPolicy: true, personalData: true, userRules: true },
		bills: { receiveFormat: 'both' },
	});

	const settings = await apiWeb.user.settings();
	expect(settings.agreements.privacyPolicy).toBeTruthy();
	expect(settings.bills.receiveFormat).toBe('both');
});

test('should update password and throw errors accordingly', async () => {
	const { user, password } = await utils.signupAndLogin(apiWeb);

	//const newPassword = faker.internet.password(8, false, /[A-Z\d@$!%*#?&]/, faker.datatype.number(1));
	const newPassword = utils.randomEmailPassword().password;
	const repeatPassword = newPassword;

	await expect(apiWeb.user.profilePasswordUpdate({ newPassword, repeatPassword })).rejects.toThrow(errors.PasswordRequired);
	await expect(apiWeb.user.profilePasswordUpdate({ currentPassword: password, repeatPassword })).rejects.toThrow(errors.PasswordRequired);
	await expect(apiWeb.user.profilePasswordUpdate({ currentPassword: password, newPassword })).rejects.toThrow(errors.PasswordRequired);
	await expect(
		apiWeb.user.profilePasswordUpdate({ currentPassword: password, newPassword, repeatPassword: repeatPassword + '1' }),
	).rejects.toThrow(errors.NotEqual);

	await expect(apiWeb.user.profilePasswordUpdate({ currentPassword: password + '1', newPassword, repeatPassword })).rejects.toThrow(
		errors.InvalidPassword,
	);

	await expect(
		apiWeb.user.profilePasswordUpdate({ currentPassword: password, newPassword: '123', repeatPassword: '123' }),
	).rejects.toThrow(errors.WeakPassword);

	await apiWeb.user.profilePasswordUpdate({ currentPassword: password, newPassword, repeatPassword });

	await apiWeb.auth.logout();

	const res = await apiWeb.auth.login({ loginType: LOGIN_TYPE.email, email: user.email, password: newPassword });

	expect(res).toHaveProperty('token');
});

test('should update email with correct verification code', async () => {
	const {
		user: { email: email2 },
	} = await utils.signupAndLogin(apiWeb);

	const {
		user: { email },
		password,
	} = await utils.signupAndLogin(apiWeb);

	const newEmail = faker.internet.email(null, null, faker.internet.domainWord() + '.test');

	// should error if an email already exists
	await expect(apiWeb.user.updateEmailCode({ email: email2 })).rejects.toThrow(errors.UserAlreadyExists);

	await apiWeb.user.updateEmailCode({ email: newEmail });

	const user = await User.findOne({ email }).select('+verificationCode');

	await expect(apiWeb.user.updateEmail({ email: newEmail, code: '1234' })).rejects.toThrow(errors.InvalidUpdateVerification);

	await apiWeb.user.updateEmail({ email: newEmail, code: user.verificationCode });

	await apiWeb.auth.logout();

	await expect(apiWeb.auth.login({ loginType: LOGIN_TYPE.email, email, password })).rejects.toThrow(errors.UserNotFound);

	const res = await apiWeb.auth.login({ loginType: LOGIN_TYPE.email, email: newEmail, password });

	expect(res).toBeTruthy();
});

test('should NOT verify code when verification type is incorrect', async () => {
	const {
		user: { email },
		password,
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });
	let user = await User.findOne({ email }).select('+verificationCode');
	const newEmail = faker.internet.email(null, null, faker.internet.domainWord() + `${new Date()}`);
	// trying to change email with forgot password verification code
	await expect(apiWeb.user.updateEmail({ email: newEmail, code: user.verificationCode })).rejects.toThrow(
		errors.InvalidUpdateVerification,
	);
	await apiWeb.user.updateEmailCode({ email: newEmail });
	user = await User.findOne({ email }).select('+verificationCode');

	await expect(
		apiWeb.auth.passwordChange({ email, verificationCode: user.verificationCode, password, password1: password }),
	).rejects.toThrow(errors.InvalidVerification);
});

// profile password-update
test('should not update profile password when user is not verified', async () => {
	const { password } = await utils.signupAndLogin(apiWeb);

	await apiWeb.user
		.profilePasswordUpdate({ currentPassword: password, newPassword: 'Password123', repeatPassword: 'Password123' })
		.catch(res => {
			expect(res.error.message).toBe(errors.UserNotFound);
		});
});

test('should update phone with correct verification code', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);
	const newPhone = faker.phone.phoneNumber('+3706#######');
	await apiWeb.user.updatePhoneCode({ phone: newPhone });
	const user = await User.findOne({ email }).select('+verificationCode');
	await expect(apiWeb.user.updatePhone({ phone: newPhone, code: '1234' })).rejects.toThrow(errors.InvalidUpdateVerification);
	await apiWeb.user.updatePhone({ phone: newPhone, code: user.verificationCode });
	const userUpdated = await User.findOne({ email }).select('+verificationCode');
	expect(userUpdated.phone).toBe(newPhone);
});

test('should error if phone number exists for another user', async () => {
	await utils.signupAndLogin(apiWeb);

	const phone = faker.datatype.number(10000000);
	await apiWeb.user.update({ phone });

	// loging in with another user
	await utils.signupAndLogin(apiWeb);

	await expect(apiWeb.user.validatePhone(phone)).rejects.toThrow(errors.PhoneExists);
});

afterAll(async () => {
	await mongoose.disconnect();
	await mongoose.connection.close();
});
