const { faker } = require('@faker-js/faker');
const moment = require('moment');
//common services
const { utils } = require('../testUtils/testUtils');
const ApiWeb = require('../testUtils/api.web');
const { httpStatus, VERIFICATION_TYPES } = require('../utils/constants');
const { errors, LOGIN_TYPE } = require('../../common/consts');
const apiWeb = new ApiWeb();
//db
const mongoose = require('mongoose');
require('../utils/dbConnect');
//models
const User = require('../models/User');
const { VERIFICATION_EXPIRY_HOURS } = require('../services/verificationService');
const { expireVerificationCode } = require('../../cron/jobs');

// signup (email)
test('should not be able to use weak password of less than 8 symbols', async () => {
	const password = faker.internet.password(6);

	await expect(apiWeb.auth.signup({ password })).rejects.toThrow({
		message: utils.throwMessage(httpStatus.BadRequest, errors.WeakPassword),
	});
});
// signup (email)
test('should not be able to use weak password of no capital letters', async () => {
	const password = faker.internet.password(8).toLowerCase();

	await expect(apiWeb.auth.signup({ password })).rejects.toThrow({
		message: utils.throwMessage(httpStatus.BadRequest, errors.WeakPassword),
	});
});
// signup (email)
test('should not be able to use weak password of no digitals', async () => {
	const password = faker.random.words(1);

	await expect(apiWeb.auth.signup({ password })).rejects.toThrow({
		message: utils.throwMessage(httpStatus.BadRequest, errors.WeakPassword),
	});
});
// signup (email)
test('should not be able to insert duplicate email', async () => {
	const {
		user: { email },
		password,
	} = await utils.signupAndLogin(apiWeb);

	await expect(apiWeb.auth.signup({ email, password })).rejects.toThrow({
		message: utils.throwMessage(httpStatus.BadRequest, errors.UserAlreadyExists),
	});
});

// signup (email)
test('should receive OK status on signup', async () => {
	expect(await apiWeb.auth.signup()).toBe('OK');
});

// login (email)
test('should not be able to login without verification', async () => {
	const { email, password } = utils.randomEmailPassword();

	await apiWeb.auth.signup({ email, password });

	await expect(utils.logInEmail(apiWeb, { email, password })).rejects.toThrow(errors.UserNotFound);
});

// login (email)
test('should have empty verification code after login', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	const userVerified = await User.findOne({ email }).select('+verificationCode');
	expect(userVerified.verificationCode).toBeNull();
});

// login (email)
test('should be able to login with verification code and correct credentials', async () => {
	const { resLogin } = await utils.signupAndLogin(apiWeb);
	expect(resLogin).toHaveProperty('token');
});

// login (phone)
test('should send verification code when login with phone', async () => {
	const loginType = LOGIN_TYPE.phone;

	await utils.signupAndLogin(apiWeb);
	const phone = utils.randomPhone();
	await apiWeb.user.update({ phone });

	await apiWeb.auth.sendCode({ loginType, phone });
	const user = await User.findOne({ phone }).select('+verificationCode');

	expect(String(user.verificationCode)).toHaveLength(6);
	expect(user.verificationType).toBe(VERIFICATION_TYPES.phoneLogin);
});

// login (phone)
test('should be able to login with phone', async () => {
	const phone = utils.randomPhone();
	const login = await utils.logInPhone(apiWeb, { phone });
	expect(login).toHaveProperty('token');
});

// password-forgot
test('should not send verification code when user is not verified', async () => {
	const { email, password } = utils.randomEmailPassword();
	await apiWeb.auth.signup({ email, password });

	await expect(apiWeb.auth.passwordForgot({ email })).rejects.toThrow({
		message: utils.throwMessage(httpStatus.Unauthorized, errors.InvalidVerification),
	});
});

// password-forgot email
test('should receive verification code via email on forgot password', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });
	const user = await User.findOne({ email }).select('+verificationCode');
	expect(user.verificationCode).not.toBeNull();
});

// password-forgot phone
test('should receive verification code via phone on forgot password', async () => {
	await utils.signupAndLogin(apiWeb);
	const phone = utils.randomPhone();
	await apiWeb.user.update({ phone });
	await apiWeb.auth.passwordForgot({ phone });
	const user = await User.findOne({ phone }).select('+verificationCode');
	expect(user.verificationCode).not.toBeNull();
});

// verify-code
test('should not verify incorrect code after password forgot', async () => {
	const {
		user: { email, verificationCode },
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });

	await expect(apiWeb.auth.verifyCode({ email, verificationCode })).rejects.toThrow({
		message: utils.throwMessage(httpStatus.Unauthorized, errors.InvalidVerification),
	});
});

// verify-code
test('should verify code after password forgot', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });
	const user = await User.findOne({ email }).select('+verificationCode');

	await apiWeb.auth.verifyCode({ email, verificationCode: user.verificationCode }).catch(res => {
		expect(res.statusCode).toBe(httpStatus.Ok);
	});
});

//password-update
test('should not update password when verification code is incorrect', async () => {
	const {
		user: { email, verificationCode },
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });

	const { password } = utils.randomEmailPassword();

	await apiWeb.auth.passwordChange({ email, verificationCode, password, password1: password }).catch(res => {
		expect(res.error.message).toBe(errors.InvalidVerification);
	});
});

//password-update
test('should not update password when user is not verified', async () => {
	const { email, password } = utils.randomEmailPassword();
	await apiWeb.auth.signup({ email, password });

	await apiWeb.auth.passwordChange({ email, verificationCode: '123', password, password1: password }).catch(res => {
		expect(res.error.message).toBe(errors.UserNotFound);
	});
});

//password-update
test('should not update when passwords are not equal', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });
	const user = await User.findOne({ email }).select('+verificationCode');

	const password = utils.randomEmailPassword();
	const password1 = utils.randomEmailPassword();

	await apiWeb.auth.passwordChange({ email, verificationCode: user.verificationCode, password, password1 }).catch(res => {
		expect(res.error.message).toBe(errors.NotEqual);
	});
});

//password-update email
test('should update password with email when verification code is correct', async () => {
	const {
		user: { email },
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });
	const user = await User.findOne({ email }).select('+verificationCode');

	const { password } = utils.randomEmailPassword();

	await apiWeb.auth.passwordChange({ email, verificationCode: user.verificationCode, password, password1: password }).catch(res => {
		expect(res.statusCode).toBe(httpStatus.Ok);
	});
});

// password-update phone
test('should update password with phone when verification code is correct', async () => {
	await utils.signupAndLogin(apiWeb);
	const phone = utils.randomPhone();
	await apiWeb.user.update({ phone });
	await apiWeb.auth.passwordForgot({ phone });
	const user = await User.findOne({ phone }).select('+verificationCode');
	const { password } = utils.randomEmailPassword();
	await apiWeb.auth.passwordChange({ phone, verificationCode: user.verificationCode, password, password1: password }).catch(res => {
		expect(res.statusCode).toBe(httpStatus.Ok);
	});
});

// logout
test('should not be able to logout when unauthorized', async () => {
	await apiWeb.auth.logout().catch(res => {
		expect(res.statusCode).toBe(httpStatus.Unauthorized);
	});
});

// logout
test('should be able to logout', async () => {
	await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.logout().catch(res => {
		expect(res.statusCode).toBe(httpStatus.NoContent);
	});
});

test('should NOT verify code when verification type is incorrect', async () => {
	const {
		user: { email },
		password,
	} = await utils.signupAndLogin(apiWeb);

	await apiWeb.auth.passwordForgot({ email });

	let user = await User.findOne({ email }).select('+verificationCode');

	const newEmail = faker.internet.email(null, null, faker.internet.domainWord() + `${new Date()}`);

	// trying to change email with forgot password verification code
	await expect(apiWeb.user.updateEmail({ email: newEmail, code: user.verificationCode })).rejects.toThrow(
		errors.InvalidUpdateVerification,
	);

	await apiWeb.user.updateEmailCode({ email: newEmail });

	user = await User.findOne({ email }).select('+verificationCode');

	await expect(
		apiWeb.auth.passwordChange({ email, verificationCode: user.verificationCode, password, password1: password }),
	).rejects.toThrow(errors.InvalidVerification);
});

test('should expire verification fields', async () => {
	const password = 'Asdsdsds12345';
	const email = faker.internet.email(null, null, faker.internet.domainWord() + '.test');

	await apiWeb.auth.signup({ email, password });

	let user = await User.findOne({ email });

	user.verificationExpiry = moment(user.verificationExpiry)
		.subtract(VERIFICATION_EXPIRY_HOURS + 1, 'hours')
		.toDate();

	await user.save();

	await expireVerificationCode();

	user = await User.findOne({ email }).select('+verificationCode');

	expect(user.verificationType).toBeNull();
	expect(user.verificationCode).toBeNull();
	expect(user.verificationExpiry).toBeNull();
});

afterAll(async () => {
	await mongoose.disconnect();
	await mongoose.connection.close();
});
