const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { OWNED_UNIT_STATUSES } = require('../../common/consts');

const buildingSchema = new Schema(
	{
		building: { type: Schema.Types.ObjectId, ref: 'Building', required: true },
		buildingExtId: { type: String, required: true },
		unitExtId: { type: String, required: true },
		title: String,
		label: { type: String, required: true },
		unitNumber: { type: Number, required: true },
		unitLetter: String,
		status: { type: String, enum: Object.values(OWNED_UNIT_STATUSES), required: true },
		owner: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	},
	{ timestamps: true },
);

const OwnedUnit = mongoose.model('OwnedUnit', buildingSchema);

module.exports = OwnedUnit;
