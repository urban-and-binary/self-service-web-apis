const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const buildingSchema = new Schema(
	{
		title: { type: String, required: true },
		description: String,
		// If buildingExtId and/or unitExtId is applied, the announcement will be shown to
		// all users which are associated with an OwnedUnit, which has buildingExtId and/or unitExtId
		buildingExtId: String,
		unitExtId: String,
		// unitLabel should be built on receiving announcement, either just giving it
		// the buildings title, or any of the ownedUnit label, found by buildingExtId & unitExtId
		unitLabel: String,
		// Announcment for specific user - optional field. If this is specified
		// the announcment will be shown ONLY for the specified user
		forSpecUser: { type: Schema.Types.ObjectId, ref: 'User' },

		// Announcement that has been read by users
		readByUser: [{ type: Schema.Types.ObjectId, ref: 'User' }],
	},
	{ timestamps: true },
);

const Announcement = mongoose.model('Announcement', buildingSchema);

module.exports = Announcement;
