const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// agreements
const userAgreementSchema = new Schema({
	privacyPolicy: { type: Boolean, required: true, default: false },
	personalData: { type: Boolean, required: true, default: false },
	userRules: { type: Boolean, required: true, default: false },
	advertisment: { type: Boolean, required: true, default: false },
});

// bills
const userBillsSchema = new Schema({
	receiveFormat: { type: String, required: true, enum: ['digital', 'paper', 'both'], default: 'digital' },
});

const userSettingSchema = new Schema(
	{
		user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
		agreements: userAgreementSchema,
		bills: userBillsSchema,
	},
	{ timestamps: true },
);

const UserSetting = mongoose.model('UserSetting', userSettingSchema);

module.exports = UserSetting;
