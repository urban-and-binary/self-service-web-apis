const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//common services
const { errors } = require('../../common/consts');
const { OWNER_ROLE } = require('../../common/permissions');
const { VERIFICATION_TYPES } = require('../utils/constants');

const SALT_ROUNDS = 10;

const validateEmail = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/;

/* check for >=1 capital letter, >= 1 digital, min 8 symbols, special symbols allowed */
const passwordStrength = password => /^(?=.*[A-Z])(?=.*\d)[a-zA-Z\d@$!%*#?&]{8,}$/.test(password);

async function hashPassword(password) {
	if (!passwordStrength(password)) {
		throw Error(errors.WeakPassword);
	}
	const hash = await bcrypt.hash(password, SALT_ROUNDS);
	return hash;
}

const userSchema = new Schema(
	{
		email: {
			type: String,
			required: true,
			unique: true,
			trim: true,
			match: [validateEmail, errors.InvalidEmail],
		},
		password: { type: String, required: true, select: false },
		verificationType: { type: String, enum: [...Object.values(VERIFICATION_TYPES), null] },
		verificationCode: { type: Number, default: null, select: false },
		verificationExpiry: { type: Date, default: null },
		verifiedAt: { type: Date, default: null },
		name: { type: String },
		surname: { type: String },
		phone: { type: String, unique: true, sparse: true },
		address: { type: String },
		postcode: { type: String },
		city: { type: String },
		country: { type: String, default: 'Lithuania' },
		countryCode: { type: String, default: 'lt' },
		isOnboarded: { type: Boolean, required: true, default: false },
		role: { type: String, enum: [OWNER_ROLE], required: true },
	},
	{ timestamps: true },
);

userSchema.virtual('units', {
	ref: 'OwnedUnit',
	localField: '_id',
	foreignField: 'owner',
});

userSchema.virtual('settings', {
	ref: 'UserSetting',
	localField: '_id',
	foreignField: 'user',
	justOne: true,
});

userSchema.set('toObject', { virtuals: true });

userSchema.pre('save', async function(next) {
	if (this.password && this.isModified('password')) {
		this.password = await hashPassword(this.password);
	}
	next();
});

userSchema.pre('findOneAndUpdate', async function(next) {
	if ('password' in this.getUpdate()) {
		this.getUpdate().password = await hashPassword(this.getUpdate().password);
	}
	next();
});

userSchema.methods.isValidPassword = async function(password) {
	const compare = await bcrypt.compare(password, this.password);
	return compare;
};

const User = mongoose.model('User', userSchema);

//avoid duplicates
User.createIndexes();

module.exports = User;
