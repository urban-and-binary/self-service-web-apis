const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const buildingSchema = new Schema(
	{
		externalId: { type: String, unique: true, sparse: true },
		title: { type: String, required: true, text: true },
		name: { type: String },
		street: { type: String, required: true },
		houseNumber: { type: Number, required: true },
		houseLetter: { type: String },
		postcode: { type: String },
		municipality: { type: String },
		city: { type: String, required: true },
		countryCode: { type: String, required: true, default: 'lt' },
		floors: Number,
		buildYear: Number,
	},
	{ timestamps: true },
);

buildingSchema.virtual('ownedUnits', {
	ref: 'OwnedUnit',
	localField: '_id',
	foreignField: 'building',
});

buildingSchema.set('toObject', { virtuals: true });

buildingSchema.index({ title: 'text' });

buildingSchema.index({ _id: 1, title: 1 });

const Building = mongoose.model('Building', buildingSchema);

Building.createIndexes();

module.exports = Building;
