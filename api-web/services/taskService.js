const { OWNED_UNIT_STATUSES, TASK_STATUS, errors } = require('../../common/consts');
const { salesforce } = require('../../config');
const OwnedUnit = require('../models/OwnedUnit');
const { salesforceApi } = require('./salesforceService');

const SALESFORCE_TASK_STATUS = {
	['New']: TASK_STATUS.planned,
	['In Progress']: TASK_STATUS.inProgress,
	['On Hold']: TASK_STATUS.onHold,
	['Completed']: TASK_STATUS.done,
	['Closed']: TASK_STATUS.closed,
	['Cannot Complete']: TASK_STATUS.cannotComplete,
	['Canceled']: TASK_STATUS.canceled,
};

const SALESFORCE_TASK_SORT_FIELDS = {
	startDate: 'StartDate',
	endDate: 'EndDate',
};

const getSalesforceTasks = async ({
	status = 'all',
	ownedUnit = 'all',
	sortField = 'startDate',
	sortOrder = 'asc',
	pageSize = 5,
	page = 1,
	owner,
}) => {
	let ownedUnitExternalIds;

	if (!ownedUnit || ownedUnit === 'all') {
		ownedUnitExternalIds = await OwnedUnit.find({ owner, status: OWNED_UNIT_STATUSES.approved }).distinct('unitExtId');
	} else {
		ownedUnitExternalIds = await OwnedUnit.find({ owner, _id: ownedUnit, status: OWNED_UNIT_STATUSES.approved }).distinct('unitExtId');
	}

	const statusKey = Object.keys(SALESFORCE_TASK_STATUS).find(key => SALESFORCE_TASK_STATUS[key] === status) || 'all';

	const tasks = await salesforceApi({
		path: '/UnitsWorkOrders',
		method: 'post',
		doThrow: true,
		body: {
			unitIds: ownedUnitExternalIds,
			status: statusKey,
			sortField: SALESFORCE_TASK_SORT_FIELDS[sortField] || 'StartDate',
			sortOrder,
			pageSize,
			page,
		},
	});

	return {
		tasks: tasks.workOrders.map(task => ({
			...task,
			status: SALESFORCE_TASK_STATUS[task.status] || 'unknown',
		})),
		total: tasks.total,
	};
};

const getTaskFileUrl = async ({ owner, taskId, fileId }) => {
	// Validating file accesisibility
	const task = await salesforceApi({ path: `/UnitsWorkOrders/${taskId}` });

	if (!task) {
		throw Error(errors.NotFound);
	}

	let ownedUnit = [];

	if (task.unitId) {
		ownedUnit = await OwnedUnit.find({ owner, unitExtId: task.unitId, status: OWNED_UNIT_STATUSES.approved })
			.select('_id')
			.lean();
	}

	if (task.buildingId) {
		ownedUnit = await OwnedUnit.find({ owner, buildingExtId: task.buildingId, status: OWNED_UNIT_STATUSES.approved })
			.select('_id')
			.lean();
	}

	if (ownedUnit.length === 0) {
		throw Error(errors.NotFound);
	}

	const document = task.workOrderDocuments && task.workOrderDocuments.find(document => document.Id === fileId);

	if (!document) {
		throw Error(errors.NotFound);
	}

	return salesforce.baseUrl + `/services/data/v55.0/sobjects/ContentVersion/${document.Id}/VersionData`;
};

module.exports = {
	getSalesforceTasks,
	getTaskFileUrl,
};
