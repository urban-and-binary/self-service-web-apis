const { errors } = require('../../common/consts');
const { salesforceApi } = require('./salesforceService');
const { sortBy } = require('lodash');

const declareMeters = async ({ meters }) => {
	// validating fields
	for (const meter of meters) {
		const valueTo = parseFloat(meter.valueTo);

		if (!valueTo) {
			throw Error(errors.InvalidField);
		}

		const meterReadings = await salesforceApi({ path: `/MA/Meter/${meter.id}/Reading` });
		let lastValueTo = 0;

		if (meterReadings && meterReadings.length) {
			const meterReadSorted = sortBy(meterReadings, ['readingDateTime']).reverse();
			const latestMeterReading = meterReadSorted[0];
			lastValueTo = parseFloat(latestMeterReading.valueTo) || 0;
		}

		if (valueTo < lastValueTo) {
			throw Error(errors.InvalidField);
		}

		meter.valueFrom = lastValueTo;
	}

	// // updating fields
	const meterUpdateCalls = [];

	meters.forEach(meter => {
		meterUpdateCalls.push(
			salesforceApi({
				path: `/MA/Meter/${meter.id}/Reading`,
				method: 'post',
				body: { valueTo: meter.valueTo, valueFrom: meter.valueFrom, source: 'Self Service' },
				doThrow: true,
			}),
		);
	});

	try {
		// // using promise all to do parallel meter update calls
		// // instead of waiting for each one to finish
		await Promise.all(meterUpdateCalls);
	} catch (error) {
		if (error.message.includes('NUMBER_OUTSIDE_VALID_RANGE')) {
			throw Error(errors.InvalidField);
		}

		throw Error(error);
	}
};

module.exports = {
	declareMeters,
};
