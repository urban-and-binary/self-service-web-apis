const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const { errors } = require('../../common/consts');
const User = require('../models/User');
const { emailSubject, VERIFICATION_TYPES, SEND_TYPE } = require('../utils/constants');
const { TEMPLATES, sendVerificationMail } = require('./mailService');
// services
const { service: userService } = require('./userService');
const { verifyCode, sendVerificationSMS } = require('./verificationService');
//constants
const TOKEN_SECRET = crypto.randomBytes(64).toString('hex');
const TOKEN_EXPIRY = '1h';

const issueJWT = ({ userId, email }) => {
	const payload = {
		sub: userId,
		email,
		iat: Date.now(),
	};
	const token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: TOKEN_EXPIRY });
	return token;
};

const signUp = async ({ email, password }) => {
	if (!(email && password)) {
		throw new Error(errors.EmailAndPasswordRequired);
	}
	/* check if user already exists */
	let user = await User.findOne({ email });

	/* check if user is already verified */
	if (user && user.verifiedAt !== null) {
		throw Error(errors.UserAlreadyExists);
	}

	/* create a new user or update existing if user changed password on signup */
	if (!user) {
		user = await userService.createUser({ email, password });
	} else {
		await userService.updateUser(user, [{ password }]);
	}

	/* send verification email */
	const verify = sendVerificationMail(user, TEMPLATES.EMAIL_CONFIRM, emailSubject.emailConfirm);
	const verificationCode = verify.verificationCode;
	const verificationExpiry = verify.verificationExpiry;

	/* update user with verification code */
	await userService.updateUser(user, [{ verificationCode }, { verificationExpiry }, { verificationType: VERIFICATION_TYPES.signup }]);
};

const loginEmail = async ({ email, password, verificationCode = null }) => {
	if (!(email && password)) {
		throw Error(errors.EmailAndPasswordRequired);
	}

	/* check if user exists */
	const user = await userService.findUser({ email }, true, true);

	/* check if user is not verified and verificationCode missing */
	if (user.verifiedAt === null && verificationCode === null) {
		throw Error(errors.UserNotFound);
	}

	const comparePassword = await user.isValidPassword(password);

	/* verify user: if user is not verified && verificationCode is in request */
	/* create user settings */
	if (user.verifiedAt === null && verificationCode !== null) {
		const verify = await verifyCode(user, verificationCode, VERIFICATION_TYPES.signup, comparePassword);
		if (verify.is) {
			await userService.createUserSettings(user._id);
			await userService.updateUser(user, [
				{ verifiedAt: verify.date },
				{ verificationCode: null },
				{ verificationType: null },
				{ verificationExpiry: null },
			]);
		} else {
			throw Error(errors.InvalidVerification);
		}
	}

	/* check password for verified user */
	if (!comparePassword) {
		throw Error(errors.InvalidCredentials);
	}

	return issueJWT({ userId: user._id, email });
};

const loginPhone = async ({ phone, verificationCode }) => {
	if (!phone) {
		throw Error(errors.PhoneRequired);
	}

	/* check if user exists */
	const user = await userService.findUser({ phone }, false, true);

	/* check if user is verified */
	if (user.verifiedAt === null) {
		throw Error(errors.UserNotFound);
	}

	/* if verification code is correct set code, expiry, type to null */
	const verify = await verifyCode(user, verificationCode, VERIFICATION_TYPES.phoneLogin);
	if (verify.is) {
		await userService.updateUser(user, [{ verificationCode: null }, { verificationExpiry: null }, { verificationType: null }]);
	} else {
		throw Error(errors.InvalidVerification);
	}

	return issueJWT({ userId: user._id, email: user.email });
};

const loginVerificationSMS = async ({ phone }) => {
	const user = await userService.findUser({ phone }, false, true);

	/* check if user is verified */
	if (user.verifiedAt === null) {
		throw Error(errors.UserNotFound);
	}

	/* send verification SMS */
	const verify = sendVerificationSMS(phone);
	const verificationCode = verify.verificationCode;
	const verificationExpiry = verify.verificationExpiry;

	/* update user with verification code */
	await userService.updateUser(user, [{ verificationCode }, { verificationExpiry }, { verificationType: VERIFICATION_TYPES.phoneLogin }]);
};

const verificationCode = async ({ email, verificationCode }) => {
	/* check for email & verification code */
	if (!(email && verificationCode)) {
		throw Error(errors.EmailAndVerificationCodeRequired);
	}

	const user = await userService.findUser({ email }, false, true);

	/* check if code is in correct */
	const verify = await verifyCode(user, verificationCode, VERIFICATION_TYPES.forgot);
	if (!verify.is) {
		throw Error(errors.InvalidVerification);
	}
};

const verificationCodePhone = async ({ phone, verificationCode }) => {
	/* check for phone & verification code */
	if (!(phone && verificationCode)) {
		throw Error(errors.PhoneAndVerificationCodeRequired);
	}

	const user = await userService.findUser({ phone }, false, true);

	/* check if code is in correct */
	const verify = await verifyCode(user, verificationCode, VERIFICATION_TYPES.forgot);
	if (!verify.is) {
		throw Error(errors.InvalidVerification);
	}
};

const passwordChange = async ({ email, phone, verificationCode, password, password1 }) => {
	if (!email && !phone) {
		throw Error(errors.EmailOrPhoneRequired);
	}
	if (!verificationCode) {
		throw Error(errors.VerificationCodeRequired);
	}
	if (!password && !password1) {
		throw Error(errors.PasswordRequired);
	}
	if (password !== password1) {
		throw Error(errors.NotEqual);
	}

	const user = await userService.findUser(email ? { email } : { phone }, false, true);
	/* check if user is verified */
	if (user.verifiedAt === null) {
		throw Error(errors.UserNotFound);
	}

	/* if code is correct: update password, remove verificationCode and expiry */
	const verify = await verifyCode(user, verificationCode, VERIFICATION_TYPES.forgot);
	if (verify.is) {
		await userService.updateUser(user, [
			{ password },
			{ verificationCode: null },
			{ verificationType: null },
			{ verificationExpiry: null },
		]);
	} else {
		throw Error(errors.InvalidVerification);
	}
};

const passwordForgot = async ({ email, phone }) => {
	if (!email && !phone) {
		throw Error(errors.EmailOrPhoneRequired);
	}

	const user = await userService.findUser(email ? { email } : { phone });
	await userService.generateCodeForUser({
		user,
		sendType: email ? SEND_TYPE.email : SEND_TYPE.phone,
		sendTo: email ? email : phone,
		type: VERIFICATION_TYPES.forgot,
	});
};

module.exports = {
	TOKEN_SECRET,
	issueJWT,
	signUp,
	verificationCode,
	verificationCodePhone,
	passwordChange,
	passwordForgot,
	loginEmail,
	loginPhone,
	loginVerificationSMS,
};
