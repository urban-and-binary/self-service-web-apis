const { default: axios } = require('axios');
const { errors } = require('../../common/consts');
const { salesforce } = require('../../config');

const getToken = async () => {
	try {
		const resp = await axios.post(
			`${salesforce.loginUrl}/services/oauth2/token?grant_type=password&client_id=${salesforce.clientId}&client_secret=${salesforce.clientSecret}&username=${salesforce.username}&password=${salesforce.password}`,
		);

		return resp.data.access_token;
	} catch (error) {
		const errMessage = (error.response && error.response.data && error.response.data.message) || error.message;
		console.error('get salesforce token error', errMessage);
		return null;
	}
};

const salesforceApi = async ({ path, body, method = 'get', doThrow = false }) => {
	const url = `${salesforce.baseUrl}/services/apexrest${path}`;

	const token = await getToken();

	if (token) {
		try {
			const axiosParams = [url];

			if (body) {
				axiosParams.push(body);
			}

			axiosParams.push({
				headers: {
					Authorization: `Bearer ${token}`,
				},
			});

			const resp = await axios[method](...axiosParams);

			return resp.data;
		} catch (error) {
			const errMessage = (error.response && error.response.data && error.response.data.message) || error.message;
			console.error(`sales force api error with path - ${path}`, errMessage);
			if (doThrow) {
				throw Error(`Sales force - ${errMessage}`);
			}
			return null;
		}
	}
};

const salesforceFileStream = async ({ fileUrl }) => {
	const token = await getToken();

	if (token) {
		return await axios.get(fileUrl, {
			responseType: 'stream',
			headers: {
				Authorization: `Bearer ${token}`,
			},
		});
	} else {
		throw Error(errors.NotFound);
	}
};

module.exports = {
	salesforceApi,
	salesforceFileStream,
};
