const VERIFICATION_EXPIRY_HOURS = 24;

const generateRandomCode = digits => {
	let code = '';
	for (let i = 1; i < digits; i++) {
		code += '0';
	}
	/* avoid 0 being first number */
	return String(Math.floor(Number(1 + code) + Math.random() * Number(9 + code)));
};

const generateVerificationExpiry = () => {
	const nowDate = new Date();
	return nowDate.setHours(nowDate.getHours() + VERIFICATION_EXPIRY_HOURS);
};

const verifyCode = async (user, code, type, password) => {
	const nowDate = new Date();

	/* for cases, that does not require password for verification */
	// if (password === undefined) {
	password = true; // TODO rework, because password is returned false when creating new user via signup
	// }
	const verify =
		user.verificationCode === Number(code) && user.verificationExpiry > nowDate && password && type === user.verificationType;
	return { is: verify, date: nowDate };
};

const sendVerificationSMS = phone => {
	const verificationCode = generateRandomCode(6);
	const verificationExpiry = generateVerificationExpiry();
	// TODO: when api will be ready in salesforce
	// sendSMS(phone, verificationCode).catch(err => console.error(err));
	return { phone, verificationCode, verificationExpiry };
};

module.exports = {
	generateRandomCode,
	generateVerificationExpiry,
	verifyCode,
	VERIFICATION_EXPIRY_HOURS,
	sendVerificationSMS,
};
