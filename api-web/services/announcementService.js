const { ANNOUNCEMENT_QUERY_TYPE_VALUES, OWNED_UNIT_STATUSES, errors } = require('../../common/consts');
const { paginatedQuery } = require('../utils/query');
const Announcement = require('../models/Announcement');
const OwnedUnit = require('../models/OwnedUnit');
const { salesforceApi } = require('./salesforceService');

const createAnnouncement = annData => Announcement.create(annData);

const announcementRead = (annId, userId) => Announcement.findByIdAndUpdate(annId, { $push: { readByUser: userId } });

const listAnnouncements = async ({ type, ownedUnit, userId, sortField = 'createdAt', sortOrder = 1, pageSize = 5, page = 1 }) => {
	const andQuery = [];

	if (type === ANNOUNCEMENT_QUERY_TYPE_VALUES.private) {
		andQuery.push({ forSpecUser: userId });
	} else {
		const ownedUnits = await OwnedUnit.find({ owner: userId, status: OWNED_UNIT_STATUSES.approved }).lean();

		const buildExtIds = [];
		const unitExtIds = [];

		ownedUnits.forEach(owUnit => {
			buildExtIds.push(owUnit.buildingExtId);
			unitExtIds.push(owUnit.unitExtId);
		});

		// so this initial or query is for the "common" announcement type
		const orQuery = [
			// messages specific for user ownedUnits which are approved
			{
				forSpecUser: null,
				buildingExtId: { $in: buildExtIds },
				$or: [{ unitExtId: null }, { unitExtId: { $in: unitExtIds } }],
			},
			// announcement for everyone
			{ forSpecUser: null, buildingExtId: null, unitExtId: null },
		];

		if (!type || type === ANNOUNCEMENT_QUERY_TYPE_VALUES.all) {
			// if its the "all" announcement type query, we also want to retrieve
			// the user specific announcements
			orQuery.push({ forSpecUser: userId });
		}

		andQuery.push({ $or: orQuery });
	}

	if (ownedUnit && ownedUnit !== 'all') {
		const ownedUnitObj = await OwnedUnit.findOne({ _id: ownedUnit, owner: userId, status: OWNED_UNIT_STATUSES.approved }).lean();

		// checking like this because announcement can be for the whole building
		// or a specific unit
		andQuery.push({ buildingExtId: ownedUnitObj.buildingExtId, $or: [{ unitExtId: null }, { unitExtId: ownedUnitObj.unitExtId }] });
	}

	const { total, data } = await paginatedQuery({
		Model: Announcement,
		query: { $and: andQuery },
		page,
		pageSize,
		sortField,
		sortOrder,
	});

	return {
		total,
		data: data.map(ann => ({
			_id: ann._id,
			title: ann.title,
			description: ann.description,
			unitLabel: ann.unitLabel,
			forSpecUser: ann.forSpecUser,
			buildingExtId: ann.buildingExtId,
			unitExtId: ann.unitExtId,
			read: ann.readByUser.some(userRead => userRead.equals(userId)),
			createdAt: ann.createdAt,
		})),
	};
};

const formAnnUnreadFindQuery = async userId => {
	const ownedUnits = await OwnedUnit.find({ owner: userId, status: OWNED_UNIT_STATUSES.approved }).lean();

	const buildExtIds = [];
	const unitExtIds = [];

	ownedUnits.forEach(owUnit => {
		buildExtIds.push(owUnit.buildingExtId);
		unitExtIds.push(owUnit.unitExtId);
	});

	return {
		$or: [
			// messages specific for user
			{ forSpecUser: userId },
			// messages specific for user ownedUnits which are approved
			{
				forSpecUser: null,
				buildingExtId: { $in: buildExtIds },
				$or: [{ unitExtId: null }, { unitExtId: { $in: unitExtIds } }],
			},
			// announcement for everyone
			{ forSpecUser: null, buildingExtId: null, unitExtId: null },
		],
		readByUser: { $ne: userId },
	};
};

const getUserUnreadAnnouncements = async userId => {
	const findQuery = await formAnnUnreadFindQuery(userId);

	return Announcement.find(findQuery)
		.select('title description unitLabel createdAt')
		.sort({ createdAt: -1 });
};

const markAllAnnsRead = async userId => {
	const findQuery = await formAnnUnreadFindQuery(userId);

	return Announcement.find(findQuery).updateMany({ $push: { readByUser: userId } });
};

const createSealesforceAnnouncements = async ({ buildingId, unitId, title, text }) => {
	let buildingExtId = buildingId;
	let building, unit, unitLabel;

	if (unitId) {
		unit = ((await salesforceApi({ path: `/Unit/${unitId}` })) || [])[0];
		if (!unit) {
			throw Error(errors.NotFound);
		}

		buildingExtId = unit.buildingId;
	}

	if (buildingExtId) {
		building = ((await salesforceApi({ path: `/Building/${buildingExtId}` })) || [])[0];
		if (!building) {
			throw Error(errors.NotFound);
		}
	}

	if (building) {
		unitLabel = `${building.name}${unit ? `- ${unit.unitNumber || ''}${unit.unitLetter || ''}` : ''}, ${
			building.municipality ? building.municipality + ', ' : ''
		}${building.city}`;
	}

	await createAnnouncement({ title, description: text, unitLabel, buildingExtId, unitExtId: unitId });
};

module.exports = {
	createAnnouncement,
	announcementRead,
	listAnnouncements,
	getUserUnreadAnnouncements,
	markAllAnnsRead,
	createSealesforceAnnouncements,
};
