const { EmailParams, Recipient, Attachment } = require('mailersend');
const MailerSend = require('mailersend');
const { mailersendApiToken, emailFrom, emailFromName, debugEmailResponse } = require('../../config');
const { generateRandomCode, generateVerificationExpiry } = require('./verificationService');

// MailerSend email template IDs
const TEMPLATES = {
	EMAIL_CONFIRM: 'pq3enl6k83r42vwr',
	EMAIL_RESEND_PASSWORD: 'v69oxl5e1rzl785k',
	EMAIL_UNIT_ASSIGNED: 'o65qngkd7n3lwr12',
	EMAIL_UNIT_ASSIGN_FAIL: 'jy7zpl98wop45vx6',
};

const mailersend = new MailerSend({
	api_key: mailersendApiToken,
});

const sendMail = async ({ from = emailFrom, fromName = emailFromName, to, subject, template, data, attachments: files }) => {
	if (process.env.NODE_ENV !== 'production') {
		console.info('Not sending email in development environment');
		return;
	}

	if (to.endsWith('.test')) {
		console.info('Not sending emails on test runs');
		return;
	}

	if (!mailersendApiToken) {
		console.warn('Missing mailer API token');
		return;
	}

	if (!to) throw Error('MissingEmailRecipient');
	if (!template) throw Error('MissingEmailTemplate');

	console.info(`Sending email to ${to}`);

	const recipients = prepareRecipients(to);
	const variables = prepareVariables(recipients, data);
	const attachments = prepareAttachments(files);

	const emailParams = new EmailParams()
		.setFrom(from)
		.setFromName(fromName)
		.setRecipients(recipients)
		.setTemplateId(template)
		.setAttachments(attachments)
		.setSubject(subject);

	if (variables) emailParams.setVariables(variables);

	const response = await mailersend.send(emailParams);
	if (debugEmailResponse) console.info('Email response', response);

	if (response.status !== 202) throw Error(response.statusText);
};

const prepareAttachments = (files = []) => {
	if (!files.length) return;

	/*
		MailerSend expected variables format:
		const attachments = [
			new Attachment(fs.readFileSync('/path/to/file.pdf', {encoding: 'base64'}), 'file.pdf')
		]
	*/

	const attachments = files.map(file => new Attachment(file.buffer, file.name));
	return attachments;
};

const prepareVariables = (recipients, data) => {
	if (!data) return;

	/*
		MailerSend expected variables format:
		const variables = [
			{
				email: "your@client.com",
				substitutions: [
					{
						var: 'test',
						value: 'Test Value'
					}
				],
			}
		];
	*/

	const substitutions = Object.keys(data).map(key => ({
		var: key,
		value: data[key],
	}));

	const variables = recipients.map(recipient => ({
		email: recipient.email,
		substitutions,
	}));

	return variables;
};

const prepareRecipients = emails => {
	/*
		MailerSend expected recipients format:
		const recipients = [
			new Recipient("your@client.com", "Your Client")
		];
	*/

	// Array of emails
	if (Array.isArray(emails)) return emails.map(email => new Recipient(email));

	// Single email
	return [new Recipient(emails)];
};

const sendVerificationMail = (user, template, subject) => {
	const verificationCode = generateRandomCode(6);
	const verificationExpiry = generateVerificationExpiry();
	sendMail({
		to: user.email,
		template,
		subject,
		data: {
			email: user.email,
			name: user.name || '',
			code: verificationCode,
		},
	}).catch(err => console.error(err));
	return { verificationCode, verificationExpiry };
};

module.exports = {
	TEMPLATES,
	sendMail,
	sendVerificationMail,
};
