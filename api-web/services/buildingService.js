const { errors } = require('../../common/consts');
const { formUnitLabel } = require('../../common/utils');
const Building = require('../models/Building');
const OwnedUnit = require('../models/OwnedUnit');
const { formBuildingLabel } = require('../utils/general');
const { salesforceApi } = require('./salesforceService');
const { keyBy } = require('lodash');

const DIACRITIC_MAP = ['aą', 'cč', 'eęė', 'iį', 'sš', 'uųū', 'zž'];

// So this function will be turning the passed in string
// to diacriting insensitive word, specifically for searching by regex
// Works only with Lithuanian letters currently
const turnDiacriticInsensitive = word => {
	let diacriticInsensitiveWord = '';

	if (word && word.length) {
		for (let i = 0; i < word.length; i++) {
			const letter = word[i];

			const diacritic = DIACRITIC_MAP.find(diacriticEquivalent => diacriticEquivalent.includes(letter));

			// these brackets are needed for regex
			const diacriticLetter = diacritic ? `[${diacritic}]` : letter;

			diacriticInsensitiveWord += diacriticLetter;
		}
	} else {
		diacriticInsensitiveWord = word;
	}

	return diacriticInsensitiveWord;
};

// NOTE: lastPageItemId && lastPageItemTitle need to be
// passed in the query for this to work
const queryBuildings = async queryParams => {
	// NOTE: so we'll be using the "keyset" approach for pagination
	// using the lastPageItem's data as the so to say "pageNumber". This is to achieve better performance
	// in comparison to the ".skip" pagination approach. Do know this approach will not
	// act as normal pagination but will work more like infinite scrolling.
	// For more info on the "keyset" approach and how to handle other cases like $or
	// see - https://medium.com/swlh/mongodb-pagination-fast-consistent-ece2a97070f3

	const find = {};

	if (queryParams) {
		const { search, lastPageItemId, lastPageItemTitle } = queryParams;

		// querying section
		if (search) {
			const searchTerms = search.split(' ');

			find.$and = [];

			searchTerms.forEach(searchTerm =>
				find.$and.push({
					title: { $regex: turnDiacriticInsensitive(searchTerm), $options: 'i' },
				}),
			);
		}

		// pagination section
		if (lastPageItemId && lastPageItemTitle) {
			find.$or = [
				{ title: { $gt: lastPageItemTitle } },
				{
					title: lastPageItemTitle,
					_id: { $gt: lastPageItemId },
				},
			];
		}
	}

	// starting the actual query here
	const buildingQuery = Building.find(find).sort({ title: 1, _id: 1 });

	if (queryParams && queryParams.pageSize) {
		buildingQuery.limit(queryParams.pageSize);
	}

	return buildingQuery.lean().exec();
};

const createBuilding = async ({ street, houseNumber, houseLetter = '', postcode, city, countryCode }) => {
	const title = `${street} ${houseNumber}${houseLetter}, ${city}, ${postcode}`;

	const building = await Building.create({
		title,
		street,
		houseNumber,
		houseLetter,
		postcode,
		city,
		countryCode,
	});

	return building;
};

const getBuildingUnits = async buildingId => {
	// NOTE: yes this is how you get units of a building
	const units = (await salesforceApi({ path: `/Unit/${buildingId}` })) || [];
	return units;
};

const upsertSalesforceBuilding = async buildings => {
	const bulkBuildingWrite = [];

	if (!buildings || !Array.isArray(buildings)) {
		throw Error(errors.RequestParameterMissing);
	}

	buildings.forEach(building => {
		const { id, name, municipality, city, street, houseNumber, houseLetter, postCode, floors, buildYear } = building;

		if (!id || !name || !city || !street || !houseNumber) {
			throw Error(errors.FieldMissing);
		}

		bulkBuildingWrite.push({
			updateOne: {
				filter: { externalId: id },
				update: {
					externalId: id,
					title: formBuildingLabel(building),
					name,
					street,
					houseNumber,
					houseLetter,
					postcode: postCode,
					city,
					municipality,
					floors,
					buildYear,
				},
				upsert: true,
			},
		});
	});

	await Building.bulkWrite(bulkBuildingWrite);

	const buildingIds = buildings.map(building => building.id);

	const buildingsByKeys = keyBy(buildings, 'id');

	const ownedUnits = await OwnedUnit.find({ buildingExtId: { $in: buildingIds } }).lean();

	const bulkUnitArray = [];

	ownedUnits.forEach(ownedUnit => {
		const buildingUpdate = buildingsByKeys[ownedUnit.buildingExtId];

		bulkUnitArray.push({
			updateOne: {
				filter: { _id: ownedUnit._id },
				update: {
					label: formUnitLabel(buildingUpdate, ownedUnit),
				},
			},
		});
	});

	// Then we updated ownedUnits of the updated building with the new values
	await OwnedUnit.bulkWrite(bulkUnitArray);
};

module.exports = {
	createBuilding,
	queryBuildings,
	getBuildingUnits,
	upsertSalesforceBuilding,
};
