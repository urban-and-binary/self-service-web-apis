const OwnedUnit = require('../models/OwnedUnit');
const User = require('../models/User');
const { OWNED_UNIT_STATUSES, errors, UNIT_METER_TYPES } = require('../../common/consts');
const { salesforceApi } = require('./salesforceService');
const { sendMail, TEMPLATES } = require('./mailService');
const { emailSubject } = require('../utils/constants');
const { selfServiceSupportEmail, salesforce } = require('../../config');
const { createAnnouncement } = require('./announcementService');
const { sortBy, keyBy } = require('lodash');
const moment = require('moment');
const Building = require('../models/Building');
const { formUnitLabel } = require('../../common/utils');

const SALESFORCE_METER_TYPES_MAP = {
	['Cold Water']: UNIT_METER_TYPES.coldWater,
	['Hot Water']: UNIT_METER_TYPES.hotWater,
	['Electricity']: UNIT_METER_TYPES.electricity,
	['Heating']: UNIT_METER_TYPES.heating,
};

const createOwnedUnit = async ({
	building,
	buildingExtId,
	unitExtId,
	title,
	owner,
	skipSF = false,
	testLabel = null,
	testUnitNumber = null,
}) => {
	const ownedUnitExist = await OwnedUnit.findOne({ unitExtId, owner });

	if (ownedUnitExist) {
		throw Error(errors.UnitAlreadyAssigned);
	}

	let label = testLabel;
	let unit;

	if (!skipSF) {
		unit = ((await salesforceApi({ path: `/Unit/${unitExtId}` })) || [])[0];

		if (!unit) {
			throw Error(errors.NotFound);
		}

		const localBuilding = await Building.findById(building);

		if (!localBuilding) {
			throw Error(errors.NotFound);
		}

		label = formUnitLabel(localBuilding, unit);
	}

	const ownedUnit = await OwnedUnit.create({
		building,
		buildingExtId,
		unitExtId,
		title,
		label,
		owner,
		status: OWNED_UNIT_STATUSES.pending,
		unitNumber: skipSF ? testUnitNumber : unit.unitNumber,
		unitLetter: unit && unit.unitLetter,
	});

	return ownedUnit;
};

const getOwnedUnits = ({ owner, status }) => {
	const findQuery = { owner };

	if (status) {
		findQuery.status = status;
	}

	return OwnedUnit.find(findQuery).sort({ label: 1 });
};

const getOwnedUnit = async ({ id, owner }) => {
	const ownedUnit = await OwnedUnit.findOne({ _id: id, status: OWNED_UNIT_STATUSES.approved, owner })
		.populate('building')
		.lean();

	if (!ownedUnit) {
		throw Error(errors.NotFound);
	}

	const unit = ((await salesforceApi({ path: `/Unit/${ownedUnit.unitExtId}?detail=true` })) || [])[0];

	if (!unit) {
		throw Error(errors.NotFound);
	}

	ownedUnit.areaIndoor = unit.areaIndoor;
	ownedUnit.floorNumber = unit.floorNumber;

	ownedUnit.documents = unit.unitDocuments;

	ownedUnit.funds = unit.accumulativeFunds.map(accumulatedFund => ({
		type: accumulatedFund.Type__c,
		totalAmount: accumulatedFund.Balance__c,
		currency: accumulatedFund.CurrencyIsoCode,
	}));

	return ownedUnit;
};

const getUnitContacts = async ({ unitExtId, role }) => {
	return (await salesforceApi({ path: `/Unit/${unitExtId}/Contacts/?role=${role}` })) || [];
};

const validateOwnedUnit = async ({ ownerId, ownedUnitId }) => {
	const ownedUnit = await OwnedUnit.findById(ownedUnitId);

	const owners = await getUnitContacts({ unitExtId: ownedUnit.unitExtId, role: 'Owner' });
	const owner = await User.findById(ownerId)
		.lean()
		.exec();

	const validOwner = owners.some(extOwner => {
		if (!owner.name || !owner.surname || !extOwner.firstName || !extOwner.lastName) {
			return false;
		}

		return (
			owner.name.toLowerCase() === extOwner.firstName.toLowerCase() && owner.surname.toLowerCase() === extOwner.lastName.toLowerCase()
		);
	});

	let status = OWNED_UNIT_STATUSES.failed;

	if (validOwner) {
		status = OWNED_UNIT_STATUSES.approved;
		sendMail({
			to: owner.email,
			subject: emailSubject.emailUnitAssigned,
			template: TEMPLATES.EMAIL_UNIT_ASSIGNED,
			data: {
				addressLabel: ownedUnit.label,
			},
		}).catch(err => console.error(`EMAIL_UNIT_ASSIGNED Email send failed with error - ${err.message}`));

		await createAnnouncement({
			title: 'Unit added successfully',
			description: `${ownedUnit.label} - unit added successfully`,
			forSpecUser: ownerId,
			buildingExtId: ownedUnit.buildingExtId,
			unitExtId: ownedUnit.unitExtId,
			unitLabel: ownedUnit.label,
		});
	} else {
		// so if validation fails, meaning there's no such contact associated with the unit, we'll add
		// this kind of contact to the contact list
		salesforceApi({
			path: `/Unit/${ownedUnit.unitExtId}/Contacts`,
			method: 'post',
			body: {
				firstName: owner.name,
				lastName: owner.surname,
				fullName: `${owner.name} ${owner.surname}`,
				email: owner.email,
				// and currently at least we'll be creating, only owners via this endpoint
				role: 'Owner',
				phone: owner.phone,
			},
		});

		sendMail({
			to: selfServiceSupportEmail,
			subject: emailSubject.emailUnitAssignFailure,
			template: TEMPLATES.EMAIL_UNIT_ASSIGN_FAIL,
			data: {
				ownerName: `${owner.name} ${owner.surname}`,
				ownerEmail: owner.email,
				addressLabel: ownedUnit.label,
			},
		}).catch(err => console.error(`EMAIL_UNIT_ASSIGN_FAIL Email send failed with error - ${err.message}`));
	}

	ownedUnit.status = status;

	await ownedUnit.save();
};

const ownedUnitsWithMeters = async userId => {
	const ownedUnits = await getOwnedUnits({ owner: userId, status: OWNED_UNIT_STATUSES.approved }).lean();

	for (const ownedUnit of ownedUnits) {
		let lastReadingUnit;
		// getting meters of a unit
		const unitMeters = (await salesforceApi({ path: `/MA/Meter/${ownedUnit.unitExtId}` })) || [];

		const unit = ((await salesforceApi({ path: `/Unit/${ownedUnit.unitExtId}?withCustomerCode=true` })) || [])[0];

		const meters = [];
		for (const unitMeter of unitMeters) {
			const localType = SALESFORCE_METER_TYPES_MAP[unitMeter.type];

			// will only be returning known meter types
			if (localType) {
				if (!lastReadingUnit || moment(lastReadingUnit).isBefore(moment(unitMeter.lastReading))) {
					lastReadingUnit = moment(unitMeter.lastReading).toISOString();
				}

				const meter = { ...unitMeter, localType };

				let valueFrom = 0;
				let valueTo = 0;
				let difference = 0;
				let tariff = 0;

				// getting readings of a meter
				const meterReadings = await salesforceApi({ path: `/MA/Meter/${unitMeter.id}/Reading` });

				if (meterReadings && meterReadings.length) {
					const meterReadSorted = sortBy(meterReadings, ['readingDateTime']).reverse();
					const latestMeterReading = meterReadSorted[0];

					valueFrom = latestMeterReading.valueFrom ? parseFloat(latestMeterReading.valueFrom) : 0;
					valueTo = latestMeterReading.valueTo ? parseFloat(latestMeterReading.valueTo) : 0;
					difference = latestMeterReading.difference ? parseFloat(latestMeterReading.difference) : 0;
					tariff = latestMeterReading.tariff ? parseFloat(latestMeterReading.tariff) : 0;
				}

				meter.valueFrom = valueFrom;
				meter.valueTo = valueTo;
				meter.difference = difference;
				meter.tariff = tariff;

				meters.push(meter);
			}
		}

		ownedUnit.meters = meters;
		ownedUnit.lastReading = lastReadingUnit;
		ownedUnit.customerCode = unit.customerCode;
	}

	return ownedUnits;
};

const updateSalesforceUnit = async units => {
	const bulkWriteArray = [];

	if (!units || !Array.isArray(units)) {
		throw Error(errors.RequestParameterMissing);
	}

	const keyUnits = keyBy(units, 'id');

	const unitIds = units.map(unit => unit.id);

	const ownedUnits = await OwnedUnit.find({ unitExtId: { $in: unitIds } })
		.populate('building')
		.lean();

	ownedUnits.forEach(ownedUnit => {
		const unit = keyUnits[ownedUnit.unitExtId];

		const { unitNumber, unitLetter } = unit;

		if (!unit.unitNumber) {
			throw Error(errors.FieldMissing);
		}

		bulkWriteArray.push({
			updateOne: {
				filter: { _id: ownedUnit._id },
				update: {
					label: formUnitLabel(ownedUnit.building, unit),
					unitNumber,
					unitLetter,
				},
			},
		});
	});

	await OwnedUnit.bulkWrite(bulkWriteArray);
};

const getOwnedUnitFileUrl = async ({ ownerId, ownedUnitId, fileId }) => {
	// Validating file accesisibility
	const ownedUnit = await OwnedUnit.findOne({ _id: ownedUnitId, status: OWNED_UNIT_STATUSES.approved, owner: ownerId }).lean();

	if (!ownedUnit) {
		throw Error(errors.NotFound);
	}

	const unit = ((await salesforceApi({ path: `/Unit/${ownedUnit.unitExtId}?detail=true` })) || [])[0];

	if (!unit) {
		throw Error(errors.NotFound);
	}

	const document = unit.unitDocuments && unit.unitDocuments.find(document => document.Id === fileId);

	if (!document) {
		throw Error(errors.NotFound);
	}

	return salesforce.baseUrl + `/services/data/v55.0/sobjects/ContentVersion/${document.Id}/VersionData`;
};

module.exports = {
	createOwnedUnit,
	getOwnedUnits,
	getOwnedUnit,
	validateOwnedUnit,
	ownedUnitsWithMeters,
	getUnitContacts,
	updateSalesforceUnit,
	getOwnedUnitFileUrl,
};
