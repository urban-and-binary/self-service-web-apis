const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const { TOKEN_SECRET } = require('./authService');
const User = require('../models/User');
const { roles } = require('../../common/permissions');

const options = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: TOKEN_SECRET,
};

const strategy = new JwtStrategy(options, (payload, done) => {
	User.findOne({ _id: payload.sub }, (err, user) => {
		if (err) {
			return done(err, false);
		}
		if (user) {
			return done(null, { ...user, permissions: roles[user.role] || [] });
		}
	}).lean();
});

module.exports = passport => {
	passport.use(strategy);
};
