const _ = require('lodash');
//models
const User = require('../models/User');
const UserSetting = require('../models/UserSetting');
const { errors } = require('../../common/consts');
const { OWNER_ROLE } = require('../../common/permissions');
const { VERIFICATION_TYPES, SEND_TYPE, emailSubject } = require('../utils/constants');
const { sendVerificationMail, TEMPLATES } = require('./mailService');
const { verifyCode, sendVerificationSMS } = require('./verificationService');

const service = {};

service.findUser = async (query, withPassword = false, withVerCode = false) => {
	const userQuery = User.findOne(query);

	if (withPassword) {
		userQuery.select('+password');
	}

	if (withVerCode) {
		userQuery.select('+verificationCode');
	}

	const user = await userQuery;

	if (!user) {
		throw Error(errors.UserNotFound);
	}

	return user;
};

service.updateUser = async (user, fields = []) => {
	for (let i in fields) {
		let fieldName = Object.keys(fields[i])[0];
		let fieldValue = fields[i][fieldName];
		user[fieldName] = fieldValue;
	}
	await user.save();
};

service.findAndUpdateUser = async (userId, { name, surname, phone, address, postcode, city, country, countryCode, isOnboarded }) => {
	const user = await User.findByIdAndUpdate(
		userId,
		{ name, surname, phone, address, postcode, city, country, countryCode, isOnboarded },
		{ new: true },
	);

	if (!user) {
		throw Error(errors.NotFound);
	}
};

service.createUser = async ({ role = OWNER_ROLE, ...otherFields }) => {
	return await User.create({ role, ...otherFields });
};

service.createUserSettings = async userId => {
	const setting = await UserSetting.create({ user: userId, agreements: {}, bills: {} });
	return setting._id;
};

service.getUserSession = async (userId, userPermissions) => {
	/* check if user exists */
	const user = await User.findById(userId);

	if (!user) {
		throw Error(errors.UserNotFound);
	}

	return {
		name: user.name,
		surname: user.surname,
		email: user.email,
		verifiedAt: user.verifiedAt,
		isOnboarded: user.isOnboarded,
		role: user.role,
		permissions: userPermissions,
	};
};

service.getUserSettings = async userId => {
	const userSetting = await UserSetting.findOne({ user: userId });
	if (!userSetting) {
		throw Error(errors.NotFound);
	}
	return { agreements: userSetting.agreements, bills: userSetting.bills };
};

service.updateUserSettings = async ({ agreements, bills, userId }) => {
	/* find user settings */
	const setting = await UserSetting.findOne({ user: userId });

	/* check if user settings exists */
	if (!setting) {
		throw Error(errors.NotFound);
	}
	if (!(agreements || bills)) {
		throw Error(errors.RequestParameterMissing);
	}

	if (!_.isEmpty(agreements)) {
		/* cannot change agreements after onboarding */
		const { privacyPolicy, personalData, userRules } = agreements;

		let _agreements = agreements;
		if (!(privacyPolicy && personalData && userRules)) {
			_agreements = _.omit(agreements, ['privacyPolicy', 'personalData', 'userRules']);
		}

		/* update agreements */
		if (!_.isEmpty(_agreements)) {
			setting.agreements = _agreements;
		}
	}

	/* update bills */
	if (!_.isEmpty(bills)) {
		setting.bills = bills;
	}
	await setting.save();
};

service.profilePasswordUpdate = async ({ currentPassword, newPassword, repeatPassword, userId }) => {
	if (!(currentPassword && newPassword && repeatPassword)) {
		throw Error(errors.PasswordRequired);
	}

	/* check if passwords match */
	if (newPassword !== repeatPassword) {
		throw Error(errors.NotEqual);
	}

	const user = await service.findUser({ _id: userId }, true);

	const valid = await user.isValidPassword(currentPassword);

	if (valid) {
		user.password = newPassword;
		await user.save();
	} else {
		throw Error(errors.InvalidPassword);
	}
};

service.generateCodeForUser = async ({ user, sendTo, sendType, type }) => {
	/* check if user is verified */
	if (user.verifiedAt === null) {
		throw Error(errors.InvalidVerification);
	}

	/* send verification email */
	const verify =
		sendType === SEND_TYPE.phone
			? sendVerificationSMS(sendTo)
			: sendVerificationMail(user, TEMPLATES.EMAIL_RESEND_PASSWORD, emailSubject.emailResetPassword);
	const verificationCode = verify.verificationCode;
	const verificationExpiry = verify.verificationExpiry;

	/* update user with verification code */
	await service.updateUser(user, [{ verificationCode }, { verificationType: type }, { verificationExpiry }]);
};

service.updateEmailCode = async ({ userId, email }) => {
	const userExists = await User.findOne({ email });

	if (userExists) {
		throw Error(errors.UserAlreadyExists);
	}

	const user = await service.findUser({ _id: userId });

	await service.generateCodeForUser({ user, sendTo: email, type: VERIFICATION_TYPES.email });
};

service.updateEmail = async ({ userId, email, code }) => {
	const user = await service.findUser({ _id: userId }, false, true);

	const verify = await verifyCode(user, code, VERIFICATION_TYPES.email);

	if (verify.is) {
		await service.updateUser(user, [{ email }, { verificationCode: null }, { verificationType: null }, { verificationExpiry: null }]);
	} else {
		throw Error(errors.InvalidUpdateVerification);
	}
};

service.updatePhoneCode = async ({ userId, phone }) => {
	const user = await service.findUser({ _id: userId });

	await service.generateCodeForUser({ user, sendType: SEND_TYPE.phone, sendTo: phone, type: VERIFICATION_TYPES.phone });
};

service.updatePhone = async ({ userId, phone, code }) => {
	const user = await service.findUser({ _id: userId }, false, true);

	const verify = await verifyCode(user, code, VERIFICATION_TYPES.phone);

	if (verify.is) {
		await service.updateUser(user, [{ phone }, { verificationCode: null }, { verificationType: null }, { verificationExpiry: null }]);
	} else {
		throw Error(errors.InvalidUpdateVerification);
	}
};

service.validatePhone = async ({ phone, userId }) => {
	// Currently only checks if phone is unique
	const user = await User.findOne({ phone, _id: { $ne: userId } });

	if (user) {
		throw Error(errors.PhoneExists);
	}
};

module.exports = {
	service,
};
