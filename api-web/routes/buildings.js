const router = require('express').Router();
const aw = require('express-async-wrap');
//common services
const { httpStatus, interceptErrors } = require('../utils/constants');
const { createBuilding, queryBuildings, getBuildingUnits } = require('../services/buildingService');
const { checkPermissions, permissions } = require('../../common/permissions');
// jsdoc
import '../../common/jsdoc';

router.use((req, res, next) => {
	checkPermissions({
		userPermissions: req.user.permissions,
		accessPerms: [permissions.units[req.method === 'GET' ? 'read' : 'write']],
	});

	next();
});

/**
 * @description Route for inserting building
 * @name POST api/web/buildings
 * @memberOf buildings
 * @function
 * @param {Object} req.body
 * @param {string} req.body.street
 * @param {string} req.body.houseNumber
 * @param {string} [req.body.houseLetter='']
 * @param {string} req.body.postcode
 * @param {string} req.body.city
 * @param {string} req.body.countryCode
 * @returns {string} success: http status 201, error: http status 400
 */
router.post(
	'/',
	aw(async (req, res) => {
		// TODO this should have higher access level requirements
		try {
			const building = await createBuilding(req.body);

			res.status(httpStatus.Created).json(building);
		} catch (err) {
			res.status(httpStatus.BadRequest).json({ message: interceptErrors(err) });
			console.error(err);
		}
	}),
);

/**
 * @description Route for getting buildings
 * @name GET api/web/buildings
 * @memberOf buildings
 * @function
 * @param {Object} [req.query='']
 * @param {string} req.query.search
 * @param {string} req.query.lastPageItemId
 * @param {string} req.query.lastPageItemTitle
 * @returns {Buildings}
 */
router.get(
	'/',
	aw(async (req, res) => {
		try {
			const results = await queryBuildings(req.query);

			res.json(results);
		} catch (err) {
			res.status(httpStatus.BadRequest).json({ message: interceptErrors(err) });
			console.error(err);
		}
	}),
);

/**
 * @description Route for getting building Units from salesforce
 * @name GET api/web/:buildingId/units
 * @memberOf buildings
 * @function
 * @param {string} req.params.buildingId
 * @returns ???
 */
router.get(
	'/:buildingId/units',
	aw(async (req, res) => {
		const { buildingId } = req.params;

		res.json(await getBuildingUnits(buildingId));
	}),
);

module.exports = router;
