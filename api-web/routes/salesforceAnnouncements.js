const router = require('express').Router();
const aw = require('express-async-wrap');
const { errors } = require('../../common/consts');
const { tokenForSalesforce } = require('../../config');
const { createSealesforceAnnouncements } = require('../services/announcementService');
const { httpStatus } = require('../utils/constants');
// jsdoc
import '../../common/jsdoc';

/**
 * @description Route for saving announcements from salesforce
 * @name POST api/web/salesforceAnnouncements
 * @memberOf announcements
 * @function
 * @param {Object} req.body
 * @param {string} req.body.buildingId
 * @param {string} req.body.unitId
 * @param {string} req.body.title
 * @param {string} req.body.text
 * @returns success: http status 200, error: http status 400
 */
router.post(
	'/',
	aw(async (req, res) => {
		if (`Bearer ${tokenForSalesforce}` !== req.header('authorization')) {
			throw Error(errors.NotAuthorized);
		}

		await createSealesforceAnnouncements(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

module.exports = router;
