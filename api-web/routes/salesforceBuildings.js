const router = require('express').Router();
const aw = require('express-async-wrap');
const { errors } = require('../../common/consts');
const { tokenForSalesforce } = require('../../config');
const { upsertSalesforceBuilding } = require('../services/buildingService');
const { httpStatus } = require('../utils/constants');
// jsdoc
import '../../common/jsdoc';

/**
 * @description Route for inserting building from salesforce
 * @name POST api/web/salesforceBuildings
 * @memberOf buildings
 * @function
 * @param {Object} req.body
 * @param {string} req.body.id
 * @param {string} req.body.name
 * @param {string} req.body.municipality
 * @param {string} req.body.city
 * @param {string} req.body.street
 * @param {string} req.body.houseNumber
 * @param {string} req.body.houseLetter
 * @param {string} req.body.postCode
 * @returns success: http status 200, error: http status 400
 */
router.post(
	'/',
	aw(async (req, res) => {
		if (`Bearer ${tokenForSalesforce}` !== req.header('authorization')) {
			throw Error(errors.NotAuthorized);
		}

		await upsertSalesforceBuilding(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

module.exports = router;
