const router = require('express').Router();
const aw = require('express-async-wrap');
//controllers
const { service: userService } = require('../services/userService');
// consts
const { httpStatus } = require('../utils/constants');
// jsdoc
import '../../common/jsdoc';

/**
 * @description Route for getting info about user
 * @name GET api/web/users
 * @memberOf users
 * @function
 * @returns {UserExpanded} user
 */
router.get(
	'/',
	aw(async (req, res) => {
		res.json(await userService.findUser({ _id: req.user._id }));
	}),
);

/**
 * @description Route for updating user profile
 * @name PUT api/web/users/profile/info
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {string} req.body.name
 * @param {string} req.body.surname
 * @param {string} req.body.phone
 * @param {string} req.body.address
 * @param {string} req.body.postcode
 * @param {string} req.body.city
 * @param {string} req.body.country
 * @param {string} req.body.countryCode
 * @param {string} req.body.isOnboarded
 * @returns {User}
 */
router.put(
	'/profile/info',
	aw(async (req, res) => {
		await userService.findAndUpdateUser(req.user._id, req.body);
		res.json(await userService.getUserSession(req.user._id, req.user.permissions));
	}),
);

/**
 * @description Route for changing password in profile
 * @name PUT api/web/users/profile/password
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {string} req.body.currentPassword
 * @param {string} req.body.newPassword
 * @param {string} req.body.repeatPassword
 * @returns {User}
 */
router.put(
	'/profile/password',
	aw(async (req, res) => {
		await userService.profilePasswordUpdate({ ...req.body, userId: req.user._id });
		res.json(await userService.getUserSession(req.user._id, req.user.permissions));
	}),
);

/**
 * @description Route for generating code for update email in profile
 * @name POST api/web/users/profile/email-code
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {string} req.body.email
 * @returns {string} 200 OK
 */
router.post(
	'/profile/email-code',
	aw(async (req, res) => {
		await userService.updateEmailCode({ userId: req.user._id, email: req.body.email });
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for updating email in profile
 * @name POST api/web/users/profile/email
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {string} req.body.email
 * @param {string} req.body.code
 * @returns {string} 200 OK
 */
router.put(
	'/profile/email',
	aw(async (req, res) => {
		await userService.updateEmail({ userId: req.user._id, email: req.body.email, code: req.body.code });
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for generating code for update phone in profile
 * @name POST api/web/users/profile/phone-code
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {string} req.body.phone
 * @returns {string} 200 OK
 */
router.post(
	'/profile/phone-code',
	aw(async (req, res) => {
		await userService.updatePhoneCode({ userId: req.user._id, phone: req.body.phone });
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for updating phone in profile
 * @name POST api/web/users/profile/phone
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {string} req.body.phone
 * @param {string} req.body.code
 * @returns {string} 200 OK
 */
router.put(
	'/profile/phone',
	aw(async (req, res) => {
		await userService.updatePhone({ userId: req.user._id, phone: req.body.phone, code: req.body.code });
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for validating phone number
 * @name POST api/web/users/validate-phone
 * @memberOf users
 * @function
 * @param {string} req.query.phone
 * @returns {string} 200 OK
 */
router.get(
	'/validate-phone',
	aw(async (req, res) => {
		await userService.validatePhone({ phone: req.query.phone, userId: req.user._id });
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for getting user settings
 * @name GET api/web/users/settings
 * @memberOf users
 * @function
 * @returns {Settings}
 */
router.get(
	'/settings',
	aw(async (req, res) => {
		res.json(await userService.getUserSettings(req.user._id));
	}),
);

/**
 * @description Route for updating user settings
 * @name PUT api/web/users/settings
 * @memberOf users
 * @function
 * @param {Object} req.body
 * @param {Object} req.body.agreements
 * @param {boolean} req.body.agreements.privacyPolicy
 * @param {boolean} req.body.agreements.personalData
 * @param {boolean} req.body.agreements.userRules
 * @param {boolean} req.body.agreements.advertisment
 * @param {Object} req.body.bills
 * @param {string} req.body.bills.receiveFormat
 * @returns {string} 200 OK
 */
router.put(
	'/settings',
	aw(async (req, res) => {
		await userService.updateUserSettings({ ...req.body, userId: req.user._id });
		res.sendStatus(httpStatus.Ok);
	}),
);

module.exports = router;
