const router = require('express').Router();
const aw = require('express-async-wrap');
//models
const Users = require('../models/User');

// TODO: legacy from the project start, probably not needed
router.post(
	'/test',
	aw(async (req, res) => {
		const { email, role, name } = req.body;
		let permissions = ['units.read'];
		if (role === 'owner') {
			permissions.push('billing.read', 'billing.write');
		}
		const updateUser = await Users.findOneAndUpdate({ email }, { name, role, permissions }, { new: true });
		res.json(updateUser);
	}),
);

module.exports = router;
