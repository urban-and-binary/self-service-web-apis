const router = require('express').Router();
const aw = require('express-async-wrap');
const { salesforceFileStream } = require('../services/salesforceService');
const { getSalesforceTasks, getTaskFileUrl } = require('../services/taskService');
// jsdoc
require('../../common/jsdoc');

/**
 * @description Route for getting tasks
 * @name GET api/web/tasks
 * @memberOf tasks
 * @function
 * @param {String} req.query.status Task status one of - "all", "planned", "inProgress", "done" . Defaults to "all"
 * @param {String} req.query.ownedUnit Owned unit ID, will filter by tasks associated to the specific unit & to the building of the unit or "all". Defaults to "all".
 * @param {String} req.query.sortField Sort field one of - "startDate", "endDate" . Defaults to "startDate"
 * @param {String} req.query.sortOrder Sort order one of - "asc", "desc" . Defaults to "asc"
 * @param {Number} req.query.pageSize Defaults to 5.
 * @param {Number} req.query.page Defaults to 1.
 * @returns {Array.<Task>}
 */

router.get(
	'/',
	aw(async (req, res) => {
		res.json(await getSalesforceTasks({ ...req.query, owner: req.user._id }));
	}),
);

/**
 * @description Route to download task file
 * @name GET api/web/tasks/{taskId}/file/{fileId}
 * @memberOf tasks
 * @function
 * @param {String} req.params.id task ID, the one retrieved with the list of tasks
 * @param {String} req.params.fileId Id of the file to download, you should get this with the api/web/tasks GET endpoint in the "workOrderDocuments" object
 * @returns {File}
 */
router.get(
	'/:id/file/:fileId',
	aw(async (req, res) => {
		const fileUrl = await getTaskFileUrl({ owner: req.user._id, taskId: req.params.id, fileId: req.params.fileId });

		const responseStream = await salesforceFileStream({ fileUrl });

		responseStream.data.pipe(res);
	}),
);

module.exports = router;
