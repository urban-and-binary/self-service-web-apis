const router = require('express').Router();
const aw = require('express-async-wrap');
const {
	createOwnedUnit,
	getOwnedUnits,
	validateOwnedUnit,
	ownedUnitsWithMeters,
	getOwnedUnit,
	getOwnedUnitFileUrl,
} = require('../services/ownedUnitService');
const { httpStatus } = require('../utils/constants');
const { checkPermissions, permissions } = require('../../common/permissions');
const { salesforceFileStream } = require('../services/salesforceService');
// jsdoc
require('../../common/jsdoc');

router.use((req, res, next) => {
	checkPermissions({
		userPermissions: req.user.permissions,
		accessPerms: [permissions.units[req.method === 'GET' ? 'read' : 'write']],
	});

	next();
});

router.post(
	'/',
	aw(async (req, res) => {
		const { building, buildingExtId, unitExtId, title } = req.body;
		const ownedUnit = await createOwnedUnit({ building, buildingExtId, unitExtId, title, owner: req.user._id });

		// this function will be called without awaiting so we need to do a try and catch here
		// as our errorHandler middleware will not be able to handle it
		// once the response has been sent
		validateOwnedUnit({ ownerId: req.user._id, ownedUnitId: ownedUnit._id }).catch(err =>
			console.error('Failed to validate owner unit for', err),
		);

		res.sendStatus(httpStatus.Ok);
	}),
);

router.get(
	'/',
	aw(async (req, res) => {
		res.json(await getOwnedUnits({ ...req.query, owner: req.user._id }));
	}),
);

router.get(
	'/meters',
	aw(async (req, res) => {
		res.json(await ownedUnitsWithMeters(req.user._id));
	}),
);

router.get(
	'/:id',
	aw(async (req, res) => {
		res.json(await getOwnedUnit({ id: req.params.id, owner: req.user._id }));
	}),
);

/**
 * @description Route to download unit file
 * @name GET api/web/owned-unit/{ownedUnitId}/file/{fileId}
 * @memberOf ownedUnit
 * @function
 * @param {String} req.params.id ownedUnit ID
 * @param {String} req.params.fileId Id of the file to download, you should get this with the api/web/owned-unit/{ownedUnitId} GET endpoint in the "documents" object
 * @returns {File}
 */
router.get(
	'/:id/file/:fileId',
	aw(async (req, res) => {
		const fileUrl = await getOwnedUnitFileUrl({ ownedUnitId: req.params.id, ownerId: req.user._id, fileId: req.params.fileId });

		const responseStream = await salesforceFileStream({ fileUrl });

		responseStream.data.pipe(res);
	}),
);

module.exports = router;
