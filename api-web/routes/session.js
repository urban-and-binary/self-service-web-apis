const router = require('express').Router();
const aw = require('express-async-wrap');
//services
const { service: userService } = require('../services/userService');
// jsdoc
import '../../common/jsdoc';

/**
 * @description Route for getting info about user
 * @name GET api/web/session
 * @memberOf session
 * @function
 * @param {Object} req.user
 * @param {Array} req.user.permissions
 * @returns {User} user
 */
router.get(
	'/',
	aw(async (req, res) => {
		res.json(await userService.getUserSession(req.user._id, req.user.permissions));
	}),
);

module.exports = router;
