const router = require('express').Router();
const aw = require('express-async-wrap');
const {
	listAnnouncements,
	announcementRead,
	getUserUnreadAnnouncements,
	markAllAnnsRead,
} = require('../services/announcementService');
const { httpStatus } = require('../utils/constants');

router.get(
	'/',
	aw(async (req, res) => {
		res.json(await listAnnouncements({ ...req.query, userId: req.user._id }));
	}),
);

router.post(
	'/:annId/read',
	aw(async (req, res) => {
		await announcementRead(req.params.annId, req.user._id);
		res.sendStatus(httpStatus.Ok);
	}),
);

router.post(
	'/all-read',
	aw(async (req, res) => {
		await markAllAnnsRead(req.user._id);
		res.sendStatus(httpStatus.Ok);
	}),
);

router.get(
	'/unread',
	aw(async (req, res) => {
		res.json(await getUserUnreadAnnouncements(req.user._id));
	}),
);

module.exports = router;
