const router = require('express').Router();
const aw = require('express-async-wrap');
const { declareMeters } = require('../services/metersService');
const { httpStatus } = require('../utils/constants');
// jsdoc
import '../../common/jsdoc';

/**
 * @description Route for declaring meters
 * @name POST api/web/meters/reading
 * @memberOf meters
 * @function
 * @param {Object} req.body
 * @param {Array.<{id: String, valueTo: String|Number}>} req.body.meters
 * @returns {string} 200 OK
 */
router.post(
	'/reading',
	aw(async (req, res) => {
		await declareMeters(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

module.exports = router;
