const router = require('express').Router();
const aw = require('express-async-wrap');
const { errors } = require('../../common/consts');
const { tokenForSalesforce } = require('../../config');
const { updateSalesforceUnit } = require('../services/ownedUnitService');
const { httpStatus } = require('../utils/constants');

router.put(
	'/',
	aw(async (req, res) => {
		if (`Bearer ${tokenForSalesforce}` !== req.header('authorization')) {
			throw Error(errors.NotAuthorized);
		}

		await updateSalesforceUnit(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

module.exports = router;
