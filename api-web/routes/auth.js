const router = require('express').Router();
const passport = require('passport');
const aw = require('express-async-wrap');
//services
const {
	signUp,
	loginEmail,
	loginPhone,
	loginVerificationSMS,
	passwordForgot,
	verificationCode,
	verificationCodePhone,
	passwordChange,
} = require('../services/authService');
// consts
const { httpStatus } = require('../utils/constants');
const { LOGIN_TYPE } = require('../../common/consts');
// jsdoc
import '../../common/jsdoc';

/**
 * @description Route serving signup form
 * @memberOf auth
 * @name POST api/web/auth/signup
 * @function
 * @param {Object} req.body
 * @param {string} req.body.email
 * @param {string} req.body.password
 * @returns success: http status 200, error: http status 400
 */
router.post(
	'/signup',
	aw(async (req, res) => {
		await signUp(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route serving 'Login' form
 * @memberOf auth
 * @name POST api/web/auth/login
 * @function
 * @param {Object} req.body
 * @param {string} req.body.loginType email || phone
 * @param {string} req.body.email
 * @param {string} req.body.password
 * @param {string} [req.body.verificationCode=null]
 * @returns {string} token
 */
router.post(
	'/login',
	aw(async (req, res) => {
		const loginType = req.body.loginType;

		let token;
		if (loginType === LOGIN_TYPE.email) {
			token = await loginEmail(req.body);
		} else if (loginType === LOGIN_TYPE.phone) {
			token = await loginPhone(req.body);
		}
		return res.status(httpStatus.Ok).json({ token });
	}),
);

/**
 * @description Route generating verification code for 'Forgot password' flow
 * @name POST api/web/auth/password-forgot
 * @memberOf auth
 * @function
 * @param {Object} req.body
 * @param {string} req.body.email
 * @returns {string} 200 OK
 */
router.post(
	'/password-forgot',
	aw(async (req, res) => {
		await passwordForgot(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route sending verification code in 'Login with phone'
 * @name POST api/web/auth/send-code
 * @memberOf auth
 * @function
 * @param {Object} req.body
 * @param {string} req.body.phone
 * @returns {string} 200 OK
 */
router.post(
	'/send-code',
	aw(async (req, res) => {
		const loginType = req.body.loginType;
		if (loginType === LOGIN_TYPE.phone) {
			await loginVerificationSMS(req.body);
		}
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for code verification in 'Forgot password' flow
 * @name POST api/web/auth/verify-code
 * @memberOf auth
 * @function
 * @param {Object} req.body
 * @param {string} req.body.email
 * @param {string} req.body.verificationCode
 * @returns {string} 200 OK
 */
router.post(
	'/verify-code',
	aw(async (req, res) => {
		if (req.body.email) {
			await verificationCode(req.body);
		} else {
			await verificationCodePhone(req.body);
		}
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for changing password in 'Forgot password' flow
 * @name POST api/web/auth/password-change
 * @memberOf auth
 * @function
 * @param {Object} req.body
 * @param {string} req.body.email
 * @param {string} req.body.verificationCode
 * @param {string} req.body.password
 * @param {string} req.body.password1
 * @returns {string} 200 OK
 */
router.post(
	'/password-change',
	aw(async (req, res) => {
		await passwordChange(req.body);
		res.sendStatus(httpStatus.Ok);
	}),
);

/**
 * @description Route for 'Logout'
 * @name POST api/web/auth/logout
 * @memberOf auth
 * @function
 * @returns {string} 204 No Content
 */
router.post(
	'/logout',
	passport.authenticate('jwt', { session: false }),
	aw(async (req, res) => {
		if (req.isAuthenticated()) {
			req.logout();
		}
		res.sendStatus(httpStatus.NoContent);
	}),
);

module.exports = router;
