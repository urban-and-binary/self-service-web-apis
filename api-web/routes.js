const express = require('express');
const passport = require('passport');
const router = express.Router();

router.use('/auth', require('./routes/auth'));
router.use('/salesforce-announcements', require('./routes/salesforceAnnouncements'));
router.use('/salesforce-buildings', require('./routes/salesforceBuildings'));
router.use('/salesforce-units', require('./routes/salesforceUnits'));

//routes with authentification
router.use('/', passport.authenticate('jwt', { session: false }));

router.use('/session', require('./routes/session'));
router.use('/buildings', require('./routes/buildings'));
router.use('/onboarding', require('./routes/onboarding'));
router.use('/users', require('./routes/users'));
router.use('/owned-unit', require('./routes/ownedUnit'));
router.use('/meters', require('./routes/meters'));
router.use('/announcements', require('./routes/announcements'));
router.use('/tasks', require('./routes/tasks'));

module.exports = router;
