const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const { dbConnection } = require('../config');
const mongoose = require('mongoose');
const passport = require('passport');

mongoose.connect(dbConnection, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'api-web connection to mongodb error: '));
db.once('open', function() {
	console.info('api-web connected to mongodb successfully');
});

app.use(bodyParser.json({ limit: '2mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '2mb', extended: true }));

require('./services/passport')(passport);
app.use(passport.initialize());

app.disable('x-powered-by');
app.enable('trust proxy');

app.use('/api/web', require('./routes'));

app.use(require('./middlewares/errorHandler'));

module.exports = app;
