'use strict';

const app = require('./app');
const config = require('../config');

app.listen(config.httpPort, '0.0.0.0', () => {
	console.info(`api-web running on http://0.0.0.0:${config.httpPort}`);
});
