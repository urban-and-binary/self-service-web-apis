const formBuildingLabel = ({ street, houseNumber, houseLetter, municipality, city }) =>
	`${street} ${houseNumber}${houseLetter || ''}, ${municipality ? municipality + ', ' : ''}${city}`;

module.exports = { formBuildingLabel };
