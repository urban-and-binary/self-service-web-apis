const { dbConnection } = require('../../config');
const mongoose = require('mongoose');

mongoose.connect(dbConnection, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'util dbConnection to mongodb error: '));
db.once('open', function() {
	console.info('util dbConnection connected to mongodb successfully');
});

module.exports = db;
