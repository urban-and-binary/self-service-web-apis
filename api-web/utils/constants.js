const { errors } = require('../../common/consts');

const httpStatus = {
	Ok: 200,
	Created: 201,
	Accepted: 202,
	NoContent: 204,
	BadRequest: 400,
	Unauthorized: 401,
	NotFound: 404,
	ServerError: 500,
};

const emailSubject = {
	emailConfirm: 'Confirm email',
	emailResetPassword: 'Reset password',
	emailUnitAssigned: 'Unit assigned successfully',
	emailUnitAssignFailure: 'Failed to assign unit to owner',
};

const interceptErrors = function(err) {
	if (err.code === 11000) {
		return errors.DuplicateKey;
	}
	if (err.name === 'CastError') {
		return errors.NotFound;
	}
	return err.message;
};

const VERIFICATION_TYPES = {
	signup: 'signup',
	forgot: 'forgot',
	email: 'email',
	phone: 'phone',
	phoneLogin: 'phoneLogin',
};

const SEND_TYPE = {
	email: 'email',
	phone: 'phone',
};

module.exports = {
	httpStatus,
	interceptErrors,
	emailSubject,
	VERIFICATION_TYPES,
	SEND_TYPE,
};
