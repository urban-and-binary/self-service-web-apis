// accepts page starting from 1
const paginatedQuery = async ({ Model, select, query = {}, sortField = 'createdAt', sortOrder = 1, page = 1, pageSize = 5 }) => {
	const pageSizeNum = parseInt(pageSize, 10);
	const pageNum = parseInt(page, 10);

	const mainQuery = Model.find(query)
		.sort({ [sortField]: sortOrder })
		.limit(pageSizeNum)
		.skip((pageNum - 1) * pageSizeNum)
		.lean();

	if (select) {
		mainQuery.select(select);
	}

	const [total, data] = await Promise.all([Model.find(query).countDocuments(), mainQuery]);

	return {
		total,
		data,
	};
};

module.exports = {
	paginatedQuery,
};
