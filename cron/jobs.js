const Building = require('../api-web/models/Building');
const User = require('../api-web/models/User');
const { salesforceApi } = require('../api-web/services/salesforceService');
const { formBuildingLabel } = require('../api-web/utils/general');

const fetchBuildings = async () => {
	const buildings = await salesforceApi({ path: '/Building' });

	// NOTE: this approach might not trigger ".pre" middleware on the model
	// so use a different approach if needed. Or add the ".pre" conditions here
	if (buildings) {
		const bulkBuildings = [];

		buildings.forEach(building => {
			bulkBuildings.push({
				updateOne: {
					filter: { externalId: building.id },
					update: {
						externalId: building.id,
						title: formBuildingLabel(building),
						name: `${building.name}`,
						street: building.street,
						houseNumber: building.houseNumber,
						houseLetter: building.houseLetter,
						postcode: building.postCode,
						city: building.city,
						municipality: building.municipality,
						floors: building.floors,
						buildYear: building.buildYear,
					},
					upsert: true,
				},
			});
		});

		try {
			await Building.bulkWrite(bulkBuildings);
		} catch (error) {
			console.error('Bulk write error', error.message);
		}
	}
};

const expireVerificationCode = async () => {
	await User.updateMany(
		{ $and: [{ verificationExpiry: { $ne: null } }, { verificationExpiry: { $lte: new Date() } }] },
		{ verificationType: null, verificationCode: null, verificationExpiry: null },
	);
};

module.exports = {
	fetchBuildings,
	expireVerificationCode,
};
