const { CronJob } = require('cron');

function queueAsyncCronJob(schedule, job, title) {
	var jobIsRunning = false;

	new CronJob(
		schedule,
		function() {
			if (jobIsRunning) {
				return;
			}
			jobIsRunning = true;

			job()
				.then(() => {
					jobIsRunning = false;
				})
				.catch(error => {
					jobIsRunning = false;
					if (error) {
						console.error(`Cron job "${title}" error`, error);
					}
				});
		},
		null,
		true,
		'Europe/Vilnius',
	);
}

module.exports = {
	queueAsyncCronJob,
};
