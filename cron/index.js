const mongoose = require('mongoose');
const { dbConnection } = require('../config');
const { fetchBuildings, expireVerificationCode } = require('./jobs');
const { queueAsyncCronJob } = require('./scheduler');

mongoose.connect(dbConnection, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'cron connection to mongodb error: '));
db.once('open', function() {
	console.info('cron connected to mongodb successfully');
});

process.on('uncaughtException', error => {
	console.error('uncaughtException', error);
	process.exit(1);
});

process.on('unhandledRejection', error => {
	console.error('unhandledRejection', error);
	process.exit(1);
});

queueAsyncCronJob('0 0 * * *', fetchBuildings, 'Fetching buildings at midnight');
queueAsyncCronJob('0 1 * * *', expireVerificationCode, 'Clearing verification data after expiry');
